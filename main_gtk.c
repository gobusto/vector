/**
@file main_gtk.c

@brief Provides a simple GUI front-end for using V2D and V2DX.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include "v2dx/misc/swfconst.h"
#include "v2d_cairo.h"
#include "v2dx/load.h"
#include "v2dx/save.h"

/* Constants. */

#define V2DUI_PROGRAM_NAME "Vector2D GUI" /**< @brief The program name. */

#define V2DUI_HANDLE_SIZE 6  /**< @brief The size of the selection handles. */

#define V2DUI_FRAME_WIDTH 20  /**< @brief about */

/**
@brief [INTERNAL] about

writeme
*/

static GtkTargetEntry target_list[] = {
  { "CreateNewItem", GTK_TARGET_SAME_APP | GTK_TARGET_OTHER_WIDGET, 123 }
};

/* Function prototypes; only given for functions which NEED them to compile. */

void on_raise(GtkButton *button, gpointer item);
void on_lower(GtkButton *button, gpointer item);
void on_delete_item(GtkButton *button, gpointer item);

/**
@brief [INTERNAL] Specifies the columns stored in the Drawables GtkListStore.

Note that not all of these are visible; The first "column" is a "draw" pointer.
*/

enum
{
  V2DUI_DRAW_POINTER, /**< @brief Stores a pointer to the drawable itself. */
  V2DUI_DRAW_NAME,    /**< @brief Stores the unique name of this drawable. */
  V2DUI_DRAW_TYPE,    /**< @brief The "type" of this drawable as a string. */
  V2DUI_NUM_DRAW_COLS /**< @brief The total number of columns stored here. */
};

/**
@brief [INTERNAL] Specifies the effect of mouse movement on selected items.

This should be NONE unless the mouse button is being held down.
*/

enum
{

  V2DUI_MOVE_NONE,      /**< @brief No action is being performed. */

  V2DUI_MOVE_CLICK,     /**< @brief A selected item has been clicked on. */
  V2DUI_MOVE_CLICK_2,   /**< @brief An unselected item has been clicked. */

  V2DUI_MOVE_TRANSLATE, /**< @brief Move selected items on both axes. */

  V2DUI_MOVE_SCALE_N,   /**< @brief Scale the selected items north. */
  V2DUI_MOVE_SCALE_E,   /**< @brief Scale the selected items east.  */
  V2DUI_MOVE_SCALE_S,   /**< @brief Scale the selected items south. */
  V2DUI_MOVE_SCALE_W,   /**< @brief Scale the selected items west.  */
  V2DUI_MOVE_SCALE_NE,  /**< @brief Scale the selected items north-east. */
  V2DUI_MOVE_SCALE_NW,  /**< @brief Scale the selected items north-west. */
  V2DUI_MOVE_SCALE_SE,  /**< @brief Scale the selected items south-east. */
  V2DUI_MOVE_SCALE_SW,  /**< @brief Scale the selected items south-west. */

  V2DUI_MOVE_ROTATE,    /**< @brief Rotate selected items about a point. */
  V2DUI_MOVE_SKEW_X,    /**< @brief Skew selected items along the X axis. */
  V2DUI_MOVE_SKEW_Y,    /**< @brief Skew selected items along the Y axis. */

  V2DUI_MOVE_SELECT     /**< @brief Select items within an AABB region. */

} v2dui_motion = V2DUI_MOVE_NONE;

/* Global variables. */

v2d_rect_s v2dui_highlight; /**< @brief The mouse selection rectangle. */

v2d_bool_t v2dui_any_selected = V2D_FALSE;  /**< @brief Any items selected?  */
v2d_bool_t v2dui_alt_handle = V2D_FALSE;    /**< @brief Rotate/skew handles? */
v2d_rect_s v2dui_selected_aabb;             /**< @brief AABB for all items.  */

v2d_stage_s *v2dui_stage = NULL;  /**< @brief The currently-loaded stage. */

GtkWindow *v2dui_window = NULL;   /**< @brief The main GTK+ window. */
GtkWidget *v2dui_canvas = NULL;   /**< @brief The main canvas drawing area. */
GtkWidget *v2dui_timeline = NULL; /**< @brief GtkGrid for the item timelines. */
GtkSpinButton *v2dui_zoom = NULL; /**< @brief The zoom level spin-box widget. */

GtkListStore     *v2dui_draw_list = NULL; /**< @brief The GTK drawables list. */
GtkTreeSelection *v2dui_draw_item = NULL; /**< @brief Selected drawable item. */

/* Cairo "rendering quality" variables. */

cairo_antialias_t v2dui_quality = CAIRO_ANTIALIAS_GRAY; /**< @brief Quality. */

GtkWidget *v2dui_aa_high   = NULL;  /**< @brief The high quality menu item.   */
GtkWidget *v2dui_aa_medium = NULL;  /**< @brief The medium quality menu item. */
GtkWidget *v2dui_aa_low    = NULL;  /**< @brief The low quality menu item.    */

/**
@brief [INTERNAL] Get an X/Y coordinate pair in canvas space.

This is useful for converting mouse coordinates to canvas positions.

@param x The X coordinate to be converted.
@param y The Y coordinate to be converted.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t get_canvas_xy(v2d_real_t *x, v2d_real_t *y)
{

  v2d_real_t w = gtk_widget_get_allocated_width (v2dui_canvas);
  v2d_real_t h = gtk_widget_get_allocated_height(v2dui_canvas);
  v2d_real_t s = gtk_spin_button_get_value(v2dui_zoom) / 100.0;

  if (!v2dui_stage) { return V2D_FALSE; }

  if (x) { *x = (*x - (w - (v2dui_stage->width  * s)) / 2.0) / s; }
  if (y) { *y = (*y - (h - (v2dui_stage->height * s)) / 2.0) / s; }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Get the currently-selected drawable.

This is used when drawing the preview image, or when adding a new display item
to the stage.

@return A pointer to the selected drawable on success, or NULL on failure.
*/

static v2d_draw_s *get_drawable(void)
{

  GtkTreeIter iter;
  GValue value = G_VALUE_INIT;
  v2d_draw_s *draw = NULL;

  if (!gtk_tree_selection_get_selected(v2dui_draw_item, NULL, &iter)) { return NULL; }

  gtk_tree_model_get_value(GTK_TREE_MODEL(v2dui_draw_list), &iter, V2DUI_DRAW_POINTER, &value);
  draw = g_value_peek_pointer(&value);
  g_value_unset(&value);

  return draw;

}

/**
@brief [INTERNAL] Redraw the "drawable" preview widget.

In future, this might also be used to draw per-row icons in the list widget...

@param widget The widget that contains the Cairo context.
@param cr The Cairo context to draw to.
@param drawable A pointer to the drawable to use, or NULL for the selected one.
@return Always FALSE. See the GTK documentation for further details.
*/

static gboolean on_thumb(GtkWidget *widget, cairo_t *cr, gpointer drawable)
{

  v2d_draw_s *draw = NULL;
  v2d_rect_s aabb;
  v2d_real_t w, h, xs, ys, s;

  cairo_set_antialias(cr, CAIRO_ANTIALIAS_GRAY);

  /* Draw the background. */

  cairo_set_source_rgba(cr, 0, 1, 1, 1);
  cairo_paint(cr);

  draw = drawable ? drawable : get_drawable();
  if (!draw) { return FALSE; }

  /* Position it so that it appears at the centre of the display area. */

  w = gtk_widget_get_allocated_width(widget);
  h = gtk_widget_get_allocated_height(widget);
  if (!v2dDrawGetAABB(draw, &aabb, 0)) { return FALSE; }

  xs = w / (aabb.x2-aabb.x1);
  ys = h / (aabb.y2-aabb.y1);
  s = xs < ys ? xs : ys;

  cairo_translate(cr, w/2, h/2);
  cairo_scale(cr, s, s);
  cairo_translate(cr, 0 - (aabb.x1 + ((aabb.x2-aabb.x1) / 2.0)),
                      0 - (aabb.y1 + ((aabb.y2-aabb.y1) / 2.0)));

  /* Draw the shape. */

  cv2d_draw(cr, draw, 0);

  return FALSE;

}

/**
@brief [INTERNAL] Redraw the main canvas widget.

@param widget The widget that contains the Cairo context.
@param cr The Cairo context to draw to.
@param user_data Ignored; not used.
@return Always FALSE. See the GTK documentation for further details.
*/

static gboolean on_redraw(GtkWidget *widget, cairo_t *cr, gpointer user_data)
{

  double s = gtk_spin_button_get_value(v2dui_zoom)/100.0;

  v2d_real_t point[8][2];

  v2d_real_t f;
  int i = 0;

  if (user_data) { }

  if (!v2dui_stage) { return FALSE; }

  cairo_save(cr);

  cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, 1);
  cairo_paint(cr);

  /* Position the actual stage in the very centre of the drawing area. */

  cairo_translate(cr, gtk_widget_get_allocated_width(widget)/2.0,
                      gtk_widget_get_allocated_height(widget)/2.0);
  cairo_scale(cr, s, s);
  cairo_translate(cr, v2dui_stage->width/-2.0, v2dui_stage->height/-2.0);

  /* Draw the stage background rectangle. */

  cairo_set_line_width(cr, 1/s);
  cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);

  cairo_rectangle(cr, 0, 0, v2dui_stage->width, v2dui_stage->height);

  cairo_set_source_rgba(cr, v2dui_stage->rgba[0]/255.0, v2dui_stage->rgba[1]/255.0, v2dui_stage->rgba[2]/255.0, v2dui_stage->rgba[3]/255.0);
  cairo_fill_preserve(cr);

  cairo_set_source_rgba(cr, 0, 0, 0, 1);
  cairo_stroke(cr);

  /* Draw the stage display items. */

  if (v2dui_motion == V2DUI_MOVE_NONE ||
      v2dui_motion == V2DUI_MOVE_CLICK ||
      v2dui_motion == V2DUI_MOVE_CLICK_2) { f = 1/s; } else { f = 0; }

  cairo_set_antialias(cr, v2dui_quality);
  cv2d_draw_group(cr, v2dui_stage->item, f);

  /* If required, also draw "handle" boxes around the edge of the rectangle. */

  if (v2dui_any_selected && f > 0)
  {

    const double handle = (V2DUI_HANDLE_SIZE/2.0) * 1/s;

    cairo_set_line_width(cr, 1/s);

    if (v2dui_alt_handle) { cairo_set_antialias(cr, CAIRO_ANTIALIAS_GRAY); }
    else                  { cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE); }

    point[0][0] = v2dui_selected_aabb.x1; point[0][1] = v2dui_selected_aabb.y1;
    point[1][0] = v2dui_selected_aabb.x1; point[1][1] = v2dui_selected_aabb.y2;
    point[2][0] = v2dui_selected_aabb.x2; point[2][1] = v2dui_selected_aabb.y1;
    point[3][0] = v2dui_selected_aabb.x2; point[3][1] = v2dui_selected_aabb.y2;
    point[4][0] = v2dui_selected_aabb.x1 + ((v2dui_selected_aabb.x2-v2dui_selected_aabb.x1)/2); point[4][1] = v2dui_selected_aabb.y1;
    point[5][0] = v2dui_selected_aabb.x1 + ((v2dui_selected_aabb.x2-v2dui_selected_aabb.x1)/2); point[5][1] = v2dui_selected_aabb.y2;
    point[6][1] = v2dui_selected_aabb.y1 + ((v2dui_selected_aabb.y2-v2dui_selected_aabb.y1)/2); point[6][0] = v2dui_selected_aabb.x1;
    point[7][1] = v2dui_selected_aabb.y1 + ((v2dui_selected_aabb.y2-v2dui_selected_aabb.y1)/2); point[7][0] = v2dui_selected_aabb.x2;

    for (i = 0; i < 8; i++)
    {

      if (v2dui_alt_handle)
      { cairo_arc(cr, point[i][0], point[i][1], handle, 0, 2*V2D_PI); }
      else
      { cairo_rectangle(cr, point[i][0]-handle, point[i][1]-handle, handle*2, handle*2); }

      cairo_set_source_rgba(cr, 1, 1, 1, 1);
      cairo_fill_preserve(cr);
      cairo_set_source_rgba(cr, 0, 0, 0, 1);
      cairo_stroke(cr);

    }

  }

  if (v2dui_motion == V2DUI_MOVE_SELECT) { cv2d_draw_aabb(cr, 1/s, &v2dui_highlight); }

  cairo_restore(cr);

  return FALSE;

}

/**
@brief [INTERNAL] Redraw a "timeline" widget for a particular display item.

@todo Actually implement this...

@param widget The widget that contains the Cairo context.
@param cr The Cairo context to draw to.
@param p_item A pointer to the item to draw a timeline for.
@return Always FALSE. See the GTK documentation for further details.
*/

static gboolean draw_timeline(GtkWidget *widget, cairo_t *cr, gpointer p_item)
{

  v2d_anim_s *anim = v2dItemGetAnim((v2d_item_s*)p_item);
  v2d_real_t height = gtk_widget_get_allocated_height(widget);

  cairo_set_source_rgba(cr, 0.8, 0.8, 0.8, 1);
  cairo_paint(cr);

  cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);

  for (; anim; anim = v2dAnimGetNext(anim))
  {

    cairo_new_path(cr);
    cairo_move_to(cr, anim->frame * V2DUI_FRAME_WIDTH, 0);
    cairo_rel_line_to(cr, V2DUI_FRAME_WIDTH, 0);
    cairo_rel_line_to(cr, 0, height);
    cairo_rel_line_to(cr, -V2DUI_FRAME_WIDTH, 0);
    cairo_close_path(cr);

    cairo_set_line_width(cr, 2);
    cairo_set_source_rgba(cr, 0.9, 0.9, 0.9, 1);
    cairo_fill_preserve(cr);
    cairo_set_source_rgba(cr, 0.4, 0.4, 0.4, 1);
    cairo_stroke(cr);

  }

  return FALSE;

}

/**
@brief [INTERNAL] Update various widgets so that they reflect the stage data.

This is called whenever something changes the stage so that everything shown is
correct.
*/

static void update_status(void)
{

  const char *title = NULL;

  /* Update the window title. */

  if      (!v2dui_stage          ) { title = V2DUI_PROGRAM_NAME; }
  else if (!v2dui_stage->title   ) { title = "Untitled";         }
  else if (!v2dui_stage->title[0]) { title = "<blank>";          }
  else                             { title = v2dui_stage->title; }

  gtk_window_set_title(v2dui_window, title);

  /* Update the canvas. */

  if (v2dui_stage)
  {

    v2dui_any_selected = v2dGetSelectedAABB(v2dui_stage->item, 0, &v2dui_selected_aabb);
    if (!v2dui_any_selected) { v2dui_alt_handle = V2D_FALSE; }

    gtk_widget_set_size_request(v2dui_canvas,
        v2dui_stage->width  * gtk_spin_button_get_value(v2dui_zoom)/100,
        v2dui_stage->height * gtk_spin_button_get_value(v2dui_zoom)/100);

  }
  else { gtk_widget_set_size_request(v2dui_canvas, 1, 1); }

  gdk_window_invalidate_rect(gtk_widget_get_parent_window(v2dui_canvas), NULL, TRUE);

}

/**
@brief [INTERNAL] Update the "drawables" list.

@todo This COMPLETELY re-generates the list, but it might be more efficient to
simply update anything that has changed...
*/

static void list_drawables(void)
{

  v2d_draw_s *draw = NULL;
  GtkTreeIter iter;

  gtk_list_store_clear(v2dui_draw_list);

  for (draw = v2dStageGetDraw(v2dui_stage); draw; draw = v2dDrawGetNext(draw))
  {

    gtk_list_store_append(v2dui_draw_list, &iter);

    gtk_list_store_set(v2dui_draw_list, &iter,
        V2DUI_DRAW_POINTER, draw,
        V2DUI_DRAW_NAME, "<Unnamed Draw>",
        V2DUI_DRAW_TYPE, draw->type == V2D_TYPE_SHAPE ? "Shape" : (draw->type == V2D_TYPE_GROUP ? "Group" : "Text"),
        -1);

  }

}

static void on_select(GtkButton *button, gpointer void_item)
{
  if (button || void_item) { }
  /* TODO: Prompt to rename here; update the button text + the button label. */
}

static void list_items(void)
{

  v2d_item_s *item = NULL;
  GtkWidget *widget = NULL;
  GtkWidget *prior = NULL;

  GList *children, *iter;

  children = gtk_container_get_children(GTK_CONTAINER(v2dui_timeline));
  for(iter = children; iter; iter = g_list_next(iter))
  { gtk_widget_destroy(GTK_WIDGET(iter->data)); }
  g_list_free(children);

  if (!v2dui_stage) { return; }

  for (item = v2dGroupGetFinalItem(v2dui_stage->item); item; item = v2dItemGetPrev(item))
  {

    widget = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(v2dui_timeline), widget);
    gtk_widget_set_size_request(widget, 32, 32);
    g_signal_connect(G_OBJECT(widget), "draw", G_CALLBACK(on_thumb), v2dItemGetAnim(item)->draw[V2D_DEFAULT]);
    prior = widget;

    widget = gtk_button_new_with_label("<Unnamed Item>");
    gtk_grid_attach_next_to(GTK_GRID(v2dui_timeline), widget, prior, GTK_POS_RIGHT, 1, 1);
    g_signal_connect(G_OBJECT(widget), "clicked", G_CALLBACK(on_select), item);
    prior = widget;

    widget = gtk_button_new();
    gtk_button_set_image(GTK_BUTTON(widget), gtk_image_new_from_icon_name("go-up", GTK_ICON_SIZE_BUTTON));
    gtk_grid_attach_next_to(GTK_GRID(v2dui_timeline), widget, prior, GTK_POS_RIGHT, 1, 1);
    g_signal_connect(G_OBJECT(widget), "clicked", G_CALLBACK(on_raise), item);
    prior = widget;

    widget = gtk_button_new();
    gtk_button_set_image(GTK_BUTTON(widget), gtk_image_new_from_icon_name("go-down", GTK_ICON_SIZE_BUTTON));
    gtk_grid_attach_next_to(GTK_GRID(v2dui_timeline), widget, prior, GTK_POS_RIGHT, 1, 1);
    g_signal_connect(G_OBJECT(widget), "clicked", G_CALLBACK(on_lower), item);
    prior = widget;

    widget = gtk_button_new();
    gtk_button_set_image(GTK_BUTTON(widget), gtk_image_new_from_stock(GTK_STOCK_DELETE, GTK_ICON_SIZE_BUTTON));
    gtk_grid_attach_next_to(GTK_GRID(v2dui_timeline), widget, prior, GTK_POS_RIGHT, 1, 1);
    g_signal_connect(G_OBJECT(widget), "clicked", G_CALLBACK(on_delete_item), item);
    prior = widget;

    widget = gtk_drawing_area_new();
    gtk_grid_attach_next_to(GTK_GRID(v2dui_timeline), widget, prior, GTK_POS_RIGHT, 1, 1);
    gtk_widget_set_size_request(widget, 32, 32);
    gtk_widget_set_vexpand(widget, FALSE);
    gtk_widget_set_hexpand(widget, TRUE);
    g_signal_connect(G_OBJECT(widget), "draw", G_CALLBACK(draw_timeline), item);

  }

  gtk_widget_show_all(v2dui_timeline);

}

/**
@brief [INTERNAL] Updates/resets everything on-screen when a stage is created.

This is called when a new, blank stage is created, or is loaded from a file.
*/

static void init_stage(void)
{
  list_drawables();
  list_items();
  gtk_spin_button_set_value(v2dui_zoom, 100);
  update_status();
}

/**
@brief [INTERNAL] about

@param button about
@param item about
*/

void on_raise(GtkButton *button, gpointer item)
{
  if (button) { }
  v2dLinkMove(((v2d_item_s*)item)->link, V2D_TRUE);
  list_items();
  update_status();
}

/**
@brief [INTERNAL] about

@param button about
@param item about
*/

void on_lower(GtkButton *button, gpointer item)
{
  if (button) { }
  v2dLinkMove(((v2d_item_s*)item)->link, V2D_FALSE);
  list_items();
  update_status();
}

/**
@brief [INTERNAL] about

@param button about
@param item about
*/

void on_delete_item(GtkButton *button, gpointer item)
{
  if (button) { }
  v2dItemDelete(item);
  list_items();
  update_status();
}

/**
@brief [INTERNAL] about

@param widget about
@param event about
@param user_data about
@return writeme
*/

static gboolean on_press(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  GdkCursor *cursor = NULL;
  v2d_item_s *item = NULL;
  v2d_anim_s *anim = NULL;
  v2d_rect_s aabb;

  if (user_data) { }

  if (!v2dui_stage || event->button.button != 1) { return FALSE; }

  gtk_widget_grab_focus(widget);

  /* Initialise selection box. */

  v2dui_highlight.x1 = event->button.x;
  v2dui_highlight.y1 = event->button.y;
  get_canvas_xy(&v2dui_highlight.x1, &v2dui_highlight.y1);
  v2dui_highlight.x2 = v2dui_highlight.x1;
  v2dui_highlight.y2 = v2dui_highlight.y1;

  /* Store a back-up copy of the current matrix for future reference. */

  for (item = v2dGroupGetItem(v2dui_stage->item); item; item = v2dItemGetNext(item))
  {
    anim = v2dItemGetFrame(item, 0);
    if (anim) { v2dMatrixCopy(item->old, anim->matrix); }
  }

  /* Work out which "mode" is required, based on where the user has clicked. */

  v2dui_motion = V2DUI_MOVE_SELECT;

  /* Has one of the "handle boxes" been clicked on? */

  if (v2dui_any_selected)
  {

    v2d_real_t h_size = ((V2DUI_HANDLE_SIZE / 2.0) + 1) / (gtk_spin_button_get_value(v2dui_zoom)/100.0);
    v2d_rect_s handle;
    int i, j;

    for (j = 0; j < 3; j++)
    for (i = 0; i < 3; i++)
    {

      handle.x1 = v2dui_selected_aabb.x1 + ((v2dui_selected_aabb.x2-v2dui_selected_aabb.x1) * (i/2.0));
      handle.x2 = handle.x1 + h_size;
      handle.x1 = handle.x1 - h_size;

      handle.y1 = v2dui_selected_aabb.y1 + ((v2dui_selected_aabb.y2-v2dui_selected_aabb.y1) * (j/2.0));
      handle.y2 = handle.y1 + h_size;
      handle.y1 = handle.y1 - h_size;

      if (v2dCollideAABB(&v2dui_highlight, &handle))
      {

        if (v2dui_alt_handle)
        {
          if      (i == 0 && j == 0) { v2dui_motion = V2DUI_MOVE_ROTATE; }
          else if (i == 1 && j == 0) { v2dui_motion = V2DUI_MOVE_SKEW_X; }
          else if (i == 2 && j == 0) { v2dui_motion = V2DUI_MOVE_ROTATE; }
          else if (i == 0 && j == 1) { v2dui_motion = V2DUI_MOVE_SKEW_Y; }
          else if (i == 1 && j == 1) { continue; }
          else if (i == 2 && j == 1) { v2dui_motion = V2DUI_MOVE_SKEW_Y; }
          else if (i == 0 && j == 2) { v2dui_motion = V2DUI_MOVE_ROTATE; }
          else if (i == 1 && j == 2) { v2dui_motion = V2DUI_MOVE_SKEW_X; }
          else if (i == 2 && j == 2) { v2dui_motion = V2DUI_MOVE_ROTATE; }
        }
        else
        {
          if      (i == 0 && j == 0) { v2dui_motion = V2DUI_MOVE_SCALE_NW; }
          else if (i == 1 && j == 0) { v2dui_motion = V2DUI_MOVE_SCALE_N;  }
          else if (i == 2 && j == 0) { v2dui_motion = V2DUI_MOVE_SCALE_NE; }
          else if (i == 0 && j == 1) { v2dui_motion = V2DUI_MOVE_SCALE_W;  }
          else if (i == 1 && j == 1) { continue; }
          else if (i == 2 && j == 1) { v2dui_motion = V2DUI_MOVE_SCALE_E;  }
          else if (i == 0 && j == 2) { v2dui_motion = V2DUI_MOVE_SCALE_SW; }
          else if (i == 1 && j == 2) { v2dui_motion = V2DUI_MOVE_SCALE_S;  }
          else if (i == 2 && j == 2) { v2dui_motion = V2DUI_MOVE_SCALE_SE; }
        }

      }

    }

  }

  /* Otherwise, has the shape itself been clicked on? */

  if (v2dui_motion == V2DUI_MOVE_SELECT)
  {

    for (item = v2dGroupGetFinalItem(v2dui_stage->item); item; item = v2dItemGetPrev(item))
    {

      if (!v2dItemVisible(item, 0) || !v2dItemGetAABB(item, 0, &aabb)) { continue; }

      if (v2dCollideAABB(&v2dui_highlight, &aabb))
      {

        if (item->flags & V2D_ITEM_FLAG_SELECTED)
        {
          v2dui_motion = V2DUI_MOVE_CLICK;
          break;
        }
        else
        {
          /* Deselect everything, except for this. */
          v2dItemSelect(v2dui_stage->item, 0, NULL, V2D_TRUE);
          item->flags |= V2D_ITEM_FLAG_SELECTED;
          /* Switch to MOVE mode for the single selected item. */
          v2dui_motion = V2DUI_MOVE_CLICK_2;
          break;
        }

      }

    }

  }

  /* Update the mouse cursor image. */

  switch (v2dui_motion)
  {

    case V2DUI_MOVE_CLICK:
    case V2DUI_MOVE_CLICK_2:
    case V2DUI_MOVE_TRANSLATE:
      cursor = gdk_cursor_new(GDK_FLEUR);
    break;

    case V2DUI_MOVE_SCALE_E: case V2DUI_MOVE_SCALE_W: case V2DUI_MOVE_SKEW_X:
      cursor = gdk_cursor_new(GDK_SB_H_DOUBLE_ARROW);
    break;

    case V2DUI_MOVE_SCALE_N: case V2DUI_MOVE_SCALE_S: case V2DUI_MOVE_SKEW_Y:
      cursor = gdk_cursor_new(GDK_SB_V_DOUBLE_ARROW);
    break;

    case V2DUI_MOVE_SCALE_NE: case V2DUI_MOVE_SCALE_NW:
    case V2DUI_MOVE_SCALE_SE: case V2DUI_MOVE_SCALE_SW:
      cursor = gdk_cursor_new(GDK_SIZING);
    break;

    case V2DUI_MOVE_ROTATE:
      cursor = gdk_cursor_new(GDK_EXCHANGE);
    break;

    case V2DUI_MOVE_SELECT:
      cursor = gdk_cursor_new(GDK_CROSSHAIR);
    break;

    case V2DUI_MOVE_NONE: default: break;

  }

  if (cursor)
  {
    gdk_window_set_cursor(gtk_widget_get_parent_window(v2dui_canvas), cursor);
    g_object_unref(cursor);
  }

  update_status();

  return FALSE;

}

/**
@brief [INTERNAL] about

@param widget about
@param event about
@param user_data about
@return writeme
*/

static gboolean on_release(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  if (widget || event || user_data) { }
  v2dRectRepair(&v2dui_highlight);

  if (v2dui_stage)
  {

    if (v2dui_motion == V2DUI_MOVE_CLICK)
    { v2dui_alt_handle = !v2dui_alt_handle; }
    else if (v2dui_motion == V2DUI_MOVE_SELECT)
    { v2dItemSelect(v2dui_stage->item, 0, &v2dui_highlight, V2D_TRUE); }

  }

  gdk_window_set_cursor(gtk_widget_get_parent_window(v2dui_canvas), NULL);
  v2dui_motion = V2DUI_MOVE_NONE;
  update_status();
  return FALSE;

}

/**
@brief [INTERNAL] about

@param widget about
@param event about
@param user_data about
@return writeme
*/

static gboolean on_motion(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  v2d_item_s *item = NULL;
  v2d_anim_s *anim = NULL;
  v2d_rect_s  rect;

  if (!v2dui_stage) { return FALSE; }

  if (widget || user_data) { }

  if (v2dui_motion == V2DUI_MOVE_CLICK || v2dui_motion == V2DUI_MOVE_CLICK_2)
  { v2dui_motion = V2DUI_MOVE_TRANSLATE; }

  v2dui_highlight.x2 = event->motion.x;
  v2dui_highlight.y2 = event->motion.y;
  get_canvas_xy(&v2dui_highlight.x2, &v2dui_highlight.y2);

  for (item = v2dGroupGetItem(v2dui_stage->item); item; item = v2dItemGetNext(item))
  {

    if (item->flags & V2D_ITEM_FLAG_SELECTED)
    {

      anim = v2dItemGetFrame(item, 0);
      if (!anim) { continue; }
      if (!v2dDrawGetAABB(anim->draw[V2D_STATE_IDLE], &rect, 0)) { continue; }
      v2dMatrixRect(item->old, &rect);

      switch (v2dui_motion)
      {

        case V2DUI_MOVE_TRANSLATE:
        {

          v2d_real_t x = item->old[2][0];
          v2d_real_t y = item->old[2][1];

          x += v2dui_highlight.x2 - v2dui_highlight.x1;
          y += v2dui_highlight.y2 - v2dui_highlight.y1;

          v2dMatrixPosition(anim->matrix, x, y);

        }
        break;

        case V2DUI_MOVE_SCALE_N:
        case V2DUI_MOVE_SCALE_E:
        case V2DUI_MOVE_SCALE_S:
        case V2DUI_MOVE_SCALE_W:
        case V2DUI_MOVE_SCALE_NE:
        case V2DUI_MOVE_SCALE_NW:
        case V2DUI_MOVE_SCALE_SE:
        case V2DUI_MOVE_SCALE_SW:
        {

          v2d_real_t x = 1;
          v2d_real_t y = 1;
          v2d_rect_s new_rect;
          v2d_bool_t xo, yo;

          if (v2dui_motion == V2DUI_MOVE_SCALE_NE ||
              v2dui_motion == V2DUI_MOVE_SCALE_E ||
              v2dui_motion == V2DUI_MOVE_SCALE_SE)
          {
            x += (v2dui_highlight.x2 - v2dui_highlight.x1) / (v2dui_selected_aabb.x2 - v2dui_selected_aabb.x1);
            xo = V2D_FALSE;
          }

          if (v2dui_motion == V2DUI_MOVE_SCALE_NW ||
              v2dui_motion == V2DUI_MOVE_SCALE_W ||
              v2dui_motion == V2DUI_MOVE_SCALE_SW)
          {
            x += (v2dui_highlight.x1 - v2dui_highlight.x2) / (v2dui_selected_aabb.x2 - v2dui_selected_aabb.x1);
            xo = V2D_TRUE;
          }

          if (v2dui_motion == V2DUI_MOVE_SCALE_SE ||
              v2dui_motion == V2DUI_MOVE_SCALE_S ||
              v2dui_motion == V2DUI_MOVE_SCALE_SW)
          {
            yo = V2D_FALSE;
            y += (v2dui_highlight.y2 - v2dui_highlight.y1) / (v2dui_selected_aabb.y2 - v2dui_selected_aabb.y1);
          }

          if (v2dui_motion == V2DUI_MOVE_SCALE_NE ||
              v2dui_motion == V2DUI_MOVE_SCALE_N ||
              v2dui_motion == V2DUI_MOVE_SCALE_NW)
          {
            yo = V2D_TRUE;
            y += (v2dui_highlight.y1 - v2dui_highlight.y2) / (v2dui_selected_aabb.y2 - v2dui_selected_aabb.y1);
          }

          v2dMatrixCopy(anim->matrix, item->old);
          v2dMatrixScale(anim->matrix, x, y);

          if (v2dGetSelectedAABB(v2dui_stage->item, 0, &new_rect))
          {

            v2d_real_t temp = 0;

            if (x < 0)
            {
              temp        = new_rect.x1;
              new_rect.x1 = new_rect.x2;
              new_rect.x2 = temp;
            }

            if (y < 0)
            {
              temp        = new_rect.y1;
              new_rect.y1 = new_rect.y2;
              new_rect.y2 = temp;
            }

            v2dMatrixTranslate(anim->matrix,
              (rect.x1-new_rect.x1) - (xo ? (new_rect.x2-new_rect.x1) - (rect.x2-rect.x1) : 0),
              (rect.y1-new_rect.y1) - (yo ? (new_rect.y2-new_rect.y1) - (rect.y2-rect.y1) : 0));

          }

        }
        break;

        case V2DUI_MOVE_ROTATE:
        {
          v2d_real_t a = (v2dui_highlight.x2 - v2dui_highlight.x1) / (rect.x2 - rect.x1);
          v2dMatrixCopy(anim->matrix, item->old);
          v2dMatrixRotate(anim->matrix, a);
        }
        break;

        case V2DUI_MOVE_SKEW_X:
        {
          v2d_real_t x = (v2dui_highlight.x2 - v2dui_highlight.x1) / (rect.x2 - rect.x1);
          v2dMatrixCopy(anim->matrix, item->old);
          v2dMatrixShear(anim->matrix, x, 0);
        }

        break;

        case V2DUI_MOVE_SKEW_Y:
        {
          v2d_real_t y = (v2dui_highlight.y2 - v2dui_highlight.y1) / (rect.y2 - rect.y1);
          v2dMatrixCopy(anim->matrix, item->old);
          v2dMatrixShear(anim->matrix, 0, y);
        }
        break;

        case V2DUI_MOVE_CLICK: case V2DUI_MOVE_CLICK_2:
        case V2DUI_MOVE_SELECT: case V2DUI_MOVE_NONE: default: break;

      }

    }

  }

  update_status();
  return FALSE;

}

/**
@brief [INTERNAL] Called when a row is highlighted in the "drawables" list.

This simply requests that the preview window is re-drawn, so that the selected
drawable is instantly visible to the user.

@param widget Represents the currently-selected rows. Currently ignored.
@param preview A pointer to the preview widget. Used to request a redraw.
*/

static void on_draw_select(GtkTreeSelection *widget, gpointer preview)
{
  if (widget) { }
  gdk_window_invalidate_rect(gtk_widget_get_parent_window(GTK_WIDGET(preview)), NULL, TRUE);
}

/**
@brief [INTERNAL] Called when an row on the "drawables" list is double-clicked.

@param tree_view The tree view that called this function.
@param path Ignored; not used.
@param column Ignored; not used.
@param user_data Ignored; not used.
*/

static void on_draw_activate(GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data)
{

  v2d_draw_s *draw = get_drawable();
  v2d_text_s *as_text = v2dDrawAsText(draw);

  GtkWidget *dialog = NULL;
  GtkWidget *grid = NULL;
  GtkWidget *temp = NULL;

  GtkWidget *fill_color = NULL; /* Shape or text fill color picker. */
  GtkWidget *font_style = NULL; /* Font styling details. */
  GtkTextBuffer *text_value = NULL; /* Text content. */

  PangoFontDescription *font = NULL;
  GdkRGBA rgba;

  if (tree_view || path || column || user_data) { }

  if (!v2dui_stage || !draw) { return; }

  /* Create a file selection dialog, and add a grid container to it. */

  dialog = gtk_dialog_new_with_buttons ("Drawable Properties", v2dui_window,
      GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OK, GTK_RESPONSE_OK,
      NULL);

  temp = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
  grid = gtk_grid_new();
  gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 8);
  gtk_container_add(GTK_CONTAINER(temp), grid);

  /* Initialise type-specific widgets. */

  switch (draw->type)
  {

    case V2D_TYPE_SHAPE:
      temp = gtk_label_new("Shape properties aren't editable yet, sorry!");
      gtk_grid_attach(GTK_GRID(grid), temp, 0, 0, 1, 1);
    break;

    case V2D_TYPE_TEXT:
    {

      GtkWidget *hbox = NULL;

      /* Create a text color chooser. */

      rgba.red   = as_text->rgba[0]/255.0;
      rgba.green = as_text->rgba[1]/255.0;
      rgba.blue  = as_text->rgba[2]/255.0;
      rgba.alpha = as_text->rgba[3]/255.0;

      font_style = gtk_font_button_new();
      gtk_font_button_set_show_style(GTK_FONT_BUTTON(font_style), TRUE);
      gtk_grid_attach(GTK_GRID(grid), font_style, 0, 0, 1, 1);

      fill_color = gtk_color_button_new();
      gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(fill_color), &rgba);
      gtk_color_chooser_set_use_alpha(GTK_COLOR_CHOOSER(fill_color), TRUE);
      gtk_grid_attach(GTK_GRID(grid), fill_color, 1, 0, 1, 1);

      font = pango_font_description_new();

      if (font)
      {

        PangoStyle  slant  = PANGO_STYLE_NORMAL;
        PangoWeight weight = PANGO_WEIGHT_NORMAL;

        switch (as_text->slant)
        {
          case V2D_FONT_SLANT_OBLIQUE: slant = PANGO_STYLE_OBLIQUE; break;
          case V2D_FONT_SLANT_ITALIC:  slant = PANGO_STYLE_ITALIC;  break;
          case V2D_FONT_SLANT_NORMAL:  slant = PANGO_STYLE_NORMAL;  break;
          default: break;
        }

        if (as_text->weight == V2D_FONT_WEIGHT_BOLD) { weight = PANGO_WEIGHT_BOLD; }

        pango_font_description_set_style(font, slant);
        pango_font_description_set_weight(font, weight);
        pango_font_description_set_family(font, as_text->font ? as_text->font : "sans");
        pango_font_description_set_size(font, as_text->size * PANGO_SCALE);

        gtk_font_chooser_set_font_desc(GTK_FONT_CHOOSER(font_style), font);
        pango_font_description_free(font);

      }

      /* Add a text content editor. */

      hbox = gtk_scrolled_window_new(NULL, NULL);
      gtk_grid_attach(GTK_GRID(grid), hbox, 0, 1, 3, 1);

      temp = gtk_text_view_new();
      gtk_container_add(GTK_CONTAINER(hbox), temp);

      text_value = gtk_text_view_get_buffer(GTK_TEXT_VIEW(temp));
      gtk_text_buffer_set_text(text_value, as_text->data ? as_text->data : "", -1);

    }
    break;

    case V2D_TYPE_GROUP:
      temp = gtk_label_new("Group properties aren't editable yet, sorry!");
      gtk_grid_attach(GTK_GRID(grid), temp, 0, 0, 1, 1);
    break;

    default: break;

  }

  /* Run the dialog. */

  gtk_window_set_resizable(GTK_WINDOW(dialog), FALSE);
  gtk_widget_show_all(dialog);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
  {

    /* Update type-specific properties. */

    switch (draw->type)
    {

      case V2D_TYPE_SHAPE:

      break;

      case V2D_TYPE_TEXT:
      {

        GtkTextIter text_start;
        GtkTextIter text_end;
        gchar *text_data = NULL;

        gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(fill_color), &rgba);
        v2dTextSetColor(as_text, rgba.red*255, rgba.green*255, rgba.blue*255, rgba.alpha*255);

        font = pango_font_description_from_string(gtk_font_button_get_font_name(GTK_FONT_BUTTON(font_style)));
        if (font)
        {

          v2d_font_slant_t  slant  = V2D_FONT_SLANT_NORMAL;
          v2d_font_weight_t weight = V2D_FONT_WEIGHT_NORMAL;

          switch (pango_font_description_get_style(font))
          {
            case PANGO_STYLE_OBLIQUE: slant = V2D_FONT_SLANT_OBLIQUE; break;
            case PANGO_STYLE_ITALIC:  slant = V2D_FONT_SLANT_ITALIC;  break;
            case PANGO_STYLE_NORMAL:  slant = V2D_FONT_SLANT_NORMAL;  break;
            default: break;
          }

          if (pango_font_description_get_weight(font) > PANGO_WEIGHT_MEDIUM)
          { weight = V2D_FONT_WEIGHT_BOLD; }

          v2dTextSetSize(as_text, pango_font_description_get_size(font) / PANGO_SCALE);
          v2dTextSetFont(as_text, pango_font_description_get_family(font));
          v2dTextSetSlant(as_text, slant);
          v2dTextSetWeight(as_text, weight);

          pango_font_description_free(font);

        }

        gtk_text_buffer_get_iter_at_offset(text_value, &text_start, 0);
        gtk_text_buffer_get_iter_at_offset(text_value, &text_end, -1);
        text_data = gtk_text_buffer_get_text(text_value, &text_start, &text_end, TRUE);
        if (text_data)
        {
          v2dTextSetData(as_text, text_data[0] ? text_data : NULL);
          g_free(text_data);
        }

      }
      break;

      case V2D_TYPE_GROUP:

      break;

      default: break;

    }

    /* Update the display. */

    list_drawables();
    list_items();
    update_status();

  }

  /* Destroy the dialog. */

  gtk_widget_destroy(dialog);

}

/**
@brief [INTERNAL] Called whenever the "zoom" widget changes.

This simply requests that the stage is re-drawn, so that the new zoom level is
used.

@param widget The widget that triggered the event. Currently ignored.
@param user_data Ignored; not used.
*/

static void on_zoom(GtkSpinButton *widget, gpointer user_data)
{
  if (widget || user_data) { }
  update_status();
}

/**
@brief [INTERNAL] Create a new, blank stage.

@todo Ask the user if they want to save any changes first.

@param widget The widget that triggered the event. Currently ignored.
@param user_data Ignored; not used.
*/

static void on_new(GtkWidget *widget, gpointer user_data)
{

  if (widget || user_data) { }

  v2dStageDelete(v2dui_stage);
  v2dui_stage = v2dStageCreate(400, 300, 20);
  init_stage();

}

/**
@brief [INTERNAL] Import an SVG and replace/merge the current stage with it.

Merging it with the current stage allows it to be placed as a display item.

@param widget The widget that triggered the event. Currently ignored.
@param merge_flag_as_pointer FALSE = Replace the stage. TRUE = Merge into it.
*/

static void on_import(GtkWidget *widget, gpointer merge_flag_as_pointer)
{

  const v2d_bool_t merge_mode = GPOINTER_TO_INT(merge_flag_as_pointer);

  GtkWidget     *dialog = NULL;
  GtkFileFilter *svg_filter = NULL;

  if (!v2dui_stage && merge_mode) { return; }

  if (widget) { }

  /* Create a file selection dialog. */

  dialog = gtk_file_chooser_dialog_new (merge_mode ? "Import" : "Open Project",
      v2dui_window,
      GTK_FILE_CHOOSER_ACTION_OPEN,
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
      NULL);

  /* Add an SVG file-type filter. */

  svg_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(svg_filter, "SVG - Scalable Vector Graphics");
  gtk_file_filter_add_pattern(svg_filter, "*.svg");
  gtk_file_filter_add_pattern(svg_filter, "*.SVG");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), svg_filter);

  /* Run the dialog. */

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
  {

    v2d_stage_s *other = NULL;
    char *filename;

    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    other = v2dxLoadSVG(filename, 20);

    if (!other)
    {
      GtkWidget *error = gtk_message_dialog_new(GTK_WINDOW(dialog),
          GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
          "Could not import %s", filename);
      gtk_dialog_run(GTK_DIALOG(error));
      gtk_widget_destroy(error);
    }

    g_free(filename);

    /* Replace the current stage OR merge the new stage structure into it. */

    if (merge_mode)
    {
      v2dStageMerge(v2dui_stage, other);
      list_drawables();
    }
    else
    {
      v2dStageDelete(v2dui_stage);
      v2dui_stage = other;
      init_stage();
    }

  }

  /* Destroy the dialog. */

  gtk_widget_destroy(dialog);

}

/**
@brief [INTERNAL] about

writeme

@param quality about
@return writeme
*/

static v2d_bool_t v2duiRunExportDialogPNG(cairo_antialias_t *quality)
{

  GtkWidget *dialog = NULL;
  GtkWidget *grid = NULL;
  GtkWidget *temp = NULL;

  GtkWidget *antialias = NULL;

  if (!quality) { return V2D_FALSE; }

  /* Create a dialog. */

  dialog = gtk_dialog_new_with_buttons ("PNG Export Options", v2dui_window,
      GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OK, GTK_RESPONSE_OK,
      NULL);

  temp = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
  grid = gtk_grid_new();
  gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 8);
  gtk_container_add(GTK_CONTAINER(temp), grid);

  /* Create an anti-aliasing quality drop-down box. */

  temp = gtk_label_new("Antialiasing Quality");
  gtk_grid_attach(GTK_GRID(grid), temp, 0, 0, 1, 1);

  antialias = gtk_combo_box_text_new();
  gtk_grid_attach(GTK_GRID(grid), antialias, 1, 0, 1, 1);

  gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(antialias), "None");
  gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(antialias), "Grey");
  gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(antialias), "RGBA");
  gtk_combo_box_set_active(GTK_COMBO_BOX(antialias), 2);

  /* Run the dialog. Report failure if the user chooses to cancel here. */

  gtk_window_set_resizable(GTK_WINDOW(dialog), FALSE);
  gtk_widget_show_all(dialog);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK)
  {
    gtk_widget_destroy(dialog);
    return V2D_FALSE;
  }

  /* Set the quality level based on what the user has chosen. */

  if (gtk_combo_box_get_active(GTK_COMBO_BOX(antialias)) == 2)
  { *quality = CAIRO_ANTIALIAS_SUBPIXEL; }
  else if (gtk_combo_box_get_active(GTK_COMBO_BOX(antialias)) == 1)
  { *quality = CAIRO_ANTIALIAS_GRAY; }
  else
  { *quality = CAIRO_ANTIALIAS_NONE; }

  /* Destroy the dialog and report success. */

  gtk_widget_destroy(dialog);
  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

writeme

@param flags about
@param version about
@return writeme
*/

static v2d_bool_t v2duiRunExportDialogSWF(v2d_flag_t *flags, v2d_ui8_t *version)
{

  GtkWidget *dialog = NULL;
  GtkWidget *grid = NULL;
  GtkWidget *temp = NULL;

  GtkWidget *ver_box = NULL;
  GtkWidget *compress = NULL;
  GtkWidget *use_avm2 = NULL;
  GtkWidget *prevent_import = NULL;
  GtkWidget *enable_telemetry = NULL;
  GtkWidget *enable_network = NULL;

  if (!flags || !version) { return V2D_FALSE; }

  /* Create a dialog. */

  dialog = gtk_dialog_new_with_buttons ("SWF Export Options", v2dui_window,
      GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OK, GTK_RESPONSE_OK,
      NULL);

  temp = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
  grid = gtk_grid_new();
  gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 8);
  gtk_container_add(GTK_CONTAINER(temp), grid);

  /* Create "version" spinbox. */

  temp = gtk_label_new("Version");
  gtk_grid_attach(GTK_GRID(grid), temp, 0, 0, 1, 1);

  ver_box = gtk_spin_button_new_with_range(SWF_VERSION_MIN, SWF_VERSION_MAX, 1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(ver_box), 10);
  gtk_grid_attach(GTK_GRID(grid), ver_box, 1, 0, 1, 1);

  /* Create a "compression" combobox, with "None", "LZMA" and "ZLIB". */

  temp = gtk_label_new("Compression");
  gtk_grid_attach(GTK_GRID(grid), temp, 2, 0, 1, 1);

  compress = gtk_combo_box_text_new();
  gtk_grid_attach(GTK_GRID(grid), compress, 3, 0, 1, 1);

  gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(compress), "None");
  gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(compress), "ZLIB");
  gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(compress), "LZMA");
  gtk_combo_box_set_active(GTK_COMBO_BOX(compress), 0);

  /* Create checkbox widgets. */

  use_avm2 = gtk_check_button_new_with_label("Generate AVM2 bytecode");
  gtk_grid_attach(GTK_GRID(grid), use_avm2, 0, 1, 2, 1);

  prevent_import = gtk_check_button_new_with_label("Do not allow import");
  gtk_grid_attach(GTK_GRID(grid), prevent_import, 2, 1, 2, 1);

  enable_telemetry = gtk_check_button_new_with_label("Enable telemetry");
  gtk_grid_attach(GTK_GRID(grid), enable_telemetry, 0, 2, 2, 1);

  enable_network = gtk_check_button_new_with_label("Enable network access");
  gtk_grid_attach(GTK_GRID(grid), enable_network, 2, 2, 2, 1);

  /* Run the dialog. Report failure if the user chooses to cancel here. */

  gtk_window_set_resizable(GTK_WINDOW(dialog), FALSE);
  gtk_widget_show_all(dialog);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK)
  {
    gtk_widget_destroy(dialog);
    return V2D_FALSE;
  }

  /* Get the options selected. */

  *version = gtk_spin_button_get_value(GTK_SPIN_BUTTON(ver_box));
  *flags = 0;

  if (gtk_combo_box_get_active(GTK_COMBO_BOX(compress)) == 1)
  { *flags |= V2DX_SAVE_FLAG_ZLIB; }
  else if (gtk_combo_box_get_active(GTK_COMBO_BOX(compress)) == 2)
  { *flags |= V2DX_SAVE_FLAG_SWF_LZMA; }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(use_avm2)))
  { *flags |= V2DX_SAVE_FLAG_SWF_AVM2; }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(prevent_import)))
  { *flags |= V2DX_SAVE_FLAG_SWF_PROTECTED; }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(enable_telemetry)))
  { *flags |= V2DX_SAVE_FLAG_SWF_TELEMETRY; }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(enable_network)))
  { *flags |= V2DX_SAVE_FLAG_SWF_USENETWORK; }

  /* Destroy the dialog and report success. */

  gtk_widget_destroy(dialog);
  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

writeme

@param flags about
@return writeme
*/

static v2d_bool_t v2duiRunExportDialogSVG(v2d_flag_t *flags)
{

  GtkWidget *dialog = NULL;
  GtkWidget *grid = NULL;
  GtkWidget *temp = NULL;

  GtkWidget *use_zlib = NULL;
  GtkWidget *embed_resources = NULL;
  GtkWidget *use_backdrop = NULL;
  GtkWidget *use_svg_1_2 = NULL;

  if (!flags) { return V2D_FALSE; }

  /* Create a dialog. */

  dialog = gtk_dialog_new_with_buttons ("SVG Export Options", v2dui_window,
      GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OK, GTK_RESPONSE_OK,
      NULL);

  temp = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
  grid = gtk_grid_new();
  gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 8);
  gtk_container_add(GTK_CONTAINER(temp), grid);

  /* Create checkbox widgets. */

  use_zlib = gtk_check_button_new_with_label("Use ZLIB compression");
  gtk_grid_attach(GTK_GRID(grid), use_zlib, 0, 0, 1, 1);

  embed_resources = gtk_check_button_new_with_label("Embed external files");
  gtk_grid_attach(GTK_GRID(grid), embed_resources, 1, 0, 1, 1);

  use_backdrop = gtk_check_button_new_with_label("Background rectangle");
  gtk_grid_attach(GTK_GRID(grid), use_backdrop, 0, 1, 1, 1);

  use_svg_1_2 = gtk_check_button_new_with_label("Use SVG 1.2 features");
  gtk_grid_attach(GTK_GRID(grid), use_svg_1_2, 1, 1, 1, 1);

  /* Run the dialog. Report failure if the user chooses to cancel here. */

  gtk_window_set_resizable(GTK_WINDOW(dialog), FALSE);
  gtk_widget_show_all(dialog);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_OK)
  {
    gtk_widget_destroy(dialog);
    return V2D_FALSE;
  }

  /* Set the export flags, based on the checkboxes ticked by the user. */

  *flags = 0;

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(use_zlib)))
  { *flags |= V2DX_SAVE_FLAG_ZLIB; }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(embed_resources)))
  { *flags |= V2DX_SAVE_FLAG_SVG_EMBED; }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(use_backdrop)))
  { *flags |= V2DX_SAVE_FLAG_SVG_BACKDROP; }
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(use_svg_1_2)))
  { *flags |= V2DX_SAVE_FLAG_SVG_VER_1_2; }

  /* Destroy the dialog and report success. */

  gtk_widget_destroy(dialog);
  return V2D_TRUE;

}

/**
@brief [INTERNAL] Export the current stage in SWF, SVG, or PNG format.

@todo Other pixel-based formats (such as BMP) could be supported by passing the
raw RGBA data to a specialised file-writing function.

@param widget The widget that triggered the event. Currently ignored.
@param user_data Ignored; not used.
*/

static void on_export(GtkWidget *widget, gpointer user_data)
{

  GtkWidget *dialog = NULL;

  GtkFileFilter *swf_filter = NULL;
  GtkFileFilter *svg_filter = NULL;
  GtkFileFilter *png_filter = NULL;
  GtkFileFilter *file_type  = NULL;

  char *filename = NULL;
  v2d_bool_t status = V2D_TRUE;

  if (widget || user_data) { }
  if (!v2dui_stage) { return; }

  /* Create a file selection dialog. */

  dialog = gtk_file_chooser_dialog_new ("Export Project", v2dui_window,
                                        GTK_FILE_CHOOSER_ACTION_SAVE,
                                        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                        GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                        NULL);

  /* Add an SWF file-type filter. */

  swf_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(swf_filter, "SWF - Flash Animation");
  gtk_file_filter_add_pattern(swf_filter, "*.swf");
  gtk_file_filter_add_pattern(swf_filter, "*.SWF");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), swf_filter);

  /* Add an SVG file-type filter. */

  svg_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(svg_filter, "SVG - Scalable Vector Graphics");
  gtk_file_filter_add_pattern(svg_filter, "*.svg");
  gtk_file_filter_add_pattern(svg_filter, "*.SVG");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), svg_filter);

  /* Add a PNG file-type filter. */

  png_filter = gtk_file_filter_new();
  gtk_file_filter_set_name(png_filter, "PNG - Portable Network Graphics");
  gtk_file_filter_add_pattern(png_filter, "*.png");
  gtk_file_filter_add_pattern(png_filter, "*.PNG");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), png_filter);

  /* Run the file selection dialog. */

  if (gtk_dialog_run(GTK_DIALOG(dialog)) != GTK_RESPONSE_ACCEPT)
  {
    gtk_widget_destroy(dialog);
    return;
  }

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
  file_type = gtk_file_chooser_get_filter(GTK_FILE_CHOOSER(dialog));
  gtk_widget_destroy(dialog);

  /* Export the stage to a file, depending on the format chosen by the user. */

  status = V2D_TRUE;

  if (file_type == png_filter)
  {

    cairo_antialias_t quality = CAIRO_ANTIALIAS_NONE;

    if (v2duiRunExportDialogPNG(&quality))
    {

      cairo_surface_t *surface = NULL;
      cairo_t *cr = NULL;

      surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32 , v2dui_stage->width, v2dui_stage->height);
      cr = cairo_create(surface);

      cairo_set_source_rgba(cr, v2dui_stage->rgba[0]/255.0, v2dui_stage->rgba[1]/255.0, v2dui_stage->rgba[2]/255.0, v2dui_stage->rgba[3]/255.0);
      cairo_paint(cr);

      cairo_set_antialias(cr, quality);
      cv2d_draw_group(cr, v2dui_stage->item, 0);

      status = cairo_surface_write_to_png(surface, filename) == CAIRO_STATUS_SUCCESS;

      cairo_destroy(cr);
      cairo_surface_destroy(surface);

    }

  }
  else if (file_type == swf_filter)
  {

    v2d_flag_t flags = 0;
    v2d_ui8_t version = 0;

    if (v2duiRunExportDialogSWF(&flags, &version))
    {
      v2dDrawSort(v2dui_stage->draw, V2D_TRUE);
      status = v2dxSaveSWF(v2dui_stage, filename, version, flags);
    }

  }
  else if (file_type == svg_filter)
  {

    v2d_flag_t flags = 0;

    if (v2duiRunExportDialogSVG(&flags))
    { status = v2dxSaveSVG(v2dui_stage, filename, flags); }

  }
  else { status = V2D_FALSE; }

  /* If something went wrong, display a message to the user. */

  if (!status)
  {
    dialog = gtk_message_dialog_new(GTK_WINDOW(v2dui_window), GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "Could not export %s", filename);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }

  /* Free the temporary filename string buffer. */
  g_free(filename);

}

/**
@brief [INTERNAL] about

@param widget The widget that triggered the event.
@param user_data about
*/

static void on_quality(GtkWidget *widget, gpointer user_data)
{

  static v2d_bool_t lock_callback = V2D_FALSE;

  GtkWidget *option[3];
  int i = 0;

  if (user_data) { }

  if (lock_callback) { return; }
  lock_callback = V2D_TRUE;

  option[0] = v2dui_aa_high;
  option[1] = v2dui_aa_medium;
  option[2] = v2dui_aa_low;

  for (i = 0; i < 3; i++)
  {

    if (option[i] == widget)
    {

      if (!gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget)))
      { gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(widget), TRUE); }

      if      (widget == v2dui_aa_high  ) { v2dui_quality = CAIRO_ANTIALIAS_SUBPIXEL; }
      else if (widget == v2dui_aa_medium) { v2dui_quality = CAIRO_ANTIALIAS_GRAY;     }
      else                                { v2dui_quality = CAIRO_ANTIALIAS_NONE;     }

      update_status();

    }
    else if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(option[i])))
    { gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(option[i]), FALSE); }

  }

  lock_callback = V2D_FALSE;

}

/**
@brief [INTERNAL] Display the "stage properties" dialog to the user.

@param widget The widget that triggered the event. Currently ignored.
@param user_data Ignored; not used.
*/

static void on_properties(GtkWidget *widget, gpointer user_data)
{

  GtkWidget *dialog = NULL;
  GtkWidget *title = NULL;
  GtkWidget *backdrop = NULL;
  GtkWidget *width = NULL;
  GtkWidget *height = NULL;
  GtkWidget *fps = NULL;

  GtkWidget *grid = NULL;
  GtkWidget *hbox = NULL;
  GtkWidget *temp = NULL;

  GdkRGBA rgba;

  if (!v2dui_stage) { return; }

  if (widget || user_data) { }

  /* Create a file selection dialog, and add a grid container to it. */

  dialog = gtk_dialog_new_with_buttons ("Stage Properties", v2dui_window,
      GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
      GTK_STOCK_OK, GTK_RESPONSE_OK,
      NULL);

  temp = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
  grid = gtk_grid_new();
  gtk_grid_set_row_spacing(GTK_GRID(grid), 8);
  gtk_grid_set_column_spacing(GTK_GRID(grid), 8);
  gtk_container_add(GTK_CONTAINER(temp), grid);

  /* Create the 'title' entry box. */

  temp = gtk_label_new("Title");
  gtk_grid_attach(GTK_GRID(grid), temp, 0, 0, 1, 1);

  title = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(title), v2dui_stage->title ? v2dui_stage->title : "");
  gtk_grid_attach(GTK_GRID(grid), title, 1, 0, 1, 1);

  /* Create stage backdrop color chooser. */

  rgba.red   = v2dui_stage->rgba[0]/255.0;
  rgba.green = v2dui_stage->rgba[1]/255.0;
  rgba.blue  = v2dui_stage->rgba[2]/255.0;
  rgba.alpha = v2dui_stage->rgba[3]/255.0;

  temp = gtk_label_new("Backdrop");
  gtk_grid_attach(GTK_GRID(grid), temp, 2, 0, 1, 1);

  backdrop = gtk_color_button_new();
  gtk_color_chooser_set_rgba(GTK_COLOR_CHOOSER(backdrop), &rgba);
  gtk_color_chooser_set_use_alpha(GTK_COLOR_CHOOSER(backdrop), TRUE);
  gtk_grid_attach(GTK_GRID(grid), backdrop, 3, 0, 1, 1);

  /* Create the canvas size widgets. */

  temp = gtk_label_new("Canvas");
  gtk_grid_attach(GTK_GRID(grid), temp, 0, 1, 1, 1);

  hbox = gtk_grid_new();
  gtk_grid_set_column_spacing(GTK_GRID(hbox), 8);
  gtk_grid_attach(GTK_GRID(grid), hbox, 1, 1, 3, 1);

  width = gtk_spin_button_new_with_range(1, SWF_MAXWIDTH, 1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(width), v2dui_stage->width);
  gtk_container_add(GTK_CONTAINER(hbox), width);

  temp = gtk_label_new("x");
  gtk_container_add(GTK_CONTAINER(hbox), temp);

  height = gtk_spin_button_new_with_range(1, SWF_MAXHEIGHT, 1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(height), v2dui_stage->height);
  gtk_container_add(GTK_CONTAINER(hbox), height);

  temp = gtk_label_new("@");
  gtk_container_add(GTK_CONTAINER(hbox), temp);

  fps = gtk_spin_button_new_with_range(1, SWF_MAXFPS, 1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(fps), v2dui_stage->fps);
  gtk_container_add(GTK_CONTAINER(hbox), fps);

  temp = gtk_label_new("FPS");
  gtk_container_add(GTK_CONTAINER(hbox), temp);

  /* Run the dialog. */

  gtk_window_set_resizable(GTK_WINDOW(dialog), FALSE);
  gtk_widget_show_all(dialog);

  if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
  {

    gtk_color_chooser_get_rgba(GTK_COLOR_CHOOSER(backdrop), &rgba);

    v2dStageSetColor(v2dui_stage, rgba.red*255, rgba.green*255, rgba.blue*255, rgba.alpha*255);
    v2dStageSetTitle(v2dui_stage, gtk_entry_get_text(GTK_ENTRY(title)));
    v2dStageSetFPS(v2dui_stage, gtk_spin_button_get_value(GTK_SPIN_BUTTON(fps)));
    v2dStageSetSize(v2dui_stage, gtk_spin_button_get_value(GTK_SPIN_BUTTON(width)),
        gtk_spin_button_get_value(GTK_SPIN_BUTTON(height)));

    update_status();

  }

  /* Destroy the dialog. */

  gtk_widget_destroy(dialog);

}

/**
@brief [INTERNAL] Display the "about" dialog to the user.

@param widget The widget that triggered the event. Currently ignored.
@param user_data Ignored; not used.
*/

static void on_about(GtkWidget *widget, gpointer user_data)
{

  const char *documenters[] = {"Thomas Glyn Dennis (go.busto@gmail.com)", NULL};
  const char *authors[] = {"Thomas Glyn Dennis (go.busto@gmail.com)", NULL};
  const char *artists[] = {"You, hopefully...", NULL};

  if (widget || user_data) { }

  gtk_show_about_dialog(v2dui_window,
      "logo-icon-name", GTK_STOCK_SELECT_COLOR,
      "program-name", V2DUI_PROGRAM_NAME,
      "comments", "A GTK+ front-end for V2D and V2DX.",
      "copyright", "Copyright 2013-2014 by Thomas Glyn Dennis.",
      "license-type", GTK_LICENSE_GPL_3_0,
      "website-label", "https://gitorious.org/vector/",
      "website", "https://gitorious.org/vector/",
      "authors", authors,
      "documenters", documenters,
      /* "translator-credits", "None, yet...", */
      "artists", artists,
      NULL);

}

/**
@brief [INTERNAL] about

writeme

@param widget about
@param event about
@param user_data about
@return writeme
*/

static gboolean on_key_drawable(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  v2d_draw_s *draw = get_drawable();
  if (!draw || v2dui_motion != V2DUI_MOVE_NONE) { return FALSE; }

  if (widget || user_data) { }

  if (event->key.keyval == GDK_KEY_Delete)
  {

    v2dDrawDelete(draw);

    list_drawables();
    list_items();
    update_status();

  }

  return FALSE;

}

/**
@brief [INTERNAL] about

writeme

@param widget about
@param event about
@param user_data about
@return writeme
*/

static gboolean on_key_canvas(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{

  v2d_item_s *item = NULL;
  v2d_item_s *next = NULL;

  if (!v2dui_stage || v2dui_motion != V2DUI_MOVE_NONE) { return FALSE; }

  if (widget || user_data) { }

  switch (event->key.keyval)
  {

    case GDK_KEY_Delete:

      for (item = v2dGroupGetItem(v2dui_stage->item); item; item = next)
      {
        next = v2dItemGetNext(item);
        if (item->flags & V2D_ITEM_FLAG_SELECTED) { v2dItemDelete(item); }
      }

      list_items();
      update_status();

    break;

    case GDK_KEY_a:
      if (event->key.state & GDK_CONTROL_MASK)
      {
        v2dItemSelect(v2dui_stage->item, 0, NULL, V2D_FALSE);
        update_status();
      }
    break;

    default: break;

  }

  return FALSE;

}

/**
@brief [INTERNAL] Callback for a drawable-to-canvas drag-and-drop operation.

@param widget The widget receiving the drag-and-drop signal.
@param context The GTK drag-and-drop context.
@param x The mouse X position.
@param y The mouse Y position.
@param drag_time The GTK time value for this drag-and-drop event.
@param user_data Ignored; not used.
@return
*/

static gboolean on_drag_drop(GtkWidget *widget, GdkDragContext *context,
    gint x, gint y, guint drag_time, gpointer user_data)
{

  v2d_draw_s *draw = get_drawable();
  v2d_item_s *item = NULL;
  v2d_rect_s aabb;
  v2d_real_t cx = x;
  v2d_real_t cy = y;

  if (context || drag_time || user_data) { }

  /* If something is wrong, don't attempt to handle this DND operation. */

  if (!v2dui_stage || !draw) { return TRUE; }

  /* Grab the focus - "Delete" should delete the new item, not the drawable! */

  gtk_widget_grab_focus(widget);

  /* Deselect anything that is currently selected. */

  v2dItemSelect(v2dui_stage->item, 0, NULL, V2D_TRUE);

  /* Create a new instance of the drawable at the mouse cursor position. */

  get_canvas_xy(&cx, &cy);
  v2dDrawGetAABB(draw, &aabb, 0);
  cx -= aabb.x1 + ((aabb.x2-aabb.x1) / 2.0);
  cy -= aabb.y1 + ((aabb.y2-aabb.y1) / 2.0);

  item = v2dItemCreate(v2dui_stage->item, draw, 0, cx, cy, V2D_FALSE);

  /* Select the new item. */

  if (item) { item->flags |= V2D_ITEM_FLAG_SELECTED; }

  /* Update the display. */

  list_items();
  update_status();

  /* Don't propogate this drag-and-drop signal any further. */

  return FALSE;

}

/**
@brief [INTERNAL] Create the various widgets that make up the menu bar.

@return writeme
*/

static GtkWidget *create_menu_bar(void)
{

  GtkWidget *menubar = NULL;
  GtkWidget *menu = NULL;
  GtkWidget *menu_item = NULL;

  /* Create the menu bar. */

  menubar = gtk_menu_bar_new();

  /* File menu. */

  menu_item = gtk_menu_item_new_with_label("File");
  gtk_container_add(GTK_CONTAINER(menubar), menu_item);

  menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);

  menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_NEW, NULL);
  g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(on_new), NULL);
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_OPEN, NULL);
  g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(on_import), GINT_TO_POINTER(V2D_FALSE));
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_CONVERT, NULL);
  gtk_menu_item_set_label(GTK_MENU_ITEM(menu_item), "Export...");
  g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(on_export), NULL);
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_QUIT, NULL);
  g_signal_connect(G_OBJECT(menu_item), "activate", gtk_main_quit, NULL);
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  /* View menu. */

  menu_item = gtk_menu_item_new_with_label("View");
  gtk_container_add(GTK_CONTAINER(menubar), menu_item);

  menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);

  menu_item = gtk_menu_item_new_with_label("Quality");
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);

  v2dui_aa_high = gtk_check_menu_item_new_with_label("High");
  gtk_container_add(GTK_CONTAINER(menu), v2dui_aa_high);

  v2dui_aa_medium = gtk_check_menu_item_new_with_label("Medium");
  gtk_container_add(GTK_CONTAINER(menu), v2dui_aa_medium);

  v2dui_aa_low = gtk_check_menu_item_new_with_label("Low");
  gtk_container_add(GTK_CONTAINER(menu), v2dui_aa_low);

  gtk_check_menu_item_set_draw_as_radio(GTK_CHECK_MENU_ITEM(v2dui_aa_high  ), TRUE);
  gtk_check_menu_item_set_draw_as_radio(GTK_CHECK_MENU_ITEM(v2dui_aa_medium), TRUE);
  gtk_check_menu_item_set_draw_as_radio(GTK_CHECK_MENU_ITEM(v2dui_aa_low   ), TRUE);

  g_signal_connect(G_OBJECT(v2dui_aa_high  ), "activate", G_CALLBACK(on_quality), NULL);
  g_signal_connect(G_OBJECT(v2dui_aa_medium), "activate", G_CALLBACK(on_quality), NULL);
  g_signal_connect(G_OBJECT(v2dui_aa_low   ), "activate", G_CALLBACK(on_quality), NULL);

  /* Project menu. */

  menu_item = gtk_menu_item_new_with_label("Project");
  gtk_container_add(GTK_CONTAINER(menubar), menu_item);

  menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);

  menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_OPEN, NULL);
  gtk_menu_item_set_label(GTK_MENU_ITEM(menu_item), "Import...");
  g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(on_import), GINT_TO_POINTER(V2D_TRUE));
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_PROPERTIES, NULL);
  g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(on_properties), NULL);
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  /* Help menu. */

  menu_item = gtk_menu_item_new_with_label("Help");
  gtk_container_add(GTK_CONTAINER(menubar), menu_item);

  menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_item), menu);

  menu_item = gtk_image_menu_item_new_from_stock(GTK_STOCK_ABOUT, NULL);
  g_signal_connect(G_OBJECT(menu_item), "activate", G_CALLBACK(on_about), NULL);
  gtk_container_add(GTK_CONTAINER(menu), menu_item);

  return menubar;

}

/**
@brief [INTERNAL] Create the various widgets that make up the tool bar.

@return about
*/

static GtkWidget *create_tool_bar(void)
{

  GtkToolbar *toolbar = NULL;
  GtkToolItem *item = NULL;

  /* Create a toolbar. */

  toolbar = GTK_TOOLBAR(gtk_toolbar_new());

  /* ... */

  item = gtk_separator_tool_item_new();
  gtk_separator_tool_item_set_draw(GTK_SEPARATOR_TOOL_ITEM(item), FALSE);
  gtk_tool_item_set_expand(item, TRUE);
  gtk_toolbar_insert(toolbar, item, -1);
  item = gtk_separator_tool_item_new();
  gtk_toolbar_insert(toolbar, item, -1);

  item = gtk_tool_item_new();
  gtk_toolbar_insert(toolbar, item, -1);

  v2dui_zoom = GTK_SPIN_BUTTON(gtk_spin_button_new_with_range(1, 1000, 5));
  gtk_spin_button_set_value(v2dui_zoom, 100);
  g_signal_connect(G_OBJECT(v2dui_zoom), "value-changed", G_CALLBACK(on_zoom), NULL);
  gtk_container_add(GTK_CONTAINER(item), GTK_WIDGET(v2dui_zoom));

  return GTK_WIDGET(toolbar);

}

/**
@brief [INTERNAL] Create the "drawables" list-view widget.

@param container The container widget to add the tree view to.
@param preview A pointer to the preview canvas widget; needed for signals.
*/

static GtkWidget *create_draw_list(GtkWidget *preview)
{

  GtkTreeView *tree = NULL;
  GtkTreeViewColumn *column = NULL;
  GtkCellRenderer *renderer = NULL;

  v2dui_draw_list = gtk_list_store_new(V2DUI_NUM_DRAW_COLS, G_TYPE_POINTER, G_TYPE_STRING, G_TYPE_STRING);
  tree = GTK_TREE_VIEW(gtk_tree_view_new_with_model(GTK_TREE_MODEL(v2dui_draw_list)));
  g_object_unref(G_OBJECT(v2dui_draw_list));

  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes("Name", renderer,
                                                    "text", V2DUI_DRAW_NAME,
                                                    NULL);
  gtk_tree_view_append_column(tree, column);

  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes("Type",
                                                    renderer,
                                                    "text", V2DUI_DRAW_TYPE,
                                                    NULL);
  gtk_tree_view_append_column(tree, column);

  v2dui_draw_item = gtk_tree_view_get_selection(tree);
  gtk_tree_view_enable_model_drag_source(tree, GDK_BUTTON1_MASK, target_list, 1, GDK_ACTION_COPY);
  g_signal_connect(G_OBJECT(v2dui_draw_item), "changed", G_CALLBACK(on_draw_select), preview);
  g_signal_connect(G_OBJECT(tree), "row-activated", G_CALLBACK(on_draw_activate), NULL);
  g_signal_connect(G_OBJECT(tree), "key-press-event", G_CALLBACK(on_key_drawable), NULL);

  return GTK_WIDGET(tree);

}

/**
@brief [INTERNAL] about

writeme

@return about
*/

static GtkWidget *create_drawable_pane(void)
{

  GtkWidget *pane = NULL;
  GtkWidget *scroll = NULL;
  GtkWidget *preview = NULL;

  pane = gtk_paned_new(GTK_ORIENTATION_VERTICAL);

  /* Create the "drawable" preview canvas and add that to the left pane too. */

  preview = gtk_drawing_area_new();
  gtk_widget_set_size_request(preview, 128, 128);
  gtk_widget_set_vexpand(preview, TRUE);
  gtk_widget_set_hexpand(preview, TRUE);
  g_signal_connect(G_OBJECT(preview), "draw", G_CALLBACK(on_thumb), NULL);
  gtk_paned_pack1(GTK_PANED(pane), preview, FALSE, TRUE);

  /* Create the "drawables" list. */

  scroll = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_paned_pack2(GTK_PANED(pane), scroll, TRUE, FALSE);

  /* Have the thumbnail redraw itself + make the tree view request a redraw. */

  gtk_container_add(GTK_CONTAINER(scroll), create_draw_list(preview));

  return pane;

}

/**
@brief [INTERNAL] Create the widgets that make up the top "timeline" pane.

@todo For some reason, the timeline/viewport widgets obscure the black "scroll"
widget background on Trisquel 6.0 (Toutatis, GTK 3.4.2) with a white fill. This
doesn't happen on Debian 8.0 (Jessie, GTK 3.8.6), so it'll probably fix itself
once Trisquel upgrades to a more recent GTK vesion.

@return The scrolled window used to contain the timeline widget(s).
*/

static GtkWidget *create_timeline_pane(void)
{

  GtkWidget *scroll = NULL;
  GdkRGBA rgba;

  rgba.red = 0;
  rgba.green = 0;
  rgba.blue = 0;
  rgba.alpha = 1;

  scroll = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
  gtk_widget_override_background_color(scroll, GTK_STATE_NORMAL, &rgba);

  v2dui_timeline = gtk_grid_new();
  gtk_container_set_border_width(GTK_CONTAINER(v2dui_timeline), 8);
  gtk_orientable_set_orientation(GTK_ORIENTABLE(v2dui_timeline), GTK_ORIENTATION_VERTICAL);
  gtk_grid_set_row_spacing(GTK_GRID(v2dui_timeline), 4);
  gtk_grid_set_column_spacing(GTK_GRID(v2dui_timeline), 8);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scroll), v2dui_timeline);

  return scroll;

}

/**
@brief [INTERNAL] about

writeme

@return about
*/

static GtkWidget *create_editing_pane(void)
{

  GtkWidget *grid = NULL;
  GtkWidget *scroll = NULL;

  /* Create the main canvas widget, and add it to the right-hand pane. */

  grid = gtk_grid_new();
  gtk_orientable_set_orientation(GTK_ORIENTABLE(grid), GTK_ORIENTATION_VERTICAL);

  scroll = gtk_scrolled_window_new(NULL, NULL);
  gtk_widget_set_vexpand(scroll, TRUE);
  gtk_widget_set_hexpand(scroll, TRUE);
  gtk_container_add(GTK_CONTAINER(grid), scroll);

  v2dui_canvas = gtk_drawing_area_new();
  gtk_widget_set_can_focus(v2dui_canvas, TRUE);
  gtk_drag_dest_set(v2dui_canvas, GTK_DEST_DEFAULT_MOTION, target_list, 1, GDK_ACTION_COPY);
  gtk_widget_set_events(v2dui_canvas, gtk_widget_get_events(v2dui_canvas) |
      GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_PRESS_MASK |
      GDK_BUTTON1_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK);

  g_signal_connect(G_OBJECT(v2dui_canvas), "drag-drop", G_CALLBACK(on_drag_drop), NULL);
  g_signal_connect(G_OBJECT(v2dui_canvas), "draw", G_CALLBACK(on_redraw), NULL);
  g_signal_connect(G_OBJECT(v2dui_canvas), "button-press-event", G_CALLBACK(on_press), NULL);
  g_signal_connect(G_OBJECT(v2dui_canvas), "button-release-event", G_CALLBACK(on_release), NULL);
  g_signal_connect(G_OBJECT(v2dui_canvas), "motion-notify-event", G_CALLBACK(on_motion), NULL);
  g_signal_connect(G_OBJECT(v2dui_canvas), "key-press-event", G_CALLBACK(on_key_canvas), NULL);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scroll), v2dui_canvas);

  gtk_container_add(GTK_CONTAINER(grid), create_tool_bar());

  return grid;

}

/**
@brief This is where the program begins.

@param argc Length of the argv array.
@param argv Program arguments.
@return Zero on success or non-zero on failure.
*/

int main(int argc, char *argv[])
{

  GtkContainer *grid = NULL;
  GtkWidget *h_pane = NULL;
  GtkWidget *v_pane = NULL;
  int i = 0;

  gtk_init(&argc, &argv);

  /* Create the main window and a "grid" to contain the child widgets. */

  gtk_window_set_default_icon_name(GTK_STOCK_SELECT_COLOR);
  v2dui_window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
  gtk_window_set_default_size(v2dui_window, 600, 400);
  gtk_window_maximize(v2dui_window);
  g_signal_connect(GTK_WIDGET(v2dui_window), "destroy", gtk_main_quit, NULL);

  grid = GTK_CONTAINER(gtk_grid_new());
  gtk_orientable_set_orientation(GTK_ORIENTABLE(grid), GTK_ORIENTATION_VERTICAL);
  gtk_container_add(GTK_CONTAINER(v2dui_window), GTK_WIDGET(grid));

  gtk_container_add(grid, create_menu_bar());

  h_pane = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add(grid, h_pane);

  v_pane = gtk_paned_new(GTK_ORIENTATION_VERTICAL);
  gtk_paned_pack2(GTK_PANED(h_pane), v_pane, TRUE, FALSE);

  gtk_paned_pack1(GTK_PANED(h_pane), create_drawable_pane(), FALSE, TRUE);
  gtk_paned_pack1(GTK_PANED(v_pane), create_timeline_pane(), FALSE, TRUE);
  gtk_paned_pack2(GTK_PANED(v_pane), create_editing_pane(), TRUE, FALSE);

  gtk_widget_show_all(GTK_WIDGET(v2dui_window));

  gtk_menu_item_activate(GTK_MENU_ITEM(v2dui_aa_high));

  /* Load in the requested file, or create a new, blank scene. */

  for (i = 1; i < argc; i++)
  {
    v2dui_stage = v2dxLoadSVG(argv[i], 20);
    if (v2dui_stage) { break; }
  }

  if (!v2dui_stage) { on_new(NULL, NULL); }

  init_stage();

  /* Start the main program loop. */

  gtk_main();

  /* Free the stage from memory and terminate the program. */

  v2dStageDelete(v2dui_stage);

  return 0;

}
