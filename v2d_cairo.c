/**
@file v2d_cairo.c

@brief Contains functions for drawing V2D scenes with Cairo.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include "v2d_cairo.h"

/*
[PUBLIC] about
*/

v2d_bool_t cv2d_draw_aabb(cairo_t *cr, double line_width, v2d_rect_s *aabb)
{

  const double dashes = 4 * line_width;

  if (!cr || line_width <= 0 || !aabb) { return V2D_FALSE; }

  cairo_save(cr);

  /* Draw a single-pixel wide, non-antialiased line. */

  cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
  cairo_set_line_width(cr, line_width);

  /* Create an outline, and then draw it using both back and white dashes. */

  cairo_rectangle(cr, aabb->x1, aabb->y1, aabb->x2-aabb->x1, aabb->y2-aabb->y1);

  cairo_set_dash(cr, &dashes, 1, 0);
  cairo_set_source_rgba(cr, 0, 0, 0, 1);
  cairo_stroke_preserve(cr);

  cairo_set_dash(cr, &dashes, 1, dashes);
  cairo_set_source_rgba(cr, 1, 1, 1, 1);
  cairo_stroke(cr);

  cairo_restore(cr);

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t cv2d_draw_shape(cairo_t *cr, v2d_shape_s *shape)
{

  v2d_path_s *path = NULL;
  v2d_real_t x, y;

  if (!cr || !shape) { return V2D_FALSE; }

  cairo_set_fill_rule(cr, (shape->flags & V2D_SHAPE_FLAG_EVENODD) ?
      CAIRO_FILL_RULE_EVEN_ODD : CAIRO_FILL_RULE_WINDING);

  /* Draw the shape. */

  x = 0; y = 0;

  cairo_new_path(cr);

  for (path = v2dShapeGetPath(shape); path; path = v2dPathGetNext(path))
  {

    switch (path->type)
    {

      case V2D_PATH_QUAD:
      {

        v2d_real_t rx = (path->flags & V2D_PATH_FLAG_RELATIVE) ? 0 : x;
        v2d_real_t ry = (path->flags & V2D_PATH_FLAG_RELATIVE) ? 0 : y;

        /* http://stackoverflow.com/questions/3162645/convert-a-quadratic-bezier-to-a-cubic */

        v2d_real_t c1x = rx                            + 2/3.0*(path->point[V2D_CONTROL_1][0]-rx);
        v2d_real_t c1y = ry                            + 2/3.0*(path->point[V2D_CONTROL_1][1]-ry);
        v2d_real_t c2x = path->point[V2D_END_POINT][0] + 2/3.0*(path->point[V2D_CONTROL_1][0]-path->point[V2D_END_POINT][0]);
        v2d_real_t c2y = path->point[V2D_END_POINT][1] + 2/3.0*(path->point[V2D_CONTROL_1][1]-path->point[V2D_END_POINT][1]);

        if (path->flags & V2D_PATH_FLAG_RELATIVE) { cairo_rel_curve_to(cr, c1x, c1y, c2x, c2y, path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }
        else                                      { cairo_curve_to    (cr, c1x, c1y, c2x, c2y, path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }

      }
      break;

      case V2D_PATH_CUBE:
        if (path->flags & V2D_PATH_FLAG_RELATIVE) { cairo_rel_curve_to(cr, path->point[V2D_CONTROL_1][0], path->point[V2D_CONTROL_1][1], path->point[V2D_CONTROL_2][0], path->point[V2D_CONTROL_2][1], path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }
        else                                      { cairo_curve_to    (cr, path->point[V2D_CONTROL_1][0], path->point[V2D_CONTROL_1][1], path->point[V2D_CONTROL_2][0], path->point[V2D_CONTROL_2][1], path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }
      break;

      case V2D_PATH_LINE:
        if (path->flags & V2D_PATH_FLAG_RELATIVE) { cairo_rel_line_to(cr, path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }
        else                                      { cairo_line_to    (cr, path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }
      break;

      case V2D_PATH_MOVE: default:
        if (path->flags & V2D_PATH_FLAG_RELATIVE) { cairo_rel_move_to(cr, path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }
        else                                      { cairo_move_to    (cr, path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]); }
      break;

    }

    /* Update the reference position. */

    if (path->flags & V2D_PATH_FLAG_RELATIVE)
    {
      x += path->point[V2D_END_POINT][0];
      y += path->point[V2D_END_POINT][1];
    }
    else
    {
      x = path->point[V2D_END_POINT][0];
      y = path->point[V2D_END_POINT][1];
    }

  }

  /* Note: Some miter'd lines have problems being closed properly... */

  if (shape->flags & V2D_SHAPE_FLAG_CLOSE) { cairo_close_path(cr); }

  /* Fill the shape. */

  if (shape->fill)
  {

    if (v2dIsFlatRGBA(shape->fill))
    {
      v2d_fill_color_s *grad = v2dFillAsColor(shape->fill);
      cairo_set_source_rgba(cr, grad->rgba[0]/255.0, grad->rgba[1]/255.0, grad->rgba[2]/255.0, grad->rgba[3]/255.0);
    }
    else if (shape->fill->type == V2D_FILL_COLOR)
    {
      /* TODO: Gradient-based fills. */
      v2d_fill_color_s *grad = v2dFillAsColor(shape->fill);
      cairo_set_source_rgba(cr, grad->rgba[0]/255.0, grad->rgba[1]/255.0, grad->rgba[2]/255.0, grad->rgba[3]/255.0);
    }
    else
    {
      /* TODO: Image-based fills. */
      v2d_fill_image_s *image = v2dFillAsImage(shape->fill);
      cairo_set_source_rgba(cr, 0.5, 0.5, 0.5, image->alpha);
    }

    if (shape->line) { cairo_fill_preserve(cr); }
    else             { cairo_fill(cr);          }

  }

  /* Draw the shape outlines. */

  if (shape->line)
  {
    cairo_set_line_width(cr, shape->line->width);
    cairo_set_miter_limit(cr, shape->line->miter);
    cairo_set_line_cap(cr, shape->line->cap);   /* TODO: Map to cairo types. */
    cairo_set_line_join(cr, shape->line->join); /* TODO: Map to cairo types. */
    cairo_set_source_rgba(cr, shape->line->rgba[0]/255.0, shape->line->rgba[1]/255.0, shape->line->rgba[2]/255.0, shape->line->rgba[3]/255.0);
    cairo_stroke(cr);
  }

  /* Report success. */

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t cv2d_draw_text(cairo_t *cr, v2d_text_s *text)
{

  cairo_font_slant_t  slant  = CAIRO_FONT_SLANT_NORMAL;
  cairo_font_weight_t weight = CAIRO_FONT_WEIGHT_NORMAL;

  if (!cr || !text) { return V2D_FALSE; }

  switch (text->slant)
  {
    case V2D_FONT_SLANT_OBLIQUE: slant = CAIRO_FONT_SLANT_OBLIQUE; break;
    case V2D_FONT_SLANT_ITALIC:  slant = CAIRO_FONT_SLANT_ITALIC;  break;
    case V2D_FONT_SLANT_NORMAL:  slant = CAIRO_FONT_SLANT_NORMAL;  break;
    default: break;
  }

  if (text->weight == V2D_FONT_WEIGHT_BOLD) { weight = CAIRO_FONT_WEIGHT_BOLD; }

  cairo_set_font_size(cr, text->size);
  cairo_select_font_face(cr, text->font ? text->font : "sans", slant, weight);
  cairo_set_source_rgba(cr, text->rgba[0]/255.0, text->rgba[1]/255.0, text->rgba[2]/255.0, text->rgba[3]/255.0);

  cairo_save(cr);
  cairo_translate(cr, 0, text->size);

  /* TODO: Need to handle newlines here... */

  cairo_new_path(cr);
  cairo_show_text(cr, text->data);
  cairo_fill(cr);

  cairo_restore(cr);

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t cv2d_draw(cairo_t *cr, v2d_draw_s *draw, double aabb_line)
{

  if (!cr || !draw) { return V2D_FALSE; }

  switch (draw->type)
  {
    case V2D_TYPE_GROUP: return cv2d_draw_group(cr, v2dDrawAsGroup(draw), aabb_line);
    case V2D_TYPE_SHAPE: return cv2d_draw_shape(cr, v2dDrawAsShape(draw));
    case V2D_TYPE_TEXT:  return cv2d_draw_text (cr, v2dDrawAsText (draw));
    default: break;
  }

  return V2D_FALSE;

}

/*
[PUBLIC] about
*/

v2d_bool_t cv2d_draw_group(cairo_t *cr, v2d_chain_s *group, double aabb_line)
{

  v2d_item_s *item = NULL;
  v2d_anim_s *anim = NULL;

  v2d_rect_s aabb;
  cairo_matrix_t gtk_matrix;

  if (!cr || !group) { return V2D_FALSE; }

  for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
  {

    anim = v2dItemGetAnim(item);

    gtk_matrix.xx = anim->matrix[0][0];
    gtk_matrix.yx = anim->matrix[0][1];
    gtk_matrix.xy = anim->matrix[1][0];
    gtk_matrix.yy = anim->matrix[1][1];
    gtk_matrix.x0 = anim->matrix[2][0];
    gtk_matrix.y0 = anim->matrix[2][1];

    cairo_save(cr);
    cairo_transform(cr, &gtk_matrix);

    cv2d_draw(cr, anim->draw[V2D_DEFAULT], aabb_line);

    cairo_restore(cr);

    if (item->flags & V2D_ITEM_FLAG_SELECTED)
    {
      v2dItemGetAABB(item, 0, &aabb);
      cv2d_draw_aabb(cr, aabb_line, &aabb);
    }

  }

  return V2D_TRUE;

}
