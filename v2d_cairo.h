/**
@file v2d_cairo.h

@brief Contains functions for drawing V2D scenes with Cairo.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __V2D_CAIRO_H__
#define __V2D_CAIRO_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <cairo.h>
#include "v2dx/v2d/stage.h"

/**
@brief about

writeme

@param cr about
@param line_width about
@param aabb about
@return writeme
*/

v2d_bool_t cv2d_draw_aabb(cairo_t *cr, double line_width, v2d_rect_s *aabb);

/**
@brief about

writeme

@param cr about
@param shape about
@return writeme
*/

v2d_bool_t cv2d_draw_shape(cairo_t *cr, v2d_shape_s *shape);

/**
@brief about

writeme

@param cr about
@param text about
@return writeme
*/

v2d_bool_t cv2d_draw_text(cairo_t *cr, v2d_text_s *text);

/**
@brief about

writeme

@param cr about
@param group about
@return writeme
*/

v2d_bool_t cv2d_draw_group(cairo_t *cr, v2d_chain_s *group, double aabb_line);

/**
@brief about

writeme

@param cr about
@param draw about
@return writeme
*/

v2d_bool_t cv2d_draw(cairo_t *cr, v2d_draw_s *draw, double aabb_line);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_CAIRO_H__ */
