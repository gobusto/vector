SOUND EFFECTS
-------------
AUTHORS: dawith
LICENSE: CC0 (Public Domain)
WEBSITE: http://opengameart.org/content/zippo-click-sound

I was fooling around with a mic (a shure SM 58) and recorded myself opening and
closing a zippo lighter. Well, one of the clicky sounds that I recorded sounded
nice enough to me for a sound of opening the main menu while in game or the
like. Anyway, do whatever you wish with it, here it is in flac format.
