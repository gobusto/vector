CHESS SVG IMAGES
----------------
AUTHORS: C. Burnett
LICENSE: Triple-licensed under GFDL, BSD, & GPL.
WEBSITE: http://en.wikipedia.org/wiki/User:Cburnett/GFDL_images/Chess

I created this SVG images in hopes to replace the 44px PNGs in current use.
Discussion on meta:Talk:WikiProject Chess. Dual-licensed under GFDL & BSD.
