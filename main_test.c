/**
@mainpage Vector2D

@section intro_sec Introduction

Vector2D is a 2D vector image manipulation framework, with minimal dependencies
on third-party libraries. It is intended to "just work" on any operating system
with a C compiler; the code should compile (without modification) on 32-bit and
64-bit systems, and should ideally work on 16-bit (or other) systems as well.

No third-party libraries are needed to compile/use it. An (optional) dependency
on ZLIB may be added at some point so that V2DX can handle compressed SVG, SWF,
and SIF files, but the codebase should otherwise remain self-sufficient.

V2D is the "core" of the program and is contained within the V2D sub-directory.
V2DX provides extra features, such as SWF export. V2D can be used without V2DX,
but V2DX cannot be used without V2D.

The intention is that programs will use the core functionality of V2D to create
vector images, displaying the scene via an API such as OpenVG or Direct2D, with
V2DX used to import/export projects as SVG, SIF, or SWF files. Features such as
scripting and animation will also be supported, assuming that the output format
supports them.

@section swf_support SWF Support

It is intended that all SWF versions up to 10 (at least) will be supported for
export by V2DX. SWF import may or may not be supported.

Exported SWF files are tested using the official Adobe player, Gnash, SWFDec,
and Lightspark. Exported files MUST work as expected with the  official Adobe
player, and SHOULD work with Gnash, SWFDec, and Lightspark where possible. If
the non-Adobe players behave differently from the official one, please open a
bug report for their development team(s) with the relevant SWF file attached.

@section other_editors Vector Drawing Programs

If you're simply looking for a vector-based drawing/animation program, you may
want to try one of the following:

Inkscape (SVG) - http://inkscape.org/

Synfig (SIF) - http://www.synfig.org/

Pencil (SWF) - http://pencil-animation.org/

Please note that none of these programs support scripts (as the Adobe Flash IDE
does) which is something that Vector2D intends to support at some stage.

@section other_libraries Alternatives

Vector2D is currently usable, but incomplete. If you're only interested in SWF
files, consider using libMing instead: http://www.libming.org/
*/

/**
@file main_test.c

@brief This file is used for testing V2D without a GUI.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "v2dx/compile.h"
#include "v2dx/load.h"
#include "v2dx/save.h"

/**
@brief Create a test scene. This scene tests matrix functionality.

@return The generated stage on success, or NULL on failure.
*/

static v2d_stage_s *test_matrix_scene(void)
{

  v2d_stage_s      *stage = NULL;

  v2d_data_s       *audio = NULL;
  v2d_data_s       *image = NULL;

  v2d_draw_s       *cat_box = NULL;
  v2d_draw_s       *red_box = NULL;

  v2d_draw_s       *draw  = NULL;
  v2d_item_s       *item  = NULL;
  v2d_anim_s       *anim  = NULL;

  v2d_shape_s      *shape = NULL;
  v2d_fill_color_s *grad  = NULL;

  int i = 0;

  /* Create a new scene - 640x480 pixels @ 50 frames-per-second. */

  stage = v2dStageCreate(640, 480, 50);
  if (!stage) { return NULL; }
  v2dStageSetColor(stage, 150, 200, 255, 255);
  v2dStageSetTitle(stage, "Hello, World!");

  /* Add some RDF-style metadata. */

  v2dStageSetMetaData(stage,
"<rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>\n\
\t<rdf:Description rdf:about='' xmlns:dc='http://purl.org/dc/1.1'>\n\
\t\t<dc:title>Matrix Transformations</dc:title>\n\
\t\t<dc:description>A brief overview of common matrix operations.</dc:description>\n\
\t</rdf:Description>\n\
</rdf:RDF>");

  /* Add a new embedded object. */
/*
  v2dStageAddEmbed(stage, "media/history.htm", NULL);
*/
  /* Add a new embedded object. */

  audio = v2dDataCreate(stage->audio, "media/jumpland44100.mp3", NULL);

  /* Add a new image object. */

  image = v2dDataCreate(stage->image, "media/Floyd-Steinberg_algorithm-original.jpg", NULL);

  /* Define a new shape object. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);

  if (draw)
  {

    shape = v2dDrawAsShape(draw);

    v2dShapeFillColor(shape, 255, 0, 0, 255);

    if (v2dShapeLineColor(shape, 0, 0, 0, 255))
    {
      v2dLineSetWidth(shape->line, 5);
      v2dLineSetJoin(shape->line, V2D_LINE_JOIN_ROUND);
    }

    v2dPathLine(v2dShapeGetPathEnd(shape), 100, 0, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape), 100, 100, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape), 0,   100, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape), 0,  0, 0);

  }

  red_box = draw;

  v2dItemCreate(stage->item, draw, 0, 20, 70, V2D_FALSE);

  item = v2dItemCreate(stage->item, draw, 0, 510, 70, V2D_FALSE);

  for (i = 1; i < 80; i++)
  {
    anim = v2dAnimCreate(item->anim, draw, i, V2D_ANIM_FLAG_VISIBLE);
    if (anim)
    {
      v2dMatrixReset(anim->matrix);
      v2dMatrixPosition(anim->matrix, 510, 70);
      v2dMatrixShear(anim->matrix, (v2d_real_t)i / -80.0, 0.0);
    }
  }

  /* Define a new shape object. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);

  if (draw)
  {

    shape = v2dDrawAsShape(draw);

    if (v2dShapeFillColor(shape, 255, 255, 0, 255))
    {

      shape->fill->flags = V2D_FILL_FLAG_RADIAL;

      grad = v2dFillColorCreate(shape->fill, 0.3);
      v2dFillColorSetRGBA(grad, 255, 0, 0, 255);

      grad = v2dFillColorCreate(shape->fill, 0.6);
      v2dFillColorSetRGBA(grad, 0, 127, 255, 255);

      grad = v2dFillColorCreate(shape->fill, 0.95);
      v2dFillColorSetRGBA(grad, 0, 0, 0, 255);

      grad = v2dFillColorCreate(shape->fill, 1.000);
      v2dFillColorSetRGBA(grad, 255, 0, 255, 0);

      v2dMatrixTranslate(shape->fill->matrix, 0.0, 0.0);

    }

    v2dPathLine(v2dShapeGetPathEnd(shape), 100, 0,   0);
    v2dPathLine(v2dShapeGetPathEnd(shape), 100, 100, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape), 0,   100, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape), 0,   0,   0);

  }

  v2dItemCreate(stage->item, draw, 0, 20, 200, V2D_FALSE);

  item = v2dItemCreate(stage->item, draw, 0, 410, 200, V2D_FALSE);

  for (i = 1; i < 80; i++)
  {

    anim = v2dAnimCreate(item->anim, draw, i, V2D_ANIM_FLAG_VISIBLE);

    if (anim)
    {

      v2dMatrixReset(anim->matrix);
      v2dMatrixPosition(anim->matrix, 410, 200);
      v2dMatrixScale(anim->matrix, 1.0 + ((v2d_real_t)i / 80.0), 1.0);

      if (i == 45)
      {
        v2dAnimSetSound(anim, V2D_DEFAULT, audio);
        v2dSoundSetLoops(anim->sound[V2D_DEFAULT], 2);
      }

    }

  }

  /* Define a new shape object. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);

  if (draw)
  {

    shape = v2dDrawAsShape(draw);

    if (v2dShapeFillImage(shape, image))
    {
      v2dMatrixTranslate(shape->fill->matrix, -50, -50);
      v2dMatrixScale(shape->fill->matrix, 0.4, 0.4);
    }

    if (v2dShapeLineColor(shape, 0, 100, 200, 200))
    {
      v2dLineSetWidth(shape->line, 10);
      v2dLineSetJoin(shape->line, V2D_LINE_JOIN_BEVEL);
    }

    v2dShapeGetPath(shape)->point[V2D_END_POINT][0] = -50;
    v2dShapeGetPath(shape)->point[V2D_END_POINT][1] = -50;

    v2dPathLine(v2dShapeGetPathEnd(shape),  50, -50, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape),  50,  50, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape), -50,  50, 0);
    v2dPathLine(v2dShapeGetPathEnd(shape), -50, -50, 0);

  }

  cat_box = draw;
  draw = v2dDrawCreateGroup(stage->draw);
  v2dItemCreate(v2dDrawAsGroup(draw), cat_box, 0, -50, 0, V2D_FALSE);
  v2dItemCreate(v2dDrawAsGroup(draw), red_box, 0, 0, -50, V2D_FALSE);

  /* Static cat. */

  v2dItemCreate(stage->item, draw, 0, 70, 380, V2D_FALSE);

  /* Spinning cat. */

  item = v2dItemCreate(stage->item, draw, 0, 510, 380, V2D_FALSE);
  v2dMatrixTranslate(v2dItemGetAnim(item)->matrix, -50, 0);

  for (i = 1; i < 90; i++)
  {
    anim = v2dAnimCreate(item->anim, draw, i, V2D_ANIM_FLAG_VISIBLE);
    if (anim)
    {
      v2dMatrixReset(anim->matrix);
      v2dMatrixPosition(anim->matrix, 510, 380);
      v2dMatrixRotate(anim->matrix, ((v2d_real_t)(i * 4) / 360.0) * (V2D_PI * 2.0));
      v2dMatrixTranslate(anim->matrix, -50, 0);
    }
  }

  /* Define a new text object. */

  draw = v2dDrawCreateText(stage->draw, "Shear");

  if (draw)
  {
    v2dTextSetColor(v2dDrawAsText(draw), 0, 50, 150, 255);
    v2dTextSetSize(v2dDrawAsText(draw), 24);
    v2dTextSetFont(v2dDrawAsText(draw), "_sans");
  }

  v2dItemCreate(stage->item, draw, 0, 140, 70, V2D_FALSE);

  /* Define a new text object. */

  draw = v2dDrawCreateText(stage->draw, "A shear applies an offset to the\nX position of a shape based on\nthe current Y coordinate, or vice\nversa. The example shown here\ndemonstrates a horizontal shear.");

  if (draw)
  {
    v2dTextSetSize(v2dDrawAsText(draw), 12);
    v2dTextSetFont(v2dDrawAsText(draw), "_sans");
  }

  v2dItemCreate(stage->item, draw, 0, 140, 100, V2D_FALSE);

  /* Define a new text object. */

  draw = v2dDrawCreateText(stage->draw, "Scale");

  if (draw)
  {
    v2dTextSetColor(v2dDrawAsText(draw), 0, 50, 150, 255);
    v2dTextSetSize(v2dDrawAsText(draw), 24);
    v2dTextSetFont(v2dDrawAsText(draw), "_sans");
  }

  v2dItemCreate(stage->item, draw, 0, 140, 200, V2D_FALSE);

  /* Define a new text object. */

  draw = v2dDrawCreateText(stage->draw, "A scaling operation stretches a\nshape along the X or Y axes. In\nthe example shown here, the\nshape is being scaled to 200%\nof the original horizontal size.");

  if (draw)
  {
    v2dTextSetSize(v2dDrawAsText(draw), 12);
    v2dTextSetFont(v2dDrawAsText(draw), "_sans");
  }

  v2dItemCreate(stage->item, draw, 0, 140, 230, V2D_FALSE);

  /* Define a new text object. */

  draw = v2dDrawCreateText(stage->draw, "Rotate");

  if (draw)
  {
    v2dTextSetColor(v2dDrawAsText(draw), 0, 50, 150, 255);
    v2dTextSetSize(v2dDrawAsText(draw), 24);
    v2dTextSetFont(v2dDrawAsText(draw), "_sans");
  }

  v2dItemCreate(stage->item, draw, 0, 140, 330, V2D_FALSE);

  /* Define a new text object. */

  draw = v2dDrawCreateText(stage->draw, "Rotation causes cats to spin.\n\nThat's pretty much it.");

  if (draw)
  {
    v2dTextSetSize(v2dDrawAsText(draw), 12);
    v2dTextSetFont(v2dDrawAsText(draw), "_sans");
  }

  v2dItemCreate(stage->item, draw, 0, 140, 360, V2D_FALSE);

  /* Define a new text object. */

  draw = v2dDrawCreateText(stage->draw, "Matrix Transformations");

  if (draw)
  {
    v2dTextSetColor(v2dDrawAsText(draw), 0, 50, 150, 255);
    v2dTextSetSize(v2dDrawAsText(draw), 36);
    v2dTextSetFont(v2dDrawAsText(draw), "_sans");
  }

  v2dItemCreate(stage->item, draw, 0, 10, 10, V2D_FALSE);

  /* Define another text object. */

  draw = v2dDrawCreateText(stage->draw, "https://commons.wikimedia.org/wiki/File:Floyd-Steinberg_algorithm-original.jpg");

  if (draw)
  {
    v2dTextSetWeight(v2dDrawAsText(draw), V2D_FONT_WEIGHT_BOLD);
    v2dTextSetSize(v2dDrawAsText(draw), 10);
    v2dTextSetFont(v2dDrawAsText(draw), "_typewriter");
  }

  item = v2dItemCreate(stage->item, draw, 0, 10, 464, V2D_FALSE);
  anim = v2dAnimCreate(item->anim, draw, 45, V2D_ANIM_FLAG_VISIBLE);

  if (anim)
  {

    v2dMatrixPosition(anim->matrix, 10, 462);

    /*v2dAnimSetScript(anim, v2dCompileASM, "togglequality\ngotoframe 9\nstop", 0);*/

  }

  return stage;

}

/**
@brief Create a test scene. This scene tests animation functionality.

@return The generated stage on success, or NULL on failure.
*/

static v2d_stage_s *test_animation_scene(void)
{

  v2d_stage_s *stage = NULL;
  v2d_shape_s *shape = NULL;
  v2d_draw_s  *draw = NULL;
  v2d_item_s  *item = NULL;
  v2d_anim_s  *anim = NULL;
  v2d_data_s  *image = NULL;
  v2d_data_s  *audio_a = NULL;
  v2d_data_s  *audio_b = NULL;
  int i = 0;

  /* Create a new stage object. */

  stage = v2dStageCreate(400, 400, 100);
  v2dStageSetColor(stage, 200, 200, 255, 255);

  /* Load media. */

  image = v2dDataCreate(stage->image, "media/Wikipe-tan_full_length.png", NULL);
  audio_a = v2dDataCreate(stage->audio, "media/oniichan/oniichan1.wav", NULL);
  audio_b = v2dDataCreate(stage->audio, "media/xylophone-1.mp3", NULL);

  /* Create a line along the bottom of the stage. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  shape = v2dDrawAsShape(draw);

  v2dShapeLineColor(shape, 0, 0, 0, 255);
  v2dPathLine(v2dShapeGetPathEnd(shape), 360, 0, 0);

  item = v2dItemCreate(stage->item, draw, 0, 20, 380, V2D_FALSE);

  /* Create a triangle shape. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  shape = v2dDrawAsShape(draw);

  shape->flags = V2D_SHAPE_FLAG_CLOSE;
  v2dShapeLineColor(shape, 0, 0, 0, 255);
  v2dShapeFillColor(shape, 255, 0, 0, 255);
  v2dPathLine(v2dShapeGetPathEnd(shape), -10, -20, 0);
  v2dPathLine(v2dShapeGetPathEnd(shape),  10, -20, 0);

  item = v2dItemCreate(stage->item, draw, 0, 20, 380, V2D_FALSE);

  for (i = 1; i <= 360; i++)
  {

    if (i == 120)
    {

      draw = v2dDrawCreateShape(stage->draw, 0, 0);
      shape = v2dDrawAsShape(draw);

      shape->flags = V2D_SHAPE_FLAG_CLOSE;
      v2dShapeLineColor(shape, 0, 0, 0, 255);
      v2dShapeFillColor(shape, 255, 255, 0, 255);
      v2dPathLine(v2dShapeGetPathEnd(shape), -10, -20, 0);
      v2dPathLine(v2dShapeGetPathEnd(shape),  10, -20, 0);

    }

    if (i == 240)
    {

      draw = v2dDrawCreateShape(stage->draw, 0, 0);
      shape = v2dDrawAsShape(draw);

      shape->flags = V2D_SHAPE_FLAG_CLOSE;
      v2dShapeLineColor(shape, 0, 0, 0, 255);
      v2dShapeFillColor(shape, 0, 255, 0, 255);
      v2dPathLine(v2dShapeGetPathEnd(shape), -10, -20, 0);
      v2dPathLine(v2dShapeGetPathEnd(shape),  10, -20, 0);

    }

    anim = v2dAnimCreate(item->anim, draw, i, V2D_ANIM_FLAG_VISIBLE);
    v2dMatrixTranslate(anim->matrix, 1, 0);
  }

  /* Define an image-filled shape. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  shape = v2dDrawAsShape(draw);

  shape->flags = V2D_SHAPE_FLAG_CLOSE;
  v2dShapeFillImage(shape, image);
  v2dPathLine(v2dShapeGetPathEnd(shape), 158, 0, V2D_PATH_FLAG_RELATIVE);
  v2dPathLine(v2dShapeGetPathEnd(shape), 0, 300, V2D_PATH_FLAG_RELATIVE);
  v2dPathLine(v2dShapeGetPathEnd(shape), -158, 0, V2D_PATH_FLAG_RELATIVE);

  /* Create one instance of the shape. */

  item = v2dItemCreate(stage->item, draw, 0, 20, 40, V2D_FALSE);
  anim = v2dAnimCreate(item->anim, draw, 240, 0);
  v2dAnimSetSound(anim, V2D_DEFAULT, audio_b);

  /* Create another instance of the shape. */

  item = v2dItemCreate(stage->item, draw, 120, 220, 40, V2D_FALSE);
  anim = v2dItemGetAnim(item);
  v2dAnimSetSound(anim, V2D_DEFAULT, audio_a);

  /* Add a link to the original image file. */

  draw = v2dDrawCreateText(stage->draw, "https://commons.wikimedia.org/wiki/File:Wikipe-tan_full_length.png");

  item = v2dItemCreate(stage->item, draw, 0, 30, 385, V2D_FALSE);
  v2dTextSetFont(v2dDrawAsText(draw), "monospace");
  v2dTextSetSize(v2dDrawAsText(draw), 8);

  /* Create a website link button. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  shape = v2dDrawAsShape(draw);

  shape->flags = V2D_SHAPE_FLAG_CLOSE;
  v2dShapeLineColor(shape, 0, 0, 0, 255);
  v2dShapeFillColor(shape, 0, 127, 255, 255);
  v2dPathLine(v2dShapeGetPathEnd(shape), 10, 0, V2D_PATH_FLAG_RELATIVE);
  v2dPathLine(v2dShapeGetPathEnd(shape), -5, 10, V2D_PATH_FLAG_RELATIVE);

  item = v2dItemCreate(stage->item, draw, 0, 10, 385, V2D_TRUE);
  v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_PRESS, v2dxCompileASM,
  "pushString \"https://commons.wikimedia.org/wiki/File:Wikipe-tan_full_length.png\" \"_blank\"\n"
  "openURL", 0);

  /* Create a play button. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  shape = v2dDrawAsShape(draw);

  shape->flags = V2D_SHAPE_FLAG_CLOSE;
  v2dShapeLineColor(shape, 0, 0, 0, 255);
  v2dShapeFillColor(shape, 0, 255, 0, 255);
  v2dPathLine(v2dShapeGetPathEnd(shape), 10, 5, V2D_PATH_FLAG_RELATIVE);
  v2dPathLine(v2dShapeGetPathEnd(shape), -10, 5, V2D_PATH_FLAG_RELATIVE);

  item = v2dItemCreate(stage->item, draw, 0, 10, 10, V2D_TRUE);
  v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_PRESS, v2dxCompileASM,
  "play\n"
  "pushstring \"playbackstate\" \"Playing\"\n"
  "setvariable", 0);

  /* Create a stop button. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  shape = v2dDrawAsShape(draw);

  shape->flags = V2D_SHAPE_FLAG_CLOSE;
  v2dShapeLineColor(shape, 0, 0, 0, 255);
  v2dShapeFillColor(shape, 255, 0, 0, 255);
  v2dPathLine(v2dShapeGetPathEnd(shape), 10, 0, V2D_PATH_FLAG_RELATIVE);
  v2dPathLine(v2dShapeGetPathEnd(shape), 0, 10, V2D_PATH_FLAG_RELATIVE);
  v2dPathLine(v2dShapeGetPathEnd(shape), -10, 0, V2D_PATH_FLAG_RELATIVE);

  item = v2dItemCreate(stage->item, draw, 0, 30, 10, V2D_TRUE);
  v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_PRESS, v2dxCompileASM,
  "stop\n"
  "pushstring \"playbackstate\" \"Paused\"\n"
  "setvariable", 0);


  /* Create a text object that shows the current state (stopped/playing). */

  draw = v2dDrawCreateText(stage->draw, "Playing");
  v2dTextSetName(v2dDrawAsText(draw), "playbackstate");
  v2dTextSetFont(v2dDrawAsText(draw), "monospace");
  v2dTextSetSize(v2dDrawAsText(draw), 10);

  item = v2dItemCreate(stage->item, draw, 0, 50, 10, V2D_FALSE);

  /* Return the result. */

  return stage;

}

/**
@brief Create a test scene. This scene tests scripting functionality.

@return The generated stage on success, or NULL on failure.
*/

static v2d_stage_s *test_button_scene(void)
{

  v2d_stage_s *stage = NULL;
  v2d_draw_s *draw = NULL;
  v2d_item_s *item = NULL;
  v2d_data_s *audio_a = NULL;
  v2d_data_s *audio_b = NULL;

  stage = v2dStageCreate(320, 240, 50);

  audio_a = v2dDataCreate(stage->audio, "media/oniichan/oniichan2.wav", NULL);
  audio_b = v2dDataCreate(stage->audio, "media/oniichan/oniichan3.wav", NULL);

  /* Create a "normal" object with a script attached. */

  draw = v2dDrawCreateGroup(stage->draw);
  v2dItemCreate(v2dDrawAsGroup(draw), v2dDrawCreateText(stage->draw, "Normal Object"), 0, 0, 0, V2D_FALSE);

  item = v2dItemCreate(stage->item, draw, 0, 20, 20, V2D_FALSE);
  v2dItemSetName(item, "normalobject");

  {

    char *code = NULL;
    FILE *src = fopen("media/script.txt", "r");

    if (src)
    {

      long len = 0;

      fseek(src, 0, SEEK_END);
      len = ftell(src);
      code = malloc(len + 1);

      if (code)
      {
        rewind(src);
        memset(code, 0, len + 1);
        fread(code, 1, len, src);
      }

      fclose(src);

    }

    if (!v2dAnimSetScript(v2dItemGetAnim(item), V2D_DEFAULT, v2dxCompileASM, code, 0))
    {
      fprintf(stderr, "Failed to compile script #1.\n");
    }

    if (code) { free(code); }

  }

  /* Create a "button" object with a script attached. */

  draw = v2dDrawCreateText(stage->draw, "Idle button");
  item = v2dItemCreate(stage->item, draw, 0, 20, 100, V2D_TRUE);
  v2dItemSetName(item, "buttonobject");

  draw = v2dDrawCreateText(stage->draw, "Over button");
  v2dAnimSetDraw(v2dItemGetAnim(item), V2D_STATE_OVER, draw);

  draw = v2dDrawCreateText(stage->draw, "Down button");
  v2dAnimSetDraw(v2dItemGetAnim(item), V2D_STATE_DOWN, draw);

  v2dAnimSetSound(v2dItemGetAnim(item), V2D_EVENT_PRESS,     audio_a);
  v2dAnimSetSound(v2dItemGetAnim(item), V2D_EVENT_RELEASE, audio_b);

  if (!v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_PRESS, v2dxCompileASM,
"pushstring \"You pressed the button!\"\n"
"print\n"
"pushnumber 0 0\n"
"pushstring \"buttonobject\"\n"
"startdrag"
  , 0)) { fprintf(stderr, "Failed to compile script #2.\n"); }

  if (!v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_RELEASE, v2dxCompileASM,
  "pushstring \"You released the button!\"\nprint\nenddrag"
  , 0)) { fprintf(stderr, "Failed to compile script #3.\n"); }

  if (!v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_MOUSEOVER, v2dxCompileASM,
  "pushstring \"You mouseover'd the button!\"\nprint"
  , 0)) { fprintf(stderr, "Failed to compile script #4.\n"); }

  if (!v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_MOUSEEXIT, v2dxCompileASM,
  "pushstring \"You mouseexit'd the button!\"\nprint"
  , 0)) { fprintf(stderr, "Failed to compile script #5.\n"); }

  /* Return the result. */

  v2dDrawSort(stage->draw, V2D_TRUE);
  return stage;

}

/**
@brief Create a test scene. This scene tests stage-merging functionality.

@return The generated stage on success, or NULL on failure.
*/

static v2d_stage_s *test_combining_scene(void)
{

  v2d_stage_s *stage_a = NULL;
  v2d_stage_s *stage_b = NULL;
  v2d_stage_s *stage_c = NULL;
  v2d_draw_s *draw = NULL;

  stage_a = v2dxLoadSVG("media/polyline01.svg", 20);
  stage_b = v2dxLoadSVG("media/Svg_example1.svg", 20);
  stage_c = v2dxLoadSVG("media/Manhwa_balloon.svg", 20);

  if (!stage_a || !stage_b || !stage_c)
  {
    v2dStageDelete(stage_a);
    v2dStageDelete(stage_b);
    v2dStageDelete(stage_c);
    return NULL;
  }

  draw = v2dStageMerge(stage_a, stage_b);
  if (!draw) { v2dStageDelete(stage_b); }
  v2dItemCreate(stage_a->item, draw, 0, 0, 0, V2D_FALSE);

  draw = v2dStageMerge(stage_a, stage_c);
  if (!draw) { v2dStageDelete(stage_c); }
  v2dItemCreate(stage_a->item, draw, 0, 0, 0, V2D_FALSE);

  return stage_a;

}

/**
@brief about

writeme

@param stage about
@param draw about
@param name about
@param x about
@param y about
*/

static void create_piece(v2d_stage_s *stage, v2d_draw_s *draw, const char *name, int x, int y, v2d_data_s *sound)
{

  v2d_item_s *item = v2dItemCreate(stage->item, draw, 0, x*48, y*48, V2D_TRUE);

  v2dAnimSetSound(v2dItemGetAnim(item), V2D_EVENT_PRESS, sound);
  v2dAnimSetScript(v2dItemGetAnim(item), V2D_EVENT_PRESS, v2dxCompileASM, "pushstring \"hello\"\nprint", 0);

}

/**
@brief Create a simple chess game.

This test various aspects of the interactivity functionality.

@return The generated stage on success, or NULL on failure.
*/

static v2d_stage_s *test_chess_scene(void)
{

  int x = 0;

  enum {
    BLACK_PAWN,
    WHITE_PAWN,
    BLACK_ROOK,
    WHITE_ROOK,
    BLACK_KNIGHT,
    WHITE_KNIGHT,
    BLACK_BISHOP,
    WHITE_BISHOP,
    BLACK_QUEEN,
    WHITE_QUEEN,
    BLACK_KING,
    WHITE_KING,
    NUM_PIECES
  } piece;

  const char *filename[NUM_PIECES] = {
    "chess/images/Chess_pdt45.svg",
    "chess/images/Chess_plt45.svg",
    "chess/images/Chess_rdt45.svg",
    "chess/images/Chess_rlt45.svg",
    "chess/images/Chess_ndt45.svg",
    "chess/images/Chess_nlt45.svg",
    "chess/images/Chess_bdt45.svg",
    "chess/images/Chess_blt45.svg",
    "chess/images/Chess_qdt45.svg",
    "chess/images/Chess_qlt45.svg",
    "chess/images/Chess_kdt45.svg",
    "chess/images/Chess_klt45.svg"
  };

  v2d_stage_s *stage = v2dStageCreate(48*8, 48*8, 1);
  v2d_data_s  *click = v2dDataCreate(stage->audio, "chess/sounds/zipclick.wav", NULL);
  v2d_draw_s  *black = v2dDrawCreateShape(stage->draw, 0, 0);
  v2d_draw_s  *white = v2dDrawCreateShape(stage->draw, 0, 0);

  v2dShapeFillColor(v2dDrawAsShape(black), 64, 64, 64, 255);
  v2dPathLine(v2dShapeGetPathEnd(v2dDrawAsShape(black)), 48, 0,  0);
  v2dPathLine(v2dShapeGetPathEnd(v2dDrawAsShape(black)), 48, 48, 0);
  v2dPathLine(v2dShapeGetPathEnd(v2dDrawAsShape(black)), 0,  48, 0);

  v2dShapeFillColor(v2dDrawAsShape(white), 192, 192, 192, 255);
  v2dPathLine(v2dShapeGetPathEnd(v2dDrawAsShape(white)), 48, 0,  0);
  v2dPathLine(v2dShapeGetPathEnd(v2dDrawAsShape(white)), 48, 48, 0);
  v2dPathLine(v2dShapeGetPathEnd(v2dDrawAsShape(white)), 0,  48, 0);

  for (x = 0; x < 8*8; x++)
  {
    v2dItemCreate(stage->item, (((x/8)+x)%2) ? black : white, 0, (x%8)*48, (x/8)*48, V2D_FALSE);
  }

  for (piece = 0; piece < NUM_PIECES; piece++)
  {

    /* Load the SVG image used to represent this piece. */

    v2d_draw_s *draw = v2dStageMerge(stage, v2dxLoadSVG(filename[piece], 1));

    /* Create instances of the piece on the board. */

    switch (piece)
    {

      case BLACK_PAWN:
        for (x = 0; x < 8; x++) { create_piece(stage, draw, "BPAWN", x, 1, click); }
      break;
      case WHITE_PAWN:
        for (x = 0; x < 8; x++) { create_piece(stage, draw, "WPAWN", x, 6, click); }
      break;

      case BLACK_ROOK:
        create_piece(stage, draw, "BROOK", 0, 0, click);
        create_piece(stage, draw, "BROOK", 7, 0, click);
      break;

      case WHITE_ROOK:
        create_piece(stage, draw, "WROOK", 0, 7, click);
        create_piece(stage, draw, "WROOK", 7, 7, click);
      break;

      case BLACK_KNIGHT:
        create_piece(stage, draw, "BKNIGHT", 1, 0, click);
        create_piece(stage, draw, "BKNIGHT", 6, 0, click);
      break;

      case WHITE_KNIGHT:
        create_piece(stage, draw, "BKNIGHT", 1, 7, click);
        create_piece(stage, draw, "BKNIGHT", 6, 7, click);
      break;

      case BLACK_BISHOP:
        create_piece(stage, draw, "BBISHOP", 2, 0, click);
        create_piece(stage, draw, "BBISHOP", 5, 0, click);
      break;

      case WHITE_BISHOP:
        create_piece(stage, draw, "BBISHOP", 2, 7, click);
        create_piece(stage, draw, "BBISHOP", 5, 7, click);
      break;

      case BLACK_QUEEN:
        create_piece(stage, draw, "BQUEEN", 3, 0, click);
      break;

      case WHITE_QUEEN:
        create_piece(stage, draw, "WQUEEN", 3, 7, click);
      break;

      case BLACK_KING:
        create_piece(stage, draw, "BKING", 4, 0, click);
      break;

      case WHITE_KING:
        create_piece(stage, draw, "WKING", 4, 7, click);
      break;

      case NUM_PIECES: default: break;

    }

  }

  /* Return the result. */

  v2dDrawSort(stage->draw, V2D_TRUE);
  return stage;

}

/**
@brief This is where the program begins.

@param argc Length of the argv array.
@param argv Program arguments.
@return Zero on success or non-zero on failure.
*/

int main(int argc, char *argv[])
{

  v2d_stage_s *stage;

  if (argc || argv) { /* Silence warnings. */ }
#if 0
  stage = test_matrix_scene();
  v2dxSaveSWF(stage, "matrices.swf", 10, V2DX_SAVE_FLAG_SWF_AVM2);
  v2dxSaveSVG(stage, "matrices.svg", V2DX_SAVE_FLAG_SVG_BACKDROP | V2DX_SAVE_FLAG_SVG_EMBED);
  v2dStageDelete(stage);

  stage = test_animation_scene();
  v2dxSaveSWF(stage, "animated.swf", 10, 0);
  v2dxSaveSVG(stage, "animated.svg", V2DX_SAVE_FLAG_SVG_BACKDROP);
  v2dStageDelete(stage);

  stage = test_button_scene();
  v2dxSaveSWF(stage, "button.swf", 7, 0);
  v2dxSaveSVG(stage, "button.svg", 0);
  v2dStageDelete(stage);

  stage = test_combining_scene();
  v2dxSaveSWF(stage, "combined.swf", 10, V2DX_SAVE_FLAG_SWF_AVM2);
  v2dxSaveSVG(stage, "combined.svg", V2DX_SAVE_FLAG_ZLIB);
  v2dStageDelete(stage);

  stage = v2dxLoadSVG("media/arcs01.svg", 20);
  v2dxSaveSWF(stage, "imported.swf", 7, 0);
  v2dxSaveSVG(stage, "imported.svg", 0);
  v2dStageDelete(stage);
#endif
  stage = test_chess_scene();
  v2dxSaveSWF(stage, "chess.swf", 4, 0);
  v2dxSaveSVG(stage, "chess.svg", 0);
  v2dStageDelete(stage);

  return 0;

}
