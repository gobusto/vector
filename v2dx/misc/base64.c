/**
@file base64.c

@brief Provides base64 encoding functionality.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "base64.h"

/*
[PUBLIC] Get the contents of a file as a string of base64-format text.
*/

size_t encodeBase64(FILE *src, size_t buffer_length, char *buffer)
{

  long   initial_offs = 0;
  size_t bytes_needed = 0;
  size_t buffer_index = 0;
  long data, bits, byte, code, i;

  /* Store the current seek position, so that the file can be reset later... */

  if (!src) { return 0; }
  initial_offs = ftell(src);

  /* Keep going until the whole input file has been written as base64 data. */

  for (buffer_index = 0, bytes_needed = 0; 1; bytes_needed += 4)
  {

    /* Read in three bytes and store then as a single 24-bit value. */

    bits = 24;
    byte = fgetc(src); if (byte != EOF) { data  = byte << 16; } else { break;     }
    byte = fgetc(src); if (byte != EOF) { data += byte << 8;  } else { bits = 8;  }
    byte = fgetc(src); if (byte != EOF) { data += byte << 0;  } else { bits = 16; }

    /* Write 6 bits at a time, mapping the value to an ASCII character code. */

    for (i = 18; i >= 0 && buffer; i -= 6, buffer_index++, bits -= 6)
    {

      if (buffer_length > 0 && buffer_index >= buffer_length) { break; }

      code = (data >> i) & 63;

      if      (bits <   0) { buffer[buffer_index] = '=';             }
      else if (code == 63) { buffer[buffer_index] = '/';             }
      else if (code == 62) { buffer[buffer_index] = '+';             }
      else if (code >= 52) { buffer[buffer_index] = '0' + code - 52; }
      else if (code >= 26) { buffer[buffer_index] = 'a' + code - 26; }
      else                 { buffer[buffer_index] = 'A' + code;      }

    }

  }

  /* Reposition the file pointer to the original offset and report the size. */

  fseek(src, initial_offs, SEEK_SET);
  return bytes_needed;

}
