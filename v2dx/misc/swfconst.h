/**
@file swfconst.h

@brief SWF Constants: Taken from various information resources on the Internet.

Copyright (C) 2010-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __SWFCONST_H__
#define __SWFCONST_H__

#ifdef __cplusplus
extern "C" {
#endif

/* From www.adobe.com/devnet/articles/flashplayer-air-feature-list.html */

#define SWF_VERSION_MIN 1  /**< @brief Lowest known SWF version. */
#define SWF_VERSION_MAX 25 /**< @brief Latest known SWF version. */

/* From http://kb2.adobe.com/cps/144/tn_14437.html */

#define SWF_MAXWIDTH  2880  /**< @brief Maximum canvas width.  */
#define SWF_MAXHEIGHT 2880  /**< @brief Maximum canvas height. */
#define SWF_MAXFRAMES 16000 /**< @brief Maximum movie frames.  */
#define SWF_MAXLAYERS 16000 /**< @brief Maximum movie layers.  */
#define SWF_MAXSYMBOL 16000 /**< @brief Maximum movie symbols. */
#define SWF_MAXMOVIES 16000 /**< @brief Maximum Flash movies.  */

#define SWF_MAXASFILE (32*1024) /**< @brief File size (KB) for a single AS script.  */
#define SWF_MAXASVMEM 12        /**< @brief Minimum memory (B) used for AS vars.    */
#define SWF_LOOPTIME  15        /**< @brief Maximum time (Secs) a loop can run for. */
#define SWF_COMPONENT 125       /**< @brief Maximum component guideline (not rule). */

/* From http://www.adobe.com/devnet/freehand/articles/logo_browser09.html */

#define SWF_MINLINE 0.25  /**< @brief Minimum line width. */

/* help.adobe.com/en_US/AS2LCR/Flash_10.0/help.html?content=00001316.html */

#define SWF_MAXLINE 255.0 /**< @brief Maximum line width. */

/* From various unofficial sources. */

#define SWF_MAXFPS      120 /**< @brief Maximum playback rate.  */
#define SWF_MAXFONTSIZE 127 /**< @brief Maximum text font size. */

/* ========================================================================= *
 *                                                                           *
 * Everything below is based on the official Adobe SWF format specification: *
 *                                                                           *
 * https://www.adobe.com/devnet/swf.html                                     *
 *                                                                           *
 * ========================================================================= */

#define SWF_TWIP_SIZE 20  /**< @brief Size of one "twip" for SWF files. */

/* FileAttributes flags. As of SWF v19, only the first 8 bits are used. */

#define SWF_FLAG_USENETWORK     0x01 /**< @brief Allow network if run locally. */
#define SWF_FLAG_RESERVED1      0x02 /**< @brief Not used.                     */
#define SWF_FLAG_NOXDOMAINCACHE 0x04 /**< @brief Disallow cross-domain cache.  */
#define SWF_FLAG_ACTIONSCRIPT3  0x08 /**< @brief Uses ActionScript3 (AVM2/ABC) */
#define SWF_FLAG_HASMETADATA    0x10 /**< @brief This file has a metadata tag. */
#define SWF_FLAG_USEGPU         0x20 /**< @brief Enable GPU acceleration.      */
#define SWF_FLAG_USEDIRECTBLIT  0x40 /**< @brief Enable hardware-acceleration. */
#define SWF_FLAG_RESERVED7      0x80 /**< @brief Not used.                     */

/* DefineShape4 flags. */

#define SWF_DEFINESHAPE4_SCALINGSTROKE    0x01  /**< @brief Scaling lines.    */
#define SWF_DEFINESHAPE4_NONSCALINGSTROKE 0x02  /**< @brief Nonscaling lines. */
#define SWF_DEFINESHAPE4_FILLWINDINGRULE  0x04  /**< @brief Use winding rule. */

/* DefineEditText flags. This assumes that they are stored as little-endian.*/

#define SWF_EDITTEXT_HASFONT      0x0001  /**< @brief about */
#define SWF_EDITTEXT_HASMAXLENGTH 0x0002  /**< @brief about */
#define SWF_EDITTEXT_HASTEXTCOLOR 0x0004  /**< @brief about */
#define SWF_EDITTEXT_READONLY     0x0008  /**< @brief about */
#define SWF_EDITTEXT_PASSWORD     0x0010  /**< @brief about */
#define SWF_EDITTEXT_MULTILINE    0x0020  /**< @brief about */
#define SWF_EDITTEXT_WORDWRAP     0x0040  /**< @brief about */
#define SWF_EDITTEXT_HASTEXT      0x0080  /**< @brief about */
#define SWF_EDITTEXT_USEOUTLINES  0x0100  /**< @brief about */
#define SWF_EDITTEXT_HTML         0x0200  /**< @brief about */
#define SWF_EDITTEXT_WASSTATIC    0x0400  /**< @brief about */
#define SWF_EDITTEXT_BORDER       0x0800  /**< @brief about */
#define SWF_EDITTEXT_NOSELECT     0x1000  /**< @brief about */
#define SWF_EDITTEXT_HASLAYOUT    0x2000  /**< @brief about */
#define SWF_EDITTEXT_AUTOSIZE     0x4000  /**< @brief about */
#define SWF_EDITTEXT_HASFONTCLASS 0x8000  /**< @brief about */

/* DefineFont flags. */

#define SWF_DEFINEFONT_BOLD    0x02 /**< @brief Use bold text.   */
#define SWF_DEFINEFONT_ITALIC  0x04 /**< @brief Use italic text. */

/* DefineFont2 flags. */

#define SWF_DEFINEFONT2_BOLD   0x01 /**< @brief Use bold text.   */
#define SWF_DEFINEFONT2_ITALIC 0x02 /**< @brief Use italic text. */

/* DefineFont3 flags. */

#define SWF_DEFINEFONT3_BOLD   0x01 /**< @brief Use bold text.   */
#define SWF_DEFINEFONT3_ITALIC 0x02 /**< @brief Use italic text. */

/* DefineFont4 flags. */

#define SWF_DEFINEFONT4_BOLD   0x01 /**< @brief Use bold text.   */
#define SWF_DEFINEFONT4_ITALIC 0x02 /**< @brief Use italic text. */

/* StartSound flags. */

#define SWF_DEFINESOUND_HASINPOINT  0x01  /**< @brief about */
#define SWF_DEFINESOUND_HASOUTPOINT 0x02  /**< @brief about */
#define SWF_DEFINESOUND_HASLOOPS    0x04  /**< @brief about */
#define SWF_DEFINESOUND_HASENVELOPE 0x08  /**< @brief about */
#define SWF_DEFINESOUND_NOMULTIPLE  0x10  /**< @brief about */
#define SWF_DEFINESOUND_STOP        0x20  /**< @brief about */
#define SWF_DEFINESOUND_UNUSED6     0x40  /**< @brief about */
#define SWF_DEFINESOUND_UNUSED7     0x80  /**< @brief about */

/* PlaceObject2 flags. Also used as the first 8 bits for PlaceObject3 flags. */

#define SWF_PLACEOBJECT2_HASCLIPACTIONS     0x80  /**< @brief HAS CLIP ACTIONS */
#define SWF_PLACEOBJECT2_HASCLIPDEPTH       0x40  /**< @brief HAS CLIP DEPTH */
#define SWF_PLACEOBJECT2_HASNAME            0x20  /**< @brief HAS NAME */
#define SWF_PLACEOBJECT2_HASRATIO           0x10  /**< @brief HAS RATIO */
#define SWF_PLACEOBJECT2_HASCOLORTRANSFORM  0x08  /**< @brief HAS COLOR TRANSFORM */
#define SWF_PLACEOBJECT2_HASMATRIX          0x04  /**< @brief HAS MATRIX */
#define SWF_PLACEOBJECT2_HASCHARACTER       0x02  /**< @brief PLACES A CHARACTER */
#define SWF_PLACEOBJECT2_MOVE               0x01  /**< @brief DEFINES A CHARACTER TO BE MOVED */

/* Push opcode data types. */

#define SWF_PUSH_STRING 0 /**< @brief Null-terminated ASCII text string. */
#define SWF_PUSH_FLOAT  1 /**< @brief IEEE 32-bit floating point number. */

/* ActionGetProperty/ActionSetProperty values. */

#define SWF_PROPERTY_X            0   /**< @brief about */
#define SWF_PROPERTY_Y            1   /**< @brief about */
#define SWF_PROPERTY_XSCALE       2   /**< @brief about */
#define SWF_PROPERTY_YSCALE       3   /**< @brief about */
#define SWF_PROPERTY_CURRENTFRAME 4   /**< @brief about */
#define SWF_PROPERTY_TOTALFRAMES  5   /**< @brief about */
#define SWF_PROPERTY_ALPHA        6   /**< @brief about */
#define SWF_PROPERTY_VISIBLE      7   /**< @brief about */
#define SWF_PROPERTY_WIDTH        8   /**< @brief about */
#define SWF_PROPERTY_HEIGHT       9   /**< @brief about */
#define SWF_PROPERTY_ROTATION     10  /**< @brief about */
#define SWF_PROPERTY_TARGET       11  /**< @brief about */
#define SWF_PROPERTY_FRAMESLOADED 12  /**< @brief about */
#define SWF_PROPERTY_NAME         13  /**< @brief about */
#define SWF_PROPERTY_DROPTARGET   14  /**< @brief about */
#define SWF_PROPERTY_URL          15  /**< @brief about */
#define SWF_PROPERTY_HIGHQUALITY  16  /**< @brief about */
#define SWF_PROPERTY_FOCUSRECT    17  /**< @brief about */
#define SWF_PROPERTY_SOUNDBUFTIME 18  /**< @brief about */
#define SWF_PROPERTY_QUALITY      19  /**< @brief about */
#define SWF_PROPERTY_XMOUSE       20  /**< @brief about */
#define SWF_PROPERTY_YMOUSE       21  /**< @brief about */

/**
@brief about

writeme
*/

typedef enum
{

  SWF_FREQUENCY_5512  = 0,  /**< @brief about */
  SWF_FREQUENCY_11025 = 1,  /**< @brief about */
  SWF_FREQUENCY_22050 = 2,  /**< @brief about */
  SWF_FREQUENCY_44100 = 3,  /**< @brief about */
  SWF_FREQUENCY_OTHER = 4   /**< @brief Not in the SWF specification. */

} swf_frequency_t;

/**
@brief about

writeme
*/

typedef enum
{

  SWF_CAP_ROUND  = 0, /**< @brief about */
  SWF_CAP_NOCAP  = 1, /**< @brief about */
  SWF_CAP_SQUARE = 2  /**< @brief about */

} swf_cap_t;

/**
@brief about

writeme
*/

typedef enum
{

  SWF_JOIN_ROUND = 0, /**< @brief about */
  SWF_JOIN_BEVEL = 1, /**< @brief about */
  SWF_JOIN_MITER = 2  /**< @brief about */

} swf_join_t;

/**
@brief Enumerates all possible types of fill.

This list currently contains everything up to and including SWF v10.
*/

typedef enum
{

  SWF_FILL_SOLID                = 0x00, /**< @brief Solid colour.           */
  SWF_FILL_GRADIENT_LINEAR      = 0x10, /**< @brief Linear gradient.        */
  SWF_FILL_GRADIENT_RADIAL      = 0x12, /**< @brief Radial gradient.        */
  SWF_FILL_GRADIENT_FOCAL       = 0x13, /**< @brief Focal radial gradient.  */
  SWF_FILL_IMAGE_SMOOTH_REPEAT  = 0x40, /**< @brief Smooth Repeating image. */
  SWF_FILL_IMAGE_SMOOTH_CLIP    = 0x41, /**< @brief Smooth Clipped image.   */
  SWF_FILL_IMAGE_NEAREST_REPEAT = 0x42, /**< @brief Rough repeated image.   */
  SWF_FILL_IMAGE_NEAREST_CLIP   = 0x43  /**< @brief Rough clipped image.    */

} swf_fill_t;

/**
@brief Enumerates all possible types of audio codec.

This list currently contains everything up to and including SWF v10.
*/

typedef enum
{

  SWF_SNDCODEC_NATIVEENDIAN    = 0, /**< @brief Uncompressed, native-endian */
  SWF_SNDCODEC_ADPCM           = 1, /**< @brief ADPCM                       */
  SWF_SNDCODEC_MP3             = 2, /**< @brief MP3                         */
  SWF_SNDCODEC_LITTLEENDIAN    = 3, /**< @brief Uncompressed, little-endian */
  SWF_SNDCODEC_NELLYMOSER16KHZ = 4, /**< @brief Nellymoser 16 kHz           */
  SWF_SNDCODEC_NELLYMOSER8KHZ  = 5, /**< @brief Nellymoser 8 kHz            */
  SWF_SNDCODEC_NELLYMOSER      = 6, /**< @brief Nellymoser                  */
  SWF_SNDCODEC_SPEEX           = 11 /**< @brief Speex                       */

} swf_codec_audio_t;

/**
@brief Enumerates all supported SWF tag types.

This list currently contains everything up to and including SWF v10.
*/

typedef enum
{

  /* Display list tags. */

  SWF_TAG_PLACEOBJECT                  = 4,   /**< @brief about */
  SWF_TAG_PLACEOBJECT2                 = 26,  /**< @brief about */
  SWF_TAG_PLACEOBJECT3                 = 70,  /**< @brief about */
  SWF_TAG_REMOVEOBJECT                 = 5,   /**< @brief about */
  SWF_TAG_REMOVEOBJECT2                = 28,  /**< @brief about */
  SWF_TAG_SHOWFRAME                    = 1,   /**< @brief Draw everything. */

  /* Control tags. */

  SWF_TAG_SETBACKGROUNDCOLOR           = 9,   /**< @brief Set backdrop color. */
  SWF_TAG_FRAMELABEL                   = 43,  /**< @brief Also Named Anchors. */
  SWF_TAG_PROTECT                      = 24,  /**< @brief about */
  SWF_TAG_END                          = 0,   /**< @brief End of file marker. */
  SWF_TAG_EXPORTASSETS                 = 56,  /**< @brief about */
  SWF_TAG_IMPORTASSETS                 = 57,  /**< @brief about */
  SWF_TAG_ENABLEDEBUGGER               = 58,  /**< @brief about */
  SWF_TAG_ENABLEDEBUGGER2              = 64,  /**< @brief about */
  SWF_TAG_SCRIPTLIMITS                 = 65,  /**< @brief about */
  SWF_TAG_SETTABINDEX                  = 66,  /**< @brief about */
  SWF_TAG_FILEATTRIBUTES               = 69,  /**< @brief about */
  SWF_TAG_IMPORTASSETS2                = 71,  /**< @brief about */
  SWF_TAG_SYMBOLCLASS                  = 76,  /**< @brief about */
  SWF_TAG_METADATA                     = 77,  /**< @brief about */
  SWF_TAG_DEFINESCALINGGRID            = 78,  /**< @brief about */
  SWF_TAG_DEFINESCENEANDFRAMELABELDATA = 86,  /**< @brief about */
  SWF_TAG_ENABLETELEMETRY              = 93,  /**< @brief about */

  /* Action tags. */

  SWF_TAG_DOACTION                     = 12,  /**< @brief about */
  SWF_TAG_DOINITACTION                 = 59,  /**< @brief about */
  SWF_TAG_DOABC                        = 82,  /**< @brief about */

  /* Shape tags. */

  SWF_TAG_DEFINESHAPE                  = 2,   /**< @brief about */
  SWF_TAG_DEFINESHAPE2                 = 22,  /**< @brief about */
  SWF_TAG_DEFINESHAPE3                 = 32,  /**< @brief about */
  SWF_TAG_DEFINESHAPE4                 = 83,  /**< @brief about */

  /* Bitmap tags. */

  SWF_TAG_DEFINEBITS                   = 6,   /**< @brief about */
  SWF_TAG_JPEGTABLES                   = 8,   /**< @brief about */
  SWF_TAG_DEFINEBITSJPEG2              = 21,  /**< @brief about */
  SWF_TAG_DEFINEBITSJPEG3              = 35,  /**< @brief about */
  SWF_TAG_DEFINEBITSLOSSLESS           = 20,  /**< @brief about */
  SWF_TAG_DEFINEBITSLOSSLESS2          = 36,  /**< @brief about */
  SWF_TAG_DEFINEBITSJPEG4              = 90,  /**< @brief about */

  /* Shape morphing tags. */

  SWF_TAG_DEFINEMORPHSHAPE             = 46,  /**< @brief about */
  SWF_TAG_DEFINEMORPHSHAPE2            = 84,  /**< @brief about */

  /* Font tags. */

  SWF_TAG_DEFINEFONT                   = 10,  /**< @brief about */
  SWF_TAG_DEFINEFONTINFO               = 13,  /**< @brief about */
  SWF_TAG_DEFINEFONTINFO2              = 62,  /**< @brief about */
  SWF_TAG_DEFINEFONT2                  = 48,  /**< @brief about */
  SWF_TAG_DEFINEFONT3                  = 75,  /**< @brief about */
  SWF_TAG_DEFINEFONTALIGNZONES         = 73,  /**< @brief about */
  SWF_TAG_DEFINEFONTNAME               = 88,  /**< @brief about */

  /* Static text tags. */

  SWF_TAG_DEFINETEXT                   = 11,  /**< @brief about */
  SWF_TAG_DEFINETEXT2                  = 33,  /**< @brief about */

  /* Dynamic text tags. */

  SWF_TAG_DEFINEEDITTEXT               = 37,  /**< @brief about */
  SWF_TAG_CSMTEXTSETTINGS              = 74,  /**< @brief about */
  SWF_TAG_DEFINEFONT4                  = 91,  /**< @brief about */

  /* Event sound tags. */

  SWF_TAG_DEFINESOUND                  = 14,  /**< @brief about */
  SWF_TAG_STARTSOUND                   = 15,  /**< @brief about */
  SWF_TAG_STARTSOUND2                  = 89,  /**< @brief about */

  /* Streaming sound tags. */

  SWF_TAG_SOUNDSTREAMHEAD              = 18,  /**< @brief about */
  SWF_TAG_SOUNDSTREAMHEAD2             = 45,  /**< @brief about */
  SWF_TAG_SOUNDSTREAMBLOCK             = 19,  /**< @brief about */

  /* Button tags. */

  SWF_TAG_DEFINEBUTTON                 = 7,   /**< @brief about */
  SWF_TAG_DEFINEBUTTON2                = 34,  /**< @brief about */
  SWF_TAG_DEFINEBUTTONCXFORM           = 23,  /**< @brief about */
  SWF_TAG_DEFINEBUTTONSOUND            = 17,  /**< @brief about */

  /* Sprite (or "Movie Clip") tags. */

  SWF_TAG_DEFINESPRITE                 = 39,  /**< @brief about */

  /* Video tags. */

  SWF_TAG_DEFINEVIDEOSTREAM            = 60,  /**< @brief about */
  SWF_TAG_VIDEOFRAME                   = 61,  /**< @brief about */

  /* Binary data tags. */

  SWF_TAG_DEFINEBINARYDATA             = 87   /**< @brief about */

} swf_tag_t;

/**
@brief about

writeme
*/

typedef enum
{

  /* SWF v3+ */

  SWF_ACTION_PLAY           = 0x06, /**< @brief Start playing at the current frame. */
  SWF_ACTION_STOP           = 0x07, /**< @brief Stop playing at the current frame. */
  SWF_ACTION_NEXTFRAME      = 0x04, /**< @brief Go to the next frame. */
  SWF_ACTION_PREVIOUSFRAME  = 0x05, /**< @brief Go to the previous frame. */
  SWF_ACTION_GOTOFRAME      = 0x81, /**< @brief Go to the specified frame. */
  SWF_ACTION_GOTOLABEL      = 0x8C, /**< @brief Go to the frame with the specified label. */
  SWF_ACTION_WAITFORFRAME   = 0x8A, /**< @brief Wait for the specified frame. */
  SWF_ACTION_GETURL         = 0x83, /**< @brief Get the specified URL. */
  SWF_ACTION_STOPSOUNDS     = 0x09, /**< @brief Stop all sounds playing. */
  SWF_ACTION_TOGGLEQUALITY  = 0x08, /**< @brief Toggle the display between high and low quality. */
  SWF_ACTION_SETTARGET      = 0x8B, /**< @brief Change the context of subsequent actions to a named object. */

  /* SWF v4+ */

  SWF_ACTION_ADD            = 0x0A, /**< @brief writeme */
  SWF_ACTION_SUBTRACT       = 0x0B, /**< @brief writeme */
  SWF_ACTION_MULTIPLY       = 0x0C, /**< @brief writeme */
  SWF_ACTION_DIVIDE         = 0x0D, /**< @brief writeme */

  SWF_ACTION_EQUALS         = 0X0E, /**< @brief writeme */
  SWF_ACTION_LESS           = 0X0F, /**< @brief writeme */

  SWF_ACTION_AND            = 0x10, /**< @brief writeme */
  SWF_ACTION_OR             = 0x11, /**< @brief writeme */
  SWF_ACTION_NOT            = 0x12, /**< @brief writeme */

  SWF_ACTION_STRINGADD      = 0x21, /**< @brief writeme */
  SWF_ACTION_STRINGEQUALS   = 0x13, /**< @brief writeme */
/*
  SWF_ACTION_STRINGEXTRACT   = ,
  SWF_ACTION_STRINGLENGTH    = ,
  SWF_ACTION_MBSTRINGEXTRACT = ,
  SWF_ACTION_MBSTRINGLENGTH  = ,
*/
  SWF_ACTION_STRINGLESS     = 0x29, /**< @brief writeme */

  SWF_ACTION_POP            = 0x17, /**< @brief writeme */
  SWF_ACTION_PUSH           = 0x96, /**< @brief writeme */

  SWF_ACTION_ASCIITOCHAR    = 0x33, /**< @brief writeme */
  SWF_ACTION_CHARTOASCII    = 0x32, /**< @brief writeme */
  SWF_ACTION_TOINTEGER      = 0x18, /**< @brief writeme */
  SWF_ACTION_MBASCIITOCHAR  = 0x37, /**< @brief writeme */
  SWF_ACTION_MBCHARTOASCII  = 0x36, /**< @brief writeme */

  SWF_ACTION_CALL           = 0x9E, /**< @brief writeme */
  SWF_ACTION_IF             = 0x9D, /**< @brief writeme */
  SWF_ACTION_JUMP           = 0x99, /**< @brief writeme */

  SWF_ACTION_GETVARIABLE    = 0x1C, /**< @brief writeme */
  SWF_ACTION_SETVARIABLE    = 0x1D, /**< @brief writeme */

  SWF_ACTION_GETURL2        = 0x9A, /**< @brief writeme */
  SWF_ACTION_GETPROPERTY    = 0x22, /**< @brief writeme */
  SWF_ACTION_GOTOFRAME2     = 0x9F, /**< @brief writeme */
  SWF_ACTION_REMOVESPRITE   = 0x25, /**< @brief writeme */
  SWF_ACTION_SETPROPERTY    = 0x23, /**< @brief writeme */
  SWF_ACTION_SETTARGET2     = 0x20, /**< @brief writeme */
  SWF_ACTION_STARTDRAG      = 0x27, /**< @brief writeme */
  SWF_ACTION_WAITFORFRAME2  = 0x8D, /**< @brief writeme */
  SWF_ACTION_CLONESPRITE    = 0x24, /**< @brief writeme */
  SWF_ACTION_ENDDRAG        = 0x28, /**< @brief writeme */

  SWF_ACTION_GETTIME        = 0x34, /**< @brief writeme */
  SWF_ACTION_RANDOMNUMBER   = 0x30, /**< @brief writeme */
  SWF_ACTION_TRACE          = 0x26  /**< @brief writeme */

} swf_action_t;

#ifdef __cplusplus
}
#endif

#endif /* __SWFCONST_H__ */
