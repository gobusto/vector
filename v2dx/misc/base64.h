/**
@file base64.h

@brief Provides base64 encoding functionality.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __V2D_BASE64_H__
#define __V2D_BASE64_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

/**
@brief Get the contents of a file as a string of base64-format text.

This function will usually be called twice: Once to get the memory needed, and
then again to fill in the (now-allocated) buffer. However, as the file pointer
could potentially change between the two calls, it is recommended that the size
returned by the second call is checked against the previously returned value.

Data is read in from the current seek position until an EOF is reached. Thus,
the calling function can skip past any unwanted file header bytes before this
function is called.

NOTE: The input file pointer is repositioned to the original offset once this
function is finished using it, so the calling function does not need to worry
about this.

@param src A pointer to the input file, opened in READ mode.
@param buffer_length If non-zero, the maximum length of the output buffer.
@param buffer An (optional) output buffer for storing the base64 text data.
@return The number of bytes needed to store the base64 text excluding the NULL.
*/

size_t encodeBase64(FILE *src, size_t buffer_length, char *buffer);

#ifdef __cplusplus
}
#endif

#endif  /* __V2D_BASE64_H__ */
