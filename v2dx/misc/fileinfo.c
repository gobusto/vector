/**
@file fileinfo.c

@brief Provides some basic information about various file formats.

Copyright (C) 2012 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fileinfo.h"

/* Constants. */

#define MPEG_VERSION_2 0x2  /**< @brief Bit-code for MPEG version 2. */
#define MPEG_VERSION_1 0x3  /**< @brief Bit-code for MPEG version 1. */

#define MPEG_LAYER_3 0x1  /**< @brief Bit-code for MPEG Audio Layer 3: "MP3". */

#define MPEG_FREQUENCY_44100 0x0  /**< @brief Bit-code for 44KHz audio. */
#define MPEG_FREQUENCY_48000 0x1  /**< @brief Bit-code for 48KHz audio. */
#define MPEG_FREQUENCY_32000 0x2  /**< @brief Bit-code for 32KHz audio. */
#define MPEG_FREQUENCY_OTHER 0x3  /**< @brief Reserved for future use.  */

#define MPEG_CHANNEL_MONO 0x3 /**< @brief Used to differentiate Mono/Stereo. */

#define TAG_HEAD_SIZE_ID3 10  /**< @brief MP3: Length of an ID3 tag in bytes. */
#define TAG_HEAD_SIZE_APE 8   /**< @brief MP3: Length of an APE tag in bytes. */

/**
@brief [INTERNAL] about

writeme

@param x about
@return writeme
*/

#define xxxBIN2UI16_LE(x) ((x)[0] + ((x)[1]<<8))

/**
@brief [INTERNAL] about

writeme

@param x about
@return writeme
*/

#define xxxBIN2UI32_LE(x) ((x)[0] + ((x)[1]<<8) + ((x)[2]<<16) + ((x)[3]<<24))

/*
[PUBLIC] Skip past an ID3 tag, if one is found in a file.

Note: This function currently assumes that there are at least 10 bytes to read.
*/

xxx_bool_t xxxFileSkipID3(FILE *mp3)
{

  unsigned char id3[TAG_HEAD_SIZE_ID3];
  long i = 0;

  if (!mp3) { return 0; }

  for (i = 0; i < TAG_HEAD_SIZE_ID3; i++) { id3[i] = fgetc(mp3); }

  if (memcmp(id3, "ID3", 3) == 0)
       { i = id3[9] + (id3[8] << 7) + (id3[7] << 14) + (id3[6] << 21); }
  else { i = 0 - TAG_HEAD_SIZE_ID3; }

  fseek(mp3, i, SEEK_CUR);

  return (i > 0);

}

/*
[PUBLIC] Check for the start of an APE tag.

Note: This function currently assumes that there are at least 8 bytes to read.
*/

xxx_bool_t xxxFileCheckAPE(FILE *mp3)
{

  unsigned char ape[TAG_HEAD_SIZE_APE];
  long i = 0;

  if (!mp3) { return 0; }

  for (i = 0; i < TAG_HEAD_SIZE_APE; i++) { ape[i] = fgetc(mp3); }
  fseek(mp3, 0 - TAG_HEAD_SIZE_APE, SEEK_CUR);

  return (memcmp(ape, "APETAGEX", TAG_HEAD_SIZE_APE) == 0);

}

/**
@brief [INTERNAL] Determine if a file contains uncompressed WAV audio data.

Based on info from https://ccrma.stanford.edu/courses/422/projects/WaveFormat/

@param file_name The name (and path) of the file to be checked.
@param out_frequency Optional "freqency" (such as 44100 Hz) output variable.
@param out_channels Optional "channels" output variable. Usually 1 or 2.
@param out_samples Optional "number of audio samples" output variable.
@param out_bits Optional "bits-per-sample" output variable. Usually 8 or 16.
@return An enumeration value describing the file format, or XXX_FILE_UNKNOWN.
*/

static xxx_file_t xxxFileTypeAudio_WAV(const char *file_name,
    xxx_ui16_t *out_frequency,
    xxx_ui8_t  *out_channels,
    xxx_ui32_t *out_samples,
    xxx_ui16_t *out_bits)
{

  FILE *wav = NULL;
  long file_size = 0;
  unsigned char data[44];

  xxx_ui32_t frequency = 0;
  xxx_ui16_t channels = 0;
  xxx_ui16_t bits = 0;

  /* Get the length of the file, read in the first 44 bytes, and close it. */

  if (!file_name) { return XXX_FILE_UNKNOWN; }
  wav = fopen(file_name, "rb");
  if (!wav) { return XXX_FILE_UNKNOWN; }

  fseek(wav, 0, SEEK_END);
  file_size = ftell(wav);

  rewind(wav);
  if (file_size >= 44) { fread(data, 1, 44, wav); }
  fclose(wav);

  /* Validate the RIFF header. */

  if (file_size < 45) { return XXX_FILE_UNKNOWN; }
  if (memcmp(&data[0], "RIFF", 4) != 0) { return XXX_FILE_UNKNOWN; }
  if (xxxBIN2UI32_LE(&data[4]) < file_size - 8) { return XXX_FILE_UNKNOWN; }
  if (memcmp(&data[8], "WAVE", 4) != 0) { return XXX_FILE_UNKNOWN; }

  /* Validate the format sub-chunk. */

  if (memcmp(&data[12], "fmt ", 4) != 0) { return XXX_FILE_UNKNOWN; }
  if (xxxBIN2UI32_LE(&data[16]) != 16) { return XXX_FILE_UNKNOWN; }
  if (xxxBIN2UI16_LE(&data[20]) != 1) { return XXX_FILE_UNKNOWN; }

  channels = xxxBIN2UI16_LE(&data[22]);
  frequency = xxxBIN2UI32_LE(&data[24]);
  /* ...ignore the 32-bit Byte Rate value at offset 28... */
  /* ...ignore the 16-bit Block Align value at offset 32... */
  bits = xxxBIN2UI16_LE(&data[34]);

  if (channels < 1 || frequency < 1 || bits < 1) { return XXX_FILE_UNKNOWN; }
  if (bits % 8) { return XXX_FILE_UNKNOWN; }

  /* Validate the data sub-chunk. */

  if (memcmp(&data[36], "data", 4) != 0) { return XXX_FILE_UNKNOWN; }

  if (out_samples)
  {
    *out_samples = xxxBIN2UI32_LE(&data[40]);
    *out_samples /= channels;
    *out_samples /= bits / 8;
  }

  /* Fill in the output variables and report success. */

  if (out_frequency) { *out_frequency = frequency; }  /* TODO: 32-to-16-bit! */
  if (out_channels ) { *out_channels  = channels;  }  /* TODO: 16-to-8-bit!  */
  if (out_bits     ) { *out_bits      = bits;      }

  return XXX_FILE_WAV;

}

/**
@brief [INTERNAL] Determine if a file contains MP3 audio data.

This is called internally by the xxxFileTypeAudio() function.

@param file_name The name (and path) of the file to be checked.
@param out_frequency Optional "freqency" (such as 44100 Hz) output variable.
@param out_channels Optional "channels" output variable. Usually 1 or 2.
@param out_samples Optional "number of audio samples" output variable.
@param out_bits Optional "bits-per-sample" output variable. Usually 8 or 16.
@return An enumeration value describing the file format, or XXX_FILE_UNKNOWN.
*/

static xxx_file_t xxxFileTypeAudio_MP3(const char *file_name,
    xxx_ui16_t *out_frequency,
    xxx_ui8_t  *out_channels,
    xxx_ui32_t *out_samples,
    xxx_ui16_t *out_bits)
{

  /*
  Info from here: http://www.mpgedit.org/mpgedit/mpeg_format/mpeghdr.htm
  This information is also included in the official Adobe SWF specification.
  The top row is for MP3 Version 1, and the bottom row is for Version 2.
  */

  const xxx_ui16_t mp3_rate_table[2][16] = {
  {999, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 999},
  {999, 8,  16, 24, 32, 40, 48, 56, 64,  80,  96,  112, 128, 144, 160, 999}};

  FILE *src = NULL;
  long file_size = 0;

  xxx_file_t format = XXX_FILE_UNKNOWN;

  /* Attempt to open the file. */

  if (!file_name) { return XXX_FILE_UNKNOWN; }
  src = fopen(file_name, "rb");
  if (!src) { return XXX_FILE_UNKNOWN; }

  /* Get the length of the file. */

  fseek(src, 0, SEEK_END);
  file_size = ftell(src);

  if (file_size < 10)
  {
    fclose(src);
    return XXX_FILE_UNKNOWN;
  }

  /* Loop through each MP3 frame in the file. */

  rewind(src);
  xxxFileSkipID3(src);

  while (ftell(src) < file_size)
  {

    xxx_ui16_t frequency = 0;
    xxx_ui16_t bit_rate  = 0;

    xxx_ui8_t version   = 0;
    xxx_ui8_t layer     = 0;
    xxx_ui8_t rate_id   = 0;
    xxx_ui8_t freq_id   = 0;
    xxx_ui8_t padding   = 0;
    xxx_ui8_t stereo    = 0;

    xxx_ui8_t byte = 0;

    long frame_size = 0;

    /* If an APE tag is found, this is the end of the file. */

    if (xxxFileCheckAPE(src)) { break; }

    /* Assume that this is not a valid MP3 file. */

    format = XXX_FILE_UNKNOWN;

    /* The first 11 bits should always be set. */

    byte = fgetc(src);

    if (byte != 0xff)
    {
      fprintf(stderr, "[MP3] Unexpected Frame Sync value @byte 1: %c\n", byte);
      break;
    }

    byte = fgetc(src);

    if (((byte >> 5) & 0x07) != 7)
    {
      fprintf(stderr, "[MP3] Unexpected Frame Sync value @byte 2: %02x\n", byte);
      break;
    }

    /* The remaining 5 bits contain some MPEG frame type information. */

    version = (byte >> 3) & 0x03;
    layer   = (byte >> 1) & 0x03;

    if (layer != MPEG_LAYER_3)
    {
      printf("[MP3] Unexpected MPEG layer ID: %d\n", layer);
      break;
    }

    if (version != MPEG_VERSION_1 && version != MPEG_VERSION_2)
    {
      fprintf(stderr, "[MP3] Unknown MPEG version ID: %d\n", version);
      break;
    }

    /* The next two bytes contain some information about the actual data. */

    byte = fgetc(src);
    rate_id = (byte >> 4) & 0x0f;
    freq_id = (byte >> 2) & 0x03;
    padding = (byte >> 1) & 0x01;

    byte = fgetc(src);
    stereo = (byte >> 6) & 0x03;

    /* Get the playback bit-rate and frequency for this frame. */

    bit_rate = mp3_rate_table[(version == MPEG_VERSION_2) ? 1 : 0][rate_id];

    if      (freq_id == MPEG_FREQUENCY_32000) { frequency = 32000; }
    else if (freq_id == MPEG_FREQUENCY_44100) { frequency = 44100; }
    else if (freq_id == MPEG_FREQUENCY_48000) { frequency = 48000; }
    else
    {
      fprintf(stderr, "[MP3] Unknown frequency value.\n");
      break;
    }

    if (version == MPEG_VERSION_2) { frequency /= 2; }

    /* Calculate the size of this frame and skip past it, on to the next one. */

    frame_size = (144 * bit_rate * 1000) / frequency;
    if (padding) { frame_size++; }

    if (ftell(src) + frame_size - 4 > file_size)
    {
      fprintf(stderr, "[MP3] Frame length goes past End-Of-File.\n");
      break;
    }

    fseek(src, frame_size - 4, SEEK_CUR);

    /* This frame appears to indicate that the file contains valid MP3 data. */

    format = XXX_FILE_MP3;

    if (out_frequency) { *out_frequency = frequency;       }
    if (out_samples)   { *out_samples   += 1152;           }
    if (out_channels)  { *out_channels  = (stereo == MPEG_CHANNEL_MONO) ? 1:2; }

    /* NOTE: SWF files assume 16-bits for compressed formats such as MP3. */

    if (out_bits) { *out_bits = 16; }

  }

  /* Close the file and return the result. */

  fclose(src);
  return format;

}

/*
[PUBLIC] Determine which type of audio file this is.
*/

xxx_file_t xxxFileTypeAudio(const char *file_name,  xxx_ui16_t *out_frequency,
                                                    xxx_ui8_t  *out_channels,
                                                    xxx_ui32_t *out_samples,
                                                    xxx_ui16_t *out_bits)
{

  xxx_file_t file_type = XXX_FILE_UNKNOWN;

  /* Initialise pass-back pointers. */

  if (out_frequency) { *out_frequency = 0; }
  if (out_samples)   { *out_samples   = 0; }
  if (out_channels)  { *out_channels  = 0; }
  if (out_bits)      { *out_bits      = 0; }

  /* Attempt to recognise the file format. */

  file_type = xxxFileTypeAudio_WAV(file_name, out_frequency, out_channels, out_samples, out_bits);
  if (file_type == XXX_FILE_UNKNOWN)
  { file_type = xxxFileTypeAudio_MP3(file_name, out_frequency, out_channels, out_samples, out_bits); }
  return file_type;

}

/*
[PUBLIC] Determine which type of image file this is.
*/

xxx_file_t xxxFileTypeImage(const char *file_name, xxx_ui32_t *w, xxx_ui32_t *h)
{

  const xxx_ui8_t PNG_HEAD[] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A};

  FILE *src = NULL;
  unsigned char data[8];

  long file_size = 0;
  long chunk_len = 0;
  long i         = 0;

  xxx_file_t format = XXX_FILE_UNKNOWN;

  /* Initialise pass-back pointers. */

  if (w) { *w = 0; }
  if (h) { *h = 0; }

  /* Attempt to open the file. */

  if (!file_name) { return XXX_FILE_UNKNOWN; }
  src = fopen(file_name, "rb");
  if (!src) { return XXX_FILE_UNKNOWN; }

  /* Get the length of the file. */

  fseek(src, 0, SEEK_END);
  file_size = ftell(src);

  /* Is this file a GIF? */

  if (format == XXX_FILE_UNKNOWN && file_size >= 10)
  {

    rewind(src); for (i = 0; i < 6; i++) { data[i] = fgetc(src); }

    if (memcmp(data, "GIF89a", 6) == 0)
    {
      format = XXX_FILE_GIF;
      for (i = 0; i < 4; i++) { data[i] = fgetc(src); }
      if (w) { *w = data[0] + (data[1]<<8); }
      if (h) { *h = data[2] + (data[3]<<8); }
    }

  }

  /* Is this file a PNG? */

  if (format == XXX_FILE_UNKNOWN && file_size >= 24)
  {

    rewind(src); for (i = 0; i < 8; i++) { data[i] = fgetc(src); }

    if (memcmp(data, PNG_HEAD, 8) == 0)
    {

      for (i = 0; i < 8; i++) { data[i] = fgetc(src); }
      chunk_len = data[3] + (data[2]<<8) + (data[1]<<16) + (data[0]<<24);

      if (memcmp(&data[4], "IHDR", 4) == 0 && chunk_len == 13)
      {
        format = XXX_FILE_PNG;
        for (i = 0; i < 8; i++) { data[i] = fgetc(src); }
        if (w) { *w = data[3] + (data[2]<<8) + (data[1]<<16) + (data[0]<<24); }
        if (h) { *h = data[7] + (data[6]<<8) + (data[5]<<16) + (data[4]<<24); }
      }

    }

  }

  /* Is this file a JPG? */

  if (format == XXX_FILE_UNKNOWN && file_size >= 6)
  {

    rewind(src); for (i = 0; i < 2; i++) { data[i] = fgetc(src); }

    if (data[0] == 0xFF && data[1] == 0xD8)
    {

      while (ftell(src) + 4 < file_size && format == XXX_FILE_UNKNOWN)
      {

        /* Get the chunk type and length. */

        for (i = 0; i < 4; i++) { data[i] = fgetc(src); }
        chunk_len = (data[3] + (data[2]<<8));
        if (chunk_len < 2 || data[0] != 0xFF) { break; }

        /* If this is an SOF0 or SOF2 marker, get the width/height. */

        if ((data[1] == 0xC0 || data[1] == 0xC2) && chunk_len >= 5)
        {
          format = XXX_FILE_JPG;
          for (i = 0; i < 5; i++) { data[i] = fgetc(src); }
          if (w) { *w = data[4] + (data[3]<<8); }
          if (h) { *h = data[2] + (data[1]<<8); }
        }

        /* Otherwise, move on to the next chunk. */

        fseek(src, chunk_len - 2, SEEK_CUR);

      }

    }

  }

  /* Close the file and return the result. */

  fclose(src);
  return format;

}
