/**
@file misc.h

@brief Miscellaneous things.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __V2DX_MISC_H__
#define __V2DX_MISC_H__

#ifdef __cplusplus
extern "C" {
#endif


/**
@brief Get a double from a string.

This is essentially just a convenient wrapper for the strtod() function.

@param text The string to parse.
@param output An (optional) output variable.
@return Non-zero on success, or zero on failure.
*/

int v2dxParseDouble(const char *text, double *output);

#ifdef __cplusplus
}
#endif

#endif  /* __V2DX_MISC_H__ */
