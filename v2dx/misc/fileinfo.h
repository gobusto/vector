/**
@file fileinfo.h

@brief Provides some basic information about various file formats.

Copyright (C) 2012 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __XXX_FILEINFO_H__
#define __XXX_FILEINFO_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

/* Common datatypes. */

typedef unsigned char  xxx_bool_t;  /**< @brief Boolean data type.        */
typedef unsigned char  xxx_ui8_t;   /**< @brief Unsigned, 8-bit integer.  */
typedef unsigned short xxx_ui16_t;  /**< @brief Unsigned, 16-bit integer. */
typedef unsigned long  xxx_ui32_t;  /**< @brief Unsigned, 32-bit integer. */

/**
@brief Enumerates known file-format types.

Notice that this isn't specific to one type of file; it contains both audio and
image file formats.
*/

typedef enum
{

  XXX_FILE_UNKNOWN, /**< @brief Unrecognised format.                          */
  XXX_FILE_JPG,     /**< @brief JPG "Joint Photographic Experts Group" image. */
  XXX_FILE_PNG,     /**< @brief PNG "Portable Network Graphics" image.        */
  XXX_FILE_GIF,     /**< @brief GIF "Graphics Interchange Format" image.      */
  XXX_FILE_MP3,     /**< @brief MP3 audio file, with or without ID3 tag data. */
  XXX_FILE_WAV      /**< @brief WAV audio file.                               */

} xxx_file_t;

/**
@brief Skip past an ID3 tag, if one is found in a file.

Some (but not all) MP3 files have an ID3 tag at the start of the file, but they
don't actually affect the MP3 audio data in any way.

@param mp3 The MP3 file to be skipped-forwards.
@return True if an ID3 tag was found (and skipped past), or false if not.
*/

xxx_bool_t xxxFileSkipID3(FILE *mp3);

/**
@brief Check for the start of an APE tag.

Some (but not all) MP3 files have an APE tag at the end of the file, but they
don't actually affect the MP3 audio data in any way.

@param mp3 The MP3 file to be checked.
@return True if an APE tag was found at the current position, or false if not.
*/

xxx_bool_t xxxFileCheckAPE(FILE *mp3);

/**
@brief Determine which type of audio file this is.

Unrecognised formats are returned as XXX_FILE_UNKNOWN with all output variables
set to zero.

@param file_name The name (and path) of the file to be checked.
@param out_frequency Optional "freqency" (such as 44100 Hz) output variable.
@param out_channels Optional "channels" output variable. Usually 1 or 2.
@param out_samples Optional "number of audio samples" output variable.
@param out_bits Optional "bits-per-sample" output variable. Usually 8 or 16.
@return An enumeration value describing the file format, or XXX_FILE_UNKNOWN.
*/

xxx_file_t xxxFileTypeAudio(const char *file_name,  xxx_ui16_t *out_frequency,
                                                    xxx_ui8_t  *out_channels,
                                                    xxx_ui32_t *out_samples,
                                                    xxx_ui16_t *out_bits);

/**
@brief Determine which type of image file this is.

Unrecognised formats are returned as XXX_FILE_UNKNOWN with all output variables
set to zero.

@param file_name The name (and path) of the file to be checked.
@param w Optional "image width" output variable.
@param h Optional "image height" output variable.
@return An enumeration value describing the file format, or XXX_FILE_UNKNOWN.
*/

xxx_file_t xxxFileTypeImage(const char *file_name, xxx_ui32_t *w, xxx_ui32_t *h);

#ifdef __cplusplus
}
#endif

#endif /* __XXX_FILEINFO_H__ */
