/**
@file xml.h

@brief Provides utilities for loading, saving, and manipulating XML data/files.

IMPORTANT: Unless noted otherwise, all text strings are stored in UTF-8 format.

Copyright (C) 2013-2015 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_XML2_H__
#define __QQQ_XML2_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief This structure is used to represent both XML nodes and node attributes.

All fields should be treated as READ ONLY. To modify an XML node or attribute,
use one of the functions provided.
*/

typedef struct _xml_s
{

  char          *name;  /**< @brief The name of this XML node/attribute. */
  char          *text;  /**< @brief Text data (nodes) or attribute value. */
  void          *user;  /**< @brief User-defined data; not used internally. */

  struct _xml_s *attr;  /**< @brief NODE ONLY: A linked list of attributes. */
  struct _xml_s *node;  /**< @brief NODE ONLY: A linked list of child nodes. */

  struct _xml_s *owner; /**< @brief The owner of this child node/attribute. */
  struct _xml_s *prev;  /**< @brief The preceding node/attribute structure. */
  struct _xml_s *next;  /**< @brief The following node/attribute structure. */

} xml_s;

/**
@brief Add a new child node to an XML node.

This macro is provided for convenience.

@param xml An XML node.
@param name The name of the new child node.
@param text The text of the new child node.
@return The new child node structure on success, or NULL on failure.
*/

#define xmlNewChildNode(xml,name,text) xmlAddChildNode(xml,xmlCreate(name,text))

/**
@brief Add a new attribute to an XML node.

This macro is provided for convenience.

@param xml An XML node.
@param name The name of the new attribute.
@param text The value of the new attribute.
@return The new attribute structure on success, or NULL on failure.
*/

#define xmlNewAttribute(xml,name,text) xmlAddAttribute(xml,xmlCreate(name,text))

/**
@brief Create a new XML node/attribute structure.

The text value is ignored for any node types whose name does not begin with a !
or # character. For ! nodes, the text represents their contents. For # nodes,
the meaning of the text value will vary by type.

"document" is used for the root element of an XML document. The text attribute
contains either the path of the file that the document was loaded from, or an
empty string if the document was not loaded from a file.

"text" is used for basic text data. The text attribute stores the data as PLAIN
text; for example, an ampersand would simply be stored as a single & character.

"entity" represents a non-predefined XML entity reference. The text attribute is
the name of the entity without the preceding & or trailing ; characters.

@param name The name of the new node/attribute.
@param text The text of the new node/attribute.
@return A new XML structure on success, or NULL on failure.
*/

xml_s *xmlCreate(const char *name, const char *text);

/**
@brief Delete an XML node/attribute.

This will automatically unlink the node from its owner and any siblings. Child
nodes and attributes will also be deleted recursively.

@param xml The node or attribute to be deleted.
@return The next node/attribute in the linked list, or NULL if there isn't one.
*/

xml_s *xmlDelete(xml_s *xml);

/**
@brief Unlink an XML node/attribute from both its parent and any siblings.

Child nodes or attributes are not affected, so this function can be used to
extract a subset of an XML document as a fully independant node tree.

@param xml The node or attribute to be unlinked.
@return The next node/attribute in the linked list, or NULL if there isn't one.
*/

xml_s *xmlUnlink(xml_s *xml);

/**
@brief Set the name value of an XML node or attribute.

If "name" is NULL, a default value of "#document" is used.

@param xml The node or attribute to rename.
@param name The new name value.
@return Zero = Success, Negative = Invalid parameter, Positive = Fatal error.
*/

int xmlSetName(xml_s *xml, const char *name);

/**
@brief Set the text value of an XML node or attribute.

If "text" is NULL, a default value of "" is used.

@param xml The node or attribute to be updated.
@param text The new text value.
@return Zero = Success, Negative = Invalid parameter, Positive = Fatal error.
*/

int xmlSetText(xml_s *xml, const char *text);

/**
@brief Append additional text to an XML node or attribute.

This function is provided for convenience.

@param xml The node or attribute to be updated.
@param text The new text value.
@return Zero = Success, Negative = Invalid parameter, Positive = Fatal error.
*/

int xmlAddText(xml_s *xml, const char *text);

/**
@brief Assign a node as the child of another.

This will automatically unlink the child node from any existing parent first.

If the "owner" is NULL, the child node will be deleted. This is to prevent the
xmlNewChildNode() macro from causing memory leaks.

@param owner The parent to add the node to.
@param node The node to be added.
@return The child node on success, or NULL if it could not be added.
*/

xml_s *xmlAddChildNode(xml_s *owner, xml_s *node);

/**
@brief Assign an attribute to a node.

This will automatically unlink the attribute from any existing owner first. If
the new owner already has an attribute with the same name, the existing version
will be deleted first, allowing the "new" attribute can replace it.

If the "owner" is NULL, the attribute will be deleted. This is to prevent the
xmlNewAttribute() macro from causing memory leaks.

@param owner The node to add the attribute to.
@param attr The attribute to be added.
@return The attribute node on success, or
*/

xml_s *xmlAddAttribute(xml_s *owner, xml_s *attr);

/**
@brief Build an XML node tree from a text string.

NOTE: This function is used internally by the xmlLoadFile() function.

@param text A string of XML text data.
@return A new XML node tree on success, or NULL on failure.
*/

xml_s *xmlLoadData(const char *text);

/**
@brief Load an XML document from a file.

This function is provided for convenience.

@param path The name and path of the file to be loaded.
@return A new XML node tree on success, or NULL on failure.
*/

xml_s *xmlLoadFile(const char *path);

/**
@brief Export an XML node tree to a file.

Note that this does not have to be a COMPLETE node tree; subsets are allowed.

@param xml The XML node tree to export.
@param path The name and path of the file to write to.
@return Zero = Success, Negative = Invalid parameter, Positive = Fatal error.
*/

int xmlSaveFile(const xml_s *xml, const char *path);

/**
@brief Try to find a node/attribute by name, using a depth-first tree search.

If "depth" is less than zero, then it's assumed that no depth limit applies.

@param xml The "root" node of the tree. Child nodes (and siblings) are checked.
@param name The name string to search for.
@param depth Maximum search depth; negative values indicate no limit applies.
@return The first matching node found on success, or NULL on failure.
*/

xml_s *xmlFindName(xml_s *xml, const char *name, int depth);

#ifdef __cplusplus
}
#endif

#endif  /* __QQQ_XML2_H__ */
