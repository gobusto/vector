/*

FILENAME: load.h
AUTHOR/S: Thomas Dennis
CREATION: 17th March 2013

Copyright (C) 2013-2014 by Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file load.h

@brief Provides functions for importing projects from various file formats.

Currently only handles SVG files.
*/

#ifndef __V2D_LOAD_H__
#define __V2D_LOAD_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "v2d/stage.h"

/**
@brief Load a project from a Scalable Vector Graphics (SVG) file.

ZLIB-compressed *.SVGZ files are currently unsupported.

@param file_name The name and path of the file to be imported.
@param fps The FPS rate to use for the imported file.
@return A new stage structure on success, or NULL on failure.
*/

v2d_stage_s *v2dxLoadSVG(const char *file_name, v2d_real_t fps);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_LOAD_H__ */
