/*

FILENAME: path.h
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file path.h

@brief about

Writeme
*/

#ifndef __V2D_PATH_H__
#define __V2D_PATH_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/chain.h"
#include "bits/type.h"
#include "constants.h"

/* Flags. */

#define V2D_PATH_FLAG_RELATIVE 0x01 /**< @brief about */
#define V2D_PATH_FLAG_UNUSED1  0x02 /**< @brief about */
#define V2D_PATH_FLAG_UNUSED2  0x04 /**< @brief about */
#define V2D_PATH_FLAG_UNUSED3  0x08 /**< @brief about */
#define V2D_PATH_FLAG_UNUSED4  0x10 /**< @brief about */
#define V2D_PATH_FLAG_UNUSED5  0x20 /**< @brief about */
#define V2D_PATH_FLAG_UNUSED6  0x40 /**< @brief about */
#define V2D_PATH_FLAG_UNUSED7  0x80 /**< @brief about */

/**
@brief about

writeme
*/

typedef enum
{

  V2D_PATH_MOVE,  /**< @brief about */
  V2D_PATH_LINE,  /**< @brief about */
  V2D_PATH_QUAD,  /**< @brief about */
  V2D_PATH_CUBE   /**< @brief about */

} v2d_move_t;

/**
@brief about

NOTE: v2dxLoadSVG_internal() currently expects this exact ordering to be used!
*/

typedef enum
{

  V2D_CONTROL_1,  /**< @brief about */
  V2D_CONTROL_2,  /**< @brief about */
  V2D_END_POINT,  /**< @brief about */
  V2D_NUM_POINTS  /**< @brief about */

} v2d_point_t;

/**
@brief about

writeme
*/

typedef struct
{

  v2d_link_s *link; /**< @brief Linked-list data. */

  v2d_move_t type;  /**< @brief about */
  v2d_flag_t flags; /**< @brief about */

  v2d_real_t point[V2D_NUM_POINTS][2];  /**< @brief about */

} v2d_path_s;

/**
@brief about

@param p about
@return writeme
*/

#define v2dPathGetNext(p) ((v2d_path_s*)v2dLinkGetNext((p) ? (p)->link : 0))

/**
@brief about

writeme

@param prev about
@param x about
@param y about
@param flags about
@return writeme
*/

#define v2dPathMove(prev,x,y,flags) v2dPathCreate(prev, V2D_PATH_MOVE, 0, 0, 0, 0, x, y, flags)

/**
@brief about

writeme

@param prev about
@param x about
@param y about
@param flags about
@return writeme
*/

#define v2dPathLine(prev,x,y,flags) v2dPathCreate(prev, V2D_PATH_LINE, 0, 0, 0, 0, x, y, flags)

/**
@brief about

writeme

@param prev about
@param a about
@param b about
@param x about
@param y about
@param flags about
@return writeme
*/

#define v2dPathQuad(prev,a,b,x,y,flags) v2dPathCreate(prev, V2D_PATH_QUAD, a, b, 0, 0, x, y, flags)

/**
@brief about

writeme

@param prev about
@param a about
@param b about
@param c about
@param d about
@param x about
@param y about
@param flags about
@return writeme
*/

#define v2dPathCube(prev,a,b,c,d,x,y,flags) v2dPathCreate(prev, V2D_PATH_CUBE, a, b, c, d, x, y, flags)

/**
@brief about

writeme

@param prev about
@param type writeme
@param cx1 about
@param cy1 about
@param cx2 about
@param cy2 about
@param x about
@param y about
@param flags about
@return writeme
*/

v2d_path_s *v2dPathCreate(v2d_path_s *prev, v2d_move_t type,
                          v2d_real_t cx1, v2d_real_t cy1,
                          v2d_real_t cx2, v2d_real_t cy2,
                          v2d_real_t x, v2d_real_t y, v2d_flag_t flags);

/**
@brief about

writeme

@param path about
*/

void v2dPathDelete(v2d_path_s *path);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_PATH_H__ */
