/*

FILENAME: item.h
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file item.h

@brief Provides structures and functions for creating animated display objects.

Display items are based on the shapes/text defined by v2d_draw_s structures and
may be animated using a series of key-frame structures.
*/

#ifndef __V2D_ITEM_H__
#define __V2D_ITEM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "anim.h"
#include "draw.h"
#include "bits/chain.h"
#include "bits/type.h"
#include "constants.h"

/* Constants. */

#define V2D_ITEM_FLAG_SELECTED 0x01 /**< @brief Used by GUI front-ends. */
#define V2D_ITEM_FLAG_UNUSED1  0x02 /**< @brief Reserved for future use. */
#define V2D_ITEM_FLAG_UNUSED2  0x04 /**< @brief Reserved for future use. */
#define V2D_ITEM_FLAG_UNUSED3  0x08 /**< @brief Reserved for future use. */
#define V2D_ITEM_FLAG_UNUSED4  0x10 /**< @brief Reserved for future use. */
#define V2D_ITEM_FLAG_UNUSED5  0x20 /**< @brief Reserved for future use. */
#define V2D_ITEM_FLAG_UNUSED6  0x40 /**< @brief Reserved for future use. */
#define V2D_ITEM_FLAG_UNUSED7  0x80 /**< @brief Reserved for future use. */

/**
@brief This structure describes a single on-screen instance of a Drawable.

Display items are used to add visible instances of shapes/text to a scene. They
don't contain any path/text data themselves - they simply use the shape defined
by whichever drawable they are told to use.

Display items may be animated by adding key-frames. For example, a display item
pointing to a "text" drawable could be animated so that the words "Hello World"
moved around the screen.
*/

typedef struct
{

  char *name; /**< @brief Optional name, used as a unique identifier. */

  v2d_link_s  *link;  /**< @brief Linked-list data. */
  v2d_chain_s *anim;  /**< @brief Animation key-frame data. */
  v2d_ui32_t   hold;  /**< @brief Number of frames after the last keyframe. */

  v2d_flag_t flags; /**< @brief Not used internally; provided for GUI use. */
  v2d_matrix_t old; /**< @brief Not used internally; provided for GUI use. */

} v2d_item_s;

/**
@brief Get the first Display Item data structure in a linked list.

@param p A pointer to a v2d_chain_s structure containing v2d_item_s structures.
@return The first image item in the linked list on success, or NULL on failure.
*/

#define v2dGroupGetItem(p) ((v2d_item_s*)((p) ? ((p)->first  ? (p)->first->data  : 0) : 0))

/**
@brief Get the final Display Item data structure in a linked list.

@param p A pointer to a v2d_chain_s structure containing v2d_item_s structures.
@return The final image item in the linked list on success, or NULL on failure.
*/

#define v2dGroupGetFinalItem(p) ((v2d_item_s*)((p) ? ((p)->last  ? (p)->last->data  : 0) : 0))

/**
@brief Get the first animation data structure in a linked list.

@param p A pointer to a v2d_item_s structure.
@return The first animation in the linked list on success, or NULL on failure.
*/

#define v2dItemGetAnim(p) ((v2d_anim_s*)((p) ? ((p)->anim->first  ? (p)->anim->first->data  : 0) : 0))

/**
@brief Get the previous display item structure in a linked-list.

@param p The current display item in the linked-list.
@return The prior display item in the linked-list, or NULL if there isn't one.
*/

#define v2dItemGetPrev(p) ((v2d_item_s*)v2dLinkGetPrev((p) ? (p)->link : 0))

/**
@brief Get the next display item structure in a linked-list.

@param p The current display item in the linked-list.
@return The next display item in the linked-list, or NULL if there isn't one.
*/

#define v2dItemGetNext(p) ((v2d_item_s*)v2dLinkGetNext((p) ? (p)->link : 0))

/**
@brief Create a new Display Item structure at the specified X/Y position.

@param chain The linked list that this item should be added to.
@param draw The "Drawable" to use when rendering this Display Item on-screen.
@param frame The index of the animation frame on which the item should appear.
@param x The initial X position of this Display Item.
@param y The initial Y position of this Display Item.
@param button If true, set the V2D_ANIM_FLAG_BUTTON flag for the first "anim".
@return A new Display Item structure on success, or NULL on failure.
*/

v2d_item_s *v2dItemCreate(v2d_chain_s *chain, v2d_draw_s *draw, v2d_ui32_t frame, v2d_real_t x, v2d_real_t y, v2d_bool_t button);

/**
@brief Free a previously created display item structure from memory.

This also frees any animation data that the Display Item contains.

@param item The Display Item structure to be deleted.
*/

void v2dItemDelete(v2d_item_s *item);

/**
@brief Assign a name to a display item.

Note that the name string should not already be used by any other items in the
same linked list.

@param item The Display Item to assign a name to.
@param name The new name string to use, or NULL to remove the current name.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dItemSetName(v2d_item_s *item, const char *name);

/**
@brief Delete any animation keyframes that reference a given drawable object.

This is called internally by v2dDrawDelete() to clean up any references to draw
items before they are deleted.

@param group The group of items to be checked.
@param draw The drawable to be "forgotten".
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dGroupForgetDraw(v2d_chain_s *group, v2d_draw_s *draw);

/**
@brief Get the animation key-frame data for a specific point on the time-line.

NOTE: This returns the PREVIOUS frame if the exact frame number doesn't exist!

@param item The Display Item structure whose animation data should be inspected.
@param frame The chronological position of the animation key-frame needed.
@return The animation frame on success, or NULL if there is none.
*/

v2d_anim_s *v2dItemGetFrame(v2d_item_s *item, v2d_ui32_t frame);

/**
@brief Determine if an item was visible on a certain frame.

@param item The Display Item structure whose animation data should be inspected.
@param frame The chronological position of the animation key-frame needed.
@return V2D_TRUE if the item was visible, or V2D_FALSE if not.
*/

v2d_bool_t v2dItemVisible(v2d_item_s *item, v2d_ui32_t frame);

/**
@brief Get the total number of animation frames used by a sequence of items.

A static scene would report one (i.e. a single frame) so zero should only ever
be returned if something is wrong.

@param group A linked list of v2d_item_s structures.
@return The total number of frames found on success, or zero on failure.
*/

v2d_ui32_t v2dGroupCountFrames(v2d_chain_s *group);

/**
@brief Get the bounding box of a display item.

@param item The display item to check.
@param frame The animation frame to get the bounding box for.
@param rect The output structure to store the AABB in.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dItemGetAABB(v2d_item_s *item, v2d_ui32_t frame, v2d_rect_s *rect);

/**
@brief Set/unset the V2D_ITEM_FLAG_SELECTED flag for items within a group.

This is a convenience function, provided for GUI front-ends to use.

Note that "discard" behaves slightly differently if no selection rectangle is
given; V2D_TRUE will select everything and V2D_FALSE will de-select everything.

@param group The group to check.
@param frame The animation frame to use when checking selection-box overlap.
@param rect If provided, specifies a selection rectangle.
@param discard If true, de-select any alread-y-selected items not in the AABB.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dItemSelect(v2d_chain_s *group, v2d_ui32_t frame, v2d_rect_s *rect, v2d_bool_t discard);

/**
@brief Get the AABB of all SELECTED items within a group.

This is a convenience function, provided for GUI front-ends to use.

@param group The group to check.
@param frame The animation frame to check.
@param rect An output structure to store the AABB.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dGetSelectedAABB(v2d_chain_s *group, v2d_ui32_t frame, v2d_rect_s *rect);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_ITEM_H__ */
