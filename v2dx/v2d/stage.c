/*

FILENAME: stage.c
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file stage.c

@brief Provides functions for creating and interacting with "stage" structures.

A "stage" can be thought of as self-contained "project" structure, and is used
to store the various things (images, sounds, text, etc.) that make up a scene.
*/

#include <stdlib.h>
#include <string.h>
#include "stage.h"

/*
[PUBLIC] Create a new, blank "stage" structure.
*/

v2d_stage_s *v2dStageCreate(v2d_real_t w, v2d_real_t h, v2d_real_t fps)
{

  v2d_stage_s *stage = NULL;

  if (w < 1 || h < 1 || fps < 1) { return NULL; }

  stage = (v2d_stage_s*)malloc(sizeof(v2d_stage_s));
  if (!stage) { return NULL; }
  memset(stage, 0, sizeof(v2d_stage_s));

  v2dStageSetSize(stage, w, h);
  v2dStageSetFPS(stage, fps);
  v2dStageSetColor(stage, 255, 255, 255, 255);

  stage->image = v2dChainCreate(V2D_LIST_RESOURCE, stage);
  stage->audio = v2dChainCreate(V2D_LIST_RESOURCE, stage);
  stage->embed = v2dChainCreate(V2D_LIST_RESOURCE, stage);
  stage->draw  = v2dChainCreate(V2D_LIST_DRAW, stage);
  stage->item  = v2dChainCreate(V2D_LIST_ITEM, stage);

  if (!stage->image || !stage->audio || !stage->embed || !stage->draw || !stage->item)
  {
    v2dStageDelete(stage);
    return NULL;
  }

  return stage;

}

/*
[PUBLIC] Free a previously created "stage" structure from memory.
*/

void v2dStageDelete(v2d_stage_s *stage)
{

  if (!stage) { return; }

  if (stage->title   ) { free(stage->title   ); }
  if (stage->metadata) { free(stage->metadata); }

  if (stage->image)
  {
    while (v2dStageGetImage(stage)) { v2dDataDelete(v2dStageGetImage(stage)); }
    v2dChainDelete(stage->image);
  }

  if (stage->audio)
  {
    while (v2dStageGetAudio(stage)) { v2dDataDelete(v2dStageGetAudio(stage)); }
    v2dChainDelete(stage->audio);
  }

  if (stage->embed)
  {
    while (v2dStageGetEmbed(stage)) { v2dDataDelete(v2dStageGetEmbed(stage)); }
    v2dChainDelete(stage->embed);
  }

  if (stage->draw)
  {
    while (v2dStageGetDraw(stage)) { v2dDrawDelete(v2dStageGetDraw(stage)); }
    v2dChainDelete(stage->draw);
  }

  if (stage->item)
  {
    while (v2dGroupGetItem(stage->item)) { v2dItemDelete(v2dGroupGetItem(stage->item)); }
    v2dChainDelete(stage->item);
  }

  free(stage);

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dStageSetTitle(v2d_stage_s *stage, const char *title)
{

  if (!stage) { return V2D_FALSE; }

  if (stage->title) { free(stage->title); stage->title = NULL; }

  if (title)
  {
    stage->title = (char*)malloc(strlen(title) + 1);
    if (!stage->title) { return V2D_FALSE; }
    strcpy(stage->title, title);
  }

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dStageSetMetaData(v2d_stage_s *stage, const char *metadata)
{

  if (!stage) { return V2D_FALSE; }

  if (stage->metadata) { free(stage->metadata); stage->metadata = NULL; }

  if (metadata)
  {
    stage->metadata = (char*)malloc(strlen(metadata) + 1);
    if (!stage->metadata) { return V2D_FALSE; }
    strcpy(stage->metadata, metadata);
  }

  return V2D_TRUE;

}

/*
[PUBLIC] Resize the display area or a stage.
*/

v2d_bool_t v2dStageSetSize(v2d_stage_s *stage, v2d_real_t w, v2d_real_t h)
{
  if (!stage || w < 1 || h < 1) { return V2D_FALSE; }
  stage->width  = w;
  stage->height = h;
  return V2D_TRUE;
}

/*
[PUBLIC] Change the frames-per-second playback rate of a stage.
*/

v2d_bool_t v2dStageSetFPS(v2d_stage_s *stage, v2d_real_t fps)
{
  if (!stage || fps < 1) { return V2D_FALSE; }
  stage->fps = fps;
  return V2D_TRUE;
}

/*
[PUBLIC] Change the background color of a stage.
*/

v2d_bool_t v2dStageSetColor(v2d_stage_s *stage, v2d_ui8_t r, v2d_ui8_t g,
                                                v2d_ui8_t b, v2d_ui8_t a)
{
  if (!stage) { return V2D_FALSE; }
  stage->rgba[0] = r;
  stage->rgba[1] = g;
  stage->rgba[2] = b;
  stage->rgba[3] = a;
  return V2D_TRUE;
}

/*
[PUBLIC] Merge the contents of one stage into another.
*/

v2d_draw_s *v2dStageMerge(v2d_stage_s *stage_a, v2d_stage_s *stage_b)
{

  v2d_draw_s *group = NULL;
  v2d_data_s *data = NULL;
  v2d_draw_s *draw = NULL;
  v2d_item_s *item = NULL;

  if (!stage_a || !stage_b) { return NULL; }

  /* Create a new "group" drawable to store the display items from stage_b. */

  group = v2dDrawCreateGroup(stage_a->draw);
  if (!group) { return NULL; }

  for (item = v2dGroupGetItem(stage_b->item); item; item = v2dGroupGetItem(stage_b->item))
  { v2dLinkInsert(v2dDrawAsGroup(group), v2dDrawAsGroup(group)->last, item->link); }

  /* Copy the various resources over from stage_b into stage_a instead. */

  for (data = v2dStageGetImage(stage_b); data; data = v2dStageGetImage(stage_b))
  { v2dLinkInsert(stage_a->image, NULL, data->link); }

  for (data = v2dStageGetAudio(stage_b); data; data = v2dStageGetAudio(stage_b))
  { v2dLinkInsert(stage_a->audio, NULL, data->link); }

  for (data = v2dStageGetEmbed(stage_b); data; data = v2dStageGetEmbed(stage_b))
  { v2dLinkInsert(stage_a->embed, NULL, data->link); }

  for (draw = v2dStageGetDraw(stage_b); draw; draw = v2dStageGetDraw(stage_b))
  { v2dLinkInsert(stage_a->draw, NULL, draw->link); }

  /* Delete the empty stage_b structure + return the group representing it. */

  v2dStageDelete(stage_b);
  return group;

}
