/*

FILENAME: draw.h
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file draw.h

@brief Unifies the various displayable objects into a single structure.

This allows text and shapes to be handled as though they were the same thing.
*/

#ifndef __V2D_DRAW_H__
#define __V2D_DRAW_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/chain.h"
#include "bits/type.h"
#include "constants.h"

#include "shape.h"
#include "text.h"

/**
@brief Enumerates possible "drawable" objects.

This is used to determine which kind of data a drawable item contains.
*/

typedef enum
{

  V2D_TYPE_GROUP, /**< @brief A linked-list of v2d_item_s Display items.      */
  V2D_TYPE_SHAPE, /**< @brief A basic 2D vector shape, with lines and curves. */
  V2D_TYPE_TEXT   /**< @brief A string of standard 7-bit ASCII or UTF-8 text. */

} v2d_type_t;

/**
@brief Combined "drawable object" structure.

This allows text and shapes to be handled as though they were the same thing.
*/

typedef struct
{

  v2d_link_s *link; /**< @brief Linked-list data. */

  v2d_type_t type;  /**< @brief Specifies which kind of drawable this is. */
  void       *data; /**< @brief Pointer to a type-specific sub-structure. */

} v2d_draw_s;

/**
@brief Get the previous drawable structure in a linked-list of drawables.

@param p The current drawable in the linked-list.
@return The previous drawable in the linked-list, or NULL if there isn't one.
*/

#define v2dDrawGetPrev(p) ((v2d_draw_s*)v2dLinkGetPrev((p) ? (p)->link : 0))

/**
@brief Get the next drawable structure in a linked-list of drawable structures.

@param p The current drawable in the linked-list.
@return The next drawable in the linked-list, or NULL if there isn't one.
*/

#define v2dDrawGetNext(p) ((v2d_draw_s*)v2dLinkGetNext((p) ? (p)->link : 0))

/**
@brief Get a drawable as a v2d_text_s structure.

@param draw The main v2d_draw_s structure.
@return A pointer to the v2d_text_s structure used by the drawable.
*/

#define v2dDrawAsText(draw) ((v2d_text_s*)((draw) ? (draw)->data : 0))

/**
@brief Get a drawable as a v2d_shape_s structure.

@param draw The main v2d_draw_s structure.
@return A pointer to the v2d_shape_s structure used by the drawable.
*/

#define v2dDrawAsShape(draw) ((v2d_shape_s*)((draw) ? (draw)->data : 0))

/**
@brief Get a drawable as a linked-list of v2d_item_s structures.

@param draw The main v2d_draw_s structure.
@return The first v2d_item_s structure in the list, or NULL if none exists.
*/

#define v2dDrawAsGroup(draw) ((v2d_chain_s*)((draw) ? (draw)->data : 0))

/**
@brief Create a new "text" drawable.

@param p The chain that the new text object should be added to.
@param text The initial text content for a "text" drawable, otherwise ignored.
@return A new drawable structure on success, or NULL on failure.
*/

#define v2dDrawCreateText(p,text) v2dDrawCreate((p),V2D_TYPE_TEXT,0,0,(text))

/**
@brief Create a new "shape" drawable.

@param p The chain that the new text object should be added to.
@param x The initial pen X position for a "shape" drawable, otherwise ignored.
@param y The initial pen Y position for a "shape" drawable, otherwise ignored.
@return A new drawable structure on success, or NULL on failure.
*/

#define v2dDrawCreateShape(p,x,y) v2dDrawCreate((p),V2D_TYPE_SHAPE,(x),(y),(const char*)0)

/**
@brief Create a new "group" drawable.

@param p The chain that the new text object should be added to.
@return A new drawable structure on success, or NULL on failure.
*/

#define v2dDrawCreateGroup(p) v2dDrawCreate((p),V2D_TYPE_GROUP,0,0,(const char*)0)

/**
@brief Create a new drawable object.

This can be a 2D vector shape, a string of text, or a group of other drawables.
It's generally easier to use the helper macros than to call this directly...

@param chain The linked-list to add the new drawable to.
@param type The type of drawable object to be created.
@param x The initial pen X position for a "shape" drawable, otherwise ignored.
@param y The initial pen Y position for a "shape" drawable, otherwise ignored.
@param text The initial text content for a "text" drawable, otherwise ignored.
@return A new drawable structure on success, or NULL on failure.
*/

v2d_draw_s *v2dDrawCreate(v2d_chain_s *chain, v2d_type_t type, v2d_real_t x, v2d_real_t y, const char *text);

/**
@brief Delete a previously created drawable object.

This also frees any type-specific data, such as path segments in vector shapes.

@param draw The drawable object to be freed.
*/

void v2dDrawDelete(v2d_draw_s *draw);

/**
@brief Sort drawables so that Groups only refer to draws which are before them.

This is mainly necessary for SWF export, since SWF items can only reference IDs
defined at an earlier position in the file. This function must be called before
v2dSaveSWF() is called, in order to ensure that scenes can be exported

@param chain The list of "drawable" structures to be checked/sorted.
@param update If false, just return V2D_FALSE for unsorted lists - don't fix them.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dDrawSort(v2d_chain_s *chain, v2d_bool_t update);

/**
@brief Get the AABB of a drawable.

For "shape" drawables, this includes the line width.

@param draw The drawable to get the AABB of.
@param aabb The rect structure to store the AABB in.
@param frame The animation frame index to use (only applies to "group" draws).
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dDrawGetAABB(v2d_draw_s *draw, v2d_rect_s *aabb, v2d_ui32_t frame);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_DRAW_H__ */
