/*

FILENAME: anim.c
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file anim.c

@brief Provides structures and functions for animating display items.

Display items are based on the shapes/text defined by v2d_draw_s structures and
may be animated using a series of key-frame structures.
*/

#include <stdlib.h>
#include <string.h>
#include "anim.h"

/*
[PUBLIC] Create a new animation keyframe.
*/

v2d_anim_s *v2dAnimCreate(v2d_chain_s *chain, v2d_draw_s *draw, v2d_ui32_t frame, v2d_flag_t flags)
{

  v2d_anim_s *anim = NULL;
  v2d_anim_s *other = NULL;

  v2d_state_t s = 0;

  if      (!chain || !draw                  ) { return NULL; }
  else if (chain->user_type != V2D_LIST_ANIM) { return NULL; }

  /* Allocate memory for the new animation frame structure. */

  anim = (v2d_anim_s*)malloc(sizeof(v2d_anim_s));
  if (!anim) { return NULL; }
  memset(anim, 0, sizeof(v2d_anim_s));

  /* Initialise the various attributes. */

  anim->flags = flags;

  for (s = 0; s < V2D_NUM_STATES; s++) { anim->draw[s] = draw; }

  anim->link = v2dLinkCreate(anim);
  v2dLinkInsert(chain, NULL, anim->link);

  if (!anim->link)
  {
    v2dAnimDelete(anim, V2D_TRUE);
    return NULL;
  }

  /* Attempt to set the frame's position in the linked list. */

  if (!v2dAnimSetFrame(anim, frame))
  {
    v2dAnimDelete(anim, V2D_TRUE);
    return NULL;
  }

  /* Copy the previous (or next) frame matrix for the new one, if possible. */

  other = v2dAnimGetPrev(anim);
  if (!other) { other = v2dAnimGetNext(anim); }

  if (other) { v2dMatrixCopy(anim->matrix, other->matrix); }
  else       { v2dMatrixReset(anim->matrix);               }

  /* Return the result. */

  return anim;

}

/*
[PUBLIC] Free a single animation frame from memory.
*/

v2d_bool_t v2dAnimDelete(v2d_anim_s *anim, v2d_bool_t force)
{

  v2d_event_t e = 0;

  if (!anim) { return V2D_FALSE; }

  /* Don't remove the final animation in a list unless forced to. */

  if (anim->link)
  {
    if (!force && !anim->link->prev && !anim->link->next) { return V2D_FALSE; }
  }

  for (e = 0; e < V2D_NUM_EVENTS; e++)
  {
    v2dSoundDelete(anim->sound[e]);
    v2dScriptDelete(anim->script[e]);
  }

  v2dLinkDelete(anim->link);
  free(anim);

  return V2D_TRUE;

}

/*
[PUBLIC] Re-position an animation keyframe on the timeline.
*/

v2d_bool_t v2dAnimSetFrame(v2d_anim_s *anim, v2d_ui32_t frame)
{

  v2d_chain_s *chain = NULL;
  v2d_anim_s  *prev = NULL;
  v2d_anim_s  *next = NULL;

  if (!anim) { return V2D_FALSE; }

  chain = anim->link->chain;

  for (prev = (v2d_anim_s*)chain->first->data; prev; prev = v2dAnimGetNext(prev))
  {

    if (anim  == prev       ) { continue;         } /* Comparing with self? */
    if (frame == prev->frame) { return V2D_FALSE; } /* Frame already taken? */

    /* Is the frame AFTER the "previous" one, but BEFORE the one after that? */

    if (frame > prev->frame)
    {

      next = v2dAnimGetNext(prev);
      if (next == anim) { next = v2dAnimGetNext(next); }

      if      (!next              ) { break; }
      else if (frame < next->frame) { break; }

    }

  }

  /* Reposition the animation frame in the chain and update the frame ID. */

  v2dLinkInsert(chain, prev ? prev->link : NULL, anim->link);

  anim->frame = frame;

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dAnimSetDraw(v2d_anim_s *anim, v2d_state_t state, v2d_draw_s *draw)
{
  if (!anim || state >= V2D_NUM_STATES || !draw) { return V2D_FALSE; }
  anim->draw[state] = draw;
  return V2D_TRUE;
}

/*
[PUBLIC] about
*/

v2d_bool_t v2dAnimSetSound(v2d_anim_s *anim, v2d_event_t event, v2d_data_s *audio)
{

  if (!anim || event >= V2D_NUM_EVENTS) { return V2D_FALSE; }

  v2dSoundDelete(anim->sound[event]);
  anim->sound[event] = NULL;

  if (!audio) { return V2D_TRUE; }

  anim->sound[event] = v2dSoundCreate(audio);
  return anim->sound[event] ? V2D_TRUE : V2D_FALSE;

}

/*
[PUBLIC] Add/replace a series of actions for a given animation frame.
*/

v2d_bool_t v2dAnimSetScript(v2d_anim_s *anim, v2d_event_t event, v2d_script_s* (*func)(const char*, v2d_flag_t), const char *code, v2d_flag_t flags)
{

  /* Free any old script data. */

  if (!anim || event >= V2D_NUM_EVENTS) { return V2D_FALSE; }

  v2dScriptDelete(anim->script[event]);
  anim->script[event] = NULL;

  if      (!code) { return V2D_TRUE;  }
  else if (!func) { return V2D_FALSE; }

  /* Attempt to compile the new script using the compiler function provided. */

  anim->script[event] = func(code, flags);

  if (!v2dScriptValidate(anim->script[event]))
  {
    v2dScriptDelete(anim->script[event]);
    anim->script[event] = NULL;
    return V2D_FALSE;
  }

  /* Report success. */

  return V2D_TRUE;

}
