/*

FILENAME: text.c
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file text.c

@brief Provides structures and functions for drawing text.

Text objects are used to define "templates" in the same way as shapes are.
*/

#include <stdlib.h>
#include <string.h>
#include "text.h"

/*
[PUBLIC] Create a new text structure.
*/

v2d_text_s *v2dTextCreate(const char *data)
{

  v2d_text_s *text = NULL;

  text = (v2d_text_s*)malloc(sizeof(v2d_text_s));
  if (!text) { return NULL; }
  memset(text, 0, sizeof(v2d_text_s));

  text->size = 10;

  text->rgba[0] = 0;
  text->rgba[1] = 0;
  text->rgba[2] = 0;
  text->rgba[3] = 255;

  v2dTextSetData(text, data);

  return text;

}

/*
[PUBLIC] Delete a previously created text structure.
*/

void v2dTextDelete(v2d_text_s *text)
{

  if (!text) { return; }

  if (text->name) { free(text->name); }
  if (text->font) { free(text->font); }
  if (text->data) { free(text->data); }

  free(text);

}

/*
[PUBLIC] Set the font size of a text object.
*/

v2d_bool_t v2dTextSetSize(v2d_text_s *text, v2d_real_t size)
{
  if (!text || size <= 0) { return V2D_FALSE; }
  text->size = size;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the variable name to use for a text object.
*/

v2d_bool_t v2dTextSetName(v2d_text_s *text, const char *name)
{

  if (!text) { return V2D_FALSE; }

  if (text->name) { free(text->name); text->name = NULL; }

  if (name)
  {
    text->name = (char*)malloc(strlen(name) + 1);
    if (!text->name) { return V2D_FALSE; }
    strcpy(text->name, name);
  }

  return V2D_TRUE;

}

/*
[PUBLIC] Set the font of a text object.
*/

v2d_bool_t v2dTextSetFont(v2d_text_s *text, const char *font)
{

  if (!text) { return V2D_FALSE; }

  if (text->font) { free(text->font); text->font = NULL; }

  if (font)
  {
    text->font = (char*)malloc(strlen(font) + 1);
    if (!text->font) { return V2D_FALSE; }
    strcpy(text->font, font);
  }

  return V2D_TRUE;

}

/*
[PUBLIC] Set the display string of a text object.
*/

v2d_bool_t v2dTextSetData(v2d_text_s *text, const char *data)
{

  if (!text) { return V2D_FALSE; }

  if (text->data) { free(text->data); text->data = NULL; }

  if (data)
  {
    text->data = (char*)malloc(strlen(data) + 1);
    if (!text->data) { return V2D_FALSE; }
    strcpy(text->data, data);
  }

  return V2D_TRUE;

}

/*
[PUBLIC] Set the font weighting of a text object.
*/

v2d_bool_t v2dTextSetWeight(v2d_text_s *text, v2d_font_weight_t weight)
{
  if (!text) { return V2D_FALSE; }
  text->weight = weight;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the font slant style of a text object.
*/

v2d_bool_t v2dTextSetSlant(v2d_text_s *text, v2d_font_slant_t slant)
{
  if (!text) { return V2D_FALSE; }
  text->slant = slant;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the colour and alpha values of a text object.
*/

v2d_bool_t v2dTextSetColor(v2d_text_s *text, v2d_ui8_t r, v2d_ui8_t g,
                                             v2d_ui8_t b, v2d_ui8_t a)
{

  if (!text) { return V2D_FALSE; }

  text->rgba[0] = r;
  text->rgba[1] = g;
  text->rgba[2] = b;
  text->rgba[3] = a;

  return V2D_TRUE;

}
