/*

FILENAME: constants.h
AUTHOR/S: Thomas Dennis
CREATION: 8th December 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file constants.h

@brief Various constants that are too V2D-specific to go in bits/type.h
*/

#ifndef __V2D_CONTANTS_H__
#define __V2D_CONTANTS_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Enumerates the possible kinds of "chain" used by V2D.

This is stored in the chain->user_type attribute in order to "tag" the role of
a chain, so that (for example) sounds cannot be added to a chain of drawables.
*/

enum
{
  V2D_LIST_ACTION,    /**< @brief Used by scripts; Stores "action" records.  */
  V2D_LIST_ANIM,      /**< @brief Used by display items; stores animations.  */
  V2D_LIST_COLOR,     /**< @brief Used by fills; stores gradient stops.      */
  V2D_LIST_DRAW,      /**< @brief Used by stages; stores drawables.          */
  V2D_LIST_ITEM,      /**< @brief Used by stages and groups; stores items.   */
  V2D_LIST_PATH,      /**< @brief Used by shapes; stores path segments.      */
  V2D_LIST_RESOURCE,  /**< @brief Used by stages; stores external filenames. */
  V2D_LIST_SOUND      /**< @brief Used by animations; stores sound events.   */
};

#ifdef __cplusplus
}
#endif

#endif /* __V2D_CONTANTS_H__ */
