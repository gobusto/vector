/*

FILENAME: script.c
AUTHOR/S: Thomas Dennis
CREATION: 12th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file script.c

@brief Provides structures and functions for storing scripts.

The core of V2D doesn't provide any way to compile source code into "actions"
directly - it simply provides the functions required for creating v2d_script_s
structures. The idea is that user-defined callback functions will be used to
parse source code, and convert it into a list of op-codes for V2D to use.

For example, a user could provide a callback function to convert Python source
code into V2D bytecode. V2D doesn't care HOW a v2d_script_s structure is made,
so long as the data that it contains is valid.
*/

#include <stdlib.h>
#include <string.h>
#include "script.h"

/*
[PUBLIC] Create a new (empty) script structure.
*/

v2d_script_s *v2dScriptCreate(v2d_flag_t flags)
{

  v2d_script_s *script = NULL;

  script = (v2d_script_s*)malloc(sizeof(v2d_script_s));
  if (!script) { return NULL; }
  memset(script, 0, sizeof(v2d_script_s));

  script->flags = flags;
  script->action = v2dChainCreate(V2D_LIST_ACTION, script);

  if (!script->action)
  {
    v2dScriptDelete(script);
    return NULL;
  }

  return script;

}

/*
[PUBLIC] Free a previously-created script structure from memory.
*/

void v2dScriptDelete(v2d_script_s *script)
{

  if (!script) { return; }

  if (script->action)
  {
    while (v2dScriptPeek(script)) { v2dActionDelete(v2dScriptPeek(script)); }
    v2dChainDelete(script->action);
  }

  free(script);

}

/*
[PUBLIC] Determine if a script structure contains valid data or not.
*/

v2d_bool_t v2dScriptValidate(v2d_script_s *script)
{

  v2d_action_s *action = NULL;
  v2d_action_s *tmp = NULL;

  if (!script) { return V2D_FALSE; }

  /* Check each action in the script. */

  for (action = v2dScriptGetAction(script); action; action = v2dActionGetNext(action))
  {

    switch (action->opcode)
    {

      case V2D_ACTION_LABEL:

        /* A label needs to have a name associated with it. */
        if (action->num_params != 1) { return V2D_FALSE; }

        /* The label name must be unique - check previous labels to be sure. */
        for (tmp = v2dActionGetPrev(action); tmp; tmp = v2dActionGetPrev(tmp))
        {
          if (tmp->opcode == V2D_ACTION_LABEL && tmp->num_params >= 1)
          {
            if (!strcmp(action->param[0], tmp->param[0])) { return V2D_FALSE; }
          }
        }

      break;

      case V2D_ACTION_SETTARGET:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_PLAY:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_STOP:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_NEXTFRAME:
        if (action->num_params != 0) { return V2D_FALSE; }
     break;

      case V2D_ACTION_PREVFRAME:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_GOTOFRAME:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_ADD:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_SUBTRACT:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_MULTIPLY:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_DIVIDE:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_CONCATENATE:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_EQUALS:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_LESS:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_STRINGEQUALS:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_STRINGLESS:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_AND:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_OR:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_NOT:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_POP:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_PUSHNUMBER:
      case V2D_ACTION_PUSHSTRING:
        if (action->num_params < 1) { return V2D_FALSE; }
      break;

      case V2D_ACTION_ASCIITOCHAR:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_CHARTOASCII:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_TOINTEGER:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_IF:
      case V2D_ACTION_JUMP:

        /* A label must be specified. */
        if (action->num_params != 1) { return V2D_FALSE; }

        /* Ensure that the label specified actually exists. */
        for (tmp = v2dScriptGetAction(script); tmp; tmp = v2dActionGetNext(tmp))
        {
          if (tmp->opcode == V2D_ACTION_LABEL && tmp->num_params >= 1)
          {
            if (strcmp(action->param[0], tmp->param[0]) == 0) { break; }
          }
        }

        /* If the loop ends without finding the label, it doesn't exist. */
        if (!tmp) { return V2D_FALSE; }

      break;

      case V2D_ACTION_SETVARIABLE:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_GETVARIABLE:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_SETPROPERTY:
        if (action->num_params != 2) { return V2D_FALSE; }
      break;

      case V2D_ACTION_GETPROPERTY:
        if (action->num_params != 1) { return V2D_FALSE; }
      break;

      case V2D_ACTION_CLONEGROUP:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_REMOVECLONE:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_STARTDRAG:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_ENDDRAG:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_OPENURL:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_STOPSOUNDS:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_TOGGLEQUALITY:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_GETTIME:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_RANDOMNUMBER:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      case V2D_ACTION_TRACE:
        if (action->num_params != 0) { return V2D_FALSE; }
      break;

      /* Unknown */

      case V2D_NUM_ACTIONS: default: return V2D_FALSE;

    }

  }

  /* Report success. */

  return V2D_TRUE;

}

/*
[PUBLIC] Add a new "action" to a script structure.
*/

v2d_action_s *v2dActionCreate(v2d_script_s *script, v2d_opcode_t opcode)
{

  v2d_action_s *action = NULL;

  if (!script || opcode >= V2D_NUM_ACTIONS) { return NULL; }

  action = (v2d_action_s*)malloc(sizeof(v2d_action_s));
  if (!action) { return NULL; }
  memset(action, 0, sizeof(v2d_action_s));

  action->opcode = opcode;

  action->link = v2dLinkCreate(action);
  v2dLinkInsert(script->action, script->action->last, action->link);

  if (!action->link)
  {
    free(action);
    return NULL;
  }

  return action;

}

/*
[PUBLIC] Delete a previously-created action record.
*/

void v2dActionDelete(v2d_action_s *action)
{

  v2d_ui8_t i = 0;

  if (!action) { return; }

  if (action->param)
  {

    for (i = 0; i < action->num_params; i++)
    {
      if (action->param[i]) { free(action->param[i]); }
    }

    free(action->param);

  }

  v2dLinkDelete(action->link);
  free(action);

}

/*
[PUBLIC] Add a parameter to an "action" structure.
*/

v2d_bool_t v2dActionAddParam(v2d_action_s *action, const char *param)
{

  void *tmp = NULL;

  if      (!action || !param        ) { return V2D_FALSE; }
  else if (action->num_params >= 255) { return V2D_FALSE; }

  /* Resize the parameter array. */

  tmp = realloc(action->param, sizeof(char**) * (action->num_params + 1));
  if (!tmp) { return V2D_FALSE; }
  action->param = (char**)tmp;

  /* Allocate memory for the new parameter string. */

  action->param[action->num_params] = (char*)malloc(strlen(param) + 1);
  if (!action->param[action->num_params]) { return V2D_FALSE; }
  strcpy(action->param[action->num_params], param);

  /* Report success. */

  action->num_params++;

  return V2D_TRUE;

}
