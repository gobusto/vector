/*

FILENAME: sound.h
AUTHOR/S: Thomas Dennis
CREATION: 8th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file sound.h

@brief Contains structures and functions for describing timed "sound" events.

See the v2d_anim_s structure documentation for more details.
*/

#ifndef __V2D_SOUND_H__
#define __V2D_SOUND_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "resource.h"
#include "bits/chain.h"
#include "bits/type.h"
#include "constants.h"

/**
@brief This structure describes a single sound "play" event.

Animation keyframes store these structures in a linked list, so that sounds are
played once a specific time (i.e. a particular frame of animation) is reached.
*/

typedef struct
{

  v2d_data_s *data; /**< @brief Pointer to an external audio data structure. */
  v2d_ui16_t loops; /**< @brief Repetitions. 1 = play once, 2 = play twice.  */

} v2d_sound_s;

/**
@brief Create a new sound event structure.

@param audio The (external) audio data used by this sound event.
@return A new sound event structure on success, or NULL on failure.
*/

v2d_sound_s *v2dSoundCreate(v2d_data_s *audio);

/**
@brief Free a previously created sound event structure from memory.

@param sound The sound event structure to be deleted.
*/

void v2dSoundDelete(v2d_sound_s *sound);

/**
@brief Set the number of repetitions for a given sound playback event.

@param sound The sound event to be updated.
@param num_loops Repetition count. 1 = play once, 2 = play twice, etc.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dSoundSetLoops(v2d_sound_s *sound, v2d_ui16_t num_loops);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_SOUND_H__ */
