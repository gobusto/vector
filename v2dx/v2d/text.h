/*

FILENAME: text.h
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file text.h

@brief Provides structures and functions for drawing text.

Text objects are used to define "templates" in the same way as shapes are.
*/

#ifndef __V2D_TEXT_H__
#define __V2D_TEXT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/type.h"
#include "constants.h"

/**
@brief Enumerates possible font weightings for rendering text.

NOTE: Try to keep this in sync with the cairo_font_weight_t structure.
*/

typedef enum
{

  V2D_FONT_WEIGHT_NORMAL, /**< @brief Standard text. */
  V2D_FONT_WEIGHT_BOLD    /**< @brief Use bold text. */

} v2d_font_weight_t;

/**
@brief Enumerates possible font slant styles for rendering text.

NOTE: Try to keep this in sync with the cairo_font_slant_t structure.
*/

typedef enum
{

  V2D_FONT_SLANT_NORMAL,  /**< @brief Standard text.    */
  V2D_FONT_SLANT_ITALIC,  /**< @brief Use italic text.  */
  V2D_FONT_SLANT_OBLIQUE  /**< @brief Use oblique text. */

} v2d_font_slant_t;

/**
@brief Defines a text object.

@todo More detail here?
*/

typedef struct
{

  v2d_real_t size;  /**< @brief The font size to use for this object. */

  char *name; /**< @brief Variable name to use (for scripts). May be NULL. */
  char *font; /**< @brief Name of the font to use for this text. May be NULL. */
  char *data; /**< @brief Visible text string for this object. May be NULL.   */

  v2d_font_weight_t weight; /**< @brief The weight used when drawing text. */
  v2d_font_slant_t  slant;  /**< @brief The font slant style for the text. */

  v2d_rgba_t rgba;  /**< @brief The RGBA colour of the text. */

} v2d_text_s;

/**
@brief Create a new text structure.

@param data The initial text content of the new text structure.
@return A new text structure on success, or NULL on failure.
*/

v2d_text_s *v2dTextCreate(const char *data);

/**
@brief Delete a previously created text structure.

@param text The text structure to be freed.
*/

void v2dTextDelete(v2d_text_s *text);

/**
@brief Set the font size of a text object.

@param text The text to modify.
@param size The new text size value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dTextSetSize(v2d_text_s *text, v2d_real_t size);

/**
@brief Set the variable name to use for a text object.

Note that this does not have to be unique, as multiple text objects could be
controlled by a single text variable.

@param text The text to modify.
@param name The name to use for the variable that defines this text.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dTextSetName(v2d_text_s *text, const char *name);

/**
@brief Set the font of a text object.

@param text The text to modify.
@param font The name of the font to use.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dTextSetFont(v2d_text_s *text, const char *font);

/**
@brief Set the display string of a text object.

@param text The text to modify.
@param data The string to be displayed.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dTextSetData(v2d_text_s *text, const char *data);

/**
@brief Set the font weighting of a text object.

@param text The text to modify.
@param weight The new text weighting value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dTextSetWeight(v2d_text_s *text, v2d_font_weight_t weight);

/**
@brief Set the font slant style of a text object.

@param text The text to modify.
@param slant The new text slant style.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dTextSetSlant(v2d_text_s *text, v2d_font_slant_t slant);

/**
@brief Set the color and alpha values of a text object.

@param text The text to modify.
@param r The new red value.
@param g The new green value.
@param b The new blue value.
@param a The new alpha value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dTextSetColor(v2d_text_s *text, v2d_ui8_t r, v2d_ui8_t g,
                                             v2d_ui8_t b, v2d_ui8_t a);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_TEXT_H__ */
