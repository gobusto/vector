/*

FILENAME: item.c
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file item.c

@brief Provides structures and functions for creating animated display objects.

Display items are based on the shapes/text defined by v2d_draw_s structures and
may be animated using a series of key-frame structures.
*/

#include <stdlib.h>
#include <string.h>
#include "item.h"

/*
[PUBLIC] Create and initialise a new display item structure.
*/

v2d_item_s *v2dItemCreate(v2d_chain_s *chain, v2d_draw_s *draw, v2d_ui32_t frame, v2d_real_t x, v2d_real_t y, v2d_bool_t button)
{

  v2d_item_s *item = NULL;
  v2d_flag_t flags = 0;

  if      (!chain || !draw                  ) { return NULL; }
  else if (chain->user_type != V2D_LIST_ITEM) { return NULL; }

  item = (v2d_item_s*)malloc(sizeof(v2d_item_s));
  if (!item) { return NULL; }
  memset(item, 0, sizeof(v2d_item_s));

  item->link = v2dLinkCreate(item);
  v2dLinkInsert(chain, chain->last, item->link);

  item->anim = v2dChainCreate(V2D_LIST_ANIM, item);

  if (!item->link || !item->anim)
  {
    v2dItemDelete(item);
    return NULL;
  }

  flags = V2D_ANIM_FLAG_VISIBLE;
  if (button) { flags |= V2D_ANIM_FLAG_BUTTON; }

  if (!v2dAnimCreate(item->anim, draw, frame, flags))
  {
    v2dItemDelete(item);
    return NULL;
  }

  v2dMatrixPosition(v2dItemGetAnim(item)->matrix, x, y);

  return item;

}

/*
[PUBLIC] Free a previously created display item structure from memory.
*/

void v2dItemDelete(v2d_item_s *item)
{

  if (!item) { return; }

  if (item->name) { free(item->name); }

  if (item->anim)
  {
    while (v2dItemGetAnim(item)) { v2dAnimDelete(v2dItemGetAnim(item), V2D_TRUE); }
    v2dChainDelete(item->anim);
  }

  v2dLinkDelete(item->link);
  free(item);

}

/**
@brief Assign a name to a display item.

Note that the name string should not already be used by any other items in the
same linked list.

@param item The Display Item to assign a name to.
@param name The new name string to use, or NULL to remove the current name.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dItemSetName(v2d_item_s *item, const char *name)
{

  if (!item) { return V2D_FALSE; }
  if (item->name) { free(item->name); item->name = NULL; }
  if (!name) { return V2D_TRUE; }

  /* TODO: Reject names with invalid characters or names starting with '__'. */

  /* TODO: Search through the linked list to ensure that the new name is OK. */

  item->name = (char*)malloc(strlen(name) + 1);
  if (!item->name) { return V2D_FALSE; }
  strcpy(item->name, name);

  return V2D_TRUE;

}

/*
[PUBLIC] Delete any animation keyframes that reference a given drawable object.
*/

v2d_bool_t v2dGroupForgetDraw(v2d_chain_s *group, v2d_draw_s *draw)
{

  v2d_item_s *item = NULL;
  v2d_anim_s *anim = NULL;
  v2d_state_t state;

  if      (!group || !draw                  ) { return V2D_FALSE; }
  else if (group->user_type != V2D_LIST_ITEM) { return V2D_FALSE; }

  for (item = v2dGroupGetItem(group); item;)
  {

    v2d_item_s *next_item = v2dItemGetNext(item);

    for (anim = v2dItemGetAnim(item); anim;)
    {

      v2d_anim_s *next_anim = v2dAnimGetNext(anim);

      for (state = 0; state < V2D_NUM_STATES; state++)
      {
        if (anim->draw[state] == draw)
        {
          if (!v2dAnimDelete(anim, V2D_FALSE)) { v2dItemDelete(item); }
          break;
        }
      }

      anim = next_anim;

    }

    item = next_item;

  }

  return V2D_TRUE;

}

/*
[PUBLIC] Get the animation keyframe data for a specific point on the time-line.
*/

v2d_anim_s *v2dItemGetFrame(v2d_item_s *item, v2d_ui32_t frame)
{

  v2d_anim_s *anim = NULL;

  for (anim = v2dItemGetAnim(item); anim; anim = v2dAnimGetNext(anim))
  {
    if      (anim->frame == frame ) { return anim;                 }
    else if (anim->frame >= frame ) { return v2dAnimGetPrev(anim); }
    else if (!v2dAnimGetNext(anim)) { return anim;                 }
  }

  return NULL;

}

/*
[PUBLIC] Determine if an item was visible on a certain frame.
*/

v2d_bool_t v2dItemVisible(v2d_item_s *item, v2d_ui32_t frame)
{
  v2d_anim_s *anim = v2dItemGetFrame(item, frame);
  if (anim) { return (anim->flags & V2D_ANIM_FLAG_VISIBLE) ? V2D_TRUE : V2D_FALSE; }
  return V2D_FALSE;
}

/*
[PUBLIC] Count animation frames (+1, because frame #0 is a frame of animation).
*/

v2d_ui32_t v2dGroupCountFrames(v2d_chain_s *group)
{

  v2d_item_s *item = NULL;

  v2d_ui32_t total_frames = 0;
  v2d_ui32_t local_frames = 0;

  if      (!group                           ) { return 0; }
  else if (group->user_type != V2D_LIST_ITEM) { return 0; }

  for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
  {
    local_frames = ((v2d_anim_s*)item->anim->last->data)->frame + item->hold;
    if (total_frames < local_frames) { total_frames = local_frames; }
  }

  return total_frames + 1;

}

/*
[PUBLIC] Get the bounding box of a display item.
*/

v2d_bool_t v2dItemGetAABB(v2d_item_s *item, v2d_ui32_t frame, v2d_rect_s *rect)
{
  v2d_anim_s *anim = v2dItemGetFrame(item, frame);
  if (!anim) { return V2D_FALSE; }
  if (!v2dDrawGetAABB(anim->draw[V2D_STATE_IDLE], rect, 0)) { return V2D_FALSE; }
  return v2dMatrixRect(anim->matrix, rect);
}

/*
[PUBLIC] Set/unset the V2D_ITEM_FLAG_SELECTED flag for items within a group.
*/

v2d_bool_t v2dItemSelect(v2d_chain_s *group, v2d_ui32_t frame, v2d_rect_s *rect, v2d_bool_t discard)
{

  v2d_item_s *item = NULL;
  v2d_anim_s *anim = NULL;
  v2d_rect_s aabb;

  if (!group) { return V2D_FALSE; }

  for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
  {

    anim = v2dItemGetFrame(item, frame);

    if (!anim)
    {
      /* Item doesn't exist on this frame. */
      if (item->flags & V2D_ITEM_FLAG_SELECTED) { item->flags -= V2D_ITEM_FLAG_SELECTED; }
    }
    else if (rect)
    {

      if (!v2dItemGetAABB(item, frame, &aabb)) { continue; }

      if (v2dCollideAABB(rect, &aabb))
      {
        /* Select items inside the selection rectangle. */
        item->flags |= V2D_ITEM_FLAG_SELECTED;
      }
      else if (discard)
      {
        /* Discard items outside the selection rectangle. */
        if (item->flags & V2D_ITEM_FLAG_SELECTED) { item->flags -= V2D_ITEM_FLAG_SELECTED; }
      }

    }
    else
    {
      /* Select/Deselect everything. */
      item->flags |= V2D_ITEM_FLAG_SELECTED;
      if (discard && item->flags & V2D_ITEM_FLAG_SELECTED) { item->flags -= V2D_ITEM_FLAG_SELECTED; }
    }

  }

  return V2D_TRUE;

}

/*
[PUBLIC] Get the AABB of all SELECTED items within a group.
*/

v2d_bool_t v2dGetSelectedAABB(v2d_chain_s *group, v2d_ui32_t frame, v2d_rect_s *rect)
{

  v2d_item_s *item = NULL;
  v2d_rect_s aabb;
  v2d_bool_t something_is_selected = V2D_FALSE;

  if (!group || !rect) { return V2D_FALSE; }

  for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
  {

    if      (!v2dItemVisible(item, frame           )) { continue; }
    else if (!v2dItemGetAABB(item, frame, &aabb    )) { continue; }
    else if (!(item->flags & V2D_ITEM_FLAG_SELECTED)) { continue; }

    if (!something_is_selected || rect->x1 > aabb.x1) { rect->x1 = aabb.x1; }
    if (!something_is_selected || rect->x2 < aabb.x2) { rect->x2 = aabb.x2; }
    if (!something_is_selected || rect->y1 > aabb.y1) { rect->y1 = aabb.y1; }
    if (!something_is_selected || rect->y2 < aabb.y2) { rect->y2 = aabb.y2; }

    something_is_selected = V2D_TRUE;

  }

  return something_is_selected;

}
