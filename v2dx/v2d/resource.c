/*

FILENAME: resource.c
AUTHOR/S: Thomas Dennis
CREATION: 22th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file resource.c

@brief Provides functions and structures for storing lists of resource names.

This allows a project to store references to external image or sound files that
it requires.
*/

#include <stdlib.h>
#include <string.h>
#include "resource.h"

/*
[PUBLIC] Create a new data structure.
*/

v2d_data_s *v2dDataCreate(v2d_chain_s *chain, const char *name, void *user)
{

  v2d_data_s *data = NULL;

  if      (!chain || !name                      ) { return NULL; }
  else if (chain->user_type != V2D_LIST_RESOURCE) { return NULL; }

  data = (v2d_data_s*)malloc(sizeof(v2d_data_s));
  if (!data) { return NULL; }
  memset(data, 0, sizeof(v2d_data_s));

  data->link = v2dLinkCreate(data);
  v2dLinkInsert(chain, NULL, data->link);

  data->file_name = (char*)malloc(strlen(name) + 1);
  if (data->file_name) { strcpy(data->file_name, name); }

  data->user_data = user;

  if (!data->link || !data->file_name)
  {
    v2dDataDelete(data);
    return NULL;
  }

  return data;

}

/*
[PUBLIC] Free a previously created data structure from memory.
*/

void v2dDataDelete(v2d_data_s *data)
{

  if (!data) { return; }

  if (data->file_name) { free(data->file_name); }

  v2dLinkDelete(data->link);
  free(data);

}

/*
[PUBLIC] Search for an item by name.
*/

v2d_data_s *v2dDataFind(v2d_chain_s *chain, const char *name)
{

  v2d_data_s *data = NULL;

  if      (!chain || !name) { return NULL; }
  else if (!chain->first  ) { return NULL; }

  for (data = (v2d_data_s*)chain->first->data; data; data = v2dDataGetNext(data))
  {
    if (strcmp(data->file_name, name) == 0) { return data; }
  }

  return NULL;

}
