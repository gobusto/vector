/*

FILENAME: script.h
AUTHOR/S: Thomas Dennis
CREATION: 12th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file script.h

@brief Provides structures and functions for storing scripts.

The core of V2D doesn't provide any way to compile source code into "actions"
directly - it simply provides the functions required for creating v2d_script_s
structures. The idea is that user-defined callback functions will be used to
parse source code, and convert it into a list of op-codes for V2D to use.

For example, a user could provide a callback function to convert Python source
code into V2D bytecode. V2D doesn't care HOW a v2d_script_s structure is made,
so long as the data that it contains is valid.
*/

#ifndef __V2D_SCRIPT_H__
#define __V2D_SCRIPT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/chain.h"
#include "bits/type.h"
#include "constants.h"

/* Script flags. */

#define V2D_SCRIPT_FLAG_UNUSED0 0x01  /**< @brief Reserved for future use. */
#define V2D_SCRIPT_FLAG_UNUSED1 0x02  /**< @brief Reserved for future use. */
#define V2D_SCRIPT_FLAG_UNUSED2 0x04  /**< @brief Reserved for future use. */
#define V2D_SCRIPT_FLAG_UNUSED3 0x08  /**< @brief Reserved for future use. */
#define V2D_SCRIPT_FLAG_UNUSED4 0x10  /**< @brief Reserved for future use. */
#define V2D_SCRIPT_FLAG_UNUSED5 0x20  /**< @brief Reserved for future use. */
#define V2D_SCRIPT_FLAG_UNUSED6 0x40  /**< @brief Reserved for future use. */
#define V2D_SCRIPT_FLAG_UNUSED7 0x80  /**< @brief Reserved for future use. */

/**
@brief Enumerates possible "opcodes" (or "actions") that a script can perform.

This is currently based on the SWF 4 action model, with a few alterations.
*/

typedef enum
{

  V2D_ACTION_LABEL,         /**< @brief Pseudo-instruction, not based on SWF. */

  V2D_ACTION_SETTARGET,     /**< @brief Set the target of any future actions. */
  V2D_ACTION_PLAY,          /**< @brief Start playing at the current frame. */
  V2D_ACTION_STOP,          /**< @brief Stop playing at the current frame.  */
  V2D_ACTION_NEXTFRAME,     /**< @brief Go to the next animation frame.     */
  V2D_ACTION_PREVFRAME,     /**< @brief Go to the previous animation frame. */
  V2D_ACTION_GOTOFRAME,     /**< @brief Go to a specified frame index.      */

  V2D_ACTION_ADD,           /**< @brief Pop [b, a] and push [a + b]. */
  V2D_ACTION_DIVIDE,        /**< @brief Pop [b, a] and push [a / b]. */
  V2D_ACTION_MULTIPLY,      /**< @brief Pop [b, a] and push [a * b]. */
  V2D_ACTION_SUBTRACT,      /**< @brief Pop [b, a] and push [a - b]. */
  V2D_ACTION_CONCATENATE,   /**< @brief Pop ["b", "a"] and push ["a" + "b"]. */

  V2D_ACTION_EQUALS,        /**< @brief Pop [b, a] and push [a == b]. */
  V2D_ACTION_LESS,          /**< @brief Pop [b, a] and push [a <  b]. */
  V2D_ACTION_STRINGEQUALS,  /**< @brief Pop ["b", "a"] and push ["a" == "b"]. */
  V2D_ACTION_STRINGLESS,    /**< @brief Pop ["b", "a"] and push ["a" <  "b"]. */

  V2D_ACTION_AND,           /**< @brief Pop [b, a] and push [a && b]. */
  V2D_ACTION_OR,            /**< @brief Pop [b, a] and push [a || b]. */
  V2D_ACTION_NOT,           /**< @brief Pop a bool, negate it, push it back. */

  V2D_ACTION_POP,           /**< @brief Pop a single value from the stack.     */
  V2D_ACTION_PUSHNUMBER,    /**< @brief Push a set of numbers on to the stack. */
  V2D_ACTION_PUSHSTRING,    /**< @brief Push a set of values on to the stack. */

  V2D_ACTION_ASCIITOCHAR,   /**< @brief Pop a UTF-8 value + push back a char. */
  V2D_ACTION_CHARTOASCII,   /**< @brief Pop a string + push the value of s[0] */
  V2D_ACTION_TOINTEGER,     /**< @brief Pop a value + push it back as an int. */

  V2D_ACTION_IF,    /**< @brief Pop a value + jump to the label if its TRUE. */
  V2D_ACTION_JUMP,  /**< @brief Unconditionally jump to the label specified. */

  V2D_ACTION_SETVARIABLE,   /**< @brief Pop a value + name to set a variable. */
  V2D_ACTION_GETVARIABLE,   /**< @brief Pop a variable name + push its value. */

  V2D_ACTION_SETPROPERTY,   /**< @brief Pop a target + set a property value.  */
  V2D_ACTION_GETPROPERTY,   /**< @brief Pop a target + push a property value. */

  V2D_ACTION_CLONEGROUP,    /**< @brief Create a new copy of a display item.  */
  V2D_ACTION_REMOVECLONE,   /**< @brief Remove a display item from the stage. */

  V2D_ACTION_STARTDRAG,     /**< @brief Begin dragging a display item.        */
  V2D_ACTION_ENDDRAG,       /**< @brief Stop dragging a display item.         */

  V2D_ACTION_OPENURL,       /**< @brief Pop [target, url] + show in browser. */
  V2D_ACTION_STOPSOUNDS,    /**< @brief Stop any currently-playing sounds. */
  V2D_ACTION_TOGGLEQUALITY, /**< @brief Toggle display quality (high/low). */
  V2D_ACTION_GETTIME,       /**< @brief Push the run-time in milliseconds. */
  V2D_ACTION_RANDOMNUMBER,  /**< @brief Pop "range" + push random integer. */
  V2D_ACTION_TRACE,         /**< @brief Pop a value and output it somehow. */

  V2D_NUM_ACTIONS           /**< @brief Total number of actions available. */

} v2d_opcode_t;

/**
@brief This structure describes a single "action" within a script.

An "action" consists of an opcode and (optionally) a list of parameters.
*/

typedef struct
{

  v2d_link_s *link; /**< @brief Linked-list data. */

  v2d_opcode_t opcode;  /**< @brief The type of action to be performed. */

  v2d_ui8_t num_params; /**< @brief The number of parameters for this action. */
  char **param;         /**< @brief A list of parameters, all stored as text. */

} v2d_action_s;

/**
@brief This structure describes a single "script" i.e. a sequence of actions.

@todo This might need a flag to differentiate between DoInitAction and DoAction
scripts. See the Adobe SWF documentation for more information about this.

@todo Perhaps this should also contain a copy of the source code text...? A GUI
editor would have to provide some way of associating a compiled script with the
source code used to generate it, in case the script needed to be changed.
*/

typedef struct
{

  v2d_flag_t flags; /**< @brief A set of flags for modifying various things. */

  v2d_chain_s *action; /**< @brief A list of "actions" for this script. */

} v2d_script_s;

/**
@brief Get the first action record in a script.

@param p The script structure to inspect.
@return The first action record in the script, or NULL if none exists.
*/

#define v2dScriptGetAction(p) ((v2d_action_s*)((p) ? ((p)->action->first ? (p)->action->first->data : 0) : 0))

/**
@brief Get the final action record in a script.

@param p The script structure to inspect.
@return The final action record in the script, or NULL if none exists.
*/

#define v2dScriptPeek(p) ((v2d_action_s*)((p) ? ((p)->action->last  ? (p)->action->last->data  : 0) : 0))

/**
@brief Get the previous action structure in a linked-list of actions.

@param p The current action in the linked-list.
@return The previous action in the linked-list, or NULL if there isn't one.
*/

#define v2dActionGetPrev(p) ((v2d_action_s*)v2dLinkGetPrev((p) ? (p)->link : 0))

/**
@brief Get the next action structure in a linked-list of actions.

@param p The current action in the linked-list.
@return The next action in the linked-list, or NULL if there isn't one.
*/

#define v2dActionGetNext(p) ((v2d_action_s*)v2dLinkGetNext((p) ? (p)->link : 0))

/**
@brief Create a new (empty) script structure.

@param flags A set of flags for altering various aspects of the script.
@return A new script structure on success, or NULL on failure.
*/

v2d_script_s *v2dScriptCreate(v2d_flag_t flags);

/**
@brief Free a previously-created script structure from memory.

@param script The script structure to be freed.
*/

void v2dScriptDelete(v2d_script_s *script);

/**
@brief Determine if a script structure contains valid data or not.

Compilers can use this to ensure that all actions have the right number of
parameters, etc.

@todo Return something more useful than a simple boolean here so that compilers
can tell where in the script an error occurs.

@param script The script structure to be inspected.
@return V2D_TRUE on success or V2D_FALSE on failure.
*/

v2d_bool_t v2dScriptValidate(v2d_script_s *script);

/**
@brief Add a new "action" to a script structure.

The new action is added to the end of the action list, and will initially have
zero parameters.

@param script The script structure to add the new action to.
@param opcode The type of action to be added.
@return A pointer to the new "action" structure on success, or NULL on failure.
*/

v2d_action_s *v2dActionCreate(v2d_script_s *script, v2d_opcode_t opcode);

/**
@brief Delete a previously-created action record.

@param action The action to be deleted.
*/

void v2dActionDelete(v2d_action_s *action);

/**
@brief Add a parameter to an "action" structure.

@param action The "action" structure to add the parameter to.
@param param The parameter value, stored as a string of text.
@return V2D_TRUE on success or V2D_FALSE on failure.
*/

v2d_bool_t v2dActionAddParam(v2d_action_s *action, const char *param);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_SCRIPT_H__ */
