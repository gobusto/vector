/*

FILENAME: sound.c
AUTHOR/S: Thomas Dennis
CREATION: 8th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file sound.c

@brief Contains structures and functions for describing timed "sound" events.

See the v2d_anim_s structure documentation for more details.
*/

#include <stdlib.h>
#include <string.h>
#include "sound.h"

/*
[PUBLIC] Create a new sound event structure.
*/

v2d_sound_s *v2dSoundCreate(v2d_data_s *audio)
{

  v2d_sound_s *sound = NULL;

  if (!audio) { return NULL; }

  sound = (v2d_sound_s*)malloc(sizeof(v2d_sound_s));
  if (!sound) { return NULL; }
  memset(sound, 0, sizeof(v2d_sound_s));

  sound->data = audio;
  sound->loops = 1;

  return sound;

}

/*
[PUBLIC] Free a previously created sound event structure from memory.
*/

void v2dSoundDelete(v2d_sound_s *sound)
{
  if (sound) { free(sound); }
}

/*
[PUBLIC] Set the number of repetitions for a given sound playback event.
*/

v2d_bool_t v2dSoundSetLoops(v2d_sound_s *sound, v2d_ui16_t num_loops)
{
  if (!sound || num_loops < 1) { return V2D_FALSE; }
  sound->loops = num_loops;
  return V2D_TRUE;
}
