/*

FILENAME: type.c
AUTHOR/S: Thomas Dennis
CREATION: 22nd December 2012

V2D UTILITIES - THIS FILE IS PART OF THE V2D SWF/SVG PROJECT

Copyright (C) 2012 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CHANGELOG:
----------
TD 22 Dec 2012: Initial version created.
TD 24 Dec 2012: Added missing Doxygen info and a matrix shear function.
TD 26 Dec 2012: Fix v2dMatrixTranslate() to account for the current scale value.
TD 06 May 2013: Added v2dMatrixGetScale() function.

*/

/**
@file type.c

@brief Defines various common datatypes and constants.

This file also provides some useful functions for working with matrices.
*/

#include <math.h>
#include <string.h>
#include "type.h"

/*
[PUBLIC] about
*/

v2d_bool_t v2dRectRepair(v2d_rect_s *rect)
{

  v2d_real_t f;

  if (!rect) { return V2D_FALSE; }

  if (rect->x1 > rect->x2)
  {
    f        = rect->x1;
    rect->x1 = rect->x2;
    rect->x2 = f;
  }

  if (rect->y1 > rect->y2)
  {
    f        = rect->y1;
    rect->y1 = rect->y2;
    rect->y2 = f;
  }

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dCollideAABB(v2d_rect_s *a, v2d_rect_s *b)
{
  if (!a || !b) { return V2D_FALSE; }
  return a->x1 <= b->x2 && a->y1 <= b->y2 && a->x2 >= b->x1 && a->y2 >= b->y1;
}

/*
[PUBLIC] about
*/

v2d_bool_t v2dMatrixRect(v2d_matrix_t matrix, v2d_rect_s *rect)
{

  v2d_rect_s temp;

  if (!matrix || !rect) { return V2D_FALSE; }

  temp.x1 = rect->x1; temp.y1 = rect->y2;
  temp.x2 = rect->x2; temp.y2 = rect->y1;

  v2dMatrixVector(matrix, &rect->x1, &rect->y1);
  v2dMatrixVector(matrix, &rect->x2, &rect->y2);
  v2dRectRepair(rect);

  v2dMatrixVector(matrix, &temp.x1, &temp.y1);
  v2dMatrixVector(matrix, &temp.x2, &temp.y2);
  v2dRectRepair(&temp);

  if (rect->x1 > temp.x1) { rect->x1 = temp.x1; }
  if (rect->y1 > temp.y1) { rect->y1 = temp.y1; }
  if (rect->x2 < temp.x2) { rect->x2 = temp.x2; }
  if (rect->y2 < temp.y2) { rect->y2 = temp.y2; }

  return V2D_TRUE;

}

/*
[PUBLIC] Set a matrix to an initial state, with no scaling/shearing/rotation.
*/

v2d_bool_t v2dMatrixReset(v2d_matrix_t matrix)
{
  if (!matrix) { return V2D_FALSE; }
  matrix[0][0] = 1.0;
  matrix[0][1] = 0.0;
  matrix[1][0] = 0.0;
  matrix[1][1] = 1.0;
  return V2D_TRUE;
}

/*
[PUBLIC] Multiply one matrix by another. m=t*m  t is the transformation matrix
*/

v2d_bool_t v2dMatrixMultiply(v2d_matrix_t matrix, v2d_matrix_t transform)
{

  v2d_matrix_t old_matrix;

  if (!matrix || !transform) { return V2D_FALSE; }

  v2dMatrixCopy(old_matrix, matrix);

  matrix[0][0] = (old_matrix[0][0] * transform[0][0]) + (old_matrix[0][1] * transform[1][0]);
  matrix[1][0] = (old_matrix[1][0] * transform[0][0]) + (old_matrix[1][1] * transform[1][0]);
  matrix[0][1] = (old_matrix[0][0] * transform[0][1]) + (old_matrix[0][1] * transform[1][1]);
  matrix[1][1] = (old_matrix[1][0] * transform[0][1]) + (old_matrix[1][1] * transform[1][1]);
/*
  matrix[2][0] = (old_matrix[2][0] * transform[0][0]) + (old_matrix[2][1] * transform[1][0]);
  matrix[2][1] = (old_matrix[2][0] * transform[0][1]) + (old_matrix[2][1] * transform[1][1]);
*/
  return V2D_TRUE;

}

/*
[PUBLIC] Set the X/Y position values of a matrix.
*/

v2d_bool_t v2dMatrixPosition(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y)
{
  if (!matrix) { return V2D_FALSE; }
  matrix[2][0] = x;
  matrix[2][1] = y;
  return V2D_TRUE;
}

/*
[PUBLIC] Update the X/Y position of a matrix, relative to the current position.
*/

v2d_bool_t v2dMatrixTranslate(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y)
{

  if (!matrix) { return V2D_FALSE; }

  matrix[2][0] += x;
  matrix[2][1] += y;

  return V2D_TRUE;

}

/*
[PUBLIC] Update the X/Y scale of a matrix, relative to the current size.
*/

v2d_bool_t v2dMatrixScale(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y)
{
# if 0
  if (!matrix) { return V2D_FALSE; }

  matrix[0][0] *= x;
  matrix[1][0] *= x;
  matrix[0][1] *= y;
  matrix[1][1] *= y;

  return V2D_TRUE;
#else
  v2d_matrix_t transform;

  if (!matrix) { return V2D_FALSE; }

  transform[0][0] =    x;  transform[1][0] = 0.0f;
  transform[0][1] = 0.0f;  transform[1][1] =    y;

  v2dMatrixMultiply(matrix, transform);

  return V2D_TRUE;
#endif
}

/*
[PUBLIC] Update the X/Y shear of a matrix relative to the current shear values.
*/

v2d_bool_t v2dMatrixShear(v2d_matrix_t matrix, v2d_real_t x, v2d_real_t y)
{
#if 0
  v2d_matrix_t old_matrix;

  if (!matrix) { return V2D_FALSE; }
  v2dMatrixCopy(old_matrix, matrix);

  matrix[0][0] = old_matrix[0][0] + (old_matrix[0][1] * x);
  matrix[1][0] = old_matrix[1][0] + (old_matrix[1][1] * x);
  matrix[0][1] = old_matrix[0][1] + (old_matrix[0][0] * y);
  matrix[1][1] = old_matrix[1][1] + (old_matrix[1][0] * y);

  return V2D_TRUE;
#else
  v2d_matrix_t transform;

  if (!matrix) { return V2D_FALSE; }

  transform[0][0] = 1.0f;  transform[1][0] =    x;
  transform[0][1] =    y;  transform[1][1] = 1.0f;

  v2dMatrixMultiply(matrix, transform);

  return V2D_TRUE;
#endif
}

/*
[PUBLIC] Update the rotation of a matrix, relative to the current orientation.
*/

v2d_bool_t v2dMatrixRotate(v2d_matrix_t matrix, v2d_real_t angle)
{
#if 0
  v2d_matrix_t old_matrix;

  if (!matrix) { return V2D_FALSE; }

  v2dMatrixCopy(old_matrix, matrix);

  matrix[0][0] = (cos(angle)*old_matrix[0][0]) + (-sin(angle)*old_matrix[0][1]);
  matrix[1][0] = (cos(angle)*old_matrix[1][0]) + (-sin(angle)*old_matrix[1][1]);
  matrix[0][1] = (sin(angle)*old_matrix[0][0]) + ( cos(angle)*old_matrix[0][1]);
  matrix[1][1] = (sin(angle)*old_matrix[1][0]) + ( cos(angle)*old_matrix[1][1]);

  return V2D_TRUE;
#else
  v2d_matrix_t transform;

  if (!matrix) { return V2D_FALSE; }

  transform[0][0] =  cos(angle);  transform[1][0] = -sin(angle);
  transform[0][1] =  sin(angle);  transform[1][1] =  cos(angle);

  v2dMatrixMultiply(matrix, transform);

  return V2D_TRUE;
#endif
}

/*
[PUBLIC] writeme
*/

v2d_bool_t v2dMatrixCopy(v2d_matrix_t output, v2d_matrix_t source)
{

  if (!output || !source) { return V2D_FALSE; }

  output[0][0] = source[0][0];
  output[0][1] = source[0][1];
  output[1][0] = source[1][0];
  output[1][1] = source[1][1];
  output[2][0] = source[2][0];
  output[2][1] = source[2][1];

  return V2D_TRUE;

}

/*
[PUBLIC] writeme
*/

v2d_real_t v2dMatrixGetScale(v2d_matrix_t matrix, v2d_bool_t y)
{

  v2d_real_t x = 0;

  if (!matrix) { return 0; }

  if (y) { x = sqrt((matrix[1][0]*matrix[1][0])+(matrix[1][1]*matrix[1][1])); }
  else   { x = sqrt((matrix[0][0]*matrix[0][0])+(matrix[0][1]*matrix[0][1])); }

  return x;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dMatrixVector(v2d_matrix_t matrix, v2d_real_t *x, v2d_real_t *y)
{

  v2d_real_t old_x, old_y;

  if (!matrix || !x || !y) { return V2D_FALSE; }

  old_x = *x;
  old_y = *y;

  *x = matrix[0][0]*old_x + matrix[1][0]*old_y + matrix[2][0];
  *y = matrix[0][1]*old_x + matrix[1][1]*old_y + matrix[2][1];

  return V2D_TRUE;

}
