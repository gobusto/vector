/**
@file chain.c

@brief Provides structures and functions for double-ended, doubly-linked lists.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "chain.h"

/*
[PUBLIC] Create a new (empty) chain.
*/

v2d_chain_s *v2dChainCreate(long user_type, void *user_data)
{

  v2d_chain_s *chain = NULL;

  chain = (v2d_chain_s*)malloc(sizeof(v2d_chain_s));
  if (!chain) { return NULL; }
  memset(chain, 0, sizeof(v2d_chain_s));

  chain->user_type = user_type;
  chain->user_data = user_data;

  return chain;

}

/*
[PUBLIC] Free a previously-created chain structure and any links it contains.
*/

void *v2dChainDelete(v2d_chain_s *chain)
{

  void *data = NULL;

  if (!chain) { return NULL; }

  data = chain->user_data;

  while (chain->first) { v2dLinkDelete(chain->first); }

  free(chain);
  return data;

}

/*
[PUBLIC] Create a new link structure.
*/

v2d_link_s *v2dLinkCreate(void *data)
{

  v2d_link_s *link = NULL;

  link = (v2d_link_s*)malloc(sizeof(v2d_link_s));
  if (!link) { return NULL; }
  memset(link, 0, sizeof(v2d_link_s));

  link->data = data;

  return link;

}

/*
[PUBLIC] Free a previously-created link structure.
*/

void *v2dLinkDelete(v2d_link_s *link)
{

  void *data = NULL;

  if (!link) { return NULL; }

  data = link->data;

  v2dLinkUnlink(link);
  free(link);
  return data;

}

/*
[PUBLIC] Remove a link structure from the chain/links it is associated with.
*/

void v2dLinkUnlink(v2d_link_s *link)
{

  if (!link) { return; }

  if (link->chain)
  {
    if (link->chain->first == link) { link->chain->first = link->next; }
    if (link->chain->last  == link) { link->chain->last  = link->prev; }
  }

  if (link->prev) { link->prev->next = link->next; }
  if (link->next) { link->next->prev = link->prev; }

  link->chain = NULL;
  link->prev = NULL;
  link->next = NULL;

}

/*
[PUBLIC] Add a link to a chain.
*/

int v2dLinkInsert(v2d_chain_s *chain, v2d_link_s *prev, v2d_link_s *link)
{

  if (!chain || !link) { return -1; }

  if (prev)
  {
    if (prev->chain != chain || prev == link) { return 1; }
  }

  v2dLinkUnlink(link);

  link->chain = chain;
  link->prev = prev;
  link->next = prev ? prev->next : chain->first;

  if (link->prev) { link->prev->next = link; }
  if (link->next) { link->next->prev = link; }

  if (chain->first == link->next) { chain->first = link; }
  if (chain->last  == link->prev) { chain->last  = link; }

  return 0;

}

/*
[PUBLIC] Move a link along a chain (either forwards or backwards) by one place.
*/

int v2dLinkMove(v2d_link_s *link, int fwd)
{

  if      (!link              ) { return -1; }
  else if (fwd  && !link->next) { return 0;  }
  else if (!fwd && !link->prev) { return 0;  }

  return v2dLinkInsert(link->chain, fwd ? link->next : link->prev->prev, link);

}
