/*

FILENAME: resource.h
AUTHOR/S: Thomas Dennis
CREATION: 22th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file resource.h

@brief Provides functions and structures for storing lists of resource names.

This allows a project to store references to external image or sound files that
it requires.
*/

#ifndef __V2D_RESOURCE_H__
#define __V2D_RESOURCE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/type.h"
#include "bits/chain.h"
#include "constants.h"

/**
@brief General "resource" data structure, mainly used for storing file names.

V2D uses this structure for storing lists of filenames. An optional "user data"
pointer is provided so that (for example) a GUI front-end could load the bitmap
data from the file specified and store a pointer to it, thus allowing the image
to be previewed in the GUI window before a project is exported as an SWF movie.

Note that anything stored in the "user data" pointer IS NOT FREED automatically
when this structure is deleted - the GUI program mentioned in the example above
would need to free the image stucture it had created manually before destroying
the v2d_data_s structure with the "user data" pointer used to reference it.
*/

typedef struct
{

  v2d_link_s *link; /**< @brief Linked list data. */

  char *file_name;  /**< @brief The file name (and path) of this data item. */
  void *user_data;  /**< @brief Optional pointer. Can be used for anything. */

} v2d_data_s;

/**
@brief Get the next data structure in a linked-list of data structures.

@param p The current data item in the linked-list.
@return The next data item in the linked-list, or NULL if there isn't one.
*/

#define v2dDataGetNext(p) ((v2d_data_s*)v2dLinkGetNext((p) ? (p)->link : 0))

/**
@brief Create a new data structure.

@param chain The linked list to add the new structure to.
@param name The file name attribute of the new data structure.
@param user An optional (user-defined) pointer. Can be used for anything.
@return A new data structure on success, or NULL on failure.
*/

v2d_data_s *v2dDataCreate(v2d_chain_s *chain, const char *name, void *user);

/**
@brief Free a previously created data structure from memory.

Note that anything stored in the "user data" pointer IS NOT FREED automatically
when this structure is deleted.

@param data The data structure to be deleted.
*/

void v2dDataDelete(v2d_data_s *data);

/**
@brief Search for an item by name.

This function simply looks through the linked-list and checks every item name.

@param chain The linked-list to search through.
@param name The name string to search for.
@return The first data item with the same name, or NULL if no match is found.
*/

v2d_data_s *v2dDataFind(v2d_chain_s *chain, const char *name);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_RESOURCE_H__ */
