/*

FILENAME: line.h
AUTHOR/S: Thomas Dennis
CREATION: 8th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file line.h

@brief Provides structures and functions for defining line-styles.

Note that this doesn't actually define a line - just the way in which a line
should be drawn.
*/

#ifndef __V2D_LINE_H__
#define __V2D_LINE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/type.h"
#include "constants.h"

/* Constants. */

#define V2D_LINE_FLAG_SCALING 0x01  /**< @brief Use scaling strokes.     */
#define V2D_LINE_FLAG_UNUSED1 0x02  /**< @brief Reserved for future use. */
#define V2D_LINE_FLAG_UNUSED2 0x04  /**< @brief Reserved for future use. */
#define V2D_LINE_FLAG_UNUSED3 0x08  /**< @brief Reserved for future use. */
#define V2D_LINE_FLAG_UNUSED4 0x10  /**< @brief Reserved for future use. */
#define V2D_LINE_FLAG_UNUSED5 0x20  /**< @brief Reserved for future use. */
#define V2D_LINE_FLAG_UNUSED6 0x40  /**< @brief Reserved for future use. */
#define V2D_LINE_FLAG_UNUSED7 0x80  /**< @brief Reserved for future use. */

/**
@brief Enumerates possible line-cap types.

NOTE: Try to keep this in sync with the cairo_line_cap_t structure.
*/

typedef enum
{

  V2D_LINE_CAP_BUTT,  /**< @brief Lines won't be drawn with a cap on the end. */
  V2D_LINE_CAP_ROUND, /**< @brief Lines will be drawn with rounded endpoints. */
  V2D_LINE_CAP_SQUARE /**< @brief Lines will be drawn with boxlike endpoints. */

} v2d_line_cap_t;

/**
@brief Enumerates possible line-join types.

NOTE: Try to keep this in sync with the cairo_line_join_t structure.
*/

typedef enum
{

  V2D_LINE_JOIN_MITER,  /**< @brief Lines are connected by "sharp" joints.  */
  V2D_LINE_JOIN_ROUND,  /**< @brief Lines are connected by rounded joints.  */
  V2D_LINE_JOIN_BEVEL   /**< @brief Lines are connected by bevelled joints. */

} v2d_line_join_t;

/**
@brief Defines a line-style.

@todo More detail?
*/

typedef struct
{

  v2d_flag_t flags;  /**< @brief Flags to alter the line in various ways. */

  v2d_real_t width; /**< @brief The width of the line.                    */
  v2d_real_t miter; /**< @brief The miter limit value (miter joins only). */

  v2d_line_cap_t  cap;  /**< @brief The line-cap style for the line.  */
  v2d_line_join_t join; /**< @brief The line-join style for the line. */

  v2d_rgba_t rgba;  /**< @brief The RGBA colour of the line. */

} v2d_line_s;

/**
@brief Create a new line-style structure.

@param width The width of the line.
@return A new line-style structure on success, or NULL on failure.
*/

v2d_line_s *v2dLineCreate(v2d_real_t width);

/**
@brief Delete a previously created line-style structure.

@param line The line structure to be freed.
*/

void v2dLineDelete(v2d_line_s *line);

/**
@brief Set various flags for a line.

@param line The line to modify.
@param flags The new set of line flags to use.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dLineSetFlags(v2d_line_s *line, v2d_flag_t flags);

/**
@brief Set the width of a line.

The new value must be more than zero.

@param line The line to modify.
@param width The new line width value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dLineSetWidth(v2d_line_s *line, v2d_real_t width);

/**
@brief Set the miter limit of a line.

The new value must be at least 1. Note that this will only affect lines if the
line join style is set to V2D_LINE_JOIN_MITER.

@param line The line to modify.
@param miter The new line miter limit value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dLineSetMiter(v2d_line_s *line, v2d_real_t miter);

/**
@brief Set the cap style of a line.

@param line The line to modify.
@param cap The new line cap style.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dLineSetCap(v2d_line_s *line, v2d_line_cap_t cap);

/**
@brief Set the join style of a line.

@param line The line to modify.
@param join The new line join style.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dLineSetJoin(v2d_line_s *line, v2d_line_join_t join);

/**
@brief Set the colour and alpha values of a line.

@param line The line to modify.
@param r The new red value.
@param g The new green value.
@param b The new blue value.
@param a The new alpha value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dLineSetColor(v2d_line_s *line, v2d_ui8_t r, v2d_ui8_t g,
                                             v2d_ui8_t b, v2d_ui8_t a);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_LINE_H__ */
