/*

FILENAME: draw.c
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file draw.c

@brief Unifies the various displayable objects into a single structure.

This allows text and shapes to be handled as though they were the same thing.
*/

#include <stdlib.h>
#include <string.h>
#include "stage.h"
#include "draw.h"
#include "item.h"

/*
[PUBLIC] Create a new drawable object.
*/

v2d_draw_s *v2dDrawCreate(v2d_chain_s *chain, v2d_type_t type, v2d_real_t x, v2d_real_t y, const char *text)
{

  v2d_draw_s *draw = NULL;

  if      (!chain                           ) { return NULL; }
  else if (chain->user_type != V2D_LIST_DRAW) { return NULL; }

  draw = (v2d_draw_s*)malloc(sizeof(v2d_draw_s));
  if (!draw) { return NULL; }
  memset(draw, 0, sizeof(v2d_draw_s));

  draw->link = v2dLinkCreate(draw);
  v2dLinkInsert(chain, chain->last, draw->link);

  draw->type = type;

  switch(draw->type)
  {
    case V2D_TYPE_SHAPE: draw->data = v2dShapeCreate(x, y);                break;
    case V2D_TYPE_TEXT:  draw->data = v2dTextCreate(text);                 break;
    case V2D_TYPE_GROUP: draw->data = v2dChainCreate(V2D_LIST_ITEM, draw); break;
    default:             draw->data = NULL;                                break;
  }

  if (!draw->link || !draw->data)
  {
    v2dDrawDelete(draw);
    return NULL;
  }

  return draw;

}

/*
[PUBLIC] Delete a previously created drawable object.
*/

void v2dDrawDelete(v2d_draw_s *draw)
{

  if (!draw) { return; }

  /* Update any display items using this drawable, so that it isn't needed. */

  if (draw->link)
  {

    if (draw->link->chain)
    {

      v2d_stage_s *stage = (v2d_stage_s*)draw->link->chain->user_data;
      v2d_draw_s  *other = NULL;

      v2dGroupForgetDraw(stage ? stage->item : NULL, draw);

      for (other = v2dStageGetDraw(stage); other; other = v2dDrawGetNext(other))
      {
        if (other->type == V2D_TYPE_GROUP)
        { v2dGroupForgetDraw(v2dDrawAsGroup(other), draw); }
      }

    }

    v2dLinkDelete(draw->link);

  }

  /* Delete the drawable itself. */

  if (draw->data)
  {

    switch(draw->type)
    {

      case V2D_TYPE_GROUP:
      {
        v2d_chain_s *group = v2dDrawAsGroup(draw);
        while (v2dGroupGetItem(group)) { v2dItemDelete(v2dGroupGetItem(group)); }
        v2dChainDelete(group);
      }
      break;

      case V2D_TYPE_SHAPE: v2dShapeDelete(v2dDrawAsShape(draw)); break;
      case V2D_TYPE_TEXT:  v2dTextDelete(v2dDrawAsText(draw));   break;
      default:                                                   break;
    }

  }

  free(draw);

}

/*
[PUBLIC] Sort drawables so that Groups only refer to the draws before them.
*/

v2d_bool_t v2dDrawSort(v2d_chain_s *chain, v2d_bool_t update)
{

  v2d_draw_s *group = NULL;
  v2d_draw_s *draw  = NULL;
  v2d_item_s *item  = NULL;
  v2d_anim_s *anim  = NULL;
  v2d_link_s *prev  = NULL;
  v2d_bool_t okay   = V2D_FALSE;
  v2d_bool_t again = V2D_FALSE;
  v2d_state_t s = 0;

  if      (!chain                           ) { return V2D_FALSE; }
  else if (chain->user_type != V2D_LIST_DRAW) { return V2D_FALSE; }
  else if (!chain->first                    ) { return V2D_TRUE;  }

  do
  {

    again = V2D_FALSE;

    for (group = (v2d_draw_s*)chain->first->data; group;)
    {

      /*
      If the drawable is a group, loop BACKWARDS through the linked list, to see
      if all OTHER drawables referenced by it are defined earlier on.
      */

      okay = V2D_TRUE;

      if (group->type == V2D_TYPE_GROUP)
      {
        for (item = v2dGroupGetItem(v2dDrawAsGroup(group)); item; item = v2dItemGetNext(item))
        {
          for (anim = v2dItemGetAnim(item); anim; anim = v2dAnimGetNext(anim))
          {
            for (s = 0; s < V2D_NUM_STATES; s++)
            {
              okay = V2D_FALSE;
              for (draw = v2dDrawGetPrev(group); draw; draw = v2dDrawGetPrev(draw))
              {
                if (anim->draw[s] == draw) { okay = V2D_TRUE; break; }
              }
              if (!okay) { break; }
            }
            if (!okay) { break; }
          }
          if (!okay) { break; }
        }
      }

      /*
      If the current drawable is (A) a group and (B) references other drawables
      which are NOT defined earlier in the list, move it further along the list.
      */

      if (!okay)
      {

        if (!update) { return V2D_FALSE; }

        prev = group->link->next;
        if (!prev) { return V2D_FALSE; }
        v2dLinkInsert(prev->chain, prev, group->link);

        again = V2D_TRUE; /* TODO: Could simply re-start from previous item. */

      }
      else { group = v2dDrawGetNext(group); }

    }

  } while (again);

  /* Success. */

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dDrawGetAABB(v2d_draw_s *draw, v2d_rect_s *aabb, v2d_ui32_t frame)
{

  v2d_item_s *item = NULL;
  v2d_bool_t valid = V2D_TRUE;

  if (!draw || !aabb) { return V2D_FALSE; }

  switch (draw->type)
  {

    case V2D_TYPE_TEXT:
      aabb->x1 = 0;
      aabb->y1 = 0;
      if (v2dDrawAsText(draw)->data)
      {
        aabb->x2 = v2dDrawAsText(draw)->size * strlen(v2dDrawAsText(draw)->data);
        aabb->y2 = v2dDrawAsText(draw)->size;
      }
      else
      {
        aabb->x2 = v2dDrawAsText(draw)->size;
        aabb->y2 = v2dDrawAsText(draw)->size;
      }
    return V2D_TRUE;

    case V2D_TYPE_SHAPE:
      /* Just pass this through to the usual shape AABB function... */
    return v2dShapeGetAABB(v2dDrawAsShape(draw), aabb, V2D_TRUE);

    case V2D_TYPE_GROUP:
      /* Handled below... */
    break;

    default:
      /* Unknown. */
    return V2D_FALSE;

  }

  /* Groups... */

  valid = V2D_FALSE;

  for (item = v2dGroupGetItem(v2dDrawAsGroup(draw)); item; item = v2dItemGetNext(item))
  {

    if (v2dItemVisible(item, frame))
    {

      v2d_rect_s temp;

      if (!v2dItemGetAABB(item, frame, &temp)) { continue; }

      if (!valid || temp.x1 < aabb->x1) { aabb->x1 = temp.x1; }
      if (!valid || temp.y1 < aabb->y1) { aabb->y1 = temp.y1; }
      if (!valid || temp.x2 > aabb->x2) { aabb->x2 = temp.x2; }
      if (!valid || temp.y2 > aabb->y2) { aabb->y2 = temp.y2; }

      valid = V2D_TRUE;

    }

  }

  return valid;

}
