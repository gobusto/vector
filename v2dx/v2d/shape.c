/*

FILENAME: shape.c
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file shape.c

@brief about

Writeme
*/

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "shape.h"

/*
[PUBLIC] about
*/

v2d_shape_s *v2dShapeCreate(v2d_real_t x, v2d_real_t y)
{

  v2d_shape_s *shape = NULL;
  v2d_path_s  *path  = NULL;

  shape = (v2d_shape_s*)malloc(sizeof(v2d_shape_s));
  if (!shape) { return NULL; }
  memset(shape, 0, sizeof(v2d_shape_s));

  shape->path = v2dChainCreate(V2D_LIST_PATH, shape);
  if (!shape->path)
  {
    v2dShapeDelete(shape);
    return NULL;
  }

  path = v2dPathMove(NULL, x, y, 0);
  if (!path)
  {
    v2dShapeDelete(shape);
    return NULL;
  }
  v2dLinkInsert(shape->path, NULL, path->link);

  return shape;

}

/*
[PUBLIC] about
*/

void v2dShapeDelete(v2d_shape_s *shape)
{

  if (!shape) { return; }

  v2dLineDelete(shape->line);
  v2dFillDelete(shape->fill);

  if (shape->path)
  {
    while (v2dShapeGetPath(shape)) { v2dPathDelete(v2dShapeGetPath(shape)); }
    v2dChainDelete(shape->path);
  }

  free(shape);

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dShapeLineClear(v2d_shape_s *shape)
{
  if (!shape) { return V2D_FALSE; }
  v2dLineDelete(shape->line);
  shape->line = NULL;
  return V2D_TRUE;
}

/*
[PUBLIC] about
*/

v2d_bool_t v2dShapeLineColor(v2d_shape_s *shape, v2d_ui8_t r, v2d_ui8_t g, v2d_ui8_t b, v2d_ui8_t a)
{

  if (!shape) { return V2D_FALSE; }
  v2dLineDelete(shape->line);

  shape->line = v2dLineCreate(1.0);
  if (!shape->line) { return V2D_FALSE; }

  shape->line->rgba[0] = r;
  shape->line->rgba[1] = g;
  shape->line->rgba[2] = b;
  shape->line->rgba[3] = a;

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dShapeFillClear(v2d_shape_s *shape)
{
  if (!shape) { return V2D_FALSE; }
  v2dFillDelete(shape->fill);
  shape->fill = NULL;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the fill-style of a shape to a flat RGBA colour.
*/

v2d_bool_t v2dShapeFillColor(v2d_shape_s *shape, v2d_ui8_t r, v2d_ui8_t g, v2d_ui8_t b, v2d_ui8_t a)
{

  if (!shape) { return V2D_FALSE; }
  v2dFillDelete(shape->fill);

  shape->fill = v2dFillCreate(NULL);
  if (!shape->fill) { return V2D_FALSE; }

  v2dFillAsColor(shape->fill)->rgba[0] = r;
  v2dFillAsColor(shape->fill)->rgba[1] = g;
  v2dFillAsColor(shape->fill)->rgba[2] = b;
  v2dFillAsColor(shape->fill)->rgba[3] = a;

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dShapeFillImage(v2d_shape_s *shape, v2d_data_s *image)
{

  if (!shape) { return V2D_FALSE; }
  v2dFillDelete(shape->fill);
  shape->fill = NULL;
  if (!image) { return V2D_FALSE; }

  shape->fill = v2dFillCreate(image);
  return (shape->fill != NULL);

}

/*
[PUBLIC] Find the bounding box for a shape.

FIXME: This currently just checks the end-points for each path segment within
the shape. It should really check the control points on bezier curves as well.
*/

v2d_bool_t v2dShapeGetAABB(v2d_shape_s *shape, v2d_rect_s *aabb, v2d_bool_t include_line_width)
{

  v2d_path_s *path = NULL;
  v2d_real_t pen_x, pen_y;

  if (!shape || !aabb) { return V2D_FALSE; }

  pen_x = 0.0; pen_y = 0.0;
  aabb->x1 = 0; aabb->y1 = 0; aabb->x2 = 0; aabb->y2 = 0;

  for (path = v2dShapeGetPath(shape); path; path = v2dPathGetNext(path))
  {

    if (path->flags & V2D_PATH_FLAG_RELATIVE)
    {
      pen_x += path->point[V2D_END_POINT][0];
      pen_y += path->point[V2D_END_POINT][1];
    }
    else
    {
      pen_x = path->point[V2D_END_POINT][0];
      pen_y = path->point[V2D_END_POINT][1];
    }

    if (path == v2dShapeGetPath(shape) || aabb->x1 > pen_x) { aabb->x1 = pen_x; }
    if (path == v2dShapeGetPath(shape) || aabb->y1 > pen_y) { aabb->y1 = pen_y; }
    if (path == v2dShapeGetPath(shape) || aabb->x2 < pen_x) { aabb->x2 = pen_x; }
    if (path == v2dShapeGetPath(shape) || aabb->y2 < pen_y) { aabb->y2 = pen_y; }

  }

  /* Account for the line width, if necessary. */

  if (shape->line && include_line_width)
  {
    aabb->x1 -= shape->line->width / 2.0;
    aabb->y1 -= shape->line->width / 2.0;
    aabb->x2 += shape->line->width / 2.0;
    aabb->y2 += shape->line->width / 2.0;
  }

  /* Report success. */

  return V2D_TRUE;

}

/*
[PUBLIC] about
*/

v2d_bool_t v2dShapeTransform(v2d_shape_s *shape, v2d_matrix_t matrix)
{

  v2d_path_s *path = NULL;
  v2d_real_t old_ref_x = 0;
  v2d_real_t old_ref_y = 0;
  v2d_real_t new_ref_x = 0;
  v2d_real_t new_ref_y = 0;
  v2d_point_t p = 0;

  if (!shape || !matrix) { return V2D_FALSE; }

  for (path = v2dShapeGetPath(shape); path; path = v2dPathGetNext(path))
  {

    for (p = 0; p < V2D_NUM_POINTS; p++)
    {

      if (path->flags & V2D_PATH_FLAG_RELATIVE)
      { path->point[p][0] += old_ref_x; path->point[p][1] += old_ref_y; }

      if (p == V2D_END_POINT)
      {
        old_ref_x = path->point[V2D_END_POINT][0];
        old_ref_y = path->point[V2D_END_POINT][1];
      }

      v2dMatrixVector(matrix, &path->point[p][0], &path->point[p][1]);

      if (path->flags & V2D_PATH_FLAG_RELATIVE)
      { path->point[p][0] -= new_ref_x; path->point[p][1] -= new_ref_y; }

      if (p == V2D_END_POINT)
      {
        if (path->flags & V2D_PATH_FLAG_RELATIVE)
        {
          new_ref_x += path->point[V2D_END_POINT][0];
          new_ref_y += path->point[V2D_END_POINT][1];
        }
        else
        {
          new_ref_x = path->point[V2D_END_POINT][0];
          new_ref_y = path->point[V2D_END_POINT][1];
        }
      }

    }

  }

  return V2D_TRUE;

}

/*
[PUBLIC] Approximate an arc, using a series of absolute path segments.
*/

v2d_bool_t v2dShapeDrawArc(v2d_shape_s *shape, v2d_real_t cx, v2d_real_t cy, v2d_real_t rx, v2d_real_t ry, v2d_real_t spin, v2d_real_t from_angle, v2d_real_t to_angle)
{

  v2d_real_t p[2][2];
  v2d_real_t num_segments, seg, x, y, a;
  int i = 0;

  if (!shape) { return V2D_FALSE; }

  num_segments = ceil(fabs(to_angle-from_angle) * (3.5/V2D_PI));

  for (seg = 1; seg <= num_segments; seg++)
  {

    for (i = 0; i < 2; i++)
    {

      a = to_angle-from_angle;
      a *= (seg - (i*0.5)) / num_segments;
      a += from_angle;

      x = (cos(a) * rx * (1+(i*0.08)));
      y = (sin(a) * ry * (1+(i*0.08)));

      p[i][0] = cx + (x * cos(spin)) + (y * -sin(spin));
      p[i][1] = cy + (x * sin(spin)) + (y *  cos(spin));

    }

    v2dPathQuad(v2dShapeGetPathEnd(shape), p[1][0], p[1][1], p[0][0], p[0][1], 0);

  }

  return V2D_TRUE;

}
