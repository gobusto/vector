/*

FILENAME: fill.h
AUTHOR/S: Thomas Dennis
CREATION: 8th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file fill.h

@brief Provides structures and functions for defining fill-styles.

Possible fill styles include flat RGBA colors, gradients, and bitmap images.
*/

#ifndef __V2D_FILL_H__
#define __V2D_FILL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "resource.h"
#include "bits/type.h"
#include "bits/chain.h"
#include "constants.h"

/* Constants */

#define V2D_FILL_FLAG_RADIAL  0x01  /**< @brief Use a RADIAL gradient if set. */
#define V2D_FILL_FLAG_UNUSED1 0x02  /**< @brief Reserved for future use.      */
#define V2D_FILL_FLAG_UNUSED2 0x04  /**< @brief Reserved for future use.      */
#define V2D_FILL_FLAG_UNUSED3 0x08  /**< @brief Reserved for future use.      */
#define V2D_FILL_FLAG_UNUSED4 0x10  /**< @brief Reserved for future use.      */
#define V2D_FILL_FLAG_UNUSED5 0x20  /**< @brief Reserved for future use.      */
#define V2D_FILL_FLAG_UNUSED6 0x40  /**< @brief Reserved for future use.      */
#define V2D_FILL_FLAG_UNUSED7 0x80  /**< @brief Reserved for future use.      */

/**
@brief Enumerates possible "types" of fill.

NOTE: Both Gradient and Single-Color fills are defined as "COLOR" a fill here.
*/

typedef enum
{

  V2D_FILL_COLOR, /**< @brief Gradient or single-color fills. */
  V2D_FILL_IMAGE  /**< @brief Bitmap-image-based fills.       */

} v2d_fill_type_t;

/**
@brief This structure describes a single color value within a gradient fill.

NOTE: Flat RGBA fills are simply simply single-color gradients.
*/

typedef struct
{

  v2d_link_s *link; /**< @brief Linked-list data. */

  v2d_real_t stop;  /**< @brief The gradient stop position, from 0.0 to 1.0. */
  v2d_rgba_t rgba;  /**< @brief The RGBA color value for this gradient stop. */

} v2d_fill_color_s;

/**
@brief This structure describes a bitmap-image-based fill.

NOTE: No image data is stored internally; the resource must NOT be freed whilst
it is being referenced by fill.
*/

typedef struct
{

  v2d_data_s *image;  /**< @brief Points to an external pixel data resource. */
  v2d_real_t alpha;   /**< @brief Fill opacity value, from zero to one.      */

} v2d_fill_image_s;

/**
@brief The main (generic) fill structure.

This is used to store both gradients and image-based fills.
*/

typedef struct
{

  v2d_fill_type_t type;   /**< @brief Is this image- or color-based? */
  void            *data;  /**< @brief Type-specific internal data.   */

  v2d_flag_t      flags;  /**< @brief Stores various modifiers for the fill. */
  v2d_matrix_t    matrix; /**< @brief Fill transformation matrix.            */

} v2d_fill_s;

/**
@brief Get a generic fill structure as a gradient color fill.

NOTE: This returns the linked list of color stop records, NOT the first record
in that list!

@param p A pointer to a v2d_fill_s structure.
@return A linked list of color stop records on success, or NULL on failure.
*/

#define v2dFillAsChain(p) ((v2d_chain_s*)((p) ? (p)->data : 0))

/**
@brief Get the first color stop in a gradient (or single-color) fill.

@param p A pointer to a v2d_fill_s structure.
@return The first color record in the gradient on success, or NULL on failure.
*/

#define v2dFillAsColor(p) ((v2d_fill_color_s*)(v2dFillAsChain(p)->first->data))

/**
@brief Get a generic fill structure as a bitmap-image-based fill.

@param p A pointer to a v2d_fill_s structure.
@return A v2d_fill_image_s structure pointer on success, or NULL on failure.
*/

#define v2dFillAsImage(p) ((v2d_fill_image_s*)((p) ? (p)->data : 0))

/**
@brief Determine if a particular fill is simply a single color.

@param p A pointer to a v2d_fill_s structure.
@return V2D_TRUE if the fill is a simple flat color, or V2D_FALSE if not.
*/

#define v2dIsFlatRGBA(p) ((p)->type == V2D_FILL_COLOR ? (v2dFillAsChain(p)->first == v2dFillAsChain(p)->last) : 0)

/**
@brief Get the next color stop record in a gradient fill.

@param p The current color stop record.
@return The next color stop in the linked-list, or NULL if there isn't one.
*/

#define v2dFillGetNext(p) ((v2d_fill_color_s*)v2dLinkGetNext((p) ? (p)->link : 0))

/**
@brief Create a new fill structure.

If no image is provided, a simple color (gradient) fill is created instead.

@param image If provided, a bitmap fill is created with this image.
@return A new fill style structure on success, or NULL on failure.
*/

v2d_fill_s *v2dFillCreate(v2d_data_s *image);

/**
@brief Delete a previously created fill structure.

@param fill The fill structure to be deleted.
*/

void v2dFillDelete(v2d_fill_s *fill);

/**
@brief Add a new gradient stop record to a color fill.

@param fill The fill to add a new gradient stop to.
@param stop The position of the stop, from 0.0 to 1.0.
@return A new gradent color stop record on success, or NULL on failure.
*/

v2d_fill_color_s *v2dFillColorCreate(v2d_fill_s *fill, v2d_real_t stop);

/**
@brief Remove a gradient stop record from a color fill.

NOTE: If the gradient only has one color record, then it cannot be deleted.

@param grad The gradient stop to be removed from the color fill.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dFillColorDelete(v2d_fill_color_s *grad);

/**
@brief Move a gradient color stop to a different position within the fill.

Values are automatically clamped to 0.0 and 1.0.

@param grad The gradient stop record to be updated.
@param stop Th new gradient stop position.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dFillColorSetStop(v2d_fill_color_s *grad, v2d_real_t stop);

/**
@brief Change the color of a gradient stop record within a color fill.

@param grad The gradient stop record to be updated.
@param r The new red value.
@param g The new green value.
@param b The new blue value.
@param a The new alpha value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dFillColorSetRGBA(v2d_fill_color_s *grad,
                          v2d_ui8_t r, v2d_ui8_t g, v2d_ui8_t b, v2d_ui8_t a);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_FILL_H__ */
