/*

FILENAME: stage.h
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file stage.h

@brief Provides functions for creating and interacting with "stage" structures.

A "stage" can be thought of as self-contained "project" structure, and is used
to store the various things (images, sounds, text, etc.) that make up a scene.
*/

#ifndef __V2D_STAGE_H__
#define __V2D_STAGE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "resource.h"
#include "draw.h"
#include "item.h"
#include "bits/chain.h"
#include "bits/type.h"
#include "constants.h"

/**
@brief The main "stage" structure, used to contain everything else.

A "stage" can be thought of as self-contained "project" structure, and is used
to store the various things (images, sounds, text, etc.) that make up a scene.
*/

typedef struct
{

  char *title;    /**< @brief Project name. Used as title text for SVG files. */
  char *metadata; /**< @brief RDF metadata, suitable for SWF and SVG files. */

  v2d_real_t width;   /**< @brief Stage width.  */
  v2d_real_t height;  /**< @brief Stage height. */
  v2d_real_t fps;     /**< @brief Frames-per-Second value for animations. */
  v2d_rgba_t rgba;    /**< @brief Stage background color. */

  v2d_chain_s *image; /**< @brief Static images, used for image-based fills.  */
  v2d_chain_s *audio; /**< @brief Sound effects or music.                     */
  v2d_chain_s *embed; /**< @brief General data for SWF DefineBinaryData tags. */

  v2d_chain_s *draw;  /**< @brief A "library" of things which can be drawn.  */
  v2d_chain_s *item;  /**< @brief Visible instances of predefined drawables. */

} v2d_stage_s;

/**
@brief Get the first Image data structure in the linked list of a stage.

@param p A pointer to a stage structure.
@return The first image item in the linked list on success, or NULL on failure.
*/

#define v2dStageGetImage(p) ((v2d_data_s*)((p) ? ((p)->image->first ? (p)->image->first->data : 0) : 0))

/**
@brief Get the first Audio data structure in the linked list of a stage.

@param p A pointer to a stage structure.
@return The first audio item in the linked list on success, or NULL on failure.
*/

#define v2dStageGetAudio(p) ((v2d_data_s*)((p) ? ((p)->audio->first ? (p)->audio->first->data : 0) : 0))

/**
@brief Get the first Embedded data structure in the linked list of a stage.

@param p A pointer to a stage structure.
@return The first data item in the linked list on success, or NULL on failure.
*/

#define v2dStageGetEmbed(p) ((v2d_data_s*)((p) ? ((p)->embed->first ? (p)->embed->first->data : 0) : 0))

/**
@brief Get the first Drawable in the linked list of a stage.

@param p A pointer to a stage structure.
@return The first drawable in the linked list on success, or NULL on failure.
*/

#define v2dStageGetDraw(p) ((v2d_draw_s*)((p) ? ((p)->draw->first  ? (p)->draw->first->data  : 0) : 0))

/**
@brief Create a new, blank "stage" structure.

@param w The width of the stage.
@param h The height of the stage.
@param fps The animation playback rate in frames-per-second.
@return A new stage structure on success, or NULL on failure.
*/

v2d_stage_s *v2dStageCreate(v2d_real_t w, v2d_real_t h, v2d_real_t fps);

/**
@brief Free a previously created "stage" structure from memory.

This will also free any images/drawables/etc. that the stage contains.

@param stage The stage to be deleted.
*/

void v2dStageDelete(v2d_stage_s *stage);

/**
@brief Set the "title" text of a stage.

This is the text shown as the "page title" of SVG or HTML5 files in a browser.

@param stage The stage to be updated.
@param title The new title text value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dStageSetTitle(v2d_stage_s *stage, const char *title);

/**
@brief Set the RDF metadata text of a stage.

RDF metadata can be used for either SVG or SWF files. See the following links:

http://www.w3.org/TR/SVG/metadata.html

http://www.adobe.com/products/xmp/

@param stage The stage to be updated.
@param metadata A string of RDF-compliant metadata text.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dStageSetMetaData(v2d_stage_s *stage, const char *metadata);

/**
@brief Resize the display area or a stage.

@param stage The stage to be updated.
@param w The new stage width.
@param h The new stage height.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dStageSetSize(v2d_stage_s *stage, v2d_real_t w, v2d_real_t h);

/**
@brief Change the frames-per-second playback rate of a stage.

@param stage The stage to be updated.
@param fps The new FPS value.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dStageSetFPS(v2d_stage_s *stage, v2d_real_t fps);

/**
@brief Change the background color of a stage.

NOTE: The alpha value may be ignored by some formats (i.e. SWF) when exported.

@param stage The stage to be updated.
@param r The new red value.
@param g The new green value.
@param b The new blue value.
@param a The new alpha value. Zero is fully transparent, one is fully opaque.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dStageSetColor(v2d_stage_s *stage, v2d_ui8_t r, v2d_ui8_t g,
                                                v2d_ui8_t b, v2d_ui8_t a);

/**
@brief Merge the contents of one stage into another.

Note that stage_b will be deleted if this function succeeds so don't attempt to
use or free it afterwards!

@param stage_a The "main" stage.
@param stage_b The stage to be merged into the main stage.
@return The draw representing the merged scene on success, or NULL on failure.
*/

v2d_draw_s *v2dStageMerge(v2d_stage_s *stage_a, v2d_stage_s *stage_b);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_STAGE_H__ */
