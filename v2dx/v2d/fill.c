/*

FILENAME: fill.c
AUTHOR/S: Thomas Dennis
CREATION: 8th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file fill.c

@brief Provides structures and functions for defining fill-styles.

Possible fill styles include flat RGBA colors, gradients, and bitmap images.
*/

#include <stdlib.h>
#include <string.h>
#include "fill.h"

/**
@brief [INTERNAL] Remove a gradient stop record from a color fill.

Unlike the public v2dFillColorDelete() function, this function allows the final
color stop to be removed from a gradient. It's used by v2dFillDelete() in order
to delete every record within a gradient fill.

@param grad The gradient stop to be removed from the color fill.
*/

static void v2dFillColorDelete_internal(v2d_fill_color_s *grad)
{
  if (!grad) { return; }
  v2dLinkDelete(grad->link);
  free(grad);
}

/*
[PUBLIC] Create a new fill structure.
*/

v2d_fill_s *v2dFillCreate(v2d_data_s *image)
{

  v2d_fill_s *fill = NULL;

  fill = (v2d_fill_s*)malloc(sizeof(v2d_fill_s));
  if (!fill) { return NULL; }
  memset(fill, 0, sizeof(v2d_fill_s));

  v2dMatrixReset(fill->matrix);

  if (image)
  {

    fill->type = V2D_FILL_IMAGE;
    fill->data = malloc(sizeof(v2d_fill_image_s));

    if (fill->data)
    {
      memset(fill->data, 0, sizeof(v2d_fill_image_s));
      v2dFillAsImage(fill)->image = image;
      v2dFillAsImage(fill)->alpha = 1.0;
    }

  }
  else
  {
    fill->type = V2D_FILL_COLOR;
    fill->data = v2dChainCreate(V2D_LIST_COLOR, fill);
    v2dFillColorCreate(fill, 0.0);
  }

  if (!fill->data)
  {
    v2dFillDelete(fill);
    return NULL;
  }

  return fill;

}

/*
[PUBLIC] Delete a previously created fill structure.
*/

void v2dFillDelete(v2d_fill_s *fill)
{

  if (!fill) { return; }

  if (fill->type == V2D_FILL_COLOR)
  {
    while (v2dFillAsChain(fill)->first) { v2dFillColorDelete_internal(v2dFillAsColor(fill)); }
    v2dChainDelete(v2dFillAsChain(fill));
  }
  else if (fill->data) { free(fill->data); }

  free(fill);

}

/*
[PUBLIC] Add a new gradient stop record to a color fill.
*/

v2d_fill_color_s *v2dFillColorCreate(v2d_fill_s *fill, v2d_real_t stop)
{

  v2d_fill_color_s *grad = NULL;

  /* A new gradient-stop record MUST be added to a pre-existing "color" fill. */

  if      (!fill                       ) { return NULL; }
  else if (fill->type != V2D_FILL_COLOR) { return NULL; }

  /* Allocate the basic shape-record structure. */

  grad = (v2d_fill_color_s*)malloc(sizeof(v2d_fill_color_s));
  if (!grad) { return NULL; }
  memset(grad, 0, sizeof(v2d_fill_color_s));

  /* Insert the new stop at the correct position. */

  grad->link = v2dLinkCreate(grad);
  v2dLinkInsert(v2dFillAsChain(fill), NULL, grad->link);

  if (!grad->link)
  {
    v2dFillColorDelete(grad);
    return NULL;
  }

  v2dFillColorSetStop(grad, stop);

  /* Initialise the various attributes. */

  grad->rgba[0] = 0;
  grad->rgba[1] = 127;
  grad->rgba[2] = 255;
  grad->rgba[3] = 255;

  /* Return the new gradient-stop record. */

  return grad;

}

/*
[PUBLIC] Remove a gradient stop record from a color fill.
*/

v2d_bool_t v2dFillColorDelete(v2d_fill_color_s *grad)
{

  if (grad)
  {
    if (grad->link)
    {
      if (!grad->link->prev && !grad->link->next) { return V2D_FALSE; }
    }
  }

  v2dFillColorDelete_internal(grad);

  return V2D_TRUE;

}

/*
[PUBLIC] Move a gradient color stop to a different position within the fill.
*/

v2d_bool_t v2dFillColorSetStop(v2d_fill_color_s *grad, v2d_real_t stop)
{

  v2d_chain_s *chain = NULL;
  v2d_fill_color_s  *prev = NULL;
  v2d_fill_color_s  *next = NULL;

  if (!grad) { return V2D_FALSE; }

  if      (stop < 0.00) { grad->stop = 0.00; }
  else if (stop > 1.00) { grad->stop = 1.00; }
  else                  { grad->stop = stop; }

  chain = grad->link->chain;

  for (prev = (v2d_fill_color_s*)chain->first->data; prev; prev = v2dFillGetNext(prev))
  {

    if (grad == prev) { continue; }

    if (grad->stop > prev->stop)
    {

      next = v2dFillGetNext(prev);
      if (next == grad) { next = v2dFillGetNext(next); }

      if      (!next                  ) { break; }
      else if (grad->stop < next->stop) { break; }

    }

  }

  v2dLinkInsert(chain, prev ? prev->link : NULL, grad->link);

  return V2D_TRUE;

}

/*
[PUBLIC] Change the color of a gradient stop record within a color fill.
*/

v2d_bool_t v2dFillColorSetRGBA(v2d_fill_color_s *grad,
                          v2d_ui8_t r, v2d_ui8_t g, v2d_ui8_t b, v2d_ui8_t a)
{
  if (!grad) { return V2D_FALSE; }
  grad->rgba[0] = r;
  grad->rgba[1] = g;
  grad->rgba[2] = b;
  grad->rgba[3] = a;
  return V2D_TRUE;
}
