/*

FILENAME: path.c
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file path.c

@brief about

Writeme
*/

#include <stdlib.h>
#include <string.h>
#include "path.h"

/*
[PUBLIC] about
*/

v2d_path_s *v2dPathCreate(v2d_path_s *prev, v2d_move_t type,
                          v2d_real_t cx1, v2d_real_t cy1,
                          v2d_real_t cx2, v2d_real_t cy2,
                          v2d_real_t x, v2d_real_t y, v2d_flag_t flags)
{

  v2d_path_s *path = NULL;

  path = (v2d_path_s*)malloc(sizeof(v2d_path_s));
  if (!path) { return NULL; }
  memset(path, 0, sizeof(v2d_path_s));

  path->link = v2dLinkCreate(path);
  if (prev) { v2dLinkInsert(prev->link->chain, prev->link, path->link); }

  if (!path->link)
  {
    v2dPathDelete(path);
    return NULL;
  }

  path->type  = type;
  path->flags = flags;

  path->point[V2D_CONTROL_1][0] = cx1;
  path->point[V2D_CONTROL_1][1] = cy1;
  path->point[V2D_CONTROL_2][0] = cx2;
  path->point[V2D_CONTROL_2][1] = cy2;
  path->point[V2D_END_POINT][0] = x;
  path->point[V2D_END_POINT][1] = y;

  return path;

}

/*
[PUBLIC] about
*/

void v2dPathDelete(v2d_path_s *path)
{
  if (!path) { return; }
  v2dLinkDelete(path->link);
  free(path);
}
