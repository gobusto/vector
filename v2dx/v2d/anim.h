/*

FILENAME: anim.h
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file anim.h

@brief Provides structures and functions for animating display items.

Display items are based on the shapes/text defined by v2d_draw_s structures and
may be animated using a series of key-frame structures.
*/

#ifndef __V2D_ANIM_H__
#define __V2D_ANIM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "draw.h"
#include "sound.h"
#include "script.h"
#include "bits/chain.h"
#include "bits/type.h"
#include "constants.h"

/* Constants. */

#define V2D_DEFAULT 0 /**< @brief about */

#define V2D_ANIM_FLAG_VISIBLE 0x01  /**< @brief If set, items aren't shown.  */
#define V2D_ANIM_FLAG_BUTTON  0x02  /**< @brief If set, represents a button. */
#define V2D_ANIM_FLAG_UNUSED2 0x04  /**< @brief Reserved for future use. */
#define V2D_ANIM_FLAG_UNUSED3 0x08  /**< @brief Reserved for future use. */
#define V2D_ANIM_FLAG_UNUSED4 0x10  /**< @brief Reserved for future use. */
#define V2D_ANIM_FLAG_UNUSED5 0x20  /**< @brief Reserved for future use. */
#define V2D_ANIM_FLAG_UNUSED6 0x40  /**< @brief Reserved for future use. */
#define V2D_ANIM_FLAG_UNUSED7 0x80  /**< @brief Reserved for future use. */

/**
@brief about

NOTE: SWF export currently assumes that this exact order will be used!
*/

typedef enum
{

  V2D_STATE_IDLE, /**< @brief about */
  V2D_STATE_OVER, /**< @brief about */
  V2D_STATE_DOWN, /**< @brief about */
  V2D_NUM_STATES  /**< @brief about */

} v2d_state_t;

/**
@brief about

NOTE: SWF export currently assumes that this exact order will be used!
*/

typedef enum
{

  V2D_EVENT_MOUSEOVER,  /**< @brief about */
  V2D_EVENT_MOUSEEXIT,  /**< @brief about */
  V2D_EVENT_PRESS,      /**< @brief about */
  V2D_EVENT_RELEASE,    /**< @brief about */
  V2D_NUM_EVENTS        /**< @brief about */

} v2d_event_t;

/**
@brief This structure describes a single key-frame of animation.

Display items use a list of these structures to store animation data.
*/

typedef struct
{

  v2d_link_s *link; /**< @brief Linked-list data. */

  v2d_ui32_t frame; /**< @brief The position of this key-frame in time.   */
  v2d_flag_t flags; /**< @brief Various flags describing this key-frame.  */

  v2d_matrix_t matrix;  /**< @brief Transformation matrix for this frame.   */

  v2d_draw_s *draw[V2D_NUM_STATES]; /**< @brief about */
  v2d_sound_s *sound[V2D_NUM_EVENTS]; /**< @brief writeme.  */
  v2d_script_s *script[V2D_NUM_EVENTS]; /**< @brief Script opcodes to be run by this frame. */

} v2d_anim_s;

/**
@brief Get the previous animation frame in a linked-list of animation frames.

@param p The current animation frame in the linked-list.
@return The previous frame in the linked-list, or NULL if there isn't one.
*/

#define v2dAnimGetPrev(p) ((v2d_anim_s*)v2dLinkGetPrev((p) ? (p)->link : 0))

/**
@brief Get the next animation frame in a linked-list of animation frames.

@param p The current animation frame in the linked-list.
@return The next frame in the linked-list, or NULL if there isn't one.
*/

#define v2dAnimGetNext(p) ((v2d_anim_s*)v2dLinkGetNext((p) ? (p)->link : 0))

/**
@brief Create a new animation keyframe.

The transformation matrix of the new frame will initially be the same as the
frame before it, if one exists. If not, the next frame in the list is copied
instead. If no next/previous frame exists, then the new frame will simply be
set to use an identity matrix.

@param chain A linked list of animation structures.
@param draw The "Drawable" to use when rendering this Display Item on-screen.
@param frame The position at which to create a new keyframe.
@param flags Animation flags.
@return A new animation frame structure on success, or NULL on failure.
*/

v2d_anim_s *v2dAnimCreate(v2d_chain_s *chain, v2d_draw_s *draw, v2d_ui32_t frame, v2d_flag_t flags);

/**
@brief Free a single animation frame from memory.

This will also free any sound events or script data associated with the frame.

@param anim The animation frame to be freed.
@param force If V2D_FALSE, don't allow the last remaining anim to be deleted.
@return V2D_TRUE on success, or V2D_FALSE if the anim couldn't be deleted.
*/

v2d_bool_t v2dAnimDelete(v2d_anim_s *anim, v2d_bool_t force);

/**
@brief Re-position an animation keyframe on the timeline.

@param anim The animation keyframe to be repositioned.
@param frame The new position of the animation keyframe on the timeline.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dAnimSetFrame(v2d_anim_s *anim, v2d_ui32_t frame);

/**
@brief writeme

@param anim about
@param state about
@param draw about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dAnimSetDraw(v2d_anim_s *anim, v2d_state_t state, v2d_draw_s *draw);

/**
@brief writeme

@param anim about
@param event about
@param audio about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dAnimSetSound(v2d_anim_s *anim, v2d_event_t event, v2d_data_s *audio);

/**
@brief Add/replace a series of actions for a given animation frame.

An existing script can be removed by setting "code" to NULL.

@param anim The animation keyframe to have a script attached to it.
@param event about
@param func A script compiler function, which converts text into a code object.
@param code Source code. The language depends on the compiler function given.
@param flags Compiler flags, passed to the given function along with the code.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dAnimSetScript(v2d_anim_s *anim, v2d_event_t event, v2d_script_s* (*func)(const char*, v2d_flag_t), const char *code, v2d_flag_t flags);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_ANIM_H__ */
