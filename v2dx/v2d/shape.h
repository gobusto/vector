/*

FILENAME: shape.h
AUTHOR/S: Thomas Dennis
CREATION: 9th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file shape.h

@brief about

Writeme
*/

#ifndef __V2D_SHAPE_H__
#define __V2D_SHAPE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "line.h"
#include "fill.h"
#include "path.h"
#include "bits/type.h"
#include "constants.h"

/* Constants. */

#define V2D_SHAPE_FLAG_CLOSE   0x01 /**< @brief about */
#define V2D_SHAPE_FLAG_EVENODD 0x02 /**< @brief about */
#define V2D_SHAPE_FLAG_UNUSED2 0x04 /**< @brief about */
#define V2D_SHAPE_FLAG_UNUSED3 0x08 /**< @brief about */
#define V2D_SHAPE_FLAG_UNUSED4 0x10 /**< @brief about */
#define V2D_SHAPE_FLAG_UNUSED5 0x20 /**< @brief about */
#define V2D_SHAPE_FLAG_UNUSED6 0x40 /**< @brief about */
#define V2D_SHAPE_FLAG_UNUSED7 0x80 /**< @brief about */

/**
@brief about

writeme
*/

typedef struct
{

  v2d_flag_t  flags;  /**< @brief about */
  v2d_line_s  *line;  /**< @brief about */
  v2d_fill_s  *fill;  /**< @brief about */
  v2d_chain_s *path;  /**< @brief about */

} v2d_shape_s;

/**
@brief about

writeme

@param p about
@return writeme
*/

#define v2dShapeGetPath(p) ((v2d_path_s*)((p) ? ((p)->path->first ? (p)->path->first->data : 0) : 0))

/**
@brief about

writeme

@param p about
@return writeme
*/

#define v2dShapeGetPathEnd(p) ((v2d_path_s*)((p) ? ((p)->path->last ? (p)->path->last->data : 0) : 0))

/**
@brief about

writeme

@param x The initial pen X position.
@param y The initial pen Y position.
@return writeme
*/

v2d_shape_s *v2dShapeCreate(v2d_real_t x, v2d_real_t y);

/**
@brief about

writeme

@param shape about
*/

void v2dShapeDelete(v2d_shape_s *shape);

/**
@brief about

writeme

@param shape about
@return writeme
*/

v2d_bool_t v2dShapeLineClear(v2d_shape_s *shape);

/**
@brief about

writeme

@param shape about
@param r about
@param g about
@param b about
@param a about
@return writeme
*/

v2d_bool_t v2dShapeLineColor(v2d_shape_s *shape, v2d_ui8_t r, v2d_ui8_t g, v2d_ui8_t b, v2d_ui8_t a);

/**
@brief about

writeme

@param shape about
@return writeme
*/

v2d_bool_t v2dShapeFillClear(v2d_shape_s *shape);

/**
@brief about

writeme

@param shape about
@param r about
@param g about
@param b about
@param a about
@return writeme
*/

v2d_bool_t v2dShapeFillColor(v2d_shape_s *shape, v2d_ui8_t r, v2d_ui8_t g, v2d_ui8_t b, v2d_ui8_t a);

/**
@brief about

writeme

@param shape about
@param image about
@return writeme
*/

v2d_bool_t v2dShapeFillImage(v2d_shape_s *shape, v2d_data_s *image);

/**
@brief Get the AABB of a shape.

@param shape The shape to get the AABB of.
@param aabb The rect structure to store the AABB in.
@param include_line_width If true, the line width is accounted for in the AABB.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dShapeGetAABB(v2d_shape_s *shape, v2d_rect_s *aabb, v2d_bool_t include_line_width);

/**
@brief about

writeme

@param shape about
@param matrix about
@return writeme
*/

v2d_bool_t v2dShapeTransform(v2d_shape_s *shape, v2d_matrix_t matrix);

/**
@brief Approximate an arc, using a series of absolute path segments.

This is used to draw SVG circles, ellipses, rounded rectangles, and arc paths.

@todo Are cx / cy necessary? They could be calculated by checking if rx/ry are
negative...

@param shape The shape to be updated.
@param cx The central X position of the ellipse.
@param cy The central Y position of the ellipse.
@param rx The horizontal radius of the ellipse.
@param ry The vertical radius of the ellipse.
@param spin Rotation transformation angle in radians.
@param from_angle The arc start point.
@param to_angle The arc end point.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dShapeDrawArc(v2d_shape_s *shape, v2d_real_t cx, v2d_real_t cy, v2d_real_t rx, v2d_real_t ry, v2d_real_t spin, v2d_real_t from_angle, v2d_real_t to_angle);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_SHAPE_H__ */
