/*

FILENAME: line.c
AUTHOR/S: Thomas Dennis
CREATION: 8th December 2012

Copyright (C) 2012-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file line.c

@brief Provides structures and functions for defining line-styles.

Note that this doesn't actually define a line - just the way in which a line
should be drawn.
*/

#include <stdlib.h>
#include <string.h>
#include "line.h"

/*
[PUBLIC] Create a new line-style structure.
*/

v2d_line_s *v2dLineCreate(v2d_real_t width)
{

  v2d_line_s *line = NULL;

  if (width <= 0) { return NULL; }

  line = (v2d_line_s*)malloc(sizeof(v2d_line_s));
  if (!line) { return NULL; }
  memset(line, 0, sizeof(v2d_line_s));

  line->flags = V2D_LINE_FLAG_SCALING;

  line->width = width;
  line->miter = 4.0;  /* This is based on what SVG 1.1 images use by default. */

  line->cap  = V2D_LINE_CAP_ROUND;  /* This is what SWF versions under 8 use. */
  line->join = V2D_LINE_JOIN_ROUND; /* This is what SWF versions under 8 use. */

  line->rgba[0] = 0;
  line->rgba[1] = 0;
  line->rgba[2] = 0;
  line->rgba[3] = 255;

  return line;

}

/*
[PUBLIC] Delete a previously created line style structure.
*/

void v2dLineDelete(v2d_line_s *line)
{
  if (line) { free(line); }
}

/*
[PUBLIC] Set various flags for a line.
*/

v2d_bool_t v2dLineSetFlags(v2d_line_s *line, v2d_flag_t flags)
{
  if (!line) { return V2D_FALSE; }
  line->flags = flags;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the width of a line.
*/

v2d_bool_t v2dLineSetWidth(v2d_line_s *line, v2d_real_t width)
{
  if (!line || width <= 0) { return V2D_FALSE; }
  line->width = width;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the miter limit of a line.
*/

v2d_bool_t v2dLineSetMiter(v2d_line_s *line, v2d_real_t miter)
{
  if (!line || miter < 1) { return V2D_FALSE; }
  line->miter = miter;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the cap style of a line.
*/

v2d_bool_t v2dLineSetCap(v2d_line_s *line, v2d_line_cap_t cap)
{
  if (!line) { return V2D_FALSE; }
  line->cap = cap;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the join style of a line.
*/

v2d_bool_t v2dLineSetJoin(v2d_line_s *line, v2d_line_join_t join)
{
  if (!line) { return V2D_FALSE; }
  line->join = join;
  return V2D_TRUE;
}

/*
[PUBLIC] Set the colour and alpha values of a line.
*/

v2d_bool_t v2dLineSetColor(v2d_line_s *line, v2d_ui8_t r, v2d_ui8_t g,
                                             v2d_ui8_t b, v2d_ui8_t a)
{

  if (!line) { return V2D_FALSE; }

  line->rgba[0] = r;
  line->rgba[1] = g;
  line->rgba[2] = b;
  line->rgba[3] = a;

  return V2D_TRUE;

}
