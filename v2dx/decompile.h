/*

FILENAME: decompile.h
AUTHOR/S: Thomas Dennis
CREATION: 18th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file decompile.h

@brief Provides utilities for decompiling scripts back into source-code format.

This is mainly useful for debugging, or as a last resort if the original source
code has been lost.
*/

#ifndef __V2D_DECOMPILE_H__
#define __V2D_DECOMPILE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "v2d/script.h"

/**
@brief Decompile a script into an assembly-like list of instructions.

The output is compatible with the v2dCompileASM() function.

@param out The file to write the decompiled source code to.
@param script The script to decompile.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dxDecompileASM(FILE *out, v2d_script_s *script);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_DECOMPILE_H__ */
