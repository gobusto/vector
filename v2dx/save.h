/*

FILENAME: save.h
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 by Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file save.h

@brief Provides functions for exporting projects to various file formats.

@todo Re-add HTML5 export once SVG and SWF are a bit more complete.
*/

#ifndef __V2D_SAVE_H__
#define __V2D_SAVE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "v2d/stage.h"

/* Export flags: General. */

#define V2DX_SAVE_FLAG_ZLIB 0x01 /**< @brief ZLIB compression. */

/* Export flags: SVG-specific. */

#define V2DX_SAVE_FLAG_SVG_EMBED    0x02 /**< @brief SVG: Embed media files. */
#define V2DX_SAVE_FLAG_SVG_BACKDROP 0x04 /**< @brief SVG: "rect" background. */
#define V2DX_SAVE_FLAG_SVG_VER_1_2  0x08 /**< @brief SVG: Use 1.2+ features. */

#define V2DX_SAVE_FLAG_SVG_UNUSED1  0x10 /**< @brief Reserved - Do not use.  */
#define V2DX_SAVE_FLAG_SVG_UNUSED2  0x20 /**< @brief Reserved - Do not use.  */
#define V2DX_SAVE_FLAG_SVG_UNUSED3  0x40 /**< @brief Reserved - Do not use.  */
#define V2DX_SAVE_FLAG_SVG_UNUSED4  0x80 /**< @brief Reserved - Do not use.  */

/* Export flags: SWF-specific. */

#define V2DX_SAVE_FLAG_SWF_LZMA       0x02 /**< @brief SWF: LZMA compression. */
#define V2DX_SAVE_FLAG_SWF_TELEMETRY  0x04 /**< @brief SWF: Enable telemetry. */
#define V2DX_SAVE_FLAG_SWF_USENETWORK 0x08 /**< @brief SWF: Network usage OK. */

#define V2DX_SAVE_FLAG_SWF_AVM2       0x10 /**< @brief SWF: Use AVM2 opcodes. */
#define V2DX_SAVE_FLAG_SWF_PROTECTED  0x20 /**< @brief SWF: Prevent import.   */
#define V2DX_SAVE_FLAG_SWF_UNUSED3    0x40 /**< @brief Reserved - Do not use. */
#define V2DX_SAVE_FLAG_SWF_UNUSED4    0x80 /**< @brief Reserved - Do not use. */

/**
@brief Export a project in Scalable Vector Graphics (SVG) format.

ZLIB-compressed files should use a .SVGZ extension instead.

@param stage The project to be exported.
@param name The name and path for the output file.
@param flags Flags to alter the output in various ways.
@return V2D_TRUE on success, or V2D_FALSE if an error was encountered.
*/

v2d_bool_t v2dxSaveSVG(v2d_stage_s *stage, const char *name, v2d_flag_t flags);

/**
@brief Export a project in Adobe Flash (SWF) format.

Currently tries to support all SWF versions up to 19.

@param stage The project to be exported.
@param name The name and path for the output file.
@param version The SWF version to export as.
@param flags Flags to alter the output in various ways.
@return V2D_TRUE on success, or V2D_FALSE if an error was encountered.
*/

v2d_bool_t v2dxSaveSWF(v2d_stage_s *stage, const char *name, v2d_ui8_t version, v2d_flag_t flags);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_SAVE_H__ */
