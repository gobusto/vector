/*

FILENAME: compile.h
AUTHOR/S: Thomas Dennis
CREATION: 12th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file compile.h

@brief Provides some basic "script compiler" functions, so you don't have to.

The core of V2D doesn't provide any way to compile source code into "actions"
directly - it simply provides the functions required for creating v2d_script_s
structures. The idea is that user-defined callback functions will be used to
parse source code, and convert it into a list of op-codes for V2D to use.

This file (as part of V2DX) provides some pre-built compiler callback functions
so that basic scripting can be done without requiring users to write their own
parsers first.
*/

#ifndef __V2D_COMPILE_H__
#define __V2D_COMPILE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "v2d/script.h"

/**
@brief Create a script structure from an assembly-like list of instructions.

The syntax is fairly basic - comments begin with a semicolon, and any other
non-blank lines are treated as instructions. As the current list of actions
is based on the SWF v3 Action Model, instruction names are based on the SWF
specification from Adobe, without the "Action" prefix.

ActionPlay      --> Play

ActionGotoFrame --> GotoFrame 16

ActionGetURL    --> GetURL "https://www.duckduckgo.com" "_level0"

You can include a quote in a string by escaping it with a back-slash: \"
Note that instruction names are not case sensitive.

@param source The source code to be compiled.
@param flags A set of flags for modifying the script behaviour in various ways.
@return A new script structure on success, or NULL on failure.
*/

v2d_script_s *v2dxCompileASM(const char *source, v2d_flag_t flags);

#ifdef __cplusplus
}
#endif

#endif /* __V2D_COMPILE_H__ */
