/*

FILENAME: save_swf.c
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 by Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file save_swf.c

@brief Provides functions for exporting projects to Adobe Flash (SWF) files.

TODO: Actually implement (optional) ZLIB/LZMA compression support...
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "save.h"
#include "misc/fileinfo.h"
#include "misc/swfconst.h"
#include "misc/misc.h"

/**
@brief [INTERNAL] Write a signed, 16-bit integer to a file as little-endian.

@param out The output file to write the value to.
@param value The value to be written.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWriteSI16_LE(FILE *out, v2d_si16_t value)
{
  if (!out) { return V2D_FALSE; }
  fputc((value >> 0) & 0xff, out);
  fputc((value >> 8) & 0xff, out);
  return V2D_TRUE;
}

/**
@brief [INTERNAL] Write an unsigned, 16-bit integer to a file as little-endian.

@param out The output file to write the value to.
@param value The value to be written.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWriteUI16_LE(FILE *out, v2d_ui16_t value)
{
  if (!out) { return V2D_FALSE; }
  fputc((value >> 0) & 0xff, out);
  fputc((value >> 8) & 0xff, out);
  return V2D_TRUE;
}

/**
@brief [INTERNAL] Write a signed, 32-bit integer to a file as little-endian.

@param out The output file to write the value to.
@param value The value to be written.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWriteSI32_LE(FILE *out, v2d_si32_t value)
{
  if (!out) { return V2D_FALSE; }
  fputc((value >> 0 ) & 0xff, out);
  fputc((value >> 8 ) & 0xff, out);
  fputc((value >> 16) & 0xff, out);
  fputc((value >> 24) & 0xff, out);
  return V2D_TRUE;
}

/**
@brief [INTERNAL] Write an unsigned, 32-bit integer to a file as little-endian.

@param out The output file to write the value to.
@param value The value to be written.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWriteUI32_LE(FILE *out, v2d_ui32_t value)
{
  if (!out) { return V2D_FALSE; }
  fputc((value >> 0 ) & 0xff, out);
  fputc((value >> 8 ) & 0xff, out);
  fputc((value >> 16) & 0xff, out);
  fputc((value >> 24) & 0xff, out);
  return V2D_TRUE;
}

/* ========================================================================== */

/**
@brief [INTERNAL] Convert a floating-point value into an integer "Twips" value.

@param x The value as a floating-point number.
@return The value in "Twips" format.
*/

#define v2dTwips(x) ((v2d_si32_t)((x)*SWF_TWIP_SIZE))

/**
@brief [INTERNAL] Convert an integer "Twips" value into a floating-point value.

@param x The value in "Twips" format.
@return The value as a floating-point number.
*/

#define v2dUnTwips(x) (((x)/(double)SWF_TWIP_SIZE))

/**
@brief [INTERNAL] Convert a real number into a fixed 8.8 (16-bit) format.

@param value The value as a floating-point number.
@return The value in fixed 8.8 format.
*/

static v2d_si16_t v2dFixed16(v2d_real_t value)
{
  v2d_si16_t whole = (v2d_si16_t)value;
  return (whole << 8) + (v2d_si16_t)((value - whole) * 0xff);
}

/**
@brief [INTERNAL] Convert a real number into a fixed 16.16 (32-bit) format.

@param value The value as a floating-point number.
@return The value in fixed 16.16 format.
*/

static v2d_si32_t v2dFixed32(v2d_real_t value)
{
  v2d_si32_t whole = (v2d_si32_t)value;
  return (whole << 16) + (v2d_si32_t)((value - whole) * 0xffff);
}

/**
@brief [INTERNAL] Write a short-format SWF tag header to an output file.

WARNING: A tag length of 63 indicates that this is a LONG-format tag header!

@param f The file to write the tag header to.
@param id The SWF tag type ID.
@param len The length of the tag contents, up to a maximum limit of 62 bytes.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

#define v2dShortTagSWF(f,id,len) v2dWriteUI16_LE((f), ((id) << 6)+((len) & 63))

/**
@brief [INTERNAL] Write a long-format SWF tag header to an output file.

@param f The file to write the tag header to.
@param id The SWF tag type ID.
@param len The length of the tag contents.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dLongTagSWF(FILE *f, swf_tag_t id, v2d_si32_t len)
{
  if (!f) { return V2D_FALSE; }
  v2dShortTagSWF(f, id, 63);
  return v2dWriteSI32_LE(f, len);
}

/**
@brief [INTERNAL] Update a "length/offset" field at a given position in a file.

This calculates the difference (in bytes) between a reference position and the
current file position, and writes it to the file at a specific location.

@param f The output file to write to.
@param ref The "reference" offset to compare with the current offset.
@param pos The position at which to write a delta value, relative to "ref".
@param as_32 If true, write a 32-bit offset value instead of a 16-bit one.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWriteOffset(FILE *f, long ref, long pos, v2d_bool_t as_32)
{

  long current = 0;

  /* Store the current file position, so that we can move back to it later. */
  if (!f) { return V2D_FALSE; }
  current = ftell(f);

  /* Move to the position of the "output" field and write the difference. */
  fseek(f, ref+pos, SEEK_SET);
  as_32 ? v2dWriteSI32_LE(f, current-ref) : v2dWriteSI16_LE(f, current-ref);

  /* Go back to the original seek position again. */
  fseek(f, current, SEEK_SET);
  return V2D_TRUE;

}

/**
@brief [INTERNAL] Write a sequence of packed bits to an output file.

@param out The file to write to.
@param state A "state-tracking" variable, used to count bits and store values.
@param flush Write non-aligned values to the file and pad them with zeroes.
@param sign If true, store negative signed values in Two's-Complement format.
@param num_bits The number of bits to use for the value to be written.
@param value The number to be written to the file.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWriteBitsSWF(FILE *out, v2d_ui16_t *state,
    v2d_bool_t flush, v2d_bool_t sign, v2d_ui8_t num_bits, v2d_si32_t value)
{

  v2d_ui8_t shift = 0;
  v2d_ui8_t accum = 0;
  v2d_ui8_t bit, i;

  if (!out) { return V2D_FALSE; }

  /* If a previous state is provided, unpack it into the "working" variables. */

  if (state) { accum = (*state >> 0) & 0xff; shift = (*state >> 8) & 0xff; }

  /* Loop through each bit in the number to be written. */

  for (i = 0; i < num_bits; i++)
  {

    if (sign && i == 0) { bit = value < 0;                     }
    else                { bit = value >> (num_bits-(i+1)) & 1; }

    accum |= bit << (7 - shift++);

    if (shift > 7) { fputc(accum, out); shift = 0; accum = 0; }

  }

  /* If there are any bits remaining to be written, flush them to file. */

  if (shift > 0 && flush) { fputc(accum, out); shift = 0; accum = 0; }

  /* If a previous state is provided, pack the "working" variables into it. */

  if (state) { *state = accum + (shift << 8); }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Write an SWF rectangle to a file.

@todo Optimise the number of bits used.

@param out The file to write to.
@param aabb The rectangle to be written.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxWriteRectSWF(FILE *out, v2d_rect_s *aabb)
{

  v2d_ui16_t state    = 0;
  v2d_ui8_t  num_bits = 0;

  if (!out || !aabb) { return V2D_FALSE; }

  num_bits = 31;  /* TODO: Work out the minimum number of bits required... */

  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 5, num_bits);
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, num_bits, v2dTwips(aabb->x1));
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, num_bits, v2dTwips(aabb->x2));
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, num_bits, v2dTwips(aabb->y1));
  v2dWriteBitsSWF(out, &state, V2D_TRUE,  V2D_TRUE, num_bits, v2dTwips(aabb->y2));

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Write an SWF Matrix to a file.

@todo Optimise the number of bits used.

@param out The file to write to.
@param matrix The transformation matrix.
@param resize writeme
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWriteMatrixSWF(FILE *out, v2d_matrix_t matrix, v2d_bool_t resize)
{

  v2d_ui16_t state = 0;

  if (!out || !matrix) { return V2D_FALSE; }

  /* Scale data (optional). */
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, 1);
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 5, 31);
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, 31, v2dFixed32(matrix[0][0] * (resize ? SWF_TWIP_SIZE : 1)));
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, 31, v2dFixed32(matrix[1][1] * (resize ? SWF_TWIP_SIZE : 1)));

  /* Rotate data (optional). */
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, 1);
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 5, 31);
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, 31, v2dFixed32(matrix[0][1] * (resize ? SWF_TWIP_SIZE : 1)));
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, 31, v2dFixed32(matrix[1][0] * (resize ? SWF_TWIP_SIZE : 1)));

  /* Translate data (required). */
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 5, 31);
  v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_TRUE, 31, v2dTwips(matrix[2][0]));
  v2dWriteBitsSWF(out, &state, V2D_TRUE,  V2D_TRUE, 31, v2dTwips(matrix[2][1]));

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Determine if two text tags use identical fonts.

For SWF tags, this is based on the font name, weight (bold), and slant (italic).

Text objects which do not define a font are assumed not to to match anything,
since they can't be associated with an SWF font tag.

@todo As SWF files do not support "oblique" font types, this function should
assume that "italic" and "oblique" (i.e. "not standard") are the same thing.

@param text_a The first text object.
@param text_b The second text object.
@return V2D_TRUE if the fonts are considered identical, or V2D_FALSE if not.
*/

static v2d_bool_t v2dxFontsMatch(v2d_draw_s *text_a, v2d_draw_s *text_b)
{

  if      (!text_a || !text_b           ) { return V2D_FALSE; }
  else if (text_a->type != V2D_TYPE_TEXT) { return V2D_FALSE; }
  else if (text_b->type != V2D_TYPE_TEXT) { return V2D_FALSE; }
  else if (!v2dDrawAsText(text_a)->font) { return V2D_FALSE; }
  else if (!v2dDrawAsText(text_b)->font) { return V2D_FALSE; }

  if (strcmp(v2dDrawAsText(text_a)->font, v2dDrawAsText(text_b)->font)) { return V2D_FALSE; }
  if (v2dDrawAsText(text_a)->weight != v2dDrawAsText(text_b)->weight  ) { return V2D_FALSE; }
  if (v2dDrawAsText(text_a)->slant  != v2dDrawAsText(text_b)->slant   ) { return V2D_FALSE; }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Determine if a drawable requires a new font definition.

@param draw The drawable to inspect.
@return V2D_TRUE if the draw is the first instance using a font name.
*/

static v2d_bool_t v2dxHasNewFont(v2d_draw_s *draw)
{

  v2d_draw_s *prev = NULL;

  if (!draw                      ) { return V2D_FALSE; }  /* Doesn't exist... */
  if (draw->type != V2D_TYPE_TEXT) { return V2D_FALSE; }  /* Not a text draw. */
  if (!v2dDrawAsText(draw)->font ) { return V2D_FALSE; }  /* No font defined. */

  for (prev = v2dDrawGetPrev(draw); prev; prev = v2dDrawGetPrev(prev))
  {
    if (v2dxFontsMatch(draw, prev)) { return V2D_FALSE; }
  }

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Get a 16-bit ID value for a data object, such as an image.

If "wanted" is NULL, the highest ID found is returned. Otherwise, the ID of the
item "wanted" is reported, with "zero" indicating that it could not be found.

@param stage about
@param wanted about
@return writeme
*/

static v2d_ui16_t v2dGetDataID(v2d_stage_s *stage, v2d_data_s *wanted)
{

  v2d_ui16_t  id   = 0;
  v2d_data_s *data = NULL;
  v2d_ui8_t   pass = 0;

  if (!stage) { return 0; }

  id = 0;

  for (pass = 0; pass < 3; pass++)
  {

    if      (pass == 0) { data = v2dStageGetImage(stage); }
    else if (pass == 1) { data = v2dStageGetAudio(stage); }
    else if (pass == 2) { data = v2dStageGetEmbed(stage); }
    else                { data = NULL;         }

    for (; data; data = v2dDataGetNext(data))
    {
      id++;
      if (data == wanted) { return id; }
    }

  }

  return wanted ? 0 : id;

}

/**
@brief [INTERNAL] Get a 16-bit GUID value for a Font object.

If "wanted" is NULL, the highest ID found is returned. Otherwise, the ID of the
item "wanted" is reported, with "zero" indicating that it could not be found.

@param stage about
@param wanted about
@return writeme
*/

static v2d_ui16_t v2dGetFontID(v2d_stage_s *stage, v2d_draw_s *wanted)
{

  v2d_ui16_t id = 0;
  v2d_draw_s *draw = NULL;

  if (!stage) { return 0; }

  id = v2dGetDataID(stage, NULL);

  for (draw = v2dStageGetDraw(stage); draw; draw = v2dDrawGetNext(draw))
  {
    if (v2dxHasNewFont(draw))
    {
      id++;
      if (v2dxFontsMatch(draw, wanted)) { return id; }
    }
  }

  return wanted ? 0 : id;

}

/**
@brief [INTERNAL] Get a 16-bit GUID value for a Draw object.

If "wanted" is NULL, the highest ID found is returned. Otherwise, the ID of the
item "wanted" is reported, with "zero" indicating that it could not be found.

@param stage about
@param wanted about
@return writeme
*/

static v2d_ui16_t v2dGetDrawID(v2d_stage_s *stage, v2d_draw_s *wanted)
{

  v2d_ui16_t id = 0;
  v2d_draw_s *draw = NULL;

  if (!stage) { return 0; }

  id = v2dGetFontID(stage, NULL);

  for (draw = v2dStageGetDraw(stage); draw; draw = v2dDrawGetNext(draw))
  {
    id++;
    if (draw == wanted) { return id; }
  }

  return wanted ? 0 : id;

}

/**
@brief [INTERNAL] Get a 16-bit ID value for a Button object.

If "wanted" is NULL, the highest ID found is returned. Otherwise, the ID of the
item "wanted" is reported, with "zero" indicating that it could not be found.

@param stage about
@param wanted about
@return writeme
*/

static v2d_ui16_t v2dGetButtonID(v2d_stage_s *stage, v2d_anim_s *wanted)
{

  v2d_ui16_t id = 0;
  v2d_chain_s *group = NULL;
  v2d_draw_s *draw = NULL;
  v2d_item_s *item = NULL;
  v2d_anim_s *anim = NULL;

  if (!stage) { return 0; }

  if (wanted)
  {
    if (!(wanted->flags & V2D_ANIM_FLAG_BUTTON)) { return 0; }
  }

  id = v2dGetDrawID(stage, NULL);

  for (draw = v2dStageGetDraw(stage); V2D_TRUE; draw = v2dDrawGetNext(draw))
  {

    if      (!draw                       ) { group = stage->item;          }
    else if (draw->type == V2D_TYPE_GROUP) { group = v2dDrawAsGroup(draw); }
    else                                   { continue;                     }

    for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
    {
      for (anim = v2dItemGetAnim(item); anim; anim = v2dAnimGetNext(anim))
      {
        if (anim->flags & V2D_ANIM_FLAG_BUTTON)
        {
          id++;
          if (anim == wanted) { return id; }
        }
      }
    }

    if (!draw) { break; }

  }

  return wanted ? 0 : id;

}

/**
@brief [INTERNAL] Get a 16-bit ID value for an Item object.

If "wanted" is NULL, the highest ID found is returned. Otherwise, the ID of the
item "wanted" is reported, with "zero" indicating that it could not be found.

@param first_item about
@param wanted about
@return writeme
*/

static v2d_ui16_t v2dGetItemID(v2d_item_s *first_item, v2d_item_s *wanted)
{

  v2d_ui16_t id = 0;
  v2d_item_s *item = NULL;

  if (!first_item) { return 0; }

  id = 0; /* Items don't share ID numbers with things such as images/fonts. */

  for (item = first_item; item; item = v2dItemGetNext(item))
  {
    id++;
    if (item == wanted) { return id; }
  }

  return wanted ? 0 : id;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Copy one file into another.

GNASH and the official Adobe Flash player can't handle ID3 tags in MP3s; audio
data which begins with an ID3 tag will not be played. As such, this function
can be requested to skip past any initial ID3 tag data before the remainder of
the file is copied.

NOTE: It seems that MP3s containing APETAGEX tags work as-is, since these tags
are found after the actual MP3 audio data and thus do not interfere with it.

For WAV files, skip past the first 44 bytes of RIFF header data. There may be
extra RIFF chunks after the audio data ends, so it's wise to specify a "limit"
value when embedding WAV files as raw audio data; GNASH plays the extra data as
audio, even if it comes after the range specified by the sample count value...

@param out A pointer to a file handle, which should be opened for writing.
@param file_name The name of the "input" file to embed into the output file.
@param format The data format. XXX_FILE_UNKNOWN copies files without changes.
@param limit If non-zero, specifies the maximum number of bytes to be copied.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dCopyFromFile(FILE *out, const char *file_name, xxx_file_t format, v2d_ui32_t limit)
{

  FILE *src = NULL;
  int byte = 0;

  v2d_ui32_t count = 0;

  if (!out || !file_name) { return V2D_FALSE; }

  /* Attempt to open the input file. If it doesn't exist, report an error. */

  src = fopen(file_name, "rb");
  if (!src)
  {
    fprintf(stderr, "[WARNING] File not found: %s\n", file_name);
    return V2D_FALSE;
  }

  /* AUDIO: For WAVs, skip the RIFF header data. For MP3, skip any ID3 tags. */

  if (format == XXX_FILE_WAV)
  {
    if (fseek(src, 44, SEEK_SET) != 0) { return V2D_FALSE; }
  }
  else if (format == XXX_FILE_MP3) { xxxFileSkipID3(src); }

  /* Copy data until either (A) the byte-limit is reached or (B) EOF. */

  for (count = 0; (byte = fgetc(src)) != EOF; ++count)
  {
    if (limit > 0 && count >= limit) { break; } else { fputc(byte, out); }
  }

  /* Close the input file and report success. */

  fclose(src);
  return V2D_TRUE;

}

/**
@brief [INTERNAL] Embed RGB or RGBA bitmap images into an SWF file.

There are two types of tag that SWF files can use to contain bitmap image data:
"DefineBitsLossless" and "DefineBitsJPEG". Both were introduced in SWF v2.

The "DefineBitsLossless" tag uses ZLIB to compress the RGB image data. The
"DefineBitsLossless2" tag (SWF v3) is similar, but also includes alpha data.
This type of image tag isn't used here, as it would (A) introduce a dependency
on ZLIB, and (B) require files to be loaded and decompressed before exporting.
This in turn would introduce dependencies (such as libJPEG, libPNG, or DevIL),
and could even lead to larger file-sizes, as decompressed JPEG images would be
recompressed "losslessly", even though they were originally in a lossy format.

The "DefineBitsJPEG2" tag is simply a JPEG file. As such, bytes can be directly
copied from the source file without conversion. Additionally, SWF v8 allows PNG
or (non-animated) GIF images to be used instead of JPEG data.

The "DefineBitsJPEG3" and "DefineBitsJPEG4" tags include ZLIB-compressed alpha
data, but these aren't used here since PNG and GIF support transparency anyway.

@todo SWF v1 stores images in "DefineBits" tags, with a single "JPEGTables" tag
being shared by all images within the SWF file. Neither of these are output yet.

@param out The file to write to.
@param stage about
@param version about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dSaveImageSWF(FILE *out, v2d_stage_s *stage, v2d_ui8_t version)
{

  v2d_ui16_t  tag_guid = 0;
  v2d_si32_t  tag_offs = 0;
  v2d_data_s *data = NULL;
  xxx_file_t  kind = XXX_FILE_UNKNOWN;

  if (!out || !stage) { return V2D_FALSE; }

  for (data = v2dStageGetImage(stage); data; data = v2dDataGetNext(data))
  {

    tag_guid = v2dGetDataID(stage, data);
    tag_offs = ftell(out);
    kind     = xxxFileTypeImage(data->file_name, NULL, NULL);

    /* Check for errors. */

    if (kind != XXX_FILE_JPG && kind != XXX_FILE_GIF && kind != XXX_FILE_PNG)
    {
      fprintf(stderr, "ERROR: Unrecognised format: %s\n", data->file_name);
    }
    else if (kind == XXX_FILE_PNG && version < 8)
    {
      fprintf(stderr, "ERROR: PNG images require SWF8+: %s\n", data->file_name);
    }
    else if (kind == XXX_FILE_GIF && version < 8)
    {
      fprintf(stderr, "ERROR: GIF images require SWF8+: %s\n", data->file_name);
    }
    else if (version < 2)
    {
      fprintf(stderr, "TODO: Cannot save SWF v1 images: %s\n", data->file_name);
    }
    else
    {

      /* Copy the image data from the original file into this SWF. */

      v2dLongTagSWF(out, SWF_TAG_DEFINEBITSJPEG2, 0);
      v2dWriteUI16_LE(out, tag_guid);
      v2dCopyFromFile(out, data->file_name, kind, 0);
      v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

    }

  }

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Embed audio data into an SWF.

writeme

@param out about
@param stage about
@param version about
@return writeme
*/

static v2d_bool_t v2dSaveAudioSWF(FILE *out, v2d_stage_s *stage, v2d_ui8_t version)
{

  v2d_data_s *data = NULL;
  v2d_si32_t  tag_offs = 0;
  v2d_ui16_t state = 0;

  xxx_file_t  kind = XXX_FILE_UNKNOWN;

  v2d_ui16_t frequency;
  v2d_ui8_t  channels;
  v2d_ui32_t samples;
  v2d_ui16_t bits;

  swf_frequency_t swf_freq = SWF_FREQUENCY_OTHER;

  if (!out || !stage) { return V2D_FALSE; }

  for (data = v2dStageGetAudio(stage); data; data = v2dDataGetNext(data))
  {

    kind = xxxFileTypeAudio(data->file_name, &frequency, &channels, &samples, &bits);

    if      (frequency == 5512 ) { swf_freq = SWF_FREQUENCY_5512;  }
    else if (frequency == 11025) { swf_freq = SWF_FREQUENCY_11025; }
    else if (frequency == 22050) { swf_freq = SWF_FREQUENCY_22050; }
    else if (frequency == 44100) { swf_freq = SWF_FREQUENCY_44100; }
    else                         { swf_freq = SWF_FREQUENCY_OTHER; }

    /* Check for errors. */

    if (kind != XXX_FILE_MP3 && kind != XXX_FILE_WAV)
    {
      fprintf(stderr, "ERROR: Unrecognised format: %s\n", data->file_name);
    }
    else if (swf_freq == SWF_FREQUENCY_OTHER)
    {
      fprintf(stderr, "ERROR: Unsupported frequency: %s\n", data->file_name);
    }
    else if (channels != 1 && channels != 2)
    {
      fprintf(stderr, "ERROR: Unsupported channel count: %s\n", data->file_name);
    }
    else if (bits != 8 && bits != 16)
    {
      fprintf(stderr, "ERROR: Unsupported bits-per-sample: %s\n", data->file_name);
    }
    else if (kind == XXX_FILE_MP3 && version < 4)
    {
      fprintf(stderr, "ERROR: MP3 audio requires SWF4+: %s\n", data->file_name);
    }
    else if (kind == XXX_FILE_MP3 && swf_freq == SWF_FREQUENCY_5512)
    {
      fprintf(stderr, "ERROR: Invalid MP3 frequency: %s\n", data->file_name);
    }
    else
    {

      swf_codec_audio_t codec;

      if      (kind == XXX_FILE_MP3) { codec = SWF_SNDCODEC_MP3;          }
      else if (version >= 4        ) { codec = SWF_SNDCODEC_LITTLEENDIAN; }
      else                           { codec = SWF_SNDCODEC_NATIVEENDIAN; }

      /*
      NOTE: Adobe say that the "16-bit" flag is ignored for compressed formats
      such as MP3. This is a lie; the official Adobe Flash player plays sounds
      without the "16-bit" flag set in a horribly distorted way.
      */

      tag_offs = ftell(out);
      v2dLongTagSWF(out, SWF_TAG_DEFINESOUND, 0);
      v2dWriteUI16_LE(out, v2dGetDataID(stage, data));
      v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 4, codec);
      v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 2, swf_freq);
      v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, (kind == XXX_FILE_MP3 || bits == 16) ? 1 : 0);
      v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, channels > 1);
      v2dWriteUI32_LE(out, samples);
      v2dWriteSI16_LE(out, 0);  /* SeekSamples (i.e. initial latency). */
      v2dCopyFromFile(out, data->file_name, kind, kind == XXX_FILE_WAV ? samples * channels * bits/8 : 0);
      v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

    }

  }

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Embed binary data into an SWF.

writeme

@param out about
@param stage about
@param version about
@return writeme
*/

static v2d_bool_t v2dSaveEmbedSWF(FILE *out, v2d_stage_s *stage, v2d_ui8_t version)
{

  v2d_si32_t tag_offs = 0;
  v2d_data_s *data = NULL;

  if (!out || !stage || version < 9) { return V2D_FALSE; }

  for (data = v2dStageGetEmbed(stage); data; data = v2dDataGetNext(data))
  {
    tag_offs = ftell(out);
    v2dLongTagSWF(out, SWF_TAG_DEFINEBINARYDATA, 0);
    v2dWriteUI16_LE(out, v2dGetDataID(stage, data));
    v2dWriteUI32_LE(out, 0);  /* Reserved. */
    v2dCopyFromFile(out, data->file_name, XXX_FILE_UNKNOWN, 0);
    v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);
  }

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] writeme

about

@param out The file to write to.
@param stage about
@param version about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dSaveFontSWF(FILE *out, v2d_stage_s *stage, v2d_ui8_t version)
{

  v2d_ui16_t  tag_guid = 0;
  v2d_si32_t  tag_offs = 0;
  v2d_ui8_t   flags = 0;
  v2d_draw_s *draw  = NULL;

  if (!out || !stage) { return V2D_FALSE; }

  for (draw = v2dStageGetDraw(stage); draw; draw = v2dDrawGetNext(draw))
  {

    if (v2dxHasNewFont(draw))
    {

      tag_guid = v2dGetFontID(stage, draw);
      tag_offs = ftell(out);

      if (version >= 10)
      {

        v2dLongTagSWF(out, SWF_TAG_DEFINEFONT4, 0);
        v2dWriteUI16_LE(out, tag_guid);

        flags = 0;
        if (v2dDrawAsText(draw)->weight != V2D_FONT_WEIGHT_NORMAL) { flags |= SWF_DEFINEFONT4_BOLD;   }
        if (v2dDrawAsText(draw)->slant  != V2D_FONT_SLANT_NORMAL ) { flags |= SWF_DEFINEFONT4_ITALIC; }
        fputc(flags, out);

        fprintf(out, "%s", v2dDrawAsText(draw)->font); fputc(0, out);

        v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

      }
      else if (version >= 3)
      {

        v2dLongTagSWF(out, SWF_TAG_DEFINEFONT2, 0);
        v2dWriteUI16_LE(out, tag_guid);

        flags = 0;
        if (v2dDrawAsText(draw)->weight != V2D_FONT_WEIGHT_NORMAL) { flags |= SWF_DEFINEFONT2_BOLD;   }
        if (v2dDrawAsText(draw)->slant  != V2D_FONT_SLANT_NORMAL ) { flags |= SWF_DEFINEFONT2_ITALIC; }
        fputc(flags, out);

        fputc(0, out);  /* LANGCODE: Zero is "default" */

        fputc(strlen(v2dDrawAsText(draw)->font), out);
        fprintf(out, "%s", v2dDrawAsText(draw)->font);

        v2dWriteUI16_LE(out, 0);

        v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

      }
      else
      {

        /* TODO: Use DefineFont here. */

      }

    }

  }

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Output a 'text' drawable in SWF format.

@todo Handle SWF versions earlier than 4.

@param out The file to write to.
@param stage The stage structure that this text drawable belongs to.
@param draw The text drawable to be output.
@param version The version of the SWF file being written to.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dSaveSWF_text(FILE *out, v2d_stage_s *stage, v2d_draw_s *draw, v2d_ui8_t version)
{

  const v2d_text_s *text = v2dDrawAsText(draw);
  const v2d_si32_t offs = ftell(out);
  const v2d_ui16_t guid = v2dGetDrawID(stage, draw);

  if (!out || !stage || !draw) { return V2D_FALSE; }

  if (version >= 4)
  {

    v2d_ui16_t flags = 0;
    v2d_rect_s aabb;

    /* SWF v4+ supports "dynamic" text that can be changed by scripts. */

    v2dLongTagSWF(out, SWF_TAG_DEFINEEDITTEXT, 0);
    v2dWriteUI16_LE(out, guid);

    /* Output a bounding box. */

    v2dDrawGetAABB(draw, &aabb, 0);
    v2dxWriteRectSWF(out, &aabb);

    /* Set text flags. */

    flags = SWF_EDITTEXT_READONLY | SWF_EDITTEXT_MULTILINE |
            SWF_EDITTEXT_NOSELECT | SWF_EDITTEXT_HASTEXTCOLOR;

    if (version >= 6) { flags |= SWF_EDITTEXT_AUTOSIZE; }

    if (text->font) { flags |= SWF_EDITTEXT_HASFONT; }
    if (text->data) { flags |= SWF_EDITTEXT_HASTEXT; }

    v2dWriteUI16_LE(out, flags);

    /* If a font has been specified, output the SWF ID and height values. */

    if (text->font)
    {
      v2dWriteUI16_LE(out, v2dGetFontID(stage, draw));
      v2dWriteUI16_LE(out, v2dTwips(text->size));
    }

    /* Output the text color. */

    fwrite(text->rgba, 1, 4, out);

    /* Output the name of the variable that defines the text value. */

    if (text->name) { fprintf(out, "%s", text->name); fputc(0, out); }
    else            { fprintf(out, "__%d", guid    ); fputc(0, out); }

    /* Output the initial text value, if one has been specified. */

    if (text->data) { fprintf(out, "%s\n", text->data); fputc(0, out); }

    /* Update tag header length field. */

    v2dWriteOffset(out, offs + 6, -4, V2D_TRUE);

  }
  else
  {

    /* TODO: Use a different SWF tag depending on version... */

    return V2D_FALSE;

  }

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Write a FillStyle array to an SWF file.

As all V2D shapes have a single fill, the "array" will always contain just one
entry. If no fill style is provided, an array with zero elements is written.

@param out The file to write to.
@param stage The stage that contains this shape.
@param shape The shape to create a FillStyle array for, or NULL.
@param tag The type of the SWF tag that contains the FillStyle array.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dFillArraySWF(FILE *out, v2d_stage_s *stage, v2d_shape_s *shape, swf_tag_t tag)
{

  if (!out || !stage || !shape) { return V2D_FALSE; }

  /* If no fill style is available, output a FillStyle array length of zero. */

  fputc(shape->fill ? 1 : 0, out);

  if (!shape->fill) { return V2D_TRUE; }

  switch (shape->fill->type)
  {

    case V2D_FILL_COLOR:
      if (v2dIsFlatRGBA(shape->fill))
      {
        fputc(SWF_FILL_SOLID, out);
        if (tag == SWF_TAG_DEFINESHAPE || tag == SWF_TAG_DEFINESHAPE2)
             { fwrite(v2dFillAsColor(shape->fill)->rgba, 1, 3, out); } /* RGB  */
        else { fwrite(v2dFillAsColor(shape->fill)->rgba, 1, 4, out); } /* RGBA */
      }
      else
      {

        v2d_fill_color_s *grad = NULL;
        v2d_ui32_t       count = 0;
        v2d_ui16_t       state = 0;
        v2d_matrix_t     matrix;

        v2d_rect_s aabb;

        /* Count the number of gradient stops. */

        count = 0;
        for (grad = v2dFillAsColor(shape->fill); grad; grad = v2dFillGetNext(grad))
        { count++; }

        if (count > 15                              ) { count = 15; }
        if (count > 8 && tag != SWF_TAG_DEFINESHAPE4) { count = 8;  }

        /* Write the type of gradient. */

        if (shape->fill->flags & V2D_FILL_FLAG_RADIAL)
        { fputc(SWF_FILL_GRADIENT_RADIAL, out); }
        else
        { fputc(SWF_FILL_GRADIENT_LINEAR, out); }

        /* Write gradient matrix. SWF files base this on the shape AABB... */

        v2dShapeGetAABB(shape, &aabb, V2D_FALSE);
        v2dMatrixCopy(matrix, shape->fill->matrix);

        v2dMatrixPosition(matrix, ((aabb.x2-aabb.x1) / 2.0) + 0.5, ((aabb.y2-aabb.y1) / 2.0) + 0.5);
        v2dMatrixScale(matrix, (aabb.x2-aabb.x1), (aabb.y2-aabb.y1));
        v2dMatrixTranslate(matrix, shape->fill->matrix[2][0], shape->fill->matrix[2][1]);
        v2dMatrixScale(matrix, SWF_TWIP_SIZE / 32768.0, SWF_TWIP_SIZE / 32768.0);

        v2dWriteMatrixSWF(out, matrix, V2D_FALSE);

        /* Output gradient header record. */

        v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 2, 0);
        v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 2, 0);
        v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 4, count);

        /* Write individual gradient-stop records. */

        count = 0;
        for (grad = v2dFillAsColor(shape->fill); grad; grad = v2dFillGetNext(grad))
        {
          if ((tag == SWF_TAG_DEFINESHAPE4 && count < 15) ||
              (tag != SWF_TAG_DEFINESHAPE4 && count < 8))
          {
            count++;
            fputc((v2d_ui8_t)(grad->stop * 255.0), out);
            if (tag == SWF_TAG_DEFINESHAPE || tag == SWF_TAG_DEFINESHAPE2)
                 { fwrite(grad->rgba, 1, 3, out); } /* RGB  */
            else { fwrite(grad->rgba, 1, 4, out); } /* RGBA */
          }
        }

      }
    break;

    case V2D_FILL_IMAGE:
        fputc(SWF_FILL_IMAGE_SMOOTH_REPEAT, out);
        v2dWriteUI16_LE(out, v2dGetDataID(stage, v2dFillAsImage(shape->fill)->image));
        v2dWriteMatrixSWF(out, shape->fill->matrix, V2D_TRUE);
    break;

    default:
      fputc(SWF_FILL_SOLID, out);
      fputc(255, out); fputc(255, out); fputc(255, out);
      if (tag != SWF_TAG_DEFINESHAPE && tag != SWF_TAG_DEFINESHAPE2)
      { fputc(255, out); }
    break;

  }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Write a LineStyle or LineStyle2 array to an SWF file.

As all V2D shapes have a single line, the "array" will always contain just one
entry. If no line style is provided, an array with zero elements is written.

@param out The file to write to.
@param stage The stage that contains this shape.
@param shape The shape to create a LineStyle or LineStyle2 array for, or NULL.
@param tag The type of the SWF tag that contains the LineStyle/LineStyle2 array.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dLineArraySWF(FILE *out, v2d_stage_s *stage, v2d_shape_s *shape, swf_tag_t tag)
{

  swf_cap_t  cap  = SWF_CAP_ROUND;
  swf_join_t join = SWF_JOIN_ROUND;
  v2d_ui16_t state = 0;
  v2d_bool_t scale = 0;

  if (!out || !stage || !shape) { return V2D_FALSE; }

  /* If no line style is available, output a LineStyle array length of zero. */

  fputc(shape->line ? 1 : 0, out);

  if (!shape->line) { return V2D_TRUE; }

  /* This is where the (single) LineStyle/LineStyle2 array entry begins... */

  v2dWriteUI16_LE(out, v2dTwips(shape->line->width));

  if (tag == SWF_TAG_DEFINESHAPE4)
  {

    /* Convert the V2D line-join/line-cap styles into their SWF equivalents. */

    if      (shape->line->cap  == V2D_LINE_CAP_BUTT  ) { cap = SWF_CAP_NOCAP;   }
    else if (shape->line->cap  == V2D_LINE_CAP_SQUARE) { cap = SWF_CAP_SQUARE;  }
    else                                               { cap = SWF_CAP_ROUND;   }

    if      (shape->line->join == V2D_LINE_JOIN_MITER) { join = SWF_JOIN_MITER; }
    else if (shape->line->join == V2D_LINE_JOIN_BEVEL) { join = SWF_JOIN_BEVEL; }
    else                                               { join = SWF_JOIN_ROUND; }

    scale = (shape->line->flags & V2D_LINE_FLAG_SCALING);

    /* StartCapStyle. */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 2, cap);

    /* JoinStyle. */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 2, join);

    /* HasFillFlag - This should always be disabled, as V2D always uses RGBA. */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, 0);

    /* The following attributes are "NoHScale" and "NoVScale", in that order. */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, !scale);
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, !scale);

    /* Pixel Hinting. Should this be enabled...? */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, 0);

    /* Reserved for future use. */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 5, 0);

    /* NoClose - This should always(?) be disabled for V2D. */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 1, 0);

    /* EndCapStyle. */
    v2dWriteBitsSWF(out, &state, V2D_FALSE, V2D_FALSE, 2, cap);

    if (join == SWF_JOIN_MITER)
    { v2dWriteUI16_LE(out, v2dTwips(shape->line->miter)); }

  }

  /* LineStyle varies slightly by "DefineShape" version. LineStyle2 doesn't. */

  if (tag == SWF_TAG_DEFINESHAPE || tag == SWF_TAG_DEFINESHAPE2)
       { fwrite(shape->line->rgba, 1, 3, out); } /* RGB  */
  else { fwrite(shape->line->rgba, 1, 4, out); } /* RGBA */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Write an X/Y coordinate to an SWF using a set number of bits.

This is mainly to avoid lots of copy/pasting in the v2dSavePathSWF() function.

@param out The file to write to.
@param state A "state-tracking" variable, used to count bits and store values.
@param num_bits The number of bits to use for the value to be written.
@param x The X coordinate to be written, in twips.
@param y The Y coordinate to be written, in twips.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dWritePosSWF(FILE *out, v2d_ui16_t *state,
                                 v2d_ui8_t num_bits, v2d_si32_t x, v2d_si32_t y)
{
  if (!out || !state) { return V2D_FALSE; }
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_TRUE, num_bits, x);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_TRUE, num_bits, y);
  return V2D_TRUE;
}

/**
@brief [INTERNAL] about

writeme

@param out about
@param state about
@return writeme
*/

static v2d_bool_t v2dSaveSWF_showLine(FILE *out, v2d_ui16_t *state, v2d_shape_s *shape, v2d_bool_t update_fill)
{

  if (!out || !state) { return V2D_FALSE; }

  /* Output a style-change record which only changes the LineStyle value. */

  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 1);  /* LineStyle. */
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, update_fill ? 1 : 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);

  if (update_fill)
  {
    v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, shape ? (shape->fill ? 1 : 0) : 0);
  }

  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, shape ? (shape->line ? 1 : 0) : 0);

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

writeme

@param out about
@param state about
@param shape about
@param path about
@param old_x about
@param old_y about
@return writeme
*/

static v2d_bool_t v2dSaveSWF_pathMove(FILE *out, v2d_ui16_t *state, v2d_shape_s *shape, v2d_path_s *path, v2d_si32_t *old_x, v2d_si32_t *old_y)
{

  v2d_si32_t pen_x = 0;
  v2d_si32_t pen_y = 0;

  if (!out || !state || !shape || !path || !old_x || !old_y) { return V2D_FALSE; }

  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, path == v2dShapeGetPath(shape)); /* LineStyle */
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, path == v2dShapeGetPath(shape)); /* FillStyle0 */
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 1);

  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 5, 31);

  pen_x = v2dTwips(path->point[V2D_END_POINT][0]);
  pen_y = v2dTwips(path->point[V2D_END_POINT][1]);

  if (!(path->flags & V2D_PATH_FLAG_RELATIVE))
  {
    pen_x -= *old_x;
    pen_y -= *old_y;
  }

  v2dWritePosSWF(out, state, 31, pen_x, pen_y);

  /*
  The official Adobe player gets stuck in a "File Is Still Loading" state
  if an invalid fill/line index is used here. Gnash and SWFDec seem to be
  okay with invalid indices, and play the SWF without any problems...
  */

  if (path == v2dShapeGetPath(shape))
  {
    /* FillStyle0 */
    v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, shape->fill ? 1 : 0);
    /* LineStyle */
    v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, shape->line ? 1 : 0);
  }

  /* Report success. */

  *old_x += pen_x;
  *old_y += pen_y;

  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

writeme

@param out about
@param state about
@param path about
@param old_x about
@param old_y about
@return writeme
*/

static v2d_bool_t v2dSaveSWF_pathLine(FILE *out, v2d_ui16_t *state, v2d_path_s *path, v2d_si32_t *old_x, v2d_si32_t *old_y)
{

  v2d_si32_t pen_x = 0;
  v2d_si32_t pen_y = 0;

  if (!out || !state || !path || !old_x || !old_y) { return V2D_FALSE; }

  pen_x = v2dTwips(path->point[V2D_END_POINT][0]);
  pen_y = v2dTwips(path->point[V2D_END_POINT][1]);

  if (!(path->flags & V2D_PATH_FLAG_RELATIVE))
  {
    pen_x -= *old_x;
    pen_y -= *old_y;
  }

  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 1);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 1);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 4, 15);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 1);

  /* Write end point. */

  v2dWritePosSWF(out, state, 17, pen_x, pen_y);

  /* Report success. */

  *old_x += pen_x;
  *old_y += pen_y;

  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

writeme

@param out about
@param state about
@param path about
@param old_x about
@param old_y about
@return writeme
*/

static v2d_bool_t v2dSaveSWF_pathQuad(FILE *out, v2d_ui16_t *state, v2d_path_s *path, v2d_si32_t *old_x, v2d_si32_t *old_y)
{

  v2d_real_t pen_x = 0;
  v2d_real_t pen_y = 0;

  if (!out || !state || !path || !old_x || !old_y) { return V2D_FALSE; }

  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 1);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 1, 0);
  v2dWriteBitsSWF(out, state, V2D_FALSE, V2D_FALSE, 4, 15);

  /* Write control point. */

  pen_x = v2dTwips(path->point[V2D_CONTROL_1][0]);
  pen_y = v2dTwips(path->point[V2D_CONTROL_1][1]);

  if (!(path->flags & V2D_PATH_FLAG_RELATIVE))
  {
    pen_x -= *old_x;
    pen_y -= *old_y;
  }

  v2dWritePosSWF(out, state, 17, pen_x, pen_y);

  *old_x += pen_x;
  *old_y += pen_y;

  /* Write end point. */

  pen_x = v2dTwips(path->point[V2D_END_POINT][0] - path->point[V2D_CONTROL_1][0]);
  pen_y = v2dTwips(path->point[V2D_END_POINT][1] - path->point[V2D_CONTROL_1][1]);

  v2dWritePosSWF(out, state, 17, pen_x, pen_y);

  *old_x += pen_x;
  *old_y += pen_y;

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

Cubic Bezier curves are only available in SWF 11 so they need to be approximated
using quadratic beziers for lower versions.

@todo Use less line segments for shorter curves.

@param out about
@param state about
@param path about
@param old_x about
@param old_y about
@return writeme
*/

static v2d_bool_t v2dSaveSWF_pathCube(FILE *out, v2d_ui16_t *state, v2d_path_s *path, v2d_si32_t *old_x, v2d_si32_t *old_y)
{

  const v2d_real_t step = 1.0/7.0;

  v2d_path_s temp;
  v2d_real_t t = 0;

  int i = 0;

  v2d_real_t pos[2];

  pos[0] = v2dUnTwips(*old_x);
  pos[1] = v2dUnTwips(*old_y);

  for (t = step; t <= 1.0; t += step)
  {

    memcpy(&temp, path, sizeof(v2d_path_s));
    if (path->flags & V2D_PATH_FLAG_RELATIVE)
    {
      temp.flags -= V2D_PATH_FLAG_RELATIVE;
      for (i = 0; i < 2; i++)
      {
        temp.point[V2D_CONTROL_1][i] += pos[i];
        temp.point[V2D_CONTROL_2][i] += pos[i];
        temp.point[V2D_END_POINT][i] += pos[i];
      }
    }

    for (i = 0; i < 2; i++)
    {
      temp.point[V2D_END_POINT][i] =
        (1-t)*(1-t)*(1-t) * pos[i] +
        3*(1-t)*(1-t)*t   * temp.point[0][i] +
        3*(1-t)*t*t       * temp.point[1][i] +
        t*t*t             * temp.point[2][i];
    }

    v2dSaveSWF_pathLine(out, state, &temp, old_x, old_y);

  }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

writeme

@param out writeme
@param stage writeme
@param draw writeme
@param version writeme
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dSaveSWF_shape(FILE *out, v2d_stage_s *stage, v2d_draw_s *draw, v2d_ui8_t version)
{

  swf_tag_t  tag_type = 0;
  v2d_si32_t tag_offs = 0;
  v2d_ui16_t guid = 0;

  v2d_shape_s *shape = NULL;
  v2d_path_s *path = NULL;
  v2d_ui16_t state = 0;

  v2d_rect_s aabb;
  v2d_si32_t ox, oy, x, y;

  if (!out || !stage || !draw) { return V2D_FALSE; }

  shape = v2dDrawAsShape(draw);

  /* Write a DefineShape tag header. */

  if      (version >= 8) { tag_type = SWF_TAG_DEFINESHAPE4; }
  else if (version >= 3) { tag_type = SWF_TAG_DEFINESHAPE3; }
  else if (version >= 2) { tag_type = SWF_TAG_DEFINESHAPE2; }
  else                   { tag_type = SWF_TAG_DEFINESHAPE;  }

  tag_offs = ftell(out);
  guid = v2dGetDrawID(stage, draw);

  v2dLongTagSWF(out, tag_type, 0);
  v2dWriteUI16_LE(out, guid);

  /* Write ShapeBounds. This AABB includes the line stroke width. */

  v2dShapeGetAABB(shape, &aabb, V2D_TRUE);
  v2dxWriteRectSWF(out, &aabb);

  if (tag_type == SWF_TAG_DEFINESHAPE4)
  {

    v2d_ui8_t swf_shape_flags = 0;

    /* Write EdgeBounds. This AABB ignores the line stroke width. */

    v2dShapeGetAABB(shape, &aabb, V2D_FALSE);
    v2dxWriteRectSWF(out, &aabb);

    /* Flags. The only values of interest are "Scaling/Non-Scaling Strokes". */

    swf_shape_flags = SWF_DEFINESHAPE4_NONSCALINGSTROKE;

    if (shape->line)
    {
      if (shape->line->flags & V2D_LINE_FLAG_SCALING)
      { swf_shape_flags = SWF_DEFINESHAPE4_SCALINGSTROKE; }
    }

    fputc(swf_shape_flags, out);

  }

  /* Write the Fill/Line style arrays. */

  v2dFillArraySWF(out, stage, shape, tag_type);
  v2dLineArraySWF(out, stage, shape, tag_type);

  /* Number of bits used for fill/line ID in a SWF "StyleChange" ShapeRecord. */

  fputc((1 << 4) + 1, out);

  /* Output path data. */

  state = 0;
  ox = 0;
  oy = 0;
  x = 0;
  y = 0;

  for (path = v2dShapeGetPath(shape); path; path = v2dPathGetNext(path))
  {

    switch (path->type)
    {

      case V2D_PATH_LINE: v2dSaveSWF_pathLine(out, &state, path, &x, &y); break;
      case V2D_PATH_QUAD: v2dSaveSWF_pathQuad(out, &state, path, &x, &y); break;
      case V2D_PATH_CUBE: v2dSaveSWF_pathCube(out, &state, path, &x, &y); break;
      case V2D_PATH_MOVE: default:

        if (path == v2dShapeGetPath(shape))
        {
          /* Handle the first "move" path normally + set style information. */
          v2dSaveSWF_pathMove(out, &state, shape, path, &x, &y);
        }
        else
        {

          /* If the current path has not been closed yet, close it first. */

          if (x != ox || y != oy)
          {

            v2d_path_s temp;
            memset(&temp, 0, sizeof(v2d_path_s));
            temp.point[V2D_END_POINT][0] = v2dUnTwips(ox);
            temp.point[V2D_END_POINT][1] = v2dUnTwips(oy);

            v2dSaveSWF_showLine(out, &state, NULL, V2D_FALSE);
            v2dSaveSWF_pathLine(out, &state, &temp, &x, &y);

          }

          /* Draw an invisible line from the current point to the new point. */

          v2dSaveSWF_showLine(out, &state, NULL, V2D_TRUE);
          v2dSaveSWF_pathLine(out, &state, path, &x, &y);
          v2dSaveSWF_showLine(out, &state, shape, V2D_TRUE);

        }

        /* A "move" indicates the start of a new path, so update the origin. */

        ox = x;
        oy = y;

      break;

    }

  }

  /* If required, close the shape by adding a line back to the start point. */

  if (x != ox || y != oy)
  {

    v2d_path_s temp;
    memset(&temp, 0, sizeof(v2d_path_s));
    temp.point[V2D_END_POINT][0] = v2dUnTwips(ox);
    temp.point[V2D_END_POINT][1] = v2dUnTwips(oy);

    if (!(shape->flags & V2D_SHAPE_FLAG_CLOSE))
    { v2dSaveSWF_showLine(out, &state, NULL, V2D_FALSE); }

    v2dSaveSWF_pathLine(out, &state, &temp, &x, &y);

  }

  /* Write an end-of-shape record and update the tag header "length" value. */

  v2dWriteBitsSWF(out, &state, V2D_TRUE, V2D_FALSE, 6, 0);
  v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

  /* Report success. */

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Add StartSound tags, if required.

NOTE: The number-of-loops value (if present) is the number of times that
a sound should be played. This means that a value of 5 will play a sound
5 times. SWFDec and the official Adobe Flash player do this but (at time
of writing) GNASH plays the sound 6 times instead. This should be filed
as a bug at some point, if it isn't already known to the developers...

@param out about
@param stage about
@param sound about
@return about
*/

static v2d_bool_t v2dxSaveSWF_soundInfo(FILE *out, v2d_stage_s *stage, v2d_sound_s *sound)
{

  if      (!out || !stage || !sound) { return V2D_FALSE; }
  else if (!sound->data            ) { return V2D_FALSE; }

  v2dWriteUI16_LE(out, v2dGetDataID(stage, sound->data));

  if (sound->loops > 1)
  {
    fputc(SWF_DEFINESOUND_HASLOOPS, out);
    v2dWriteUI16_LE(out, sound->loops);
  }
  else { fputc(0, out); }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Add StartSound tags, if required.

NOTE: The number-of-loops value (if present) is the number of times that
a sound should be played. This means that a value of 5 will play a sound
5 times. SWFDec and the official Adobe Flash player do this but (at time
of writing) GNASH plays the sound 6 times instead. This should be filed
as a bug at some point, if it isn't already known to the developers...

@param out about
@param stage about
@param sound about
@return about
*/

static v2d_bool_t v2dxSaveSWF_startSound(FILE *out, v2d_stage_s *stage, v2d_sound_s *sound)
{

  v2d_ui32_t tag_offs = 0;

  if (!out || !stage || !sound) { return V2D_FALSE; }

  tag_offs = ftell(out);
  v2dLongTagSWF(out, SWF_TAG_STARTSOUND, 0);
  v2dxSaveSWF_soundInfo(out, stage, sound);
  v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Get the minimum SWF version needed for the opcodes of a script.

@param script about
@return writeme
*/

static v2d_ui8_t v2dxSaveSWF_scriptVersion(v2d_script_s *script)
{

  if (!script) { return 1; }

  /* TODO */

  return 3;

}

/**
@brief [INTERNAL] writeme

All actions are at least one byte in size (for the opcode itself).

@param action about
@return writeme
*/

static v2d_ui16_t v2dxSaveSWF_actionSize(v2d_action_s *action)
{

  v2d_ui16_t length, i;

  if (!action) { return 0; }

  switch (action->opcode)
  {

    case V2D_ACTION_LABEL:         return 0;  /* Not a real op-code. */

    case V2D_ACTION_SETTARGET:     return 1;
    case V2D_ACTION_PLAY:          return 1;
    case V2D_ACTION_STOP:          return 1;
    case V2D_ACTION_NEXTFRAME:     return 1;
    case V2D_ACTION_PREVFRAME:     return 1;
    case V2D_ACTION_GOTOFRAME:     return 4;  /* UI16 size, UI8 flags */

    case V2D_ACTION_ADD:           return 1;
    case V2D_ACTION_DIVIDE:        return 1;
    case V2D_ACTION_MULTIPLY:      return 1;
    case V2D_ACTION_SUBTRACT:      return 1;
    case V2D_ACTION_CONCATENATE:   return 1;

    case V2D_ACTION_EQUALS:        return 1;
    case V2D_ACTION_LESS:          return 1;
    case V2D_ACTION_STRINGEQUALS:  return 1;
    case V2D_ACTION_STRINGLESS:    return 1;

    case V2D_ACTION_AND:           return 1;
    case V2D_ACTION_OR:            return 1;
    case V2D_ACTION_NOT:           return 1;

    case V2D_ACTION_POP:           return 1;

    /* UI16 size + [UI8 type, 32-bit float] */
    case V2D_ACTION_PUSHNUMBER:    return 1 + 2 + (5 * action->num_params);

    /* UI16 size + [UI8 type, len(ASCIIZ text)] including NULL terminator. */
    case V2D_ACTION_PUSHSTRING:
      for (length = 1 + 2, i = 0; i < action->num_params; i++)
      { length += 1 + strlen(action->param[i]) + 1; }
    return length;

    case V2D_ACTION_ASCIITOCHAR:   return 1;
    case V2D_ACTION_CHARTOASCII:   return 1;
    case V2D_ACTION_TOINTEGER:     return 1;

    case V2D_ACTION_IF:            return 5; /* UI16 size, SI16 offset */
    case V2D_ACTION_JUMP:          return 5; /* UI16 size, SI16 offset */

    case V2D_ACTION_SETVARIABLE:   return 1;
    case V2D_ACTION_GETVARIABLE:   return 1;

    /*
    SetProperty works slightly differently in SWF, so the following is done:

    3 bytes   UI8 ActionPush + UI16 size  SWF ActionPush record header.
    5 bytes   UI8 type + 32-bit float     SWF Set/GetProperty index value.
    ? bytes   UI8 type + len(ASCII text)  The var to use for the new value.
    1 byte    UI8 GetVariable             Replace the var name with its value.
    1 byte    UI8 GetProperty             Pop value, index, target/Push result.
    */

    case V2D_ACTION_SETPROPERTY:   return 12 + strlen(action->param[1]);
    case V2D_ACTION_GETPROPERTY:   return 9;  /* Uses an 8 byte ActionPush. */

    case V2D_ACTION_CLONEGROUP:    return 55; /* See v2dxSaveSWF_action() */
    case V2D_ACTION_REMOVECLONE:   return 1;

    case V2D_ACTION_STARTDRAG:     return 1;
    case V2D_ACTION_ENDDRAG:       return 1;

    case V2D_ACTION_OPENURL:       return 4;  /* UI16 size, UI8 flags */
    case V2D_ACTION_STOPSOUNDS:    return 1;
    case V2D_ACTION_TOGGLEQUALITY: return 1;
    case V2D_ACTION_GETTIME:       return 1;
    case V2D_ACTION_RANDOMNUMBER:  return 1;
    case V2D_ACTION_TRACE:         return 1;

    /* Unknown. */

    case V2D_NUM_ACTIONS: default: break;

  }

  return 0;

}

/**
@brief [INTERNAL] about

The Adobe document suggests that SWF v3+ is required for scripting. However,
it seems that SWF v2 (and, possibly, SWF v1) support a subset of the actions
available in SWF v3. It doesn't really matter, though - SWF v4+ is needed in
order to do anything more complicated than Play/Stop/Next-Frame, so it's the
minimum version supported by V2DX at the moment. In future, SWF v5+ might be
the minimum supported version, in order to allow for more complex scripting.

@todo Handle AVM2.

@param out about
@param stage about
@param script about
@param version about
@param flags about
@return writeme
*/

static v2d_bool_t v2dxSaveSWF_action(FILE *out, v2d_stage_s *stage, v2d_ui8_t version, v2d_flag_t flags, v2d_script_s *script)
{

  v2d_action_s *action = NULL;

  if (!out || !stage) { return V2D_FALSE; }

  if      (!script                                    ) { return V2D_TRUE;  }
  else if (version < v2dxSaveSWF_scriptVersion(script)) { return V2D_FALSE; }

  if (flags & V2DX_SAVE_FLAG_SWF_AVM2) { /* TODO: Handle this properly... */ }

  /* Loop through the script actions list and output SWF bytecode. */

  for (action = v2dScriptGetAction(script); action; action = v2dActionGetNext(action))
  {

    switch (action->opcode)
    {

      case V2D_ACTION_LABEL:     /* Not a real op-code... */           break;
      case V2D_ACTION_SETTARGET: fputc(SWF_ACTION_SETTARGET2,    out); break;
      case V2D_ACTION_PLAY:      fputc(SWF_ACTION_PLAY,          out); break;
      case V2D_ACTION_STOP:      fputc(SWF_ACTION_STOP,          out); break;
      case V2D_ACTION_NEXTFRAME: fputc(SWF_ACTION_NEXTFRAME,     out); break;
      case V2D_ACTION_PREVFRAME: fputc(SWF_ACTION_PREVIOUSFRAME, out); break;

      case V2D_ACTION_GOTOFRAME:
        fputc(SWF_ACTION_GOTOFRAME2, out);
        v2dWriteUI16_LE(out, v2dxSaveSWF_actionSize(action) - 3);
        fputc(0, out);  /* Flags (TODO: Set a GotoAndStop/GotoAndPlay flag). */
      break;

      case V2D_ACTION_ADD:          fputc(SWF_ACTION_ADD,          out); break;
      case V2D_ACTION_SUBTRACT:     fputc(SWF_ACTION_SUBTRACT,     out); break;
      case V2D_ACTION_MULTIPLY:     fputc(SWF_ACTION_MULTIPLY,     out); break;
      case V2D_ACTION_DIVIDE:       fputc(SWF_ACTION_DIVIDE,       out); break;
      case V2D_ACTION_CONCATENATE:  fputc(SWF_ACTION_STRINGADD,    out); break;
      case V2D_ACTION_EQUALS:       fputc(SWF_ACTION_EQUALS,       out); break;
      case V2D_ACTION_LESS:         fputc(SWF_ACTION_LESS,         out); break;
      case V2D_ACTION_STRINGEQUALS: fputc(SWF_ACTION_STRINGEQUALS, out); break;
      case V2D_ACTION_STRINGLESS:   fputc(SWF_ACTION_STRINGLESS,   out); break;
      case V2D_ACTION_AND:          fputc(SWF_ACTION_AND,          out); break;
      case V2D_ACTION_OR:           fputc(SWF_ACTION_OR,           out); break;
      case V2D_ACTION_NOT:          fputc(SWF_ACTION_NOT,          out); break;
      case V2D_ACTION_POP:          fputc(SWF_ACTION_POP,          out); break;

      case V2D_ACTION_PUSHNUMBER:
      {

        v2d_ui16_t i = 0;

        fputc(SWF_ACTION_PUSH, out);
        v2dWriteUI16_LE(out, v2dxSaveSWF_actionSize(action) - 3);

        for (i = 0; i < action->num_params; i++)
        {

          double tmp_double = 0;
          float  tmp_single = 0;

          v2dxParseDouble(action->param[i], &tmp_double);
          tmp_single = (float)tmp_double;

          fputc(SWF_PUSH_FLOAT, out);
          fwrite(&tmp_single, 4, 1, out);

        }

      }
      break;

      case V2D_ACTION_PUSHSTRING:
      {

        v2d_ui16_t i = 0;

        fputc(SWF_ACTION_PUSH, out);
        v2dWriteUI16_LE(out, v2dxSaveSWF_actionSize(action) - 3);

        for (i = 0; i < action->num_params; i++)
        {
          fputc(SWF_PUSH_STRING, out);
          fprintf(out, "%s", action->param[i]); fputc(0, out);
        }

      }
      break;

      /* TODO: Handle multi-byte characters? */
      case V2D_ACTION_ASCIITOCHAR: fputc(SWF_ACTION_ASCIITOCHAR, out); break;
      case V2D_ACTION_CHARTOASCII: fputc(SWF_ACTION_CHARTOASCII, out); break;
      case V2D_ACTION_TOINTEGER:   fputc(SWF_ACTION_TOINTEGER,   out); break;

      /*
      SWF files expect IF and JUMP to specify the offset in BYTES, rather than
      "actions", as WaitForFrame does. As such, some computation is necessary.

      NOTE: An offset of zero points to the start of the NEXT instruction, not
            the start of THIS (i.e. the IF/JUMP) instruction.
      */

      case V2D_ACTION_IF:
      case V2D_ACTION_JUMP:
      {

        v2d_si32_t offset_in_bytes = 0;
        v2d_action_s *tmp = NULL;

        /* Try searching forwards first. */

        offset_in_bytes = 0;

        for (tmp = v2dActionGetNext(action); tmp; tmp = v2dActionGetNext(tmp))
        {

          if (tmp->opcode == V2D_ACTION_LABEL)
          {
            if (strcmp(action->param[0], tmp->param[0]) == 0) { break; }
          }

          offset_in_bytes += (v2d_ui32_t)v2dxSaveSWF_actionSize(tmp);

        }

        /* If that didn't work, try searching backwards instead. */

        if (!tmp)
        {

          offset_in_bytes = 0;

          for (tmp = action; tmp; tmp = v2dActionGetPrev(tmp))
          {

            offset_in_bytes -= (v2d_ui32_t)v2dxSaveSWF_actionSize(tmp);

            if (tmp->opcode == V2D_ACTION_LABEL)
            {
              if (strcmp(action->param[0], tmp->param[0]) == 0) { break; }
            }

          }

        }

        /* Output the IF/JUMP instruction, along with the number of bytes. */

        if (action->opcode == V2D_ACTION_IF) { fputc(SWF_ACTION_IF,   out); }
        else                                 { fputc(SWF_ACTION_JUMP, out); }
        v2dWriteUI16_LE(out, v2dxSaveSWF_actionSize(action) - 3);
        v2dWriteUI16_LE(out, (v2d_si16_t)offset_in_bytes);

      }
      break;

      case V2D_ACTION_SETVARIABLE: fputc(SWF_ACTION_SETVARIABLE, out); break;
      case V2D_ACTION_GETVARIABLE: fputc(SWF_ACTION_GETVARIABLE, out); break;

      case V2D_ACTION_SETPROPERTY:
      case V2D_ACTION_GETPROPERTY:
      {

        float tmp_float = -1; /* Note that real SWF values are zero or more. */

        /* Convert the name string used by V2D into an SWF index value. */

        if      (!strcmp(action->param[0], "x")) { tmp_float = SWF_PROPERTY_X; }
        else if (!strcmp(action->param[0], "y")) { tmp_float = SWF_PROPERTY_Y; }

        if (action->opcode == V2D_ACTION_GETPROPERTY)
        {

          /* SWF expects to pop [SWFPropertyIndex, TargetName]... */

          fputc(SWF_ACTION_PUSH, out); v2dWriteUI16_LE(out, 5);
          fputc(SWF_PUSH_FLOAT, out); fwrite(&tmp_float, 4, 1, out);
          fputc(SWF_ACTION_GETPROPERTY, out);

        }
        else
        {

          /* SWF expects to pop [NewValue, SWFPropertyIndex, TargetName]... */

          fputc(SWF_ACTION_PUSH, out);
          v2dWriteUI16_LE(out, 7 + strlen(action->param[1]));

          fputc(SWF_PUSH_FLOAT, out);
          fwrite(&tmp_float, 4, 1, out);

          fputc(SWF_PUSH_STRING, out);
          fprintf(out, "%s", action->param[1]); fputc(0, out);

          fputc(SWF_ACTION_GETVARIABLE, out);
          fputc(SWF_ACTION_SETPROPERTY, out);

        }

      }
      break;

      /*
      The SWF CloneSprite opcode pops [depth, target, source] from the stack.
      V2D currently only specifies [target, source], so the depth parameter
      needs to be added on automatically.

      FIXME: "__Z" will eventually be too high, so a better solution is needed.

      Although it's not mentioned anywhere in the official specification, only
      items at z-depth 16384 or greater are affected by RemoveSprite opcodes:

      https://www.gnu.org/software/gnash/manual/doxygen/classgnash_1_1MovieClip.html

      http://www.senocular.com/flash/tutorials/depths/?page=2

      This has been verified using Gnash and the official Adobe Flash player.
      */

      case V2D_ACTION_CLONEGROUP:
      {

        const float ADDZ = 16383;

        fputc(SWF_ACTION_PUSH, out); v2dWriteUI16_LE(out, 20);  /* 3 bytes. */
        fputc(SWF_PUSH_STRING, out); fwrite("__Z", 1, 4, out);  /* 5 bytes. */
        fputc(SWF_PUSH_STRING, out); fwrite("__Z", 1, 4, out);  /* 5 bytes. */
        fputc(SWF_PUSH_STRING, out); fwrite("__Z", 1, 4, out);  /* 5 bytes. */
        fputc(SWF_PUSH_STRING, out); fwrite("__Z", 1, 4, out);  /* 5 bytes. */

        /*
        Get the value of __Z. It's initially undefined; converting it into a
        number will result in either zero (SWF 6 and below) or NaN (SWF 7+).
        */

        fputc(SWF_ACTION_GETVARIABLE, out);                     /* 1 byte.  */
        fputc(SWF_ACTION_TOINTEGER, out);                       /* 1 byte.  */

        /* Skip the next few instructions if __Z is already a valid number. */

        fputc(SWF_ACTION_IF, out); v2dWriteUI16_LE(out, 2);     /* 3 bytes. */
        v2dWriteUI16_LE(out, 14);                               /* 2 bytes. */

        /* Initialise __Z so that it's ready to be used as a number. */

        fputc(SWF_ACTION_PUSH, out); v2dWriteUI16_LE(out, 10);  /* 3 bytes. */
        fputc(SWF_PUSH_STRING, out); fwrite("__Z", 1, 4, out);  /* 5 bytes. */
        fputc(SWF_PUSH_FLOAT,  out); fwrite(&ADDZ, 4, 1, out);  /* 5 bytes. */

        fputc(SWF_ACTION_SETVARIABLE, out);                     /* 1 byte.  */

        /* Get the value of __Z, add one to it, and update the __Z variable. */

        fputc(SWF_ACTION_GETVARIABLE, out);                     /* 1 byte.  */

        fputc(SWF_ACTION_PUSH, out); v2dWriteUI16_LE(out, 3);   /* 3 bytes. */
        fputc(SWF_PUSH_STRING, out); fwrite("1", 1, 2, out);    /* 3 bytes. */

        fputc(SWF_ACTION_ADD, out);                             /* 1 byte.  */
        fputc(SWF_ACTION_SETVARIABLE, out);                     /* 1 byte.  */

        /* Use the value of __Z as the depth parameter for CloneSprite. */

        fputc(SWF_ACTION_GETVARIABLE, out);                     /* 1 byte.  */
        fputc(SWF_ACTION_CLONESPRITE, out);                     /* 1 byte.  */

      }
      break;

      case V2D_ACTION_REMOVECLONE: fputc(SWF_ACTION_REMOVESPRITE, out); break;
      case V2D_ACTION_STARTDRAG:   fputc(SWF_ACTION_STARTDRAG,    out); break;
      case V2D_ACTION_ENDDRAG:     fputc(SWF_ACTION_ENDDRAG,      out); break;

      case V2D_ACTION_OPENURL:
        fputc(SWF_ACTION_GETURL2, out);
        v2dWriteUI16_LE(out, v2dxSaveSWF_actionSize(action) - 3);
        fputc(0, out);  /* Flags. */
      break;

      case V2D_ACTION_STOPSOUNDS:    fputc(SWF_ACTION_STOPSOUNDS,    out); break;
      case V2D_ACTION_TOGGLEQUALITY: fputc(SWF_ACTION_TOGGLEQUALITY, out); break;
      case V2D_ACTION_GETTIME:       fputc(SWF_ACTION_GETTIME,       out); break;
      case V2D_ACTION_RANDOMNUMBER:  fputc(SWF_ACTION_RANDOMNUMBER,  out); break;
      case V2D_ACTION_TRACE:         fputc(SWF_ACTION_TRACE,         out); break;

      /* Unknown. */

      case V2D_NUM_ACTIONS: default:
        fprintf(stderr, "[ERROR] Unknown opcode: %d\n", action->opcode);
      break;

    }

  }

  /* Output an ActionEndFlag to indicate that there aren't any more actions. */

  fputc(0, out);

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] about

The Adobe document suggests that SWF v3+ is required for scripting. However,
it seems that SWF v2 (and, possibly, SWF v1) support a subset of the actions
available in SWF v3. It doesn't really matter, though - SWF v4+ is needed in
order to do anything more complicated than Play/Stop/Next-Frame, so it's the
minimum version supported by V2DX at the moment. In future, SWF v5+ might be
the minimum supported version, in order to allow for more complex scripting.

@todo Handle AVM2.

@param out about
@param stage about
@param script about
@param version about
@param flags about
@return writeme
*/

static v2d_bool_t v2dxSaveSWF_script(FILE *out, v2d_stage_s *stage, v2d_ui8_t version, v2d_flag_t flags, v2d_script_s *script)
{

  v2d_ui32_t tag_offs = 0;

  /* TODO: Need validation checks here. */

  tag_offs = ftell(out);
  v2dLongTagSWF(out, SWF_TAG_DOACTION, 0);
  v2dxSaveSWF_action(out, stage, version, flags, script);
  v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] about

writeme

@param out about
@param stage about
@param version about
@param flags about
@param button about
@return writeme
*/

static v2d_bool_t v2dxSaveSWF_button(FILE *out, v2d_stage_s *stage, v2d_ui8_t version, v2d_flag_t flags, v2d_anim_s *button)
{

  v2d_ui16_t tag_guid = 0;
  v2d_ui32_t tag_offs = 0;
  v2d_matrix_t matrix;
  v2d_state_t s;
  v2d_event_t e;

  v2d_ui32_t offs = 0;

  if (!out || !stage || !button) { return V2D_FALSE; }

  v2dMatrixReset(matrix);
  v2dMatrixPosition(matrix, 0, 0);

  tag_offs = ftell(out);
  tag_guid = v2dGetButtonID(stage, button);
  v2dLongTagSWF(out, SWF_TAG_DEFINEBUTTON2, 0);
  v2dWriteUI16_LE(out, tag_guid);

  /* Flags. */
  fputc(0, out);

  /* bytes until button action records. */
  offs = ftell(out);
  v2dWriteUI16_LE(out, 0);

  /* Button appearance information. */

  for (s = 0; s < V2D_NUM_STATES; s++)
  {

    fputc((1 << s) + (s == V2D_STATE_OVER ? 8 : 0), out);  /* Flags. */
    v2dWriteUI16_LE(out, v2dGetDrawID(stage, button->draw[s]));
    v2dWriteUI16_LE(out, 0);  /* Placement depth. */
    v2dWriteMatrixSWF(out, matrix, V2D_FALSE);
    fputc(0, out);  /* CXFORMWITHALPHA */

  }

  fputc(0, out);  /* end-of-button */

  /* Button actions. */

  for (e = 0; e < V2D_NUM_EVENTS; e++)
  {

    if (!button->script[e]) { continue; }

    v2dWriteOffset(out, offs, 0, V2D_FALSE);
    offs = ftell(out);
    v2dWriteUI16_LE(out, 0);

    /* Flags */
    fputc(1 << e, out);
    fputc(0, out);

    v2dxSaveSWF_action(out, stage, version, flags, button->script[e]);

  }

  /* End of button tag. */

  v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

  /* Button sounds. */

  for (e = 0; e < V2D_NUM_EVENTS; e++) { if (button->sound[e]) { break; } }

  if (e != V2D_NUM_EVENTS)
  {

    tag_offs = ftell(out);
    tag_guid = v2dGetButtonID(stage, button);
    v2dLongTagSWF(out, SWF_TAG_DEFINEBUTTONSOUND, 0);
    v2dWriteUI16_LE(out, tag_guid);

    if (!v2dxSaveSWF_soundInfo(out, stage, button->sound[V2D_EVENT_MOUSEEXIT])) { v2dWriteUI16_LE(out, 0); }
    if (!v2dxSaveSWF_soundInfo(out, stage, button->sound[V2D_EVENT_MOUSEOVER])) { v2dWriteUI16_LE(out, 0); }
    if (!v2dxSaveSWF_soundInfo(out, stage, button->sound[V2D_EVENT_PRESS    ])) { v2dWriteUI16_LE(out, 0); }
    if (!v2dxSaveSWF_soundInfo(out, stage, button->sound[V2D_EVENT_RELEASE  ])) { v2dWriteUI16_LE(out, 0); }

    v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

  }

  /* Report success. */

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] Output display items (and any animation data) to an SWF file.

@param out The SWF file to write the animation data to.
@param stage The "stage" structure which contains the list of display items.
@param group The group that contains the display items.
@param version The version of the SWF file. This dictates which tags are used.
@param flags about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSWF_item(FILE *out, v2d_stage_s *stage, v2d_chain_s *group, v2d_ui8_t version, v2d_flag_t flags)
{

  v2d_ui32_t num_frames = v2dGroupCountFrames(group);
  v2d_ui32_t frame = 0;

  if (!out || !stage || !group) { return V2D_FALSE; }

  for (frame = 0; frame < num_frames; frame++)
  {

    v2d_item_s *item = NULL;

    for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
    {

      v2d_anim_s *anim = v2dItemGetFrame(item, frame);
      v2d_anim_s *prev = v2dAnimGetPrev(anim);
      v2d_ui16_t tag_guid = v2dGetItemID(v2dGroupGetItem(group), item);

      v2d_ui16_t character_id = 0;
      v2d_ui32_t tag_offs = 0;

      if      (!anim               ) { continue; }
      else if (anim->frame != frame) { continue; }

      /* If this sprite was previously visible, remove the old version first. */

      if (prev)
      {
        if (v2dItemVisible(item, prev->frame))
        {

          if (prev->flags & V2D_ANIM_FLAG_BUTTON) { character_id = v2dGetButtonID(stage, prev); }
          else                                    { character_id = v2dGetDrawID(stage, prev->draw[V2D_DEFAULT]); }

          tag_offs = ftell(out);
          v2dLongTagSWF(out, version < 3 ? SWF_TAG_REMOVEOBJECT : SWF_TAG_REMOVEOBJECT2, 0);
          if (version < 3) { v2dWriteUI16_LE(out, character_id); }
          v2dWriteUI16_LE(out, tag_guid); /* Depth */
          v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

        }
      }

      /* Place a new instance of the drawable used for this keyframe. */

      if (v2dItemVisible(item, frame))
      {

        if (anim->flags & V2D_ANIM_FLAG_BUTTON)
        {
          v2dxSaveSWF_button(out, stage, version, flags, anim);
          character_id = v2dGetButtonID(stage, anim);
        }
        else
        {
          character_id = v2dGetDrawID(stage, anim->draw[V2D_DEFAULT]);
        }

        tag_offs = ftell(out);

        if (version < 3)
        {

          v2dLongTagSWF(out, SWF_TAG_PLACEOBJECT, 0);
          v2dWriteUI16_LE(out, character_id);
          v2dWriteUI16_LE(out, tag_guid); /* Depth */
          v2dWriteMatrixSWF(out, anim->matrix, V2D_FALSE);

        }
        else
        {

          v2dLongTagSWF(out, version < 8 ? SWF_TAG_PLACEOBJECT2 : SWF_TAG_PLACEOBJECT3, 0);

          if (item->name) { fputc(SWF_PLACEOBJECT2_HASCHARACTER | SWF_PLACEOBJECT2_HASMATRIX | SWF_PLACEOBJECT2_HASNAME, out); }
          else            { fputc(SWF_PLACEOBJECT2_HASCHARACTER | SWF_PLACEOBJECT2_HASMATRIX,                            out); }

          if (version >= 8) { fputc(0, out);  /* Extra flags. */ }

          v2dWriteUI16_LE(out, tag_guid); /* Depth */
          v2dWriteUI16_LE(out, character_id);
          v2dWriteMatrixSWF(out, anim->matrix, V2D_FALSE);

          if (item->name) { fprintf(out, "%s", item->name); fputc(0, out); }

        }

        v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

      }

      /* Play any sounds/run any scripts for this keyframe. */

      if (!(anim->flags & V2D_ANIM_FLAG_BUTTON))
      {
        v2dxSaveSWF_startSound(out, stage, anim->sound[V2D_DEFAULT]);
        v2dxSaveSWF_script(out, stage, version, flags, anim->script[V2D_DEFAULT]);
      }

    }

    /* Output an End-of-Frame tag. */

    v2dShortTagSWF(out, SWF_TAG_SHOWFRAME, 0);

  }

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] writeme

about

@param out The file to write to.
@param stage about
@param draw about
@param version about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dSaveSWF_group(FILE *out, v2d_stage_s *stage, v2d_draw_s *draw, v2d_ui8_t version)
{

  v2d_ui32_t tag_offs = 0;
  v2d_ui16_t guid = 0;

  if      (!out || !stage || !draw)      { return V2D_FALSE; }
  else if (draw->type != V2D_TYPE_GROUP) { return V2D_FALSE; }

  tag_offs = ftell(out);
  guid = v2dGetDrawID(stage, draw);

  v2dLongTagSWF(out, SWF_TAG_DEFINESPRITE, 0);
  v2dWriteUI16_LE(out, guid);
  v2dWriteUI16_LE(out, v2dGroupCountFrames(v2dDrawAsGroup(draw)));

  v2dxSaveSWF_item(out, stage, v2dDrawAsGroup(draw), version, 0);

  v2dShortTagSWF(out, SWF_TAG_END, 0);
  v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);

  return V2D_TRUE;

}

/* ========================================================================== */

/**
@brief [INTERNAL] about

writeme

@param out writeme
@param stage writeme
@param version writeme
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dSaveSWF_draw(FILE *out, v2d_stage_s *stage, v2d_ui8_t version)
{

  v2d_draw_s *draw = NULL;

  if (!out || !stage) { return V2D_FALSE; }

  /* Export the drawables. */

  for (draw = v2dStageGetDraw(stage); draw; draw = v2dDrawGetNext(draw))
  {
    switch (draw->type)
    {
      case V2D_TYPE_TEXT:  v2dSaveSWF_text (out, stage, draw, version); break;
      case V2D_TYPE_SHAPE: v2dSaveSWF_shape(out, stage, draw, version); break;
      case V2D_TYPE_GROUP: v2dSaveSWF_group(out, stage, draw, version); break;
      default: break;
    }
  }

  return V2D_TRUE;

}

/* ========================================================================== */

/*
[PUBLIC] Export a project in Adobe Flash (SWF) format.
*/

v2d_bool_t v2dxSaveSWF(v2d_stage_s *stage, const char *name, v2d_ui8_t version, v2d_flag_t flags)
{

  FILE *out = NULL;
  v2d_rect_s aabb;

  if (!stage || !name) { return V2D_FALSE; }

  /* Items in SWF files can only reference things which are defined earlier. */

  if (!v2dDrawSort(stage->draw, V2D_FALSE))
  {
    fprintf(stderr, "[ERROR] SWF export needs drawables to be sorted first.\n");
    return V2D_FALSE;
  }

  /* Check version/features. */

  if (version < SWF_VERSION_MIN || version > SWF_VERSION_MAX)
  {
    fprintf(stderr, "SWF version %d is not supported.\n", version);
    return V2D_FALSE;
  }
  else if (version < 9 && flags & V2DX_SAVE_FLAG_SWF_AVM2)
  {
    fprintf(stderr, "SWF %d doesn't support ActionScript 3.0\n", version);
    return V2D_FALSE;
  }
  else if (version < 6 && flags & V2DX_SAVE_FLAG_ZLIB)
  {
    fprintf(stderr, "SWF %d doesn't support ZLIB compression.\n", version);
    return V2D_FALSE;
  }
  else if (version < 13 && flags & V2DX_SAVE_FLAG_SWF_LZMA)
  {
    fprintf(stderr, "SWF %d doesn't support LZMA compression.\n", version);
    return V2D_FALSE;
  }
  else if (version < 2 && flags & V2DX_SAVE_FLAG_SWF_PROTECTED)
  {
    fprintf(stderr, "SWF %d doesn't support import protection.\n", version);
    return V2D_FALSE;
  }

  /* Handle mutually exclusive flags. */

  if (flags & V2DX_SAVE_FLAG_ZLIB)
  {
    if (flags & V2DX_SAVE_FLAG_SWF_LZMA)
    {
      fprintf(stderr, "[SWF] Can't use both ZLIB and LZMA; choose one!\n");
      return V2D_FALSE;
    }
  }

  /* Open the output file and write the file header. */

  out = fopen(name, "wb");
  if (!out) { return V2D_FALSE; }

  if      (flags & V2DX_SAVE_FLAG_ZLIB    ) { fputc(/*'C'*/'F', out); }
  else if (flags & V2DX_SAVE_FLAG_SWF_LZMA) { fputc(/*'Z'*/'F', out); }
  else                                      { fputc('F', out); }

  fputc('W', out); fputc('S', out); fputc(version, out);
  v2dWriteSI32_LE(out, 0);  /* Dummy File Length value - overwritten later. */

  /* Output basic stage information. */

  aabb.x1 = 0;
  aabb.y1 = 0;
  aabb.x2 = stage->width;
  aabb.y2 = stage->height;
  v2dxWriteRectSWF(out, &aabb);

  v2dWriteUI16_LE(out, v2dFixed16(stage->fps));
  v2dWriteUI16_LE(out, v2dGroupCountFrames(stage->item));

  /* SWF v8+: Output FileAttributes tag (Must be the first tag in the file). */

  if (version >= 8)
  {

    v2d_ui32_t swf_flags = 0;

    if (stage->metadata                      ) { swf_flags |= SWF_FLAG_HASMETADATA; }
    if (flags & V2DX_SAVE_FLAG_SWF_USENETWORK) { swf_flags |= SWF_FLAG_USENETWORK;  }

    if (version >= 9)
    {
      if (flags & V2DX_SAVE_FLAG_SWF_AVM2) { swf_flags |= SWF_FLAG_ACTIONSCRIPT3; }
    }

    if (version >= 10) { swf_flags |= SWF_FLAG_USEDIRECTBLIT | SWF_FLAG_USEGPU; }

    v2dShortTagSWF(out, SWF_TAG_FILEATTRIBUTES, 4);
    v2dWriteUI32_LE(out, swf_flags);

  }

  /* If required, write a "protect" tag to disable import into the Flash IDE. */

  if (flags & V2DX_SAVE_FLAG_SWF_PROTECTED)
  { v2dShortTagSWF(out, SWF_TAG_PROTECT, 0); }

  /* Optionally enable advanced telemetry. */

  if (flags & V2DX_SAVE_FLAG_SWF_TELEMETRY)
  {
    v2dShortTagSWF(out, SWF_TAG_ENABLETELEMETRY, 2);
    v2dWriteUI16_LE(out, 0);  /* Reserved. */
  }

  /* Write metadata. Note: FileAttributes expects a flag to be set for this. */

  if (stage->metadata)
  {
    v2d_ui32_t tag_offs = ftell(out);
    v2dLongTagSWF(out, SWF_TAG_METADATA, 0);
    fprintf(out, "%s", stage->metadata); fputc(0, out);
    v2dWriteOffset(out, tag_offs + 6, -4, V2D_TRUE);
  }

  /* Output "SetBackgroundColor" tag. */

  v2dShortTagSWF(out, SWF_TAG_SETBACKGROUNDCOLOR, 3);
  fputc(stage->rgba[0], out);
  fputc(stage->rgba[1], out);
  fputc(stage->rgba[2], out);

  /* Define Shapes, Fonts, etc. with unique IDs. */

  v2dSaveImageSWF(out, stage, version);
  v2dSaveAudioSWF(out, stage, version);
  v2dSaveEmbedSWF(out, stage, version);
  v2dSaveFontSWF(out, stage, version);
  v2dSaveSWF_draw(out, stage, version);

  /* Output the display items. */

  v2dxSaveSWF_item(out, stage, stage->item, version, flags);

  /* Update the header "file-size" value, close the file, and report success. */

  v2dShortTagSWF(out, SWF_TAG_END, 0);
  v2dWriteOffset(out, 0, 4, V2D_TRUE);

  fclose(out);

  return V2D_TRUE;

}
