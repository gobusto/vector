/*

FILENAME: save_svg.c
AUTHOR/S: Thomas Dennis
CREATION: 16th December 2012

Copyright (C) 2012-2014 by Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file save_svg.c

@brief Provides functions for exporting projects to SVG files.

@todo Actually implement (optional) ZLIB compression support...

@section animation_sec Animation

There are two ways to do animation with SVG files: Using scripts, or with SMIL.
This code originally used SMIL, but now uses scripts instead, for the following
reasons:

(1) SMIL animation is based on time values, but scripts must be able to pause
playback or jump to other frames at any point. This is further complicated by
what SWF files call "Movie Clips" i.e. sub-animations with their own timeline.

(2) SMIL animation doesn't allow direct manipulation of matrices. It's possible
to work around this:

http://math.stackexchange.com/questions/78137/decomposition-of-a-nonsquare-affine-matrix

...however, this results in larger file sizes, and playback is rather slow.

(3) Internet Explorer does not support SMIL animation.

Further information can be found here: http://www.w3.org/TR/SVG/animate.html

@section audio_sec Audio

SVG v1.2 includes support for "audio" elements - see the specification:

http://www.w3.org/TR/SVGTiny12/multimedia.html#AudioElement

Unfortunately, this feature isn't supported by anything at the moment.
Chromium 26, Epiphany 3.4, and Firefox 10 all ignore it, and Opera 9
has it listed as "unsupported" on the official website:

http://www.opera.com/docs/specs/opera9/svg/

Also, the W3 validator treats it (and any other 1.2 features) as "invalid":

http://validator.w3.org/#validate_by_upload

As a work-around, it's possible to embed HTML audio tags as foreign objects
within an SVG, as demonstrated here:

http://svg-wow.org/audio/animated-lyrics.svg

The HTML audio tag is more limited than the SVG 1.2 one is. For example, it
doesn't allow a sound to be played "X" times - sounds either repeat forever
or play once. This could be worked around using ECMAScript, however. TODO.

http://www.w3.org/wiki/HTML/Elements/audio

An awkward problem is that Chromium 26 and Epiphany 3.4 will play MP3 audio
data, but Firefox 10 won't. It can play other formats (such as OGG) but SWF
files don't support those. Perhaps V2D should allow alternative audio sources
to be specified, as the HTML5 audio tag does? This would allow Firefox to use
an alternative (OGG) version of an unsupported (MP3) audio file... TODO?
*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "save.h"
#include "misc/fileinfo.h"
#include "misc/base64.h"
#include "misc/misc.h"
#include "xml/xml.h"

/**
@brief [INTERNAL] Embed the contents of a file using base64 encoding.

See http://en.wikipedia.org/wiki/Data_URI_scheme#HTML for more information.

@param attr The XML attribute whose value should be set.
@param filename The name and path of the file to be embedded.
@param format The file type. Used to determine the "Internet Media Type" text.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxEmbedData(xml_s *attr, const char *filename, xxx_file_t format)
{

  const char *prefix = "data:application/octet-stream;base64,";

  FILE *src = NULL;
  char *buffer = NULL;
  size_t buffer_size = 0;

  v2d_bool_t status = V2D_FALSE;

  /* Get the format of the file. */

  switch (format)
  {
    case XXX_FILE_JPG: prefix = "data:image/jpeg;base64,"; break;
    case XXX_FILE_PNG: prefix = "data:image/png;base64,";  break;
    case XXX_FILE_GIF: prefix = "data:image/gif;base64,";  break;
    case XXX_FILE_MP3: prefix = "data:audio/mpeg;base64,"; break;
    case XXX_FILE_WAV: prefix = "data:audio/wav;base64,";  break;
    case XXX_FILE_UNKNOWN: default: break;
  }

  /* Open the input file. */

  if (!filename) { return V2D_FALSE; }
  src = fopen(filename, "rb");
  if (!src) { return V2D_FALSE; }

  /* Determine how much memory is needed to store the data in base64 format. */

  buffer_size = encodeBase64(src, 0, NULL);

  if (buffer_size <= 0)
  {
    fclose(src);
    return V2D_FALSE;
  }

  /* Allocate a buffer, and fill it with the base64-encoded file data. */

  buffer = (char*)malloc(buffer_size + 1);

  if (buffer)
  {

    memset(buffer, 0, buffer_size + 1);

    if (encodeBase64(src, buffer_size, buffer) == buffer_size)
    {

      if (!xmlSetText(attr, prefix)) { status = !xmlAddText(attr, buffer); }
      else                           { status = V2D_FALSE;                 }

    } else { status = V2D_FALSE; }

    free(buffer);

  } else { status = V2D_FALSE; }

  /* Close the file, and report the result. */

  fclose(src);
  return status;

}

/**
@brief [INTERNAL] Output a linked-list of display items as SVG 'use' elements.

This function is necessary for SVG viewers that don't support scripts; progams
that do (such as web browsers) are able to create instances of display items
dynamically as part of their animation logic.

It's also used to output items for groups within the 'defs' tag.

@param name The value to use for the group 'id' attribute.
@param group A linked list of display item structures.
@param root The "parent" XML node that any new XML tags should be added to.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_item(const char *name, v2d_chain_s *group, xml_s *root)
{

  xml_s *item_node = NULL;
  v2d_item_s *item = NULL;
  char tmp_text[512];

  if (!name || !group || !root) { return V2D_FALSE; }

  root = xmlNewChildNode(root, "g", NULL);
  xmlNewAttribute(root, "id", name);

  for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
  {

    item_node = xmlNewChildNode(root, "use", NULL);

    sprintf(tmp_text, "#__%p", (void*)v2dItemGetAnim(item)->draw[V2D_DEFAULT]);
    xmlNewAttribute(item_node, "xlink:href", tmp_text);

    sprintf(tmp_text, "matrix(%f %f %f %f %f %f)",
          v2dItemGetAnim(item)->matrix[0][0], v2dItemGetAnim(item)->matrix[0][1],
          v2dItemGetAnim(item)->matrix[1][0], v2dItemGetAnim(item)->matrix[1][1],
          v2dItemGetAnim(item)->matrix[2][0], v2dItemGetAnim(item)->matrix[2][1]);
    xmlNewAttribute(item_node, "transform", tmp_text);

    if (!v2dItemVisible(item, 0)) { xmlNewAttribute(item_node, "visibility", "hidden"); }

  }

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output fill-style data for a shape, if required.

Called by v2dxSaveSVG_path() internally.

NOTE: Shapes with flat RGBA fills (or without any fill at all) are ignored.

@param shape The shape containing the fill-style data.
@param root The "parent" XML node that any new XML tags should be added to.
@param flags Flags to alter the output in various ways.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_fill(v2d_shape_s *shape, xml_s *root, v2d_flag_t flags)
{

  xml_s *fill_node = NULL;

  char text[512];

  if      (!shape || !root) { return V2D_FALSE; }
  else if (!shape->fill   ) { return V2D_FALSE; }

  if (flags) { /* Silence "unused parameter" warnings. */ }

  /* Simple "flat" RGBA fills do not require any special tags to be output... */

  if (v2dIsFlatRGBA(shape->fill)) { return V2D_FALSE; }

  /* ...but gradient or bitmap fills need to be defined before they are used. */

  if (shape->fill->type == V2D_FILL_IMAGE)
  {

    const char *file_name = v2dFillAsImage(shape->fill)->image->file_name;

    xxx_file_t format;
    v2d_ui32_t width  = 0;
    v2d_ui32_t height = 0;
    xml_s *attr = NULL;

    format = xxxFileTypeImage(file_name, &width, &height);

    /* Raster image fills are implemented as "patterns" containing an image. */

    fill_node = xmlNewChildNode(root, "pattern", NULL);
    sprintf(text, "__%p", (void*)shape->fill);
    xmlNewAttribute(fill_node, "id", text);

    sprintf(text, "%lu", width ); xmlNewAttribute(fill_node, "width",  text);
    sprintf(text, "%lu", height); xmlNewAttribute(fill_node, "height", text);

    sprintf(text, "matrix(%f %f %f %f %f %f)",
          shape->fill->matrix[0][0], shape->fill->matrix[0][1],
          shape->fill->matrix[1][0], shape->fill->matrix[1][1],
          shape->fill->matrix[2][0], shape->fill->matrix[2][1]);
    xmlNewAttribute(fill_node, "patternTransform", text);
    xmlNewAttribute(fill_node, "patternUnits", "userSpaceOnUse");

    /* Once a "pattern" node has been created, add an "image" subnode to it. */

    fill_node = xmlNewChildNode(fill_node, "image", NULL);
    sprintf(text, "%lu", width ); xmlNewAttribute(fill_node, "width",  text);
    sprintf(text, "%lu", height); xmlNewAttribute(fill_node, "height", text);
    attr = xmlNewAttribute(fill_node, "xlink:href", file_name);

    if (flags & V2DX_SAVE_FLAG_SVG_EMBED) { v2dxEmbedData(attr, file_name, format); }

  }
  else if (shape->fill->type == V2D_FILL_COLOR)
  {

    v2d_fill_color_s *stop      = NULL;
    xml_s            *stop_node = NULL;

    /* Gradient fills use an xxxGradient tag to contain a set of "stop" tags. */

    if (shape->fill->flags & V2D_FILL_FLAG_RADIAL)
         { fill_node = xmlNewChildNode(root, "radialGradient", NULL); }
    else { fill_node = xmlNewChildNode(root, "linearGradient", NULL); }

    sprintf(text, "__%p", (void*)shape->fill);
    xmlNewAttribute(fill_node, "id", text);

    sprintf(text, "matrix(%f %f %f %f %f %f)",
          shape->fill->matrix[0][0], shape->fill->matrix[0][1],
          shape->fill->matrix[1][0], shape->fill->matrix[1][1],
          shape->fill->matrix[2][0], shape->fill->matrix[2][1]);
    xmlNewAttribute(fill_node, "gradientTransform", text);

    /* Once the main xxxGradient tag has been defined, add "stop" tags to it. */

    for (stop = v2dFillAsColor(shape->fill); stop; stop = v2dFillGetNext(stop))
    {

      stop_node = xmlNewChildNode(fill_node, "stop", NULL);

      sprintf(text, "%f%%", stop->stop * 100.0);
      xmlNewAttribute(stop_node, "offset", text);

      sprintf(text, "#%02x%02x%02x", stop->rgba[0], stop->rgba[1], stop->rgba[2]);
      xmlNewAttribute(stop_node, "stop-color", text);

      sprintf(text, "%f", (double)stop->rgba[3] / 255.0);
      xmlNewAttribute(stop_node, "stop-opacity", text);

    }

  }

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output a shape drawable as an SVG 'path' element.

Called by v2dxSaveSVG_draw() internally.

@param draw The shape drawable to be handled.
@param root The "parent" XML node that any new XML tags should be added to.
@param flags Flags to alter the output in various ways.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_path(v2d_draw_s *draw, xml_s *root, v2d_flag_t flags)
{

  v2d_shape_s *shape = NULL;
  v2d_path_s *path = NULL;

  xml_s *path_node = NULL;
  xml_s *path_attr = NULL;

  char tmp_text[512];

  if      (!draw || !root)               { return V2D_FALSE; }
  else if (draw->type != V2D_TYPE_SHAPE) { return V2D_FALSE; }

  shape = v2dDrawAsShape(draw);

  /* Create any "fill-style" tags first, so that the shape can utilise them. */

  v2dxSaveSVG_fill(shape, root, flags);

  /* Create a new path tag. */

  path_node = xmlNewChildNode(root, "path", NULL);
  sprintf(tmp_text, "__%p", (void*)draw);
  xmlNewAttribute(path_node, "id", tmp_text);

  /* Output line-style attributes. */

  if (shape->line)
  {

    sprintf(tmp_text, "%f", shape->line->width);
    xmlNewAttribute(path_node, "stroke-width", tmp_text);

    /* NOTE: SVG 1.2 supports "non-scaling" strokes, but SVG 1.1 doesn't... */

    if (flags & V2DX_SAVE_FLAG_SVG_VER_1_2)
    {
      if (!(shape->line->flags & V2D_LINE_FLAG_SCALING))
      { xmlNewAttribute(path_node, "vector-effect", "non-scaling-stroke"); }
    }

    /* Set the line colour. */

    sprintf(tmp_text, "#%02x%02x%02x",
        shape->line->rgba[0], shape->line->rgba[1], shape->line->rgba[2]);
    xmlNewAttribute(path_node, "stroke", tmp_text);

    sprintf(tmp_text, "%f", (double)shape->line->rgba[3] / 255.0);
    xmlNewAttribute(path_node, "stroke-opacity", tmp_text);

    /* Set the line-cap style. */

    switch(shape->line->cap)
    {
      case V2D_LINE_CAP_BUTT:            sprintf(tmp_text, "butt"  ); break;
      case V2D_LINE_CAP_SQUARE:          sprintf(tmp_text, "square"); break;
      case V2D_LINE_CAP_ROUND:  default: sprintf(tmp_text, "round" ); break;
    }

    xmlNewAttribute(path_node, "stroke-linecap", tmp_text);

    /* Set the line-join style. */

    switch(shape->line->join)
    {
      case V2D_LINE_JOIN_MITER:          sprintf(tmp_text, "miter"); break;
      case V2D_LINE_JOIN_BEVEL:          sprintf(tmp_text, "bevel"); break;
      case V2D_LINE_JOIN_ROUND: default: sprintf(tmp_text, "round"); break;
    }

    xmlNewAttribute(path_node, "stroke-linejoin", tmp_text);

    /* If necessary, output the "miter limit" for the line. */

    if (shape->line->join == V2D_LINE_JOIN_MITER)
    {
      sprintf(tmp_text, "%f", shape->line->miter);
      xmlNewAttribute(path_node, "stroke-miterlimit", tmp_text);
    }

  } else { xmlNewAttribute(path_node, "stroke", "none"); }

  /* Output fill-style attributes. */

  if (shape->fill)
  {

    if (shape->flags & V2D_SHAPE_FLAG_EVENODD)
    { xmlNewAttribute(path_node, "fill-rule", "evenodd"); }

    if (v2dIsFlatRGBA(shape->fill))
    {

      /* Simple RGBA fills specify their RGBA values directly. */

      sprintf(tmp_text, "#%02x%02x%02x", v2dFillAsColor(shape->fill)->rgba[0],
          v2dFillAsColor(shape->fill)->rgba[1], v2dFillAsColor(shape->fill)->rgba[2]);
      xmlNewAttribute(path_node, "fill", tmp_text);

      sprintf(tmp_text, "%f", (double)v2dFillAsColor(shape->fill)->rgba[3] / 255.0);
      xmlNewAttribute(path_node, "fill-opacity", tmp_text);

    }
    else
    {

      /* Gradient and image-based fills reference the pattern defined above. */

      sprintf(tmp_text, "url(#__%p)", (void*)shape->fill);
      xmlNewAttribute(path_node, "fill", tmp_text);

      /* Image-based fills may also define an opacity value. */

      if (shape->fill->type == V2D_FILL_IMAGE)
      {
        sprintf(tmp_text, "%f", v2dFillAsImage(shape->fill)->alpha);
        xmlNewAttribute(path_node, "fill-opacity", tmp_text);
      }

    }

  } else { xmlNewAttribute(path_node, "fill", "none"); }

  /* Output the SVG path data. */

  path_attr = xmlNewAttribute(path_node, "d", NULL);

  for (path = v2dShapeGetPath(shape); path; path = v2dPathGetNext(path))
  {

    switch (path->type)
    {

      case V2D_PATH_MOVE:
        sprintf(tmp_text, "M%f,%f",
            path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]);
      break;

      case V2D_PATH_LINE:
        sprintf(tmp_text, "L%f,%f",
            path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]);
      break;

      case V2D_PATH_QUAD:
        sprintf(tmp_text, "Q%f,%f %f,%f",
            path->point[V2D_CONTROL_1][0], path->point[V2D_CONTROL_1][1],
            path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]);
      break;

      case V2D_PATH_CUBE:
        sprintf(tmp_text, "C%f,%f %f,%f %f,%f",
            path->point[V2D_CONTROL_1][0], path->point[V2D_CONTROL_1][1],
            path->point[V2D_CONTROL_2][0], path->point[V2D_CONTROL_2][1],
            path->point[V2D_END_POINT][0], path->point[V2D_END_POINT][1]);
      break;

      default: tmp_text[0] = 0; break;  /* Unknown - Use an empty string. */

    }

    /* Relative path segments should use a lower-case character instead. */

    if (path->flags & V2D_PATH_FLAG_RELATIVE) { tmp_text[0] = tolower(tmp_text[0]); }

    /* Path segments are separated by spaces. Shapes are closed with a 'Z'. */

    if      (v2dPathGetNext(path))                { strcat(tmp_text, " "); }
    else if (shape->flags & V2D_SHAPE_FLAG_CLOSE) { strcat(tmp_text, "z"); }

    xmlAddText(path_attr, tmp_text);

  }

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output a text drawable as an SVG 'text' element.

Called by v2dxSaveSVG_draw() internally.

@param draw The shape drawable to be handled.
@param root The "parent" XML node that any new XML tags should be added to.
@param flags Flags to alter the output in various ways.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_text(v2d_draw_s *draw, xml_s *root, v2d_flag_t flags)
{

  v2d_text_s *text = NULL;
  xml_s *text_node = NULL;
  xml_s *span_node = NULL;

  char *text_start = NULL;
  char *text_end = NULL;
  char tmp_char = 0;
  v2d_real_t line_offset = 0;

  char tmp_text[512];

  if      (!draw || !root)              { return V2D_FALSE; }
  else if (draw->type != V2D_TYPE_TEXT) { return V2D_FALSE; }
  text = v2dDrawAsText(draw);

  if (flags) { /* Silence "unused parameter" warnings. */ }

  /* Create the main "text" tag. */

  text_node = xmlNewChildNode(root, "text", NULL);
  sprintf(tmp_text, "__%p", (void*)draw);
  xmlNewAttribute(text_node, "id", tmp_text);

  if (text->font) { xmlNewAttribute(text_node, "font-family", text->font); }

  /* Set the font size. */

  sprintf(tmp_text, "%f", text->size);
  xmlNewAttribute(text_node, "font-size", tmp_text);
  xmlNewAttribute(text_node, "y", tmp_text);

  /* Set the font colour. */

  sprintf(tmp_text, "#%02x%02x%02x", text->rgba[0], text->rgba[1], text->rgba[2]);
  xmlNewAttribute(text_node, "fill", tmp_text);

  sprintf(tmp_text, "%f", (double)text->rgba[3] / 255.0);
  xmlNewAttribute(text_node, "fill-opacity", tmp_text);

  /* Set the font weight. */

  switch(text->weight)
  {
    case V2D_FONT_WEIGHT_BOLD:            sprintf(tmp_text, "bold"  ); break;
    case V2D_FONT_WEIGHT_NORMAL: default: sprintf(tmp_text, "normal"); break;
  }

  xmlNewAttribute(text_node, "font-weight", tmp_text);

  /* Set the font slant style. */

  switch(text->slant)
  {
    case V2D_FONT_SLANT_ITALIC:          sprintf(tmp_text, "italic" ); break;
    case V2D_FONT_SLANT_OBLIQUE:         sprintf(tmp_text, "oblique"); break;
    case V2D_FONT_SLANT_NORMAL: default: sprintf(tmp_text, "normal" ); break;
  }

  xmlNewAttribute(text_node, "font-style", tmp_text);

  /* Output the text. NOTE: Empty tspan elements don't affect "dy" properly. */

  line_offset = 0;

  for (text_start = text->data, text_end = text->data; text_start; text_end++)
  {

    if (text_end[0] == '\n' || text_end[0] == 0)
    {

      if (text_start != text->data) { line_offset += text->size + 2; }

      /* Only output a new "tspan" if this is NOT a blank line. */

      while (isspace(*text_start) && text_start != text_end) { text_start++; }

      if (text_start != text_end)
      {

        tmp_char = text_end[0];
        text_end[0] = 0;
        span_node = xmlNewChildNode(text_node, "tspan", NULL);
        xmlNewChildNode(span_node, "#text", text_start);
        text_end[0] = tmp_char;

        if (line_offset > 0)
        {
          sprintf(tmp_text, "%f", line_offset);
          xmlNewAttribute(span_node, "x", "0");
          xmlNewAttribute(span_node, "dy", tmp_text);
          line_offset = 0;
        }

      }

      text_start = text_end + 1;

    }

    /* If this is the end of the text data, stop looping. */

    if (text_end[0] == 0) { break; }

  }

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output all "drawable" objects within an SVG 'defs' element.

@param stage The stage object that contains the list of drawable objects.
@param defs The "parent" XML node that the new nodes will be added to.
@param flags Flags to alter the output in various ways.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_draw(v2d_stage_s *stage, xml_s *defs, v2d_flag_t flags)
{

  v2d_draw_s *draw = NULL;

  char name[512];

  if (!stage || !defs) { return V2D_FALSE; }

  for (draw = v2dStageGetDraw(stage); draw; draw = v2dDrawGetNext(draw))
  {

    switch(draw->type)
    {

      case V2D_TYPE_GROUP:
        sprintf(name, "__%p", (void*)draw);
        v2dxSaveSVG_item(name, v2dDrawAsGroup(draw), defs);
      break;

      case V2D_TYPE_SHAPE: v2dxSaveSVG_path(draw, defs, flags); break;
      case V2D_TYPE_TEXT:  v2dxSaveSVG_text(draw, defs, flags); break;
      default: fprintf(stderr, "[WARNING] Unknown Draw Type.\n"); break;

    }

  }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output all audio tags within an SVG 'defs' element.

@param stage The stage object that contains the list of drawable objects.
@param defs The "parent" XML node that the new nodes will be added to.
@param flags Flags to alter the output in various ways.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_audio(v2d_stage_s *stage, xml_s *defs, v2d_flag_t flags)
{

  v2d_data_s *sound = NULL;
  xml_s *tmp_node = NULL;
  xml_s *attr = NULL;
  char tmp_text[512];

  if (!stage || !defs) { return V2D_FALSE; }

  for (sound = v2dStageGetAudio(stage); sound; sound = v2dDataGetNext(sound))
  {

    tmp_node = xmlNewChildNode(defs, "switch", NULL);
    sprintf(tmp_text, "__%p", (void*)sound);
    xmlNewAttribute(tmp_node, "id", tmp_text);

    tmp_node = xmlNewChildNode(tmp_node, "foreignObject", NULL);
    xmlNewAttribute(tmp_node, "width", "0");
    xmlNewAttribute(tmp_node, "height", "0");

    /* Create a HTML5 audio tag. */
    tmp_node = xmlNewChildNode(tmp_node, "audio", NULL);
    xmlNewAttribute(tmp_node, "xmlns", "http://www.w3.org/1999/xhtml");
    xmlNewAttribute(tmp_node, "preload", "auto");
    xmlNewAttribute(tmp_node, "hidden", "hidden"); /* <-- For Chromium. */

    /* Possible TODO: Allow more than a single source to be specified? */
    tmp_node = xmlNewChildNode(tmp_node, "source", NULL);
    attr = xmlNewAttribute(tmp_node, "src", sound->file_name);

    if (flags & V2DX_SAVE_FLAG_SVG_EMBED)
    {
      v2dxEmbedData(attr, sound->file_name, xxxFileTypeAudio(sound->file_name, NULL, NULL, NULL, NULL));
    }

  }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output V2D sound events as ECMAScript text for SVGs.

@param out The XML 'script' tag to append the ECMAScript text to.
@param sound The V2D sound event structure to convert into ECMAScript text.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_sound(xml_s *out, v2d_sound_s *sound)
{

  char buffer[512];

  if (!out || !sound) { return V2D_FALSE; }

  sprintf(buffer,
"var audio_node = document.getElementById('__%p').cloneNode(true);\n"
"audio_node.removeAttribute('id');\n"
"document.children[0].appendChild(audio_node);\n"
"this.sounds.push(new SoundInstance(audio_node, %d));\n", (void*)sound->data, sound->loops);
  xmlAddText(out, buffer);

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output V2D OPCODE scripts as ECMAScript text for SVGs.

@todo This is still very incomplete; the output should behave as SWF files do.

@param out The XML 'script' tag to append the ECMAScript text to.
@param script The V2D OPCODE script structure to convert into ECMAScript text.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_script(xml_s *out, v2d_script_s *script)
{

  v2d_action_s *action = NULL;
  v2d_ui8_t i = 0;
  char buffer[512];

  /* "Missing" scripts are OK; not everything that CAN be scripted WILL be! */

  if      (!out   ) { return V2D_FALSE; }
  else if (!script) { return V2D_TRUE;  }

  /*
  ECMAScript doesn't include a GOTO statement, so (un)conditional jump opcodes
  are handled using a 'for' loop and a 'switch' statement, as described below:

  + The 'for' statement declares a 'jump target' variable and loops endlessly.

  + The 'switch' has a 'default' statement as the FIRST case, so that execution
    will start from the very top of the "script" unless otherwise instructed.

  + Any 'label' opcodes are converted into 'case' statements (ECMAScript allows
    strings to be used, not just integers as C/C++ does).

  + IF/JUMP opcodes update the 'jump target' variable before 'continue'-ing the
    'for' loop so that the 'switch' statement is re-run.

  + A 'break' statement just after the 'switch' block allows the "endless" loop
    to terminate when no IF/JUMPs are required.

  NOTE: The 'jump variable' is initially "null" so that the 'default' is hit.
  */

  xmlAddText(out,
      "for (var jmp = null; 1;) {\n"
      "  var a, b;\n"
      "  switch (jmp) {\n"
      "    default:\n");

  /* Loop through each opcode and convert it into ECMAScript. */

  for (action = v2dScriptGetAction(script); action; action = v2dActionGetNext(action))
  {

    switch (action->opcode)
    {

      case V2D_ACTION_LABEL:
        xmlAddText(out, "    case \"");
        xmlAddText(out, action->param ? action->param[0] : "");
        xmlAddText(out, "\":\n");
      break;

      case V2D_ACTION_SETTARGET:
        xmlAddText(out, "__stack.pop();  // UNIMPLEMENTED: SetTarget\n");
      break;

      case V2D_ACTION_PLAY:
        xmlAddText(out, "this.play();\n");
      break;

      case V2D_ACTION_STOP:
        xmlAddText(out, "this.stop(false);\n");
      break;

      case V2D_ACTION_NEXTFRAME:
        xmlAddText(out, "// UNIMPLEMENTED: NextFrame\n");
      break;

      case V2D_ACTION_PREVFRAME:
        xmlAddText(out, "// UNIMPLEMENTED: PrevFrame\n");
      break;

      case V2D_ACTION_GOTOFRAME:
        xmlAddText(out, "__stack.pop();  // UNIMPLEMENTED: GotoFrame\n");
      break;

      case V2D_ACTION_ADD:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b + a);\n");
      break;

      case V2D_ACTION_SUBTRACT:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b - a);\n");
      break;

      case V2D_ACTION_MULTIPLY:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b * a);\n");
      break;

      case V2D_ACTION_DIVIDE:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b / a);\n");
      break;

      case V2D_ACTION_CONCATENATE:
        xmlAddText(out,
            "a = String(__stack.pop());\n"
            "b = String(__stack.pop());\n"
            "__stack.push(b + a);\n");
      break;

      case V2D_ACTION_EQUALS:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b == a);\n");
      break;

      case V2D_ACTION_LESS:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b < a);\n");
      break;

      case V2D_ACTION_STRINGEQUALS:
        xmlAddText(out,
            "a = String(__stack.pop());\n"
            "b = String(__stack.pop());\n"
            "__stack.push(b == a);\n");
      break;

      case V2D_ACTION_STRINGLESS:
        xmlAddText(out,
            "a = String(__stack.pop());\n"
            "b = String(__stack.pop());\n"
            "__stack.push(b < a);\n");
      break;

      case V2D_ACTION_AND:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b && a);\n");
      break;

      case V2D_ACTION_OR:
        xmlAddText(out,
            "a = Number(__stack.pop());\n"
            "b = Number(__stack.pop());\n"
            "__stack.push(b || a);\n");
      break;

      case V2D_ACTION_NOT:
        xmlAddText(out, "__stack.push(!Number(__stack.pop()));\n");
      break;

      case V2D_ACTION_POP:
        xmlAddText(out, "__stack.pop();\n");
      break;

      case V2D_ACTION_PUSHNUMBER:

        for (i = 0; i < action->num_params; i++)
        {
          double tmp_double = 0.0;
          v2dxParseDouble(action->param[i], &tmp_double);
          sprintf(buffer, "__stack.push(%f);\n", tmp_double);
          xmlAddText(out, buffer);
        }

      break;

      case V2D_ACTION_PUSHSTRING:

        /* TODO: Escape quote characters, etc. */

        for (i = 0; i < action->num_params; i++)
        {
          sprintf(buffer, "__stack.push(\"%s\");\n", action->param[i]);
          xmlAddText(out, buffer);
        }

      break;

      case V2D_ACTION_ASCIITOCHAR:
      break;

      case V2D_ACTION_CHARTOASCII:
      break;

      case V2D_ACTION_TOINTEGER:
        xmlAddText(out, "__stack.push(Math.floor(__stack.pop()));\n");
      break;

      case V2D_ACTION_IF:
        xmlAddText(out, "if (__stack.pop()) ");
      case V2D_ACTION_JUMP:
        xmlAddText(out, "{ jmp = \"");
        xmlAddText(out, action->param ? action->param[0] : "");
        xmlAddText(out, "\"; continue; }\n");
      break;

      case V2D_ACTION_SETVARIABLE:
        xmlAddText(out,
            "a = __stack.pop();\n"
            "b = __stack.pop();\n"
            "__var[b] = a;\n");
      break;

      case V2D_ACTION_GETVARIABLE:
        xmlAddText(out, "__stack.push(__var[__stack.pop()]);\n");
      break;

      case V2D_ACTION_SETPROPERTY:
        xmlAddText(out, "__stack.pop(); __stack.pop();  // UNIMPLEMENTED: SetProperty\n");
      break;

      case V2D_ACTION_GETPROPERTY:
        xmlAddText(out, "// UNIMPLEMENTED: GetProperty\n");
      break;

      case V2D_ACTION_CLONEGROUP:
        xmlAddText(out, "__stack.pop(); __stack.pop();  // UNIMPLEMENTED: CloneGroup\n");
      break;

      case V2D_ACTION_REMOVECLONE:
        xmlAddText(out, "__stack.pop();  // UNIMPLEMENTED: RemoveClone\n");
      break;

      case V2D_ACTION_STARTDRAG:
        xmlAddText(out, "__stack.pop(); __stack.pop(); __stack.pop();  // UNIMPLEMENTED: StartDrag\n");
      break;

      case V2D_ACTION_ENDDRAG:
        xmlAddText(out, "// UNIMPLEMENTED: EndDrag\n");
      break;

      case V2D_ACTION_OPENURL:
        xmlAddText(out,
            "a = __stack.pop();\n"  /* The target window, such as "_blank"   */
            "b = __stack.pop();\n"  /* URL, such as "http://www.example.com" */
            "window.open(b, a ? a : '_self');\n");  /* < To match SWF files. */
      break;

      case V2D_ACTION_STOPSOUNDS:
      break;

      case V2D_ACTION_TOGGLEQUALITY:
        /* Ignored - only used for SWF files. */
      break;

      case V2D_ACTION_GETTIME:
        xmlAddText(out, "__stack.push(Date.now());\n");
      break;

      case V2D_ACTION_RANDOMNUMBER:
        xmlAddText(out, "__stack.push(Math.floor(__stack.pop() * Math.random()));\n");
      break;

      case V2D_ACTION_TRACE:
        xmlAddText(out, "console.log(__stack.pop());\n");
      break;

      case V2D_NUM_ACTIONS: default:
        fprintf(stderr, "[ERROR] Unknown opcode: %d\n", action->opcode);
      break;

    }

  }

  /*
  Output the final part of the for()/switch() "GOTO emulation" logic and report
  success.
  */

  xmlAddText(out,
      "  }\n"       /* End of switch() statement scope. */
      "  break;\n"  /* No IF/JUMP opcodes have been hit; end the for() loop. */
      "}\n");       /* End of for() loop scope. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Output an ECMAScript Timeline object for a 'group' drawable.

Note that this also handles the main stage 'group' in the same way.

@param name The name of the Timeline object.
@param script The XML 'script' tag to append the ECMAScript text to.
@param group Either a 'group' drawable or the main stage item list.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_timeline(const char *name, xml_s *script, v2d_chain_s *group)
{

  v2d_ui32_t num_frames = v2dGroupCountFrames(group);

  v2d_item_s *item = NULL;
  char buffer[512];

  if (!name || !script || !group) { return V2D_FALSE; }

  /*
  The setUp() function is called when a new DisplayItem instance is created for
  this group. It creates the actual SVG elements to be controlled by the script
  via the onUpdate() function (see below).
  */

  sprintf(buffer,
"var %s = {\n\n"
"  setUp: function() {\n"
"    var item_list = [", name);
  xmlAddText(script, buffer);

  for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
  {

    if (item->name) { sprintf(buffer, "'%s'",   item->name ); }
    else            { sprintf(buffer, "'__%p'", (void*)item); }

    xmlAddText(script, buffer);

    if (v2dItemGetNext(item)) { xmlAddText(script, ", "  ); }
    else                      { xmlAddText(script, "];\n"); }

  }

  xmlAddText(script,
"    for (var i = 0; i != item_list.length; ++i) {\n"
"      var tag = this.elem.appendChild(document.createElementNS(SVGNS, 'g'));\n"
"      this.children[item_list[i]] = new DisplayItem(tag);\n"
"    }\n"
"  },\n\n");

  /*
  The onUpdate() function is called when a 'group' DisplayItem needs to move to
  another animation frame. It updates the appearance of any 'child' DisplayItem
  objects, runs keyframe-based OPCODE scripts and plays keyframe-based sounds.
  */

  xmlAddText(script,
"  onUpdate: function() {\n"
"    var item = null;\n\n");

  for (item = v2dGroupGetItem(group); item; item = v2dItemGetNext(item))
  {

    v2d_ui32_t frame = 0;

    if (item->name) { sprintf(buffer, "    item = this.children['%s'];\n",    item->name);  }
    else            { sprintf(buffer, "    item = this.children['__%p'];\n",  (void*)item); }
    xmlAddText(script, buffer);

    xmlAddText(script,
"    if (item) {\n"
"      switch (this.frame) {\n");

    for (frame = num_frames; frame > 0; --frame)
    {

      v2d_anim_s *anim = v2dItemGetFrame(item, frame-1);
      v2d_state_t state = V2D_DEFAULT;
      v2d_event_t event = V2D_DEFAULT;

      /* Be careful; this frame index may be before the first anim keyframe! */

      if (!anim)
      {
        xmlAddText(script,
"        default:\n"
"          item.elem.setAttribute('visibility', 'hidden');\n"
"        break;\n");
        break;
      }

      /* Non-keyframe indices fall through to the next keyframe 'case'. */

      sprintf(buffer, "        case %ld:\n", frame-1);
      xmlAddText(script, buffer);
      if (frame-1 != anim->frame) { continue; }

      /* Set the state. */

      for (state = 0; state < V2D_NUM_STATES; ++state)
      {

        xmlAddText(script, "          item.useid['");
        switch (state)
        {
          case V2D_STATE_IDLE: xmlAddText(script, "IDLE"); break;
          case V2D_STATE_OVER: xmlAddText(script, "OVER"); break;
          case V2D_STATE_DOWN: xmlAddText(script, "DOWN"); break;
          case V2D_NUM_STATES: default: break;
        }
        xmlAddText(script, "'] = '");

        if (anim->flags & V2D_ANIM_FLAG_BUTTON && anim->draw[state])
             { sprintf(buffer, "__%p';\n", (void*)anim->draw[state         ]); }
        else { sprintf(buffer, "__%p';\n", (void*)anim->draw[V2D_STATE_IDLE]); }
        xmlAddText(script, buffer);

      }

      /* Run any scripts for this keyframe. */

      for (event = 0; event < V2D_NUM_EVENTS; ++event)
      {

        xmlAddText(script, "          item.event['");
        switch (event)
        {
          case V2D_EVENT_MOUSEOVER: xmlAddText(script, "mouseover"); break;
          case V2D_EVENT_MOUSEEXIT: xmlAddText(script, "mouseout" ); break;
          case V2D_EVENT_PRESS:     xmlAddText(script, "mousedown"); break;
          case V2D_EVENT_RELEASE:   xmlAddText(script, "mouseup"  ); break;
          case V2D_NUM_EVENTS: default: break;
        }
        xmlAddText(script, "'] = ");

        if (anim->flags & V2D_ANIM_FLAG_BUTTON && anim->script[event])
        {

          xmlAddText(script, "\nfunction() {\n");
          v2dxSaveSVG_sound(script, anim->sound[event]);
          v2dxSaveSVG_script(script, anim->script[event]);
          xmlAddText(script, "}.bind(this);\n");

        } else { xmlAddText(script, "null;\n"); }

      }

      /* Update the visual properties of this display item. */

      sprintf(buffer,
"          item.matrix = 'matrix(%f %f %f %f %f %f)';\n"
"          item.elem.setAttribute('visibility', '%s');\n",
          anim->matrix[0][0], anim->matrix[0][1],
          anim->matrix[1][0], anim->matrix[1][1],
          anim->matrix[2][0], anim->matrix[2][1],
          anim->flags & V2D_ANIM_FLAG_VISIBLE ? "visible" : "hidden");
      xmlAddText(script, buffer);

      if (anim->flags & V2D_ANIM_FLAG_BUTTON)
      {
        xmlAddText(script,
"          item.elem.setAttribute('cursor', 'pointer');\n");
      }
      else
      {
        sprintf(buffer,
"          item.elem.setAttribute('cursor', 'auto');\n"
"          if (this.frame == %lu) {\n", anim->frame);
        xmlAddText(script, buffer);
        v2dxSaveSVG_sound(script, anim->sound[V2D_DEFAULT]);
        v2dxSaveSVG_script(script, anim->script[V2D_DEFAULT]);
        xmlAddText(script, "          }\n");
      }

      xmlAddText(script, "        break;\n");

    }

    xmlAddText(script,
"      }\n\n"
"      item.setDrawable(item.useid[item.state]);\n"
"    }\n\n");

  }

  /* Update the frame index. */

  if (num_frames > 1)
  {
    sprintf(buffer, "    this.frame = (this.frame + 1) %% %ld;\n", num_frames);
    xmlAddText(script, buffer);
  }
  else { xmlAddText(script, "    this.stop(false);\n"); }

  xmlAddText(script, "  }\n\n};\n\n");

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Generate ECMAScript to handle animation/scripting.

There are basically only two kinds of object: Timeline and DisplayItem.

Timeline_* objects describe the animation/script behaviour of 'group' drawables
and the main stage. They provide two functions: one to initialise a DisplayItem
when it is first created, and one to update DisplayItems based on their current
animation frame. In short, they're a pair of callback functions glued together
for convenience, and are only utilised indirectly.

DisplayItem objects represent a drawable INSTANCE, and are linked to either a
'use' tag (for static shapes/text) or a 'g' tag (for groups). DisplayItems used
to represent groups derive their animation/script behaviour from a Timeline_*
object.

Note that DisplayItem objects may be created or deleted at any time; consider a
shooting game where new bullets are added when a player fires and enemy sprites
are removed when they are killed.

@param stage The stage to generate code for.
@param root The "parent" XML node that the 'script' tag should be added to.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxSaveSVG_anim(v2d_stage_s *stage, xml_s *root)
{

  xml_s *script = NULL;
  v2d_draw_s *draw = NULL;
  char buffer[512];

  if (!stage || !root) { return V2D_FALSE; }

  /*
  First of all, create a <script> tag and set up some basic variables. A dummy
  'console' object is defined in case the runtime does not provide one itself.
  */

  script = xmlNewChildNode(root, "script", NULL);
  xmlNewAttribute(script, "type", "application/ecmascript");
  script = xmlNewChildNode(script, "#text", "\n'use strict'\n\n"
"var SVGNS = 'http://www.w3.org/2000/svg';\n"
"var XLINKNS = 'http://www.w3.org/1999/xlink';\n\n"

"if (!window.console) { window.console = { log: function() {} }; }\n\n"

"var __stack = [];\n"   /* As per the SWF ActionScript (AVM1) stack.         */
"var __var = {};\n\n"); /* As per the SWF ActionScript (AVM1) variable list. */

  sprintf(buffer, "var __DELAY = %d;\n\n", (int)(1000.0 / stage->fps));
  xmlAddText(script, buffer);

  /*
  writeme
  */

  xmlAddText(script,
"function SoundInstance(elem, num_loops) {\n"
"  this.elem = elem;\n"
"  this.count = num_loops;\n"
   /* switch -> foreignObject -> audio */
"  this.audio = elem.children[0].children[0];\n"
"  this.audio.addEventListener('ended', this.finished.bind(this));\n"
"  if (this.count > 0) { this.audio.play(); }\n"
"}\n\n"

"SoundInstance.prototype = {\n"
"  finished: function() {\n"
"    this.count--;\n"
"    if (this.count > 0) { this.audio.play(); }\n"
"    else if (this.elem) {\n"
"      this.elem.parentNode.removeChild(this.elem);\n"
"      this.elem = null; this.audio = null;\n"
"    }\n"
"  }\n"
"};\n\n");

  /*
  Next, define a DisplayItem instance class. This handles basic display items
  (i.e. static, non-scripted shapes/text) and groups (independent timelines).
  */

  xmlAddText(script,
"function DisplayItem(elem) {\n"
"  this.elem = elem;\n"     /* The SVG element that this DisplayItem affects. */
"  this.matrix = null;\n"   /* Stores the current transformation matrix.      */
"  this.draw = null;\n"     /* The drawable ID CURRENTLY being used.          */
"  this.state = 'IDLE';\n"  /* Current button state: IDLE, OVER, or DOWN.     */
"  this.useid = {};\n"      /* Contains drawable IDs for IDLE, OVER or DOWN.  */
"  this.event = {};\n"      /* Contains callbacks for OVER, OUT, UP or DOWN.  */
"  this.frame = 0;\n"       /* Frame index. Only used by GROUP display items. */
"  this.children = {};\n"   /* Child nodes. Only used by GROUP display items. */
"  this.sounds = [];\n"     /* Sound instances. */
"  this.cache_id = window.setInterval(this.updateCache.bind(this), 10000);\n"
"}\n\n"
); xmlAddText(script,
"DisplayItem.prototype = {\n\n"

"  play: function() {\n"
"    if (!this.interval_id) { this.interval_id = window.setInterval(this.onUpdate, __DELAY); }\n"
"  },\n\n"

  /*
  The stop() function is normally used to stop playback of a specific timeline.
  However, it's also used to clear ALL callback events if a DisplayItem changes
  from a 'group' to something else (including another group) EXCEPT the cache
  callback for the 'root' DisplayItem, as it will still need to clear its cache
  after it changes into a new type of DisplayItem.
  */

"  stop: function(recursive) {\n"
"    if (this.interval_id) { window.clearInterval(this.interval_id); }\n"
"    this.interval_id = null;\n"
"    if (recursive) {\n"
"      for (var key in this.children) {\n"
"         this.children[key].stop(recursive);\n"
"         window.clearInterval(this.children[key].cache_id);\n"
"      }\n"
"    }\n"
"  },\n\n"

); xmlAddText(script,

  /*
  This function is called every few seconds to clear out any references to data
  that it no longer needed. Currently, this only affects sound events.
  */

"  updateCache: function() {\n"
"    var new_list = [];"
"    for (var i = 0; i != this.sounds.length; ++i) {\n"
"      if (this.sounds[i].elem) { new_list.push(this.sounds[i]); }\n"
"    }\n"
"    this.sounds = new_list;"
"  },\n\n"

); xmlAddText(script,

  /*
  This method is called when a new animation frame occurs OR a button changes
  from one state to another.
  */

"  setDrawable: function(name) {\n"

    /*
    Don't do anything if the drawable hasn't changed; it's a waste of time, and
    would cause animation sub-groups to restart whenever this method is called.
    */

"    if (!name) { return; }\n"
"    else if (name == this.draw) {\n"
"      if (this.matrix) { this.elem.setAttribute('transform', this.matrix); }\n"
"      return;\n"
"    }\n"
"    this.draw = name;\n"

    /* Clear any timed callback functions and clear out the 'child' item list. */

"    this.stop(true);\n"
"    this.children = {};\n"
"    this.frame = 0;\n"

    /* Create a new USE or GROUP tag to replace the current one. */
); xmlAddText(script,
"    var draw = document.getElementById(name);\n"
"    if (draw.nodeName == 'g') {\n"
"      var node = document.createElementNS(SVGNS, 'g');\n"
"    } else {\n"
"      var node = document.createElementNS(SVGNS, 'use');\n"
"      node.setAttributeNS(XLINKNS, 'xlink:href', '#' + name)\n"
"    }\n"

    /* Set up the new node and use it as a replacement for the current one. */
); xmlAddText(script,
"    if (this.matrix) { node.setAttribute('transform', this.matrix); }\n"
"    if (this.elem.visibility) { node.setAttribute('visibility', this.elem.visibility); }\n"
"    if (this.elem.cursor) { node.setAttribute('cursor', this.elem.cursor); }\n"

"    node.onmouseover = this.setState.bind(this);\n"
"    node.onmouseout  = this.setState.bind(this);\n"
"    node.onmousedown = this.setState.bind(this);\n"
"    node.onmouseup   = this.setState.bind(this);\n"
); xmlAddText(script,
"    this.elem.parentNode.replaceChild(node, this.elem);\n"
"    this.elem = node;\n"

    /* Animated sub-groups should play by default, as per SWF files. */

"    if (draw.nodeName == 'g') {\n"
"      var timeline = eval('Timeline' + name);\n"
"      timeline.setUp.bind(this)();\n"                   /* Add child items. */
"      this.onUpdate = timeline.onUpdate.bind(this);\n"  /* Get update func. */

"      this.play();\n"      /* Call play() when created, to match SWF files. */
"      this.onUpdate();\n"  /* Do the first frame right now; may call stop(). */
"    }\n"

"  },\n\n"

  /*
  This method is called when mouse events occur. TODO: Fine-tune this, so that
  "unexpected" events (such mousedown twice, without a mouseup in between) are
  handled in a reasonable manner.
  */

); xmlAddText(script,
"  setState: function(event) {\n"
"    switch (event.type) {\n"
"      case 'mouseup':   if (this.state != 'DOWN') { return; } this.state = 'OVER'; break;\n"
"      case 'mousedown': if (this.state != 'OVER') { return; } this.state = 'DOWN'; break;\n"
"      case 'mouseover': if (this.state != 'IDLE') { return; } this.state = 'OVER'; break;\n"
"      case 'mouseout':  if (this.state != 'OVER') { return; } this.state = 'IDLE'; break;\n"
"    }\n"
); xmlAddText(script,
"    this.setDrawable(this.useid[this.state]);\n"
"    if (this.event[event.type]) { this.event[event.type](); }\n"
"  }\n\n"

"};\n\n");

  /*
  The main 'stage' group and any 'group' drawables have an associated Timeline_
  script object that defines which animation/scripting events should happen for
  any DisplayItem objects based on them.
  */

  v2dxSaveSVG_timeline("TimelineStage", script, stage->item);

  for (draw = v2dStageGetDraw(stage); draw; draw = v2dDrawGetNext(draw))
  {
    if (draw->type == V2D_TYPE_GROUP)
    {
      sprintf(buffer, "Timeline__%p", (void*)draw);
      v2dxSaveSVG_timeline(buffer, script, v2dDrawAsGroup(draw));
    }
  }

  /*
  Finally, define a main() function, to be called by onload(). It deletes any
  items already shown and creates/starts a runtime object for the main stage.
  */

  xmlAddText(script,
"function main() {\n"
"  var runtime = new DisplayItem(document.getElementById('Stage'));\n"
"  runtime.setDrawable('Stage');\n"
"}\n");

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Create an SVG XML tree from a stage structure.

This is called internally by the public v2dxSaveSVG() function.

@param stage The stage to be converted into an SVG-format XML tree.
@param flags Flags to alter the output in various ways.
@return An XML node tree representing an SVG on success, or NULL on failure.
*/

static xml_s *v2dxGetStageAsSVG(v2d_stage_s *stage, v2d_flag_t flags)
{

  xml_s *document = NULL;
  xml_s *root_xml = NULL;
  xml_s *tmp_node = NULL;

  char tmp_text[512];

  if (!stage) { return NULL; }

  /* Create an XML document. */

  document = xmlCreate(NULL, NULL);
  if (!document) { return NULL; }

  tmp_node = xmlNewChildNode(document, "?xml", NULL);
  xmlNewAttribute(tmp_node, "version", "1.0");
  xmlNewAttribute(tmp_node, "encoding", "utf-8");

  /* Create a root SVG element. */

  root_xml = xmlNewChildNode(document, "svg", NULL);

  xmlNewAttribute(root_xml, "onload", "main()");

  sprintf(tmp_text, "%dpx", (int)stage->width);
  xmlNewAttribute(root_xml, "width", tmp_text);

  sprintf(tmp_text, "%dpx", (int)stage->height);
  xmlNewAttribute(root_xml, "height", tmp_text);

  sprintf(tmp_text, "0 0 %d %d", (int)stage->width, (int)stage->height);
  xmlNewAttribute(root_xml, "viewBox", tmp_text);

  xmlNewAttribute(root_xml, "xmlns", "http://www.w3.org/2000/svg");
  xmlNewAttribute(root_xml, "xmlns:xlink", "http://www.w3.org/1999/xlink");
  xmlNewAttribute(root_xml, "baseProfile", "tiny");

  /* Output a title tag, if necessary. */

  if (stage->title)
  {
    tmp_node = xmlNewChildNode(root_xml, "title", NULL);
    xmlNewChildNode(tmp_node, "#text", stage->title);
  }

  /* Output a metadata tag, if necessary. */

  if (stage->metadata)
  {
    tmp_node = xmlNewChildNode(root_xml, "metadata", NULL);
    xmlAddChildNode(tmp_node, xmlLoadData(stage->metadata));
  }

  /* Create a "background" using a rectangle. */

  if (flags & V2DX_SAVE_FLAG_SVG_BACKDROP)
  {

    tmp_node = xmlNewChildNode(root_xml, "rect", NULL);

    sprintf(tmp_text, "%dpx", (int)stage->width);
    xmlNewAttribute(tmp_node, "width", tmp_text);

    sprintf(tmp_text, "%dpx", (int)stage->height);
    xmlNewAttribute(tmp_node, "height", tmp_text);

    sprintf(tmp_text, "#%02x%02x%02x", stage->rgba[0], stage->rgba[1], stage->rgba[2]);
    xmlNewAttribute(tmp_node, "fill", tmp_text);

    sprintf(tmp_text, "%f", (double)stage->rgba[3] / 255.0);
    xmlNewAttribute(tmp_node, "fill-opacity", tmp_text);

  }

  /* Output all possible Drawables and Sounds within a <defs> tag. */

  tmp_node = xmlNewChildNode(root_xml, "defs", NULL);
  v2dxSaveSVG_draw(stage, tmp_node, flags);
  v2dxSaveSVG_audio(stage, tmp_node, flags);

  /* Output all display items within a <g> tag that represents the stage. */
  v2dxSaveSVG_item("Stage", stage->item, root_xml);

  /* Output all animation/scripting data within a <script> tag. */
  v2dxSaveSVG_anim(stage, root_xml);

  /* Return the result. */
  return document;

}

/*
[PUBLIC] Export a project in Scalable Vector Graphics (SVG) format.
*/

v2d_bool_t v2dxSaveSVG(v2d_stage_s *stage, const char *name, v2d_flag_t flags)
{

  xml_s *document = NULL;
  int result = 0;

  document = v2dxGetStageAsSVG(stage, flags);
  result = xmlSaveFile(document, name);
  xmlDelete(document);

  return result == 0 ? V2D_TRUE : V2D_FALSE;

}
