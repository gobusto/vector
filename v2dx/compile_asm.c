/*

FILENAME: compile_asm.c
AUTHOR/S: Thomas Dennis
CREATION: 18th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file compile_asm.c

@brief Create a script structure from an assembly-like list of instructions.

The syntax is fairly basic - comments begin with a semicolon, and any other
non-blank lines are treated as instructions. As the current list of actions
is based on the SWF v3 Action Model, instruction names are based on the SWF
specification from Adobe, without the "Action" prefix.

ActionPlay      --> Play

ActionGotoFrame --> GotoFrame 16

ActionGetURL    --> GetURL "https://www.duckduckgo.com" "_level0"

You can include a quote in a string by escaping it with a back-slash: \"
Note that instruction names are not case sensitive.

@todo Needs better error checking.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "compile.h"

/**
@brief [INTERNAL] about

writeme

@param out about
@param text about
@param quote about
@param newlines about
@return writeme
*/

static long v2dxGetEscapedString(char *out, const char **text, char quote, v2d_bool_t newlines)
{

  long actual_length, i;

  v2d_bool_t escape_char = V2D_FALSE;

  if (!text) { return 0; }

  for (actual_length = 0, i = 0, escape_char = V2D_FALSE; (*text)[i] != 0; i++)
  {

    if (!escape_char)
    {
      if      ((*text)[i] == quote           ) { break; }
      else if ((*text)[i] == '\n' && newlines) { break; /* TODO: Error! */ }
      else if ((*text)[i] == '\r' && newlines) { break; /* TODO: Error! */ }
    }

    if ((*text)[i] == '\\' && !escape_char) { escape_char = V2D_TRUE;  }
    else                                    { escape_char = V2D_FALSE; }

    if (!escape_char)
    {
      if (out) { out[actual_length] = (*text)[i]; }
      actual_length++;
    }

  }

  if (out)
  {
    (*text) += i;
    if (**text == quote) { *text += 1; }
  }

  return actual_length;

}

/**
@brief [INTERNAL] Extract the next keyword or variable from a string of text.

Note that this function UPDATES the start-of-text pointer provided, so that it
points to the end of the text read.

@param text A pointer to the start-of-text pointer.
@param expecting_param If true, read as a variable. If false, read as a keyword.
@return A pointer to a malloc()'d string of text on success, or NULL on failure.
*/

static char *v2dxCompileASM_word(const char **text, v2d_bool_t expecting_param)
{

  char *result = NULL;
  long i = 0;

  v2d_bool_t is_a_string = V2D_FALSE;

  /* Ensure that the provided pointer-to-a-string-pointer is valid. */

  if (text == NULL) { return NULL; } else if (*text == NULL) { return NULL; }

  /* Skip any whitespace. Newlines mean "not found" if expecting a parameter. */

  for (; isspace(**text); *text += 1)
  {
    if (expecting_param && (**text == '\n' || **text == '\r')) { return NULL; }
  }

  /* For parameters, treat comments as EOLs. Otherwise, just skip past them. */

  while (**text == ';')
  {
    if (expecting_param) { return NULL; }
    while (**text != 0 && **text != '\n' && **text != '\r') { (*text)++; }
    while (isspace(**text)) { *text += 1; }
  }

  /* Keywords and parameters are parsed differently... */

  is_a_string = V2D_FALSE;

  if (expecting_param)
  {

    /* Determine if this parameter is a string or a number. */

    if (**text == '"')
    {

      is_a_string = V2D_TRUE;

      /* Skip past the opening " character of a string. */
      *text += 1;

      /* Search for another " character. Ignore any preceded by a backslash. */
      i = v2dxGetEscapedString(NULL, text, '"', V2D_FALSE);

    }
    else
    {
      char *end_ptr = NULL;
      strtod(*text, &end_ptr);
      for (i = 0; end_ptr != *text && end_ptr != NULL; i++, end_ptr--) {}
    }

  } else for (i = 0; isalnum((*text)[i]); i++) { /* Alphanumeric keywords. */ }

  /* Only allow zero-length strings for string variables. */

  if (i < 1 && !is_a_string) { return NULL; }

  /* Copy the text, and add a NULL-terminator byte to the end of it. */

  result = (char*)malloc(i + 1);
  if (!result) { return NULL; }
  memset(result, 0, i + 1);

  if (is_a_string) { v2dxGetEscapedString(result, text, '"', V2D_FALSE); }
  else             { memcpy(result, *text, i); (*text) += i; }

  /* Return the result - but convert keyword strings into upper case first. */

  if (!expecting_param)
  {
    for (i = 0; result[i] != 0; i++) { result[i] = toupper(result[i]); }
  }

  return result;

}

/*
[PUBLIC] Create a script structure from an assembly-like list of instructions.
*/

v2d_script_s *v2dxCompileASM(const char *source, v2d_flag_t flags)
{

  v2d_script_s *script = NULL;
  v2d_action_s *action = NULL;

  char *mnemonic = NULL;
  char *argument = NULL;

  v2d_opcode_t op_code;
  v2d_bool_t error;

  /* Create a new script object. */

  if (!source) { return NULL; }
  script = v2dScriptCreate(flags);
  if (!script) { return NULL; }

  /* Loop through each line of source code. */

  error = V2D_FALSE;

  while ((mnemonic = v2dxCompileASM_word(&source, V2D_FALSE)))
  {

    /* Convert the keyword string into an opcode. */

    if      (!strcmp(mnemonic, "LABEL"        )) { op_code = V2D_ACTION_LABEL; }

    else if (!strcmp(mnemonic, "SETTARGET"    )) { op_code = V2D_ACTION_SETTARGET; }
    else if (!strcmp(mnemonic, "PLAY"         )) { op_code = V2D_ACTION_PLAY; }
    else if (!strcmp(mnemonic, "STOP"         )) { op_code = V2D_ACTION_STOP; }
    else if (!strcmp(mnemonic, "NEXTFRAME"    )) { op_code = V2D_ACTION_NEXTFRAME; }
    else if (!strcmp(mnemonic, "PREVFRAME"    )) { op_code = V2D_ACTION_PREVFRAME; }
    else if (!strcmp(mnemonic, "GOTOFRAME"    )) { op_code = V2D_ACTION_GOTOFRAME; }

    else if (!strcmp(mnemonic, "ADD"          )) { op_code = V2D_ACTION_ADD; }
    else if (!strcmp(mnemonic, "SUB"          )) { op_code = V2D_ACTION_SUBTRACT; }
    else if (!strcmp(mnemonic, "MUL"          )) { op_code = V2D_ACTION_MULTIPLY; }
    else if (!strcmp(mnemonic, "DIV"          )) { op_code = V2D_ACTION_DIVIDE; }
    else if (!strcmp(mnemonic, "CONCATENATE"  )) { op_code = V2D_ACTION_CONCATENATE; }

    else if (!strcmp(mnemonic, "EQUALS"       )) { op_code = V2D_ACTION_EQUALS; }
    else if (!strcmp(mnemonic, "LESS"         )) { op_code = V2D_ACTION_LESS;   }
    else if (!strcmp(mnemonic, "STRINGEQUALS" )) { op_code = V2D_ACTION_STRINGEQUALS; }
    else if (!strcmp(mnemonic, "STRINGLESS"   )) { op_code = V2D_ACTION_STRINGLESS;   }

    else if (!strcmp(mnemonic, "AND"          )) { op_code = V2D_ACTION_AND; }
    else if (!strcmp(mnemonic, "OR"           )) { op_code = V2D_ACTION_OR; }
    else if (!strcmp(mnemonic, "NOT"          )) { op_code = V2D_ACTION_NOT; }

    else if (!strcmp(mnemonic, "POP"          )) { op_code = V2D_ACTION_POP; }
    else if (!strcmp(mnemonic, "PUSHNUMBER"   )) { op_code = V2D_ACTION_PUSHNUMBER; }
    else if (!strcmp(mnemonic, "PUSHSTRING"   )) { op_code = V2D_ACTION_PUSHSTRING; }

    else if (!strcmp(mnemonic, "ASCIITOCHAR"  )) { op_code = V2D_ACTION_ASCIITOCHAR; }
    else if (!strcmp(mnemonic, "CHARTOASCII"  )) { op_code = V2D_ACTION_CHARTOASCII; }
    else if (!strcmp(mnemonic, "TOINTEGER"    )) { op_code = V2D_ACTION_TOINTEGER; }

    else if (!strcmp(mnemonic, "IF"           )) { op_code = V2D_ACTION_IF; }
    else if (!strcmp(mnemonic, "GOTO"         )) { op_code = V2D_ACTION_JUMP; }

    else if (!strcmp(mnemonic, "SETVARIABLE"  )) { op_code = V2D_ACTION_SETVARIABLE; }
    else if (!strcmp(mnemonic, "GETVARIABLE"  )) { op_code = V2D_ACTION_GETVARIABLE; }

    else if (!strcmp(mnemonic, "SETPROPERTY"  )) { op_code = V2D_ACTION_SETPROPERTY; }
    else if (!strcmp(mnemonic, "GETPROPERTY"  )) { op_code = V2D_ACTION_GETPROPERTY; }

    else if (!strcmp(mnemonic, "CLONEGROUP"   )) { op_code = V2D_ACTION_CLONEGROUP; }
    else if (!strcmp(mnemonic, "REMOVECLONE"  )) { op_code = V2D_ACTION_REMOVECLONE; }

    else if (!strcmp(mnemonic, "STARTDRAG"    )) { op_code = V2D_ACTION_STARTDRAG; }
    else if (!strcmp(mnemonic, "ENDDRAG"      )) { op_code = V2D_ACTION_ENDDRAG; }

    else if (!strcmp(mnemonic, "OPENURL"      )) { op_code = V2D_ACTION_OPENURL; }
    else if (!strcmp(mnemonic, "STOPSOUNDS"   )) { op_code = V2D_ACTION_STOPSOUNDS; }
    else if (!strcmp(mnemonic, "TOGGLEQUALITY")) { op_code = V2D_ACTION_TOGGLEQUALITY; }
    else if (!strcmp(mnemonic, "GETTIME"      )) { op_code = V2D_ACTION_GETTIME; }
    else if (!strcmp(mnemonic, "RANDOMNUMBER" )) { op_code = V2D_ACTION_RANDOMNUMBER; }
    else if (!strcmp(mnemonic, "PRINT"        )) { op_code = V2D_ACTION_TRACE; }

    else                                         { op_code = V2D_NUM_ACTIONS; }

    free(mnemonic);

    /* Attempt to add a new action to the script. */

    if ((action = v2dActionCreate(script, op_code)))
    {

      while ((argument = v2dxCompileASM_word(&source, V2D_TRUE)))
      {

        /* Attempt to add any parameters to the action. */

        if (!v2dActionAddParam(action, argument))
        {
          free(argument);
          error = V2D_TRUE;
          break;
        }
        else { free(argument); }

      }

    } else { error = V2D_TRUE; }

    /* Stop looping if any errors are found. */

    if (error) { break; }

  }

  /* If the script is valid, return it. Otherwise, free it and return NULL. */

  if (error || !v2dScriptValidate(script))
  {
    v2dScriptDelete(script);
    return NULL;
  }

  return script;

}
