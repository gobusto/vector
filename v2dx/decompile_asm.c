/*

FILENAME: decompile_asm.c
AUTHOR/S: Thomas Dennis
CREATION: 18th May 2013

Copyright (C) 2013-2014 Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file decompile_asm.c

@brief Decompile a script into an assembly-like list of instructions.

The output is compatible with the v2dCompileASM() function.
*/

#include "decompile.h"

/**
@brief [INTERNAL] about

writeme

@param out about
@param text about
@param quote about
@param newlines about
@return writeme
*/

static v2d_bool_t v2dxWriteEscapedString(FILE *out, const char *text, char quote, v2d_bool_t newlines)
{

  long i = 0;

  if (!out) { return V2D_FALSE; }

  fputc(quote, out);

  if (text)
  {

    for (i = 0; text[i]; i++)
    {
      if      (text[i] == '\\' ) { fprintf(out, "\\\\"       ); }
      else if (text[i] == quote) { fprintf(out, "\\%c", quote); }
      else if (text[i] == '\r' && newlines) { fprintf(out, "\\r"); }
      else if (text[i] == '\n' && newlines) { fprintf(out, "\\n"); }
      else                       { fputc(text[i], out);         }
    }

  }

  fputc(quote, out);

  return V2D_TRUE;

}

/*
[PUBLIC] Decompile a script into an assembly-like list of instructions.
*/

v2d_bool_t v2dxDecompileASM(FILE *out, v2d_script_s *script)
{

  v2d_action_s *action = NULL;
  v2d_ui16_t i = 0;

  if (!out || !script) { return V2D_FALSE; }

  for (action = v2dScriptGetAction(script); action; action = v2dActionGetNext(action))
  {

    switch (action->opcode)
    {

      case V2D_ACTION_LABEL:         fprintf(out, "Label");         break;

      case V2D_ACTION_SETTARGET:     fprintf(out, "SetTarget");     break;
      case V2D_ACTION_PLAY:          fprintf(out, "Play");          break;
      case V2D_ACTION_STOP:          fprintf(out, "Stop");          break;
      case V2D_ACTION_NEXTFRAME:     fprintf(out, "NextFrame");     break;
      case V2D_ACTION_PREVFRAME:     fprintf(out, "PrevFrame");     break;
      case V2D_ACTION_GOTOFRAME:     fprintf(out, "GotoFrame");     break;

      case V2D_ACTION_ADD:           fprintf(out, "Add");           break;
      case V2D_ACTION_SUBTRACT:      fprintf(out, "Sub");           break;
      case V2D_ACTION_MULTIPLY:      fprintf(out, "Mul");           break;
      case V2D_ACTION_DIVIDE:        fprintf(out, "Div");           break;
      case V2D_ACTION_CONCATENATE:   fprintf(out, "Concatenate");   break;

      case V2D_ACTION_EQUALS:        fprintf(out, "Equals");        break;
      case V2D_ACTION_LESS:          fprintf(out, "Less");          break;
      case V2D_ACTION_STRINGEQUALS:  fprintf(out, "StringEquals");  break;
      case V2D_ACTION_STRINGLESS:    fprintf(out, "StringLess");    break;

      case V2D_ACTION_AND:           fprintf(out, "And");           break;
      case V2D_ACTION_OR:            fprintf(out, "Or");            break;
      case V2D_ACTION_NOT:           fprintf(out, "Not");           break;

      case V2D_ACTION_POP:           fprintf(out, "Pop");           break;
      case V2D_ACTION_PUSHNUMBER:    fprintf(out, "PushNumber");    break;
      case V2D_ACTION_PUSHSTRING:    fprintf(out, "PushString");    break;

      case V2D_ACTION_ASCIITOCHAR:   fprintf(out, "AsciiToChar");   break;
      case V2D_ACTION_CHARTOASCII:   fprintf(out, "CharToAscii");   break;
      case V2D_ACTION_TOINTEGER:     fprintf(out, "ToInteger");     break;

      case V2D_ACTION_IF:            fprintf(out, "If");            break;
      case V2D_ACTION_JUMP:          fprintf(out, "Goto");          break;

      case V2D_ACTION_SETVARIABLE:   fprintf(out, "SetVariable");   break;
      case V2D_ACTION_GETVARIABLE:   fprintf(out, "GetVariable");   break;

      case V2D_ACTION_SETPROPERTY:   fprintf(out, "SetProperty");   break;
      case V2D_ACTION_GETPROPERTY:   fprintf(out, "GetProperty");   break;

      case V2D_ACTION_CLONEGROUP:    fprintf(out, "CloneGroup");    break;
      case V2D_ACTION_REMOVECLONE:   fprintf(out, "RemoveClone");   break;

      case V2D_ACTION_STARTDRAG:     fprintf(out, "StartDrag");     break;
      case V2D_ACTION_ENDDRAG:       fprintf(out, "EndDrag");       break;

      case V2D_ACTION_OPENURL:       fprintf(out, "OpenURL");       break;
      case V2D_ACTION_STOPSOUNDS:    fprintf(out, "StopSounds");    break;
      case V2D_ACTION_TOGGLEQUALITY: fprintf(out, "ToggleQuality"); break;
      case V2D_ACTION_GETTIME:       fprintf(out, "GetTime");       break;
      case V2D_ACTION_RANDOMNUMBER:  fprintf(out, "RandomNumber");  break;
      case V2D_ACTION_TRACE:         fprintf(out, "Print");         break;

      /* Unknown. */

      case V2D_NUM_ACTIONS: default: fprintf(out, "; unknown op?"); break;

    }

    for (i = 0; i < action->num_params; i++)
    {
      fputc(' ', out);
      v2dxWriteEscapedString(out, action->param[i], '"', V2D_FALSE);
    }

    fprintf(out, "\n");

  }

  return V2D_TRUE;

}
