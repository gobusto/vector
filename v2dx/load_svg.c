/*

FILENAME: load_svg.c
AUTHOR/S: Thomas Dennis
CREATION: 17th March 2013

Copyright (C) 2013-2014 by Thomas Glyn Dennis.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
@file load_svg.c

@brief Provides functions for importing projects from SVG files.

@todo Actually implement (optional) ZLIB compression support...
*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "load.h"
#include "xml/xml.h"

/* Function prototypes. */

v2d_bool_t v2dxLoadSVG_internal(v2d_stage_s *stage, v2d_chain_s *group, xml_s *owner, xml_s *defs);

/**
@brief [INTERNAL] about

writeme

@param defs about
@param id about
@return writeme
*/

static xml_s *v2dxLoadSVG_xlink(xml_s *defs, const char *id)
{

  xml_s *node = NULL;
  xml_s *attr = NULL;

  if (!defs || !id) { return NULL; }

  for (node = defs->node; node; node = node->next)
  {
    attr = xmlFindName(node->attr, "id", 0);
    if (attr)
    {
      if (strcmp(attr->text, id) == 0) { return node; }
    }
  }

  return NULL;

}

/**
@brief [INTERNAL] Parse the text data of an SVG "transform" attribute.

@todo Handle the optional cx/cy parameters of a "rotate" instruction.

@param matrix The matrix to be transformed.
@param text The text data of the SVG "transform" attribute.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxLoadSVG_transform(v2d_matrix_t matrix, const char *text)
{

  float x, y, a;

  if (!matrix || !text) { return V2D_FALSE; }

  for (; text; text = strstr(text, ")"))
  {

    for (text += (text[0] == ')') ? 1 : 0; isspace(text[0]); text++) { }
    if (text[0] == 0) { break; }

    if (strncmp(text, "matrix(", 7) == 0)
    {

      float m[6];

      if (sscanf(text, "matrix(%f%*[, \t]%f%*[, \t]%f%*[, \t]%f%*[, \t]%f%*[, \t]%f)", m+0, m+1, m+2, m+3, m+4, m+5) == 6)
      {
        matrix[0][0] = m[0]; matrix[0][1] = m[1];
        matrix[1][0] = m[2]; matrix[1][1] = m[3];
        matrix[2][0] = m[4]; matrix[2][1] = m[5];
      }
      else { fprintf(stderr, "[SVG] Failed to parse 'matrix' correctly.\n"); }

    }
    else if (strncmp(text, "translate(", 10) == 0)
    {
      if (sscanf(text, "translate(%f%*[, \t]%f)", &x, &y) == 2)
      { v2dMatrixTranslate(matrix, x, y); }
      else
      { fprintf(stderr, "[SVG] Failed to parse 'translate' correctly.\n"); }
    }
    else if (strncmp(text, "scale(", 6) == 0)
    {
      if (sscanf(text, "scale(%f%*[, \t]%f)", &x, &y) == 2)
      {
        v2dMatrixScale(matrix, x, y);
      }
      else
      { fprintf(stderr, "[SVG] Failed to parse 'scale' correctly.\n"); }
    }
    else if (strncmp(text, "rotate(", 7) == 0)
    {
      if (sscanf(text, "rotate(%f)", &a) == 1)
      { v2dMatrixRotate(matrix, v2dAsRadians(a)); }
      else
      { fprintf(stderr, "[SVG] Failed to parse 'rotate' correctly.\n"); }
    }
    else if (strncmp(text, "skewX(", 6) == 0)
    {
      if (sscanf(text, "skewX(%f)", &x) == 1)
      { v2dMatrixShear(matrix, x, 0); }
      else
      { fprintf(stderr, "[SVG] Failed to parse 'skewX' correctly.\n"); }
    }
    else if (strncmp(text, "skewY(", 6) == 0)
    {
      if (sscanf(text, "skewY(%f)", &y) == 1)
      { v2dMatrixShear(matrix, 0, y); }
      else
      { fprintf(stderr, "[SVG] Failed to parse 'skewY' correctly.\n"); }
    }
    else { fprintf(stderr, "[SVG] Unknown transform: %s\n", text); }

  }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Attempt to load a style attribute from an SVG tag.

The value may be defined as a stand-alone attribute, or it might be included as
part of the "style" attribute text. Of course, it may also be entirely absent.

@param node The SVG tag to be checked.
@param name The name of the attribute to search for.
@param out An optional output buffer for the value of the attribute, if found.
@param out_len The size (in bytes) of the output buffer, it it's provided.
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxLoadSVG_style(xml_s *node, const char *name, char *out, size_t out_len)
{

  xml_s *attr = NULL;
  char  *text = NULL;
  size_t len, i;

  if (!node || !name) { return V2D_FALSE; }

  if (out) { memset(out, 0, out_len); }

  /* First, just look for the attribute specified by name. */

  attr = xmlFindName(node->attr, name, 0);
  if (attr)
  {
    if (out && out_len > 0) { strncpy(out, attr->text, out_len-1); }
    return V2D_TRUE;
  }

  /* If that doesn't work, look for a 'style' attribute instead. */

  attr = xmlFindName(node->attr, "style", 0);
  if (!attr) { return V2D_FALSE; }

  for (len = strlen(name), text = attr->text; text; text = strstr(text, ";"))
  {
    /* Skip past any whitespace (and the ';' that ends the previous value). */
    for (text += (text[0] == ';' ? 1 : 0); isspace(text[0]); text++) { }
    /* Is this the value we're looking for? */
    if (strncmp(text, name, len) == 0)
    {
      /* Skip past any trailing whitespace. */
      for (text += len; isspace(text[0]); text++) { }
      /* If we want 'stroke', and this is 'stroke-width', then ignore it. */
      if (text[0] == ':')
      {
        /* Skip past any leading whitespace. */
        for (text += 1; isspace(text[0]); text++) { }
        /* Count the number of characters to copy. */
        for (i = 0; text[i] != 0 && text[i] != ';'; i++) { }
        /* Copy the data, if required. */
        if (out && out_len > 0) { strncpy(out, text, (i < out_len-1) ? i : out_len-1); }
        /* Report success. */
        return V2D_TRUE;
      }
    }
  }

  /* The attribute could not be found in the 'style' text - report failure. */
  return V2D_FALSE;

}

/**
@brief [INTERNAL] Attempt to read an SVG color value from a text string.

Values may be specified as either "rgb(255,0,255)", "#FF00FF", or a pre-defined
SVG color name. Note that "rgb(100%,0%,100%)" or "#F0F" are also allowed by the
specification, and are therefore supported by this function:

http://www.w3.org/TR/SVG/types.html#ColorKeywords

NOTE: The "alpha" property of the RGBA output value is not updated in any way.

@param text A string of text containing an SVG-compliant colour value.
@param out An optional output variable for the parsed colour.
@return V2D_TRUE on success, or V2D_FALSE if something went wrong.
*/

static v2d_bool_t v2dxLoadSVG_color(const char *text, v2d_rgba_t out)
{

  int rgb[3];

  if (!text) { return V2D_FALSE; }

  if (text[0] == '#')
  {

    int num_char, value, i;

    for (num_char = 3; ; num_char = 6)
    {

      for (i = 0; i < num_char; i++)
      {

        value = text[i + 1];

        if      (value >= '0' && value <= '9') { value += 0x0 - '0'; }
        else if (value >= 'A' && value <= 'F') { value += 0xA - 'A'; }
        else if (value >= 'a' && value <= 'f') { value += 0xA - 'a'; }
        else                                   { return V2D_FALSE;   }

        if      (num_char == 3) { rgb[i  ] = (value << 4) + value; }
        else if (i % 2    == 0) { rgb[i/2] = (value << 4);         }
        else                    { rgb[i/2] +=               value; }

      }

      if      (text[num_char + 1] == 0) { break;            }
      else if (num_char           != 3) { return V2D_FALSE; }

    }

  }
  else if (sscanf(text, "rgb(%d%%, %d%%, %d%%)", &rgb[0], &rgb[1], &rgb[2]) == 3)
  {
    rgb[0] = (int)((rgb[0]/100.0) * 255.0);
    rgb[1] = (int)((rgb[1]/100.0) * 255.0);
    rgb[2] = (int)((rgb[2]/100.0) * 255.0);
  }
  else if (sscanf(text, "rgb(%d, %d, %d)", &rgb[0], &rgb[1], &rgb[2]) == 3) { }
  else if (strcmp(text, "aliceblue"           ) == 0) { rgb[0] = 240; rgb[1] = 248; rgb[2] = 255; }
  else if (strcmp(text, "antiquewhite"        ) == 0) { rgb[0] = 250; rgb[1] = 235; rgb[2] = 215; }
  else if (strcmp(text, "aqua"                ) == 0) { rgb[0] =   0; rgb[1] = 255; rgb[2] = 255; }
  else if (strcmp(text, "aquamarine"          ) == 0) { rgb[0] = 127; rgb[1] = 255; rgb[2] = 212; }
  else if (strcmp(text, "azure"               ) == 0) { rgb[0] = 240; rgb[1] = 255; rgb[2] = 255; }
  else if (strcmp(text, "beige"               ) == 0) { rgb[0] = 245; rgb[1] = 245; rgb[2] = 220; }
  else if (strcmp(text, "bisque"              ) == 0) { rgb[0] = 255; rgb[1] = 228; rgb[2] = 196; }
  else if (strcmp(text, "black"               ) == 0) { rgb[0] =   0; rgb[1] =   0; rgb[2] = 0; }
  else if (strcmp(text, "blanchedalmond"      ) == 0) { rgb[0] = 255; rgb[1] = 235; rgb[2] = 205; }
  else if (strcmp(text, "blue"                ) == 0) { rgb[0] =   0; rgb[1] =   0; rgb[2] = 255; }
  else if (strcmp(text, "blueviolet"          ) == 0) { rgb[0] = 138; rgb[1] =  43; rgb[2] = 226; }
  else if (strcmp(text, "brown"               ) == 0) { rgb[0] = 165; rgb[1] =  42; rgb[2] = 42; }
  else if (strcmp(text, "burlywood"           ) == 0) { rgb[0] = 222; rgb[1] = 184; rgb[2] = 135; }
  else if (strcmp(text, "cadetblue"           ) == 0) { rgb[0] =  95; rgb[1] = 158; rgb[2] = 160; }
  else if (strcmp(text, "chartreuse"          ) == 0) { rgb[0] = 127; rgb[1] = 255; rgb[2] = 0; }
  else if (strcmp(text, "chocolate"           ) == 0) { rgb[0] = 210; rgb[1] = 105; rgb[2] = 30; }
  else if (strcmp(text, "coral"               ) == 0) { rgb[0] = 255; rgb[1] = 127; rgb[2] = 80; }
  else if (strcmp(text, "cornflowerblue"      ) == 0) { rgb[0] = 100; rgb[1] = 149; rgb[2] = 237; }
  else if (strcmp(text, "cornsilk"            ) == 0) { rgb[0] = 255; rgb[1] = 248; rgb[2] = 220; }
  else if (strcmp(text, "crimson"             ) == 0) { rgb[0] = 220; rgb[1] =  20; rgb[2] = 60; }
  else if (strcmp(text, "cyan"                ) == 0) { rgb[0] =   0; rgb[1] = 255; rgb[2] = 255; }
  else if (strcmp(text, "darkblue"            ) == 0) { rgb[0] =   0; rgb[1] =   0; rgb[2] = 139; }
  else if (strcmp(text, "darkcyan"            ) == 0) { rgb[0] =   0; rgb[1] = 139; rgb[2] = 139; }
  else if (strcmp(text, "darkgoldenrod"       ) == 0) { rgb[0] = 184; rgb[1] = 134; rgb[2] = 11; }
  else if (strcmp(text, "darkgray"            ) == 0) { rgb[0] = 169; rgb[1] = 169; rgb[2] = 169; }
  else if (strcmp(text, "darkgreen"           ) == 0) { rgb[0] =   0; rgb[1] = 100; rgb[2] = 0; }
  else if (strcmp(text, "darkgrey"            ) == 0) { rgb[0] = 169; rgb[1] = 169; rgb[2] = 169; }
  else if (strcmp(text, "darkkhaki"           ) == 0) { rgb[0] = 189; rgb[1] = 183; rgb[2] = 107; }
  else if (strcmp(text, "darkmagenta"         ) == 0) { rgb[0] = 139; rgb[1] =   0; rgb[2] = 139; }
  else if (strcmp(text, "darkolivegreen"      ) == 0) { rgb[0] =  85; rgb[1] = 107; rgb[2] = 47; }
  else if (strcmp(text, "darkorange"          ) == 0) { rgb[0] = 255; rgb[1] = 140; rgb[2] = 0; }
  else if (strcmp(text, "darkorchid"          ) == 0) { rgb[0] = 153; rgb[1] =  50; rgb[2] = 204; }
  else if (strcmp(text, "darkred"             ) == 0) { rgb[0] = 139; rgb[1] =   0; rgb[2] = 0; }
  else if (strcmp(text, "darksalmon"          ) == 0) { rgb[0] = 233; rgb[1] = 150; rgb[2] = 122; }
  else if (strcmp(text, "darkseagreen"        ) == 0) { rgb[0] = 143; rgb[1] = 188; rgb[2] = 143; }
  else if (strcmp(text, "darkslateblue"       ) == 0) { rgb[0] =  72; rgb[1] =  61; rgb[2] = 139; }
  else if (strcmp(text, "darkslategray"       ) == 0) { rgb[0] =  47; rgb[1] =  79; rgb[2] = 79; }
  else if (strcmp(text, "darkslategrey"       ) == 0) { rgb[0] =  47; rgb[1] =  79; rgb[2] = 79; }
  else if (strcmp(text, "darkturquoise"       ) == 0) { rgb[0] =   0; rgb[1] = 206; rgb[2] = 209; }
  else if (strcmp(text, "darkviolet"          ) == 0) { rgb[0] = 148; rgb[1] =   0; rgb[2] = 211; }
  else if (strcmp(text, "deeppink"            ) == 0) { rgb[0] = 255; rgb[1] =  20; rgb[2] = 147; }
  else if (strcmp(text, "deepskyblue"         ) == 0) { rgb[0] =   0; rgb[1] = 191; rgb[2] = 255; }
  else if (strcmp(text, "dimgray"             ) == 0) { rgb[0] = 105; rgb[1] = 105; rgb[2] = 105; }
  else if (strcmp(text, "dimgrey"             ) == 0) { rgb[0] = 105; rgb[1] = 105; rgb[2] = 105; }
  else if (strcmp(text, "dodgerblue"          ) == 0) { rgb[0] =  30; rgb[1] = 144; rgb[2] = 255; }
  else if (strcmp(text, "firebrick"           ) == 0) { rgb[0] = 178; rgb[1] =  34; rgb[2] = 34; }
  else if (strcmp(text, "floralwhite"         ) == 0) { rgb[0] = 255; rgb[1] = 250; rgb[2] = 240; }
  else if (strcmp(text, "forestgreen"         ) == 0) { rgb[0] =  34; rgb[1] = 139; rgb[2] = 34; }
  else if (strcmp(text, "fuchsia"             ) == 0) { rgb[0] = 255; rgb[1] =   0; rgb[2] = 255; }
  else if (strcmp(text, "gainsboro"           ) == 0) { rgb[0] = 220; rgb[1] = 220; rgb[2] = 220; }
  else if (strcmp(text, "ghostwhite"          ) == 0) { rgb[0] = 248; rgb[1] = 248; rgb[2] = 255; }
  else if (strcmp(text, "gold"                ) == 0) { rgb[0] = 255; rgb[1] = 215; rgb[2] = 0; }
  else if (strcmp(text, "goldenrod"           ) == 0) { rgb[0] = 218; rgb[1] = 165; rgb[2] = 32; }
  else if (strcmp(text, "gray"                ) == 0) { rgb[0] = 128; rgb[1] = 128; rgb[2] = 128; }
  else if (strcmp(text, "grey"                ) == 0) { rgb[0] = 128; rgb[1] = 128; rgb[2] = 128; }
  else if (strcmp(text, "green"               ) == 0) { rgb[0] =   0; rgb[1] = 128; rgb[2] = 0; }
  else if (strcmp(text, "greenyellow"         ) == 0) { rgb[0] = 173; rgb[1] = 255; rgb[2] = 47; }
  else if (strcmp(text, "honeydew"            ) == 0) { rgb[0] = 240; rgb[1] = 255; rgb[2] = 240; }
  else if (strcmp(text, "hotpink"             ) == 0) { rgb[0] = 255; rgb[1] = 105; rgb[2] = 180; }
  else if (strcmp(text, "indianred"           ) == 0) { rgb[0] = 205; rgb[1] =  92; rgb[2] = 92; }
  else if (strcmp(text, "indigo"              ) == 0) { rgb[0] =  75; rgb[1] =   0; rgb[2] = 130; }
  else if (strcmp(text, "ivory"               ) == 0) { rgb[0] = 255; rgb[1] = 255; rgb[2] = 240; }
  else if (strcmp(text, "khaki"               ) == 0) { rgb[0] = 240; rgb[1] = 230; rgb[2] = 140; }
  else if (strcmp(text, "lavender"            ) == 0) { rgb[0] = 230; rgb[1] = 230; rgb[2] = 250; }
  else if (strcmp(text, "lavenderblush"       ) == 0) { rgb[0] = 255; rgb[1] = 240; rgb[2] = 245; }
  else if (strcmp(text, "lawngreen"           ) == 0) { rgb[0] = 124; rgb[1] = 252; rgb[2] = 0; }
  else if (strcmp(text, "lemonchiffon"        ) == 0) { rgb[0] = 255; rgb[1] = 250; rgb[2] = 205; }
  else if (strcmp(text, "lightblue"           ) == 0) { rgb[0] = 173; rgb[1] = 216; rgb[2] = 230; }
  else if (strcmp(text, "lightcoral"          ) == 0) { rgb[0] = 240; rgb[1] = 128; rgb[2] = 128; }
  else if (strcmp(text, "lightcyan"           ) == 0) { rgb[0] = 224; rgb[1] = 255; rgb[2] = 255; }
  else if (strcmp(text, "lightgoldenrodyellow") == 0) { rgb[0] = 250; rgb[1] = 250; rgb[2] = 210; }
  else if (strcmp(text, "lightgray"           ) == 0) { rgb[0] = 211; rgb[1] = 211; rgb[2] = 211; }
  else if (strcmp(text, "lightgreen"          ) == 0) { rgb[0] = 144; rgb[1] = 238; rgb[2] = 144; }
  else if (strcmp(text, "lightgrey"           ) == 0) { rgb[0] = 211; rgb[1] = 211; rgb[2] = 211; }
  else if (strcmp(text, "lightpink"           ) == 0) { rgb[0] = 255; rgb[1] = 182; rgb[2] = 193; }
  else if (strcmp(text, "lightsalmon"         ) == 0) { rgb[0] = 255; rgb[1] = 160; rgb[2] = 122; }
  else if (strcmp(text, "lightseagreen"       ) == 0) { rgb[0] =  32; rgb[1] = 178; rgb[2] = 170; }
  else if (strcmp(text, "lightskyblue"        ) == 0) { rgb[0] = 135; rgb[1] = 206; rgb[2] = 250; }
  else if (strcmp(text, "lightslategray"      ) == 0) { rgb[0] = 119; rgb[1] = 136; rgb[2] = 153; }
  else if (strcmp(text, "lightslategrey"      ) == 0) { rgb[0] = 119; rgb[1] = 136; rgb[2] = 153; }
  else if (strcmp(text, "lightsteelblue"      ) == 0) { rgb[0] = 176; rgb[1] = 196; rgb[2] = 222; }
  else if (strcmp(text, "lightyellow"         ) == 0) { rgb[0] = 255; rgb[1] = 255; rgb[2] = 224; }
  else if (strcmp(text, "lime"                ) == 0) { rgb[0] =   0; rgb[1] = 255; rgb[2] = 0; }
  else if (strcmp(text, "limegreen"           ) == 0) { rgb[0] =  50; rgb[1] = 205; rgb[2] = 50; }
  else if (strcmp(text, "linen"               ) == 0) { rgb[0] = 250; rgb[1] = 240; rgb[2] = 230; }
  else if (strcmp(text, "magenta"             ) == 0) { rgb[0] = 255; rgb[1] =   0; rgb[2] = 255; }
  else if (strcmp(text, "maroon"              ) == 0) { rgb[0] = 128; rgb[1] =   0; rgb[2] = 0; }
  else if (strcmp(text, "mediumaquamarine"    ) == 0) { rgb[0] = 102; rgb[1] = 205; rgb[2] = 170; }
  else if (strcmp(text, "mediumblue"          ) == 0) { rgb[0] =   0; rgb[1] =   0; rgb[2] = 205; }
  else if (strcmp(text, "mediumorchid"        ) == 0) { rgb[0] = 186; rgb[1] =  85; rgb[2] = 211; }
  else if (strcmp(text, "mediumpurple"        ) == 0) { rgb[0] = 147; rgb[1] = 112; rgb[2] = 219; }
  else if (strcmp(text, "mediumseagreen"      ) == 0) { rgb[0] =  60; rgb[1] = 179; rgb[2] = 113; }
  else if (strcmp(text, "mediumslateblue"     ) == 0) { rgb[0] = 123; rgb[1] = 104; rgb[2] = 238; }
  else if (strcmp(text, "mediumspringgreen"   ) == 0) { rgb[0] =   0; rgb[1] = 250; rgb[2] = 154; }
  else if (strcmp(text, "mediumturquoise"     ) == 0) { rgb[0] =  72; rgb[1] = 209; rgb[2] = 204; }
  else if (strcmp(text, "mediumvioletred"     ) == 0) { rgb[0] = 199; rgb[1] =  21; rgb[2] = 133; }
  else if (strcmp(text, "midnightblue"        ) == 0) { rgb[0] =  25; rgb[1] =  25; rgb[2] = 112; }
  else if (strcmp(text, "mintcream"           ) == 0) { rgb[0] = 245; rgb[1] = 255; rgb[2] = 250; }
  else if (strcmp(text, "mistyrose"           ) == 0) { rgb[0] = 255; rgb[1] = 228; rgb[2] = 225; }
  else if (strcmp(text, "moccasin"            ) == 0) { rgb[0] = 255; rgb[1] = 228; rgb[2] = 181; }
  else if (strcmp(text, "navajowhite"         ) == 0) { rgb[0] = 255; rgb[1] = 222; rgb[2] = 173; }
  else if (strcmp(text, "navy"                ) == 0) { rgb[0] =   0; rgb[1] =   0; rgb[2] = 128; }
  else if (strcmp(text, "oldlace"             ) == 0) { rgb[0] = 253; rgb[1] = 245; rgb[2] = 230; }
  else if (strcmp(text, "olive"               ) == 0) { rgb[0] = 128; rgb[1] = 128; rgb[2] = 0; }
  else if (strcmp(text, "olivedrab"           ) == 0) { rgb[0] = 107; rgb[1] = 142; rgb[2] = 35; }
  else if (strcmp(text, "orange"              ) == 0) { rgb[0] = 255; rgb[1] = 165; rgb[2] = 0; }
  else if (strcmp(text, "orangered"           ) == 0) { rgb[0] = 255; rgb[1] =  69; rgb[2] = 0; }
  else if (strcmp(text, "orchid"              ) == 0) { rgb[0] = 218; rgb[1] = 112; rgb[2] = 214; }
  else if (strcmp(text, "palegoldenrod"       ) == 0) { rgb[0] = 238; rgb[1] = 232; rgb[2] = 170; }
  else if (strcmp(text, "palegreen"           ) == 0) { rgb[0] = 152; rgb[1] = 251; rgb[2] = 152; }
  else if (strcmp(text, "paleturquoise"       ) == 0) { rgb[0] = 175; rgb[1] = 238; rgb[2] = 238; }
  else if (strcmp(text, "palevioletred"       ) == 0) { rgb[0] = 219; rgb[1] = 112; rgb[2] = 147; }
  else if (strcmp(text, "papayawhip"          ) == 0) { rgb[0] = 255; rgb[1] = 239; rgb[2] = 213; }
  else if (strcmp(text, "peachpuff"           ) == 0) { rgb[0] = 255; rgb[1] = 218; rgb[2] = 185; }
  else if (strcmp(text, "peru"                ) == 0) { rgb[0] = 205; rgb[1] = 133; rgb[2] = 63; }
  else if (strcmp(text, "pink"                ) == 0) { rgb[0] = 255; rgb[1] = 192; rgb[2] = 203; }
  else if (strcmp(text, "plum"                ) == 0) { rgb[0] = 221; rgb[1] = 160; rgb[2] = 221; }
  else if (strcmp(text, "powderblue"          ) == 0) { rgb[0] = 176; rgb[1] = 224; rgb[2] = 230; }
  else if (strcmp(text, "purple"              ) == 0) { rgb[0] = 128; rgb[1] =   0; rgb[2] = 128; }
  else if (strcmp(text, "red"                 ) == 0) { rgb[0] = 255; rgb[1] =   0; rgb[2] = 0; }
  else if (strcmp(text, "rosybrown"           ) == 0) { rgb[0] = 188; rgb[1] = 143; rgb[2] = 143; }
  else if (strcmp(text, "royalblue"           ) == 0) { rgb[0] =  65; rgb[1] = 105; rgb[2] = 225; }
  else if (strcmp(text, "saddlebrown"         ) == 0) { rgb[0] = 139; rgb[1] =  69; rgb[2] = 19; }
  else if (strcmp(text, "salmon"              ) == 0) { rgb[0] = 250; rgb[1] = 128; rgb[2] = 114; }
  else if (strcmp(text, "sandybrown"          ) == 0) { rgb[0] = 244; rgb[1] = 164; rgb[2] = 96; }
  else if (strcmp(text, "seagreen"            ) == 0) { rgb[0] =  46; rgb[1] = 139; rgb[2] = 87; }
  else if (strcmp(text, "seashell"            ) == 0) { rgb[0] = 255; rgb[1] = 245; rgb[2] = 238; }
  else if (strcmp(text, "sienna"              ) == 0) { rgb[0] = 160; rgb[1] =  82; rgb[2] = 45; }
  else if (strcmp(text, "silver"              ) == 0) { rgb[0] = 192; rgb[1] = 192; rgb[2] = 192; }
  else if (strcmp(text, "skyblue"             ) == 0) { rgb[0] = 135; rgb[1] = 206; rgb[2] = 235; }
  else if (strcmp(text, "slateblue"           ) == 0) { rgb[0] = 106; rgb[1] =  90; rgb[2] = 205; }
  else if (strcmp(text, "slategray"           ) == 0) { rgb[0] = 112; rgb[1] = 128; rgb[2] = 144; }
  else if (strcmp(text, "slategrey"           ) == 0) { rgb[0] = 112; rgb[1] = 128; rgb[2] = 144; }
  else if (strcmp(text, "snow"                ) == 0) { rgb[0] = 255; rgb[1] = 250; rgb[2] = 250; }
  else if (strcmp(text, "springgreen"         ) == 0) { rgb[0] =   0; rgb[1] = 255; rgb[2] = 127; }
  else if (strcmp(text, "steelblue"           ) == 0) { rgb[0] =  70; rgb[1] = 130; rgb[2] = 180; }
  else if (strcmp(text, "tan"                 ) == 0) { rgb[0] = 210; rgb[1] = 180; rgb[2] = 140; }
  else if (strcmp(text, "teal"                ) == 0) { rgb[0] =   0; rgb[1] = 128; rgb[2] = 128; }
  else if (strcmp(text, "thistle"             ) == 0) { rgb[0] = 216; rgb[1] = 191; rgb[2] = 216; }
  else if (strcmp(text, "tomato"              ) == 0) { rgb[0] = 255; rgb[1] =  99; rgb[2] = 71; }
  else if (strcmp(text, "turquoise"           ) == 0) { rgb[0] =  64; rgb[1] = 224; rgb[2] = 208; }
  else if (strcmp(text, "violet"              ) == 0) { rgb[0] = 238; rgb[1] = 130; rgb[2] = 238; }
  else if (strcmp(text, "wheat"               ) == 0) { rgb[0] = 245; rgb[1] = 222; rgb[2] = 179; }
  else if (strcmp(text, "white"               ) == 0) { rgb[0] = 255; rgb[1] = 255; rgb[2] = 255; }
  else if (strcmp(text, "whitesmoke"          ) == 0) { rgb[0] = 245; rgb[1] = 245; rgb[2] = 245; }
  else if (strcmp(text, "yellow"              ) == 0) { rgb[0] = 255; rgb[1] = 255; rgb[2] = 0; }
  else if (strcmp(text, "yellowgreen"         ) == 0) { rgb[0] = 154; rgb[1] = 205; rgb[2] = 50; }
  else                                                { return V2D_FALSE; }

  /* Store the RGB value and report success. */

  if (out) { out[0] = rgb[0]; out[1] = rgb[1]; out[2] = rgb[2]; }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Get various common style attributes, such as stroke and fill.

@todo Don't ignore gradient transformation matrices.

@todo Handle image-based 'pattern' fills.

@todo Allow a "font" attribute to be used to specify all font-* values at once.

@param draw The drawable to be updated.
@param node The SVG tag to be checked.
@param defs about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

static v2d_bool_t v2dxLoadSVG_attributes(v2d_draw_s *draw, xml_s *node, xml_s *defs)
{

  v2d_bool_t set_defaults = V2D_FALSE;

  v2d_real_t value;
  v2d_rgba_t rgba;

  v2d_shape_s *shape = NULL;
  v2d_text_s  *text = NULL;

  char buffer[256];

  if (!draw || !node) { return V2D_FALSE; }

  shape = (draw->type == V2D_TYPE_SHAPE) ? v2dDrawAsShape(draw) : NULL;
  text  = (draw->type == V2D_TYPE_TEXT)  ? v2dDrawAsText(draw)  : NULL;

  /*
  Travel up the XML node tree as far as is possible, so that attributes can be
  "inherited" from parent XML nodes. At the "top" of the tree, set the default
  SVG values for all parameters so that they can be overridden on the way down.
  */

  set_defaults = V2D_TRUE;
  if (node->owner)
  {
    if (strcmp(node->owner->name, "g") == 0 || strcmp(node->owner->name, "text") == 0)
    {
      set_defaults = V2D_FALSE;
      if (!v2dxLoadSVG_attributes(draw, node->owner, defs)) { return V2D_FALSE; }
    }
  }

  if (set_defaults)
  {
    /* SVG 1.1 default fill = 'black'. */
    if      (shape) { v2dShapeFillColor(shape, 0, 0, 0, 255); }
    else if (text ) { v2dTextSetColor  (text,  0, 0, 0, 255); }
  }

  /* Set the stroke opacity. SVG 1.1 default = '1'. */

  if (v2dxLoadSVG_style(node, "stroke-opacity", buffer, 256))
  { rgba[3] = (v2d_ui8_t)(atof(buffer) * 255); } else { rgba[3] = 255; }

  /* Get line attributes. */

  if (v2dxLoadSVG_style(node, "stroke", buffer, 256))
  {

    if (v2dxLoadSVG_color(buffer, rgba))
    {

      if (shape)
      {

        v2d_line_cap_t cap;
        v2d_line_join_t join;

        /* Create a line before setting any properties... */

        v2dShapeLineColor(shape, rgba[0], rgba[1], rgba[2], rgba[3]);

        /* Set the stroke width. SVG 1.1 default = '1'. */

        value = v2dxLoadSVG_style(node, "stroke-width", buffer, 256) ? atof(buffer) : 1;
        v2dLineSetWidth(shape->line, value);

        /* Set the line cap style. SVG 1.1 default = 'butt'. */

        cap = V2D_LINE_CAP_BUTT;
        if (v2dxLoadSVG_style(node, "stroke-linecap", buffer, 256))
        {
          if      (strcmp(buffer, "square") == 0) { cap = V2D_LINE_CAP_SQUARE; }
          else if (strcmp(buffer, "round" ) == 0) { cap = V2D_LINE_CAP_ROUND;  }
          else if (strcmp(buffer, "butt"  ) != 0) { /* Unknown - inherited? */ }
        }
        v2dLineSetCap(shape->line, cap);

        /* Set the line join style. SVG 1.1 default = 'miter'. */

        join = V2D_LINE_JOIN_MITER;
        if (v2dxLoadSVG_style(node, "stroke-linejoin", buffer, 256))
        {
          if      (strcmp(buffer, "bevel") == 0) { join = V2D_LINE_JOIN_BEVEL; }
          else if (strcmp(buffer, "round") == 0) { join = V2D_LINE_JOIN_ROUND; }
          else if (strcmp(buffer, "miter") != 0) { /* Unknown... inherited? */ }
        }
        v2dLineSetJoin(shape->line, join);

        /* Set the line join miter limit. SVG 1.1 default = '4'. */

        value = v2dxLoadSVG_style(node, "stroke-miterlimit", buffer, 256) ? atof(buffer) : 4;
        v2dLineSetMiter(shape->line, value);

        /* Set the "vector effect" property. SVG 1.2 default = 'none'. */

        shape->line->flags = V2D_LINE_FLAG_SCALING;
        if (v2dxLoadSVG_style(node, "vector-effect", buffer, 256))
        {
          if (strcmp(buffer, "non-scaling-stroke") == 0) { shape->line->flags = 0; }
        }

      }

    }

  }

  /* Set the fill opacity. SVG 1.1 default = '1'. */

  if (v2dxLoadSVG_style(node, "fill-opacity", buffer, 256))
  { rgba[3] = (v2d_ui8_t)(atof(buffer) * 255); } else { rgba[3] = 255; }

  /* Set the fill rule. SVG 1.1 default = 'nonzero'. */

  if (v2dxLoadSVG_style(node, "fill-rule", buffer, 256))
  {
    if (shape && strcmp(buffer, "evenodd") == 0)
    { shape->flags |= V2D_SHAPE_FLAG_EVENODD; }
  }

  /* Get fill attributes. */

  if (v2dxLoadSVG_style(node, "fill", buffer, 256))
  {

    if (strcmp(buffer, "none") == 0)
    {
      if      (shape) { v2dShapeFillClear(shape);          }
      else if (text ) { v2dTextSetColor(text, 0, 0, 0, 0); }
    }
    else if (v2dxLoadSVG_color(buffer, rgba))
    {
      if      (shape) { v2dShapeFillColor(shape, rgba[0], rgba[1], rgba[2], rgba[3]); }
      else if (text ) { v2dTextSetColor(  text,  rgba[0], rgba[1], rgba[2], rgba[3]); }
    }
    else if (defs && shape && strncmp(buffer, "url(#", 5) == 0)
    {

      char *temp = NULL;
      xml_s *pattern = NULL;
      xml_s *stop = NULL;
      xml_s *attr = NULL;
      v2d_fill_color_s *grad = NULL;
      v2d_matrix_t matrix;

      /* Remove the closing ')' character. */

      temp = strstr(buffer, ")");
      if (temp) { temp[0] = 0; }

      /* Search for a gradient definition with a matching ID value. */

      v2dMatrixReset(matrix);
      v2dMatrixPosition(matrix, 0, 0);

      while (V2D_TRUE)
      {

        pattern = v2dxLoadSVG_xlink(defs, &buffer[5]);
        if (!pattern)
        {
          fprintf(stderr, "[SVG] Could not find fill: %s\n", &buffer[5]);
          break;
        }

        /* FIXME: Support bitmap image fills. */

        if (strcmp(pattern->name, "pattern") == 0)
        {
          fprintf(stderr, "[SVG] FIXME: 'pattern' is unimplemented; '%s' ignored.\n", &buffer[5]);
          pattern = NULL;
          break;
        }

        /* Only allow gradient tags to be referenced as fills. */

        if (strcmp(pattern->name, "linearGradient") != 0 &&
            strcmp(pattern->name, "radialGradient") != 0)
        {
          fprintf(stderr, "[SVG] Unexpected 'fill' type: %s\n", pattern->name);
          pattern = NULL;
          break;
        }

        attr = xmlFindName(pattern->attr, "gradientTransform", 0);
        if (attr) { v2dxLoadSVG_transform(matrix, attr->text); }

        /* Some SVG files have one gradient tag reference another for stops: */

        attr = xmlFindName(pattern->attr, "xlink:href", 0);
        if (attr) { strncpy(&buffer[5-1], attr->text, 255); }
        else      { break;                                  }

      }

      /* If a matching gradient was found, create a gradient fill. */

      if (pattern)
      {

        if (v2dShapeFillColor(shape, 255, 0, 0, 255))
        {

          v2dMatrixCopy(shape->fill->matrix, matrix);

          if (strcmp(pattern->name, "radialGradient") == 0)
          { shape->fill->flags = V2D_FILL_FLAG_RADIAL; }

          for (grad = NULL, stop = pattern->node; stop; stop = stop->next)
          {

            if (strcmp(stop->name, "stop") != 0) { continue; }
            attr = xmlFindName(stop->attr, "offset", 0);
            if (!attr) { continue; }

            if (!grad)
            {
              grad = v2dFillAsColor(shape->fill);
              v2dFillColorSetStop(grad, atof(attr->text));
            }
            else { grad = v2dFillColorCreate(shape->fill, atof(attr->text)); }

            if (v2dxLoadSVG_style(stop, "stop-color", buffer, 256))
            { v2dxLoadSVG_color(buffer, rgba); }
            else
            { fprintf(stderr, "[SVG] WARN: Could not parse stop color!\n"); }

            if (v2dxLoadSVG_style(node, "stop-opacity", buffer, 256))
            { rgba[3] = (v2d_ui8_t)(atof(buffer) * 255); }
            else
            { rgba[3] = 255; }

            v2dFillColorSetRGBA(grad, rgba[0], rgba[1], rgba[2], rgba[3]);

          }

        } else { fprintf(stderr, "[SVG] WARN: Failed to create gradient!\n"); }

      }

    }

  }

  /* Get font attributes. */

  if (text)
  {

    /* Set the font family. SVG 1.1 default = 'depends on user agent'. */

    if (v2dxLoadSVG_style(node, "font-family", buffer, 256))
    { v2dTextSetFont(text, buffer); }

    /* Set the font style. SVG 1.1 default = 'normal'. */

    if (v2dxLoadSVG_style(node, "font-style", buffer, 256))
    {
      v2d_font_slant_t slant;
      if      (strcmp(buffer, "oblique") == 0) { slant = V2D_FONT_SLANT_OBLIQUE; }
      else if (strcmp(buffer, "italic" ) == 0) { slant = V2D_FONT_SLANT_ITALIC;  }
      else                                     { slant = V2D_FONT_SLANT_NORMAL;  }
      v2dTextSetSlant(text, slant);
    }

    /* Set the font weight. SVG 1.1 default = 'normal'. */

    if (v2dxLoadSVG_style(node, "font-weight", buffer, 256))
    {
      v2d_font_weight_t weight;
      if      (strcmp(buffer, "bold"  ) == 0) { weight = V2D_FONT_WEIGHT_BOLD;   }
      else if (strcmp(buffer, "bolder") == 0) { weight = V2D_FONT_WEIGHT_BOLD;   }
      else                                    { weight = V2D_FONT_WEIGHT_NORMAL; }
      v2dTextSetWeight(text, weight);
    }

    /* Set the font size. SVG 1.1 default = 'medium'. */

    if (v2dxLoadSVG_style(node, "font-size", buffer, 256))
    { v2dTextSetSize(text, atof(buffer)); }

  }

  /* Report success. */

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Convert a 'rect' tag into a drawable.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@return A new drawable on success, or NULL on failure.
*/

static v2d_draw_s *v2dxLoadSVG_rect(xml_s *node, v2d_stage_s *stage, xml_s *defs)
{

  v2d_draw_s *draw = NULL;
  v2d_shape_s *shape = NULL;
  xml_s *attr = NULL;
  v2d_real_t x, y, width, height, rx, ry;

  if (!node || !stage) { return NULL; }

  /* Read in the rectangle parameters. */

  attr = xmlFindName(node->attr, "x", 0);
  x = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "y", 0);
  y = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "width", 0);
  width = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "height", 0);
  height = attr ? atof(attr->text) : 0;

  /* As per http://www.w3.org/TR/SVG/shapes.html#RectElementXAttribute */

  attr = xmlFindName(node->attr, "rx", 0);
  rx = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "ry", 0);
  ry = attr ? atof(attr->text) : 0;

  if (rx <= 0       ) { rx = ry;         }
  if (ry <= 0       ) { ry = rx;         }
  if (rx >= width/2 ) { rx = width  / 2; }
  if (ry >= height/2) { ry = height / 2; }

  /* Create a rectangular path, with optionally rounded corners. */

  draw = v2dDrawCreateShape(stage->draw, x + rx, y);
  if (!draw) { return NULL; }
  v2dxLoadSVG_attributes(draw, node, defs);

  shape = v2dDrawAsShape(draw);
  shape->flags = V2D_SHAPE_FLAG_CLOSE;

  v2dPathLine(v2dShapeGetPathEnd(shape), x+width-rx, y, 0);
  if (rx > 0) { v2dShapeDrawArc(shape, x+width-rx, y+ry, rx, ry, 0, V2D_PI * -0.5, V2D_PI*+0.0); }
  v2dPathLine(v2dShapeGetPathEnd(shape), x+width, y+height-ry, 0);
  if (rx > 0) { v2dShapeDrawArc(shape, x+width-rx, y+height-ry, rx, ry, 0, V2D_PI * +0.0, V2D_PI*+0.5); }
  v2dPathLine(v2dShapeGetPathEnd(shape), x+rx, y+height, 0);
  if (rx > 0) { v2dShapeDrawArc(shape, x+rx, y+height-ry, rx, ry, 0, V2D_PI * +0.5, V2D_PI*+1.0); }
  v2dPathLine(v2dShapeGetPathEnd(shape), x, y+ry, 0);
  if (rx > 0) { v2dShapeDrawArc(shape, x+rx, y+ry, rx, ry, 0, V2D_PI * +1.0, V2D_PI*+1.5); }

  /* Return the drawable. */

  return draw;

}

/**
@brief [INTERNAL] Convert a 'circle' or 'ellipse' tag into a drawable.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@return A new drawable on success, or NULL on failure.
*/

static v2d_draw_s *v2dxLoadSVG_circle(xml_s *node, v2d_stage_s *stage, xml_s *defs)
{

  v2d_draw_s *draw = NULL;
  v2d_shape_s *shape = NULL;
  xml_s *attr = NULL;
  v2d_real_t cx, cy, rx, ry;

  if (!node || !stage) { return NULL; }

  if (strcmp(node->name, "circle") == 0)
  {
    attr = xmlFindName(node->attr, "r", 0);
    rx = attr ? atof(attr->text) : 0;
    ry = attr ? atof(attr->text) : 0;
  }
  else
  {
    attr = xmlFindName(node->attr, "rx", 0);
    rx = attr ? atof(attr->text) : 0;
    attr = xmlFindName(node->attr, "ry", 0);
    ry = attr ? atof(attr->text) : 0;
  }

  attr = xmlFindName(node->attr, "cx", 0);
  cx = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "cy", 0);
  cy = attr ? atof(attr->text) : 0;

  draw = v2dDrawCreateShape(stage->draw, cx+rx, cy);
  if (!draw) { return NULL; }
  v2dxLoadSVG_attributes(draw, node, defs);

  /* Create a circular path. */

  shape = v2dDrawAsShape(draw);
  shape->flags = V2D_SHAPE_FLAG_CLOSE;

  v2dShapeDrawArc(shape, cx, cy, rx, ry, 0, 0, 2*V2D_PI);

  /* Return the drawable. */

  return draw;

}

/**
@brief [INTERNAL] Convert a 'line' tag into a drawable.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@return A new drawable on success, or NULL on failure.
*/

static v2d_draw_s *v2dxLoadSVG_line(xml_s *node, v2d_stage_s *stage, xml_s *defs)
{

  v2d_draw_s *draw = NULL;
  xml_s *attr = NULL;
  v2d_real_t x1, y_1, x2, y2;

  if (!node || !stage) { return NULL; }

  attr = xmlFindName(node->attr, "x1", 0);
  x1 = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "y1", 0);
  y_1 = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "x2", 0);
  x2 = attr ? atof(attr->text) : 0;
  attr = xmlFindName(node->attr, "y2", 0);
  y2 = attr ? atof(attr->text) : 0;

  draw = v2dDrawCreateShape(stage->draw, x1, y_1);
  if (!draw) { return NULL; }
  v2dxLoadSVG_attributes(draw, node, defs);
  v2dPathLine(v2dShapeGetPath(v2dDrawAsShape(draw)), x2, y2, 0);

  /* Return the drawable. */

  return draw;

}

/**
@brief [INTERNAL] Convert a 'polyline' or 'polygon' tag into a drawable.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@return A new drawable on success, or NULL on failure.
*/

static v2d_draw_s *v2dxLoadSVG_polygon(xml_s *node, v2d_stage_s *stage, xml_s *defs)
{

  v2d_draw_s *draw = NULL;
  v2d_shape_s *shape = NULL;
  xml_s *attr = NULL;
  char *data = NULL;
  char *temp = NULL;
  v2d_real_t x, y;
  v2d_ui32_t pass;

  if (!node || !stage) { return NULL; }

  /* Try to get the path data. Something is wrong if this doesn't exist! */

  attr = xmlFindName(node->attr, "points", 0);
  if (!attr) { return NULL; }

  /* Create a "shape" drawable to store the path data. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  if (!draw) { return NULL; }
  v2dxLoadSVG_attributes(draw, node, defs);

  /* Loop through the path data text. */

  shape = v2dDrawAsShape(draw);
  if (strcmp(node->name, "polygon") == 0) { shape->flags |= V2D_SHAPE_FLAG_CLOSE; }

  for (data = attr->text, pass = 0; data[0]; pass++)
  {

    if (pass % 2) { y = strtod(data, &temp); }
    else          { x = strtod(data, &temp); }

    if (!temp) { break; } else { data = temp[0] == ',' ? &temp[1] : temp; }

    if (pass == 1)
    {
      v2dShapeGetPath(shape)->point[V2D_END_POINT][0] = x;
      v2dShapeGetPath(shape)->point[V2D_END_POINT][1] = y;
    }
    else if (pass % 2)
    {
      v2dPathLine(v2dShapeGetPathEnd(shape), x, y, 0);
    }

  }

  /* Return the drawable. */

  return draw;

}

/**
@brief [INTERNAL] Convert a 'path' tag into a drawable.

@todo Handle 's' and 't' path segments properly.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@return A new drawable on success, or NULL on failure.
*/

static v2d_draw_s *v2dxLoadSVG_path(xml_s *node, v2d_stage_s *stage, xml_s *defs)
{

  v2d_draw_s *draw = NULL;
  v2d_shape_s *shape = NULL;
  v2d_path_s *path = NULL;
  xml_s *attr = NULL;

  v2d_ui8_t num_values, i;
  v2d_real_t value[7];

  char *data = NULL;

  char path_type, prev_path_type;
  v2d_bool_t first_pass;
  v2d_bool_t new_path;
  v2d_bool_t using_prev;

  v2d_real_t ref_x, ref_y;
  v2d_real_t old_x, old_y;

  size_t z = 0;

  if (!node || !stage) { return NULL; }

  /* Try to get the path data. Something is wrong if this doesn't exist! */

  attr = xmlFindName(node->attr, "d", 0);
  if (!attr) { return NULL; }

  /* Create a "shape" drawable to store the path data. */

  draw = v2dDrawCreateShape(stage->draw, 0, 0);
  if (!draw) { return NULL; }
  v2dxLoadSVG_attributes(draw, node, defs);

  /* Loop through the path data text. */

  shape = v2dDrawAsShape(draw);
  new_path = V2D_TRUE;
  first_pass = V2D_TRUE;

  ref_x = 0;
  ref_y = 0;

  for (data = attr->text; data[0] != 0; first_pass = V2D_FALSE)
  {

    /* Skip past any whitespace to find the next "path" instruction. */

    while (isspace(data[0])) { data++; }

    if (new_path) { prev_path_type = 0; }

    for (path_type = data[0], using_prev = V2D_FALSE; 1; using_prev = V2D_TRUE)
    {

      path = v2dShapeGetPathEnd(shape);

      switch (path_type)
      {

        case 0: return draw;

        /* Close current path. */

        case 'Z': case 'z':

          for (z = 1; isspace(data[z]); z++) { }

          if (!data[z])
          {
            shape->flags |= V2D_SHAPE_FLAG_CLOSE;
            return draw;
          }
          else
          {
            v2dPathLine(path, old_x, old_y, 0);
            new_path = V2D_TRUE;
          }

        break;

        /* Move-to. NOTE: The first of these replaces the "default" one. */

        case 'M': case 'm':
          num_values = 2;
          if (!first_pass) { v2dPathMove(path, 0, 0, 0); }
        break;

        /* All kinds of straight lines. */

        case 'L': case 'l': case 'H': case 'h': case 'V': case 'v':
          num_values = tolower(path_type) == 'l' ? 2 : 1;
          v2dPathLine(path, 0, 0, 0);
        break;

        /* Quadratic bezier curves. */

        case 'Q': case 'q': case 'T': case 't':
          num_values = tolower(path_type) == 'q' ? 4 : 2;
          v2dPathQuad(path, 0, 0, 0, 0, 0);
        break;

        /* Cubic bezier curves. */

        case 'C': case 'c': case 'S': case 's':
          num_values = tolower(path_type) == 'c' ? 6 : 4;
          v2dPathCube(path, 0, 0, 0, 0, 0, 0, 0);
        break;

        /* Arcs. These are converted into other path types; see below. */

        case 'A': case 'a':
          num_values = 7;
        break;

        /*
        "The command letter can be eliminated on subsequent commands if the
        same command is used multiple times in a row (e.g. you can drop the
        second "L" in "M 100 200 L 200 100 L -100 -200" and use "M 100 200
        L 200 100 -100 -200" instead)."

        ...

        "If a moveto is followed by multiple pairs of coordinates, the
        subsequent pairs are treated as implicit lineto commands. Hence,
        implicit lineto commands will be relative if the moveto is
        relative, and absolute if the moveto is absolute."

         - http://www.w3.org/TR/SVG/paths.html
        */

        default:
          if (using_prev)
          {
            fprintf(stderr, "[SVG] Unknown path type: %c\n", path_type);
            return NULL;
          }
          else if (prev_path_type == 'M') { path_type = 'L';            }
          else if (prev_path_type == 'm') { path_type = 'l';            }
          else                            { path_type = prev_path_type; }
        continue;

      }

      /* For anything except "default", store the path type + stop here. */

      prev_path_type = path_type;
      break;

    }

    /* Only move past the letter if it exists; see "default" above. */

    if (isalpha(data[0])) { data++; }

    if (tolower(path_type) == 'z') { continue; }

    /* Read in values. */

    for (i = 0; i < num_values; i++)
    {
      char *temp = NULL;
      value[i] = strtod(data, &temp);
      if (!temp) { return NULL; }
      data = temp[0] == ',' ? &temp[1] : temp;
    }

    /* Get the new path segment, and set the 'relative' flag if necessary. */

    path = v2dShapeGetPathEnd(shape);
    if (!first_pass && islower(path_type) && path_type != 'a')
    { path->flags |= V2D_PATH_FLAG_RELATIVE; }

    /* Copy the values into the path structure. */

    switch (path_type)
    {

      case 'M': case 'm': case 'L': case 'l':
        path->point[V2D_END_POINT][0] = value[0];
        path->point[V2D_END_POINT][1] = value[1];
      break;

      case 'H': case 'h':
        path->point[V2D_END_POINT][0] = value[0];
        path->point[V2D_END_POINT][1] = 0;
        if (!(path->flags & V2D_PATH_FLAG_RELATIVE))
        { path->point[V2D_END_POINT][1] = ref_y; }
      break;

      case 'V': case 'v':
        path->point[V2D_END_POINT][0] = 0;
        path->point[V2D_END_POINT][1] = value[0];
        if (!(path->flags & V2D_PATH_FLAG_RELATIVE))
        { path->point[V2D_END_POINT][0] = ref_x; }
      break;

      /* Quadratic bezier curves. */

      case 'Q': case 'q':
        path->point[V2D_CONTROL_1][0] = value[0];
        path->point[V2D_CONTROL_1][1] = value[1];
        path->point[V2D_END_POINT][0] = value[2];
        path->point[V2D_END_POINT][1] = value[3];
      break;

      case 'T': case 't':
        fprintf(stderr, "[SVG] FIXME: Control Point ignored for 'T' curve.\n");
        path->point[V2D_CONTROL_1][0] = value[0];  /* FIXME */
        path->point[V2D_CONTROL_1][1] = value[1];  /* FIXME */
        path->point[V2D_END_POINT][0] = value[0];
        path->point[V2D_END_POINT][1] = value[1];
      break;

      /* Cubic bezier curves. */

      case 'C': case 'c':
        path->point[V2D_CONTROL_1][0] = value[0];
        path->point[V2D_CONTROL_1][1] = value[1];
        path->point[V2D_CONTROL_2][0] = value[2];
        path->point[V2D_CONTROL_2][1] = value[3];
        path->point[V2D_END_POINT][0] = value[4];
        path->point[V2D_END_POINT][1] = value[5];
      break;

      case 'S': case 's':
        fprintf(stderr, "[SVG] FIXME: Control point A ignored for 'S' curve.\n");
        path->point[V2D_CONTROL_1][0] = value[0];  /* FIXME */
        path->point[V2D_CONTROL_1][1] = value[1];  /* FIXME */
        path->point[V2D_CONTROL_2][0] = value[0];
        path->point[V2D_CONTROL_2][1] = value[1];
        path->point[V2D_END_POINT][0] = value[2];
        path->point[V2D_END_POINT][1] = value[3];
      break;

      /* http://www.w3.org/TR/SVG/implnote.html#ArcConversionEndpointToCenter */

      case 'A': case 'a':
      {

        v2d_real_t x1;
        v2d_real_t y_1;
        v2d_real_t x2;
        v2d_real_t y2;
        v2d_bool_t fa;
        v2d_bool_t fs;
        v2d_real_t rx;
        v2d_real_t ry;
        v2d_real_t phi;

        v2d_real_t cx;
        v2d_real_t cy;

        v2d_real_t a1;
        v2d_real_t a2;

        v2d_matrix_t m;
        v2d_real_t  dx1, dy1;

        v2d_real_t magic;

        /* Get the values. */

        x1 = ref_x;
        y_1 = ref_y;
        x2 = value[5] + (path_type == 'a' ? ref_x : 0);
        y2 = value[6] + (path_type == 'a' ? ref_y : 0);
        fa = (value[3] >= 1);
        fs = (value[4] >= 1);
        rx = fabs(value[0]);
        ry = fabs(value[1]);
        phi = v2dAsRadians(value[2]);

        /* Step 1: Compute (x1′, y1′) */

        dx1 = (x1-x2)/2;
        dy1 = (y_1-y2)/2;

        m[0][0] =  cos(phi); m[1][0] = sin(phi); m[2][0] = 0;
        m[0][1] = -sin(phi); m[1][1] = cos(phi); m[2][1] = 0;

        v2dMatrixVector(m, &dx1, &dy1);

        /* As per the "F.6.6 Correction of out-of-range radii" section. */

        magic = (dx1*dx1)/(rx*rx) + (dy1*dy1)/(ry*ry);
        if (magic > 1.0) { rx *= sqrt(magic); ry *= sqrt(magic); }

        /* Step 2: Compute (cx′, cy′) */

        magic = (rx*rx)*(ry*ry)-(rx*rx)*(dy1*dy1)-(ry*ry)*(dx1*dx1);
        magic /= (rx*rx)*(dy1*dy1)+(ry*ry)*(dx1*dx1);
        magic = sqrt(magic);

        cx = magic * ( (rx*dy1) / +ry ) * (fa != fs ? +1 : -1);
        cy = magic * ( (ry*dx1) / -rx ) * (fa != fs ? +1 : -1);

        /* Step 3: Compute (cx, cy) from (cx′, cy′) */

        m[0][0] = cos(phi); m[1][0] = -sin(phi); m[2][0] = 0;
        m[0][1] = sin(phi); m[1][1] =  cos(phi); m[2][1] = 0;

        v2dMatrixVector(m, &cx, &cy);

        cx += (x1+x2) / 2;
        cy += (y_1+y2) / 2;

        /*
        Fix an issue seen in "arcs01.svg" where the usual solution (see "F.6.6
        Correction of out-of-range radii" above) generates NaNs in one case.
        */

        if (v2dIsNaN(cx)) { cx = x1 + ((x2-x1)/2.0); }
        if (v2dIsNaN(cy)) { cy = y_1 + ((y2-y_1)/2.0); }

        /* Calculate the two angles. */

        dx1 = (x1-cx)*cos(-phi) + (y_1-cy)*-sin(-phi);
        dy1 = (x1-cx)*sin(-phi) + (y_1-cy)* cos(-phi);
        a1 = atan2(dy1, dx1 * (ry/rx));
        while (a1 < 0 ) { a1 += 2*V2D_PI; }

        dx1 = (x2-cx)*cos(-phi) + (y2-cy)*-sin(-phi);
        dy1 = (x2-cx)*sin(-phi) + (y2-cy)* cos(-phi);
        a2 = atan2(dy1, dx1 * (ry/rx));
        while (a2 < a1) { a2 += 2*V2D_PI; }

        /* Draw the result. */

        if (!fs) { a2 -= V2D_PI*2; }

        v2dShapeDrawArc(shape, cx, cy, rx, ry, phi, a1, a2);

      }
      break;

      /* Should never be reached... */

      default: break;

    }

    /* Update the reference position. */

    path = v2dShapeGetPathEnd(shape);

    if (path->flags & V2D_PATH_FLAG_RELATIVE)
    {
      ref_x += path->point[V2D_END_POINT][0];
      ref_y += path->point[V2D_END_POINT][1];
    }
    else
    {
      ref_x = path->point[V2D_END_POINT][0];
      ref_y = path->point[V2D_END_POINT][1];
    }

    if (new_path)
    {
      new_path = V2D_FALSE;
      old_x = ref_x;
      old_y = ref_y;
    }

  }

  /* Return the drawable. */

  return draw;

}

/**
@brief [INTERNAL] Convert a 'tspan' or 'tref' tag into a drawable.

NOTE: This also handles the main 'text' tag as well if it contains text content
outside of a 'tref' or 'tspan' child element.

@todo Handle 'tref' tags properly.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@param group about
@param ref_matrix about
@param x about
@param y about
@return V2D_TRUE on success, or NULL on failure.
*/

static v2d_bool_t v2dxLoadSVG_tspan(xml_s *node, v2d_stage_s *stage, xml_s *defs, v2d_chain_s *group, v2d_matrix_t ref_matrix, v2d_real_t *x, v2d_real_t *y)
{

  v2d_draw_s *draw = NULL;
  xml_s *text = NULL;
  xml_s *attr = NULL;
  v2d_matrix_t matrix;
  size_t text_start, len, i;

  if (!node || !stage || !group || !ref_matrix || !x || !y) { return V2D_FALSE; }

  /* Get the X/Y position of the text. */

  attr = xmlFindName(node->attr, "x", 0);
  if (attr) { *x = atof(attr->text); }

  attr = xmlFindName(node->attr, "y", 0);
  if (attr) { *y = atof(attr->text); }

  attr = xmlFindName(node->attr, "dx", 0);
  if (attr) { *x += atof(attr->text); }

  attr = xmlFindName(node->attr, "dy", 0);
  if (attr) { *y += atof(attr->text); }

  /* Get the transformation matrix. */

  v2dMatrixCopy(matrix, ref_matrix);

  attr = xmlFindName(node->attr, "transform", 0);
  if (attr) { v2dxLoadSVG_transform(matrix, attr->text); }

  v2dMatrixVector(matrix, x, y);

  /* Look for the actual text content. */

  for (text = node->node; text; text = text->next)
  {

    if (strcmp(text->name, "#text") == 0)
    {

      char *temp = NULL;
      int   pass = 0;

      /* Ignore any #text nodes if they only contain whitespace. */

      for (text_start = 0; isspace(text->text[text_start]); text_start++) { }
      if (text->text[text_start] == 0) { continue; }

      /* Get the EFFECTIVE length of the text (ignoring extra whitespace). */

      for (pass = 0; pass < 2; pass++)
      {

        for (len = 0, i = text_start; text->text[i] != 0; i++)
        {
          if (!isspace(text->text[i]) || (!isspace(text->text[i+1]) && text->text[i+1] != 0))
          {
            if (temp) { temp[len] = isspace(text->text[i]) ? ' ' : text->text[i]; }
            len++;
          }
        }

        if (pass == 0)
        {
          temp = (char*)malloc(len + 1);
          if (!temp) { return V2D_FALSE; }
          memset(temp, 0, len + 1);
        }

      }

      /* Create a text drawable. */

      draw = v2dDrawCreateText(stage->draw, temp);
      free(temp);
      if (!draw) { return V2D_FALSE; }
      v2dxLoadSVG_attributes(draw, node, defs);

      /* Create an instance of the text drawable. */

      if (!v2dItemCreate(group, draw, 0, *x, *y - v2dDrawAsText(draw)->size, V2D_FALSE)) { return V2D_FALSE; }

      /* HACK: We don't know the exact per-character widths, so fudge it... */

      *x += len * (v2dDrawAsText(draw)->size * 0.666);

    }
    else if (strcmp(text->name, "tspan") == 0)
    {
      if (!v2dxLoadSVG_tspan(text, stage, defs, group, matrix, x, y)) { return V2D_FALSE; }
    }
    else if (strcmp(text->name, "tref") == 0)
    { fprintf(stderr, "[SVG] FIXME: 'tref' nodes aren't supported.\n"); }
    else
    { fprintf(stderr, "[SVG] Unknown text node type: %s\n", text->name); }

  }

  return V2D_TRUE;

}

/**
@brief [INTERNAL] Convert a 'text' tag into a drawable.

This actually creates a main "group" drawable and adds text drawables to it for
each tspan/tref element found.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@return A new drawable on success, or NULL on failure.
*/

static v2d_draw_s *v2dxLoadSVG_text(xml_s *node, v2d_stage_s *stage, xml_s *defs)
{

  v2d_draw_s *group = NULL;
  v2d_real_t x, y;
  v2d_matrix_t matrix;

  if (!node || !stage) { return NULL; }

  group = v2dDrawCreateGroup(stage->draw);
  if (!group) { return NULL; }

  v2dMatrixReset(matrix);
  v2dMatrixPosition(matrix, 0, 0);

  x = 0;
  y = 0;

  if (!v2dxLoadSVG_tspan(node, stage, defs, v2dDrawAsGroup(group), matrix, &x, &y)) { return NULL; }

  return group;

}

/**
@brief [INTERNAL] Parse any tags which generate drawables.

@param node The SVG tag to be checked.
@param stage The stage to add the SVG data to.
@param defs about
@return A new drawable on success, or NULL on failure.
*/

static v2d_draw_s *v2dxLoadSVG_shape(xml_s *node, v2d_stage_s *stage, xml_s *defs)
{

  v2d_draw_s *draw = NULL;
  xml_s *attr = NULL;
  v2d_matrix_t matrix;

  /* Get the transformation matrix. */

  v2dMatrixReset(matrix);
  v2dMatrixPosition(matrix, 0, 0);

  attr = xmlFindName(node->attr, "transform", 0);
  if (attr) { v2dxLoadSVG_transform(matrix, attr->text); }

  /* Create the drawable. */

  if (strcmp(node->name, "use") == 0)
  {

    attr = xmlFindName(node->attr, "xlink:href", 0);
    if (!attr) { return NULL; } else if (attr->text[0] != '#') { return NULL; }

    attr = v2dxLoadSVG_xlink(defs, &attr->text[1]);
    if (!attr) { return NULL; }

    draw = v2dDrawCreateGroup(stage->draw);
    if (!draw) { return NULL; }

    if (!v2dItemCreate(v2dDrawAsGroup(draw), v2dxLoadSVG_shape(attr, stage, defs), 0, 0, 0, V2D_FALSE))
    { fprintf(stderr, "[SVG] Couldn't understand 'use' of %s\n", attr->name); }

  }
  else if (strcmp(node->name, "g") == 0)
  {
    draw = v2dDrawCreateGroup(stage->draw);
    if (!draw) { return NULL; }
    if (!v2dxLoadSVG_internal(stage, v2dDrawAsGroup(draw), node, defs)) { return NULL; }
  }
  else if (strcmp(node->name, "rect") == 0)
  { draw = v2dxLoadSVG_rect(node, stage, defs); }
  else if (strcmp(node->name, "circle") == 0 || strcmp(node->name, "ellipse") == 0)
  { draw = v2dxLoadSVG_circle(node, stage, defs); }
  else if (strcmp(node->name, "line") == 0)
  { draw = v2dxLoadSVG_line(node, stage, defs); }
  else if (strcmp(node->name, "polyline") == 0 || strcmp(node->name, "polygon") == 0)
  { draw = v2dxLoadSVG_polygon(node, stage, defs); }
  else if (strcmp(node->name, "path") == 0)
  { draw = v2dxLoadSVG_path(node, stage, defs); }
  else if (strcmp(node->name, "text") == 0)
  { return v2dxLoadSVG_text(node, stage, defs); }

  /* Apply the transformation matrix to the drawable. */

  if (draw)
  {
    if (draw->type == V2D_TYPE_GROUP)
    {
      v2d_item_s *item = NULL;
      for (item = v2dGroupGetItem(v2dDrawAsGroup(draw)); item; item = v2dItemGetNext(item))
      { v2dMatrixCopy(v2dItemGetAnim(item)->matrix, matrix); }
    }
    else if (draw->type == V2D_TYPE_SHAPE)
    { v2dShapeTransform(v2dDrawAsShape(draw), matrix); }
  }

  /* Return the result. */

  return draw;

}

/**
@brief [INTERNAL] Load SVG data from XML tags.

NOTE: This function calls itself recursively, both directly and indirectly...

@param stage The stage to add the SVG data to.
@param group The group to add the data to.
@param owner The "parent" node that is currently being processed.
@param defs about
@return V2D_TRUE on success, or V2D_FALSE on failure.
*/

v2d_bool_t v2dxLoadSVG_internal(v2d_stage_s *stage, v2d_chain_s *group, xml_s *owner, xml_s *defs)
{

  xml_s *node = NULL;
  xml_s *attr = NULL;
  v2d_draw_s *draw = NULL;

  if (!stage || !owner) { return V2D_FALSE; }

  for (node = owner->node; node; node = node->next)
  {

    draw = v2dxLoadSVG_shape(node, stage, defs);

    if (draw)
    {

      /*
      If a new drawable was specified outside of a 'defs' tag, add an instance
      of it to the scene; the definition and display item are the same thing.
      */

      if (group) { v2dItemCreate(group, draw, 0, 0, 0, V2D_FALSE); }

    }
    else if (strcmp(node->name, "defs") == 0)
    {
      if (!v2dxLoadSVG_internal(stage, NULL, node, defs)) { return V2D_FALSE; }
    }
    else if (strcmp(node->name, "title") == 0)
    {
      if (group == stage->item)
      {
        v2dStageSetTitle(stage, "");

        for (attr = node->node; attr; attr = attr->next)
        {
          if (strcmp(attr->name, "#text") == 0)
          {
            /* TODO: Should APPEND the text, not replace it. */
            v2dStageSetTitle(stage, attr->text);
          } else { fprintf(stderr, "[SVG] Not title text: %s\n", attr->name); }
        }

      } else { fprintf(stderr, "[SVG] 'title' ignored for non-'svg' tag.\n"); }
    }
    else if (strcmp(node->name, "metadata") == 0)
    { fprintf(stderr, "[SVG] FIXME: 'metadata' not loaded.\n"); }
    else if (strcmp(node->name, "script") == 0)
    { fprintf(stderr, "[SVG] WARNING: <script> tags are not imported...\n"); }
    else if (strcmp(node->name, "switch") == 0)
    { fprintf(stderr, "[SVG] FIXME: <switch> tags are not supported yet.\n"); }
    else if (strcmp(node->name, "desc") == 0) { /* Ignore for now... TODO? */ }
    else if (strcmp(node->name, "style") == 0) { /* Ignored here. */ }
    else if (strcmp(node->name, "pattern") == 0) { /* Ignored here. */ }
    else if (strcmp(node->name, "linearGradient") == 0) { /* Ignored here. */ }
    else if (strcmp(node->name, "radialGradient") == 0) { /* Ignored here. */ }
    else if (strncmp(node->name, "sodipodi:", 9) == 0) { /* Ignore this... */ }
    else if (strcmp(node->name, "#text") != 0)
    { fprintf(stderr, "[SVG] Unknown SVG element '%s'\n", node->name); }

  }

  /* Report success. */

  return V2D_TRUE;

}

/*
[PUBLIC] Load a project from a Scalable Vector Graphics (SVG) file.
*/

v2d_stage_s *v2dxLoadSVG(const char *file_name, v2d_real_t fps)
{

  v2d_stage_s *stage = NULL;

  xml_s *xml = NULL;
  xml_s *svg = NULL;
  xml_s *attr = NULL;

  float x, y, w, h;

  /* Use a default FPS rate if the value provided doesn't make sense. */

  if (fps <= 1.0) { fps = 50.0; }

  /* Attempt to load the specified XML file. */

  xml = xmlLoadFile(file_name);
  if (!xml) { return NULL; }

  /* Search for the root 'svg' element. If this is missing, it's not an SVG. */

  svg = xmlFindName(xml->node, "svg", 0);

  if (!svg)
  {
    xmlDelete(xml);
    return NULL;
  }

  /* Attempt to get the width/height of the SVG canvas. */

  attr = xmlFindName(svg->attr, "width", 0);
  if (attr) { w = atof(attr->text); } else { w = 640; }

  attr = xmlFindName(svg->attr, "height", 0);
  if (attr) { h = atof(attr->text); } else { h = 480; }

  attr = xmlFindName(svg->attr, "viewBox", 0);
  if (attr) { sscanf(attr->text, "%f %f %f %f", &x, &y, &w, &h); }

  /* Create a stage structure, based on the dimensions of the SVG image. */

  stage = v2dStageCreate(w, h, fps);

  if (stage)
  {

    attr = xmlFindName(svg->node, "defs", -1);

    if (!v2dxLoadSVG_internal(stage, stage->item, svg, attr))
    {
      v2dStageDelete(stage);
      stage = NULL;
    }
    else { v2dDrawSort(stage->draw, V2D_TRUE); }

  }

  /* Free the XML document and return the result. */

  xmlDelete(xml);
  return stage;

}
