Vector2D
========

Vector2D (or V2D) is a project to create a general framework for manipulating 2D vector images. It consists of three sub-projects:

* **V2D** provides the core functionality for creating and manipulating projects (or "stages") in memory.
* **V2DX** is built on top of V2D, and provides extra functionality, such as SVG import or SWF export.
* **V2DUI** is a basic GUI front-end, using GTK+ 3 for cross-platform support.

**V2D** and **V2DX** are aimed at programmers who want to generate SWF files in their own progams. The **V2DUI** is for non-technical users looking for a simple vector scripting/animation progam.

Background
----------

One day, for no real reason, [I](https://gitorious.org/~gobusto) found myself wondering if there were open source, cross-platform alternatives to the official Flash IDE. I discovered several possibilities, but none of them covered *everything* that SWF files can do:

* [Inkscape](http://www.inkscape.org/) allows you to create static SVG files, but isn't (currently) geared towards animation and scripting. It also focuses on SVG, rather than SWF or HTML5.
* [Pencil](http://www.pencil-animation.org/) is a vector-based animation program that can export to SWF, but it doesn't support scripting.
* [Synfig](http://www.synfig.org/) is similar to Pencil, in that it is aimed at vector animation, not scripting. It also uses a custom file format, rather than SVG or SWF.

I discovered a C library called [Ming](http://www.libming.org/) that allows SWF files to be generated programmatically, and set about building a GUI front-end for it using GTK+ 2, called [Hitoha](https://sites.google.com/site/gobusto/projects/hitoha). It sort-of worked, but had several limitations, most of which stemmed from my lack of familiarity with how SWF files are supposed to work. After a second Ming-based attempt (which also didn't work very well), I decided to bite the bullet and handle the SWF export myself. Ming is a useful library, but there are a few things that make it unsuitable for me:

* It is designed to handle SWF *export*; I needed something to hold an in-memory representation of a general vector scene.
* It only handles SWF files, so I'd need to handle SVG/HTML5/other export separately anyway. Additionally, only SWF versions 4-8 are supported.
* It depends on a lot of external libraries, which makes it difficult to build from source on Windows.

Thus, I started from the very beginning, writing a small program to export vector shape paths to SWF and SVG files. Several years of (off-and-on) work later, I uploaded what I had to [Gitorious](https://gitorious.org/), where development continues.

Technical Features
------------------

* Cross-platform. Debian and Trisquel GNU/Linux are the primary development platforms, but the code should run without alteration on Windows/MacOSX/etc.
* Written entirely in standards-compliant "clean C" (i.e. it should work natively with both C and C++) and compilable on both 32- and 64-bit systems.
* Minimal third-party dependencies. V2DX will have an (optional) dependency on ZLIB at some point, but the codebase should otherwise be self-contained.
* Free and open source software. It's also "free as in beer" in that no fee is charged for using it.

What it does
------------

The project was originally geared towards generating Adobe Flash SWF files. As such, it aims to provide most (if not all) of the functionality that SWF files offer:

* Vector shapes and text, with solid, gradient, or bitmap (JPEG/PNG) fills.
* Key-frame animation using matrix transforms. Color transforms may also be supported in the future.
* Sound effects, in either WAV or MP3 format.
* Scripting for animation key-frames and "button" objects. Any language can be used (not just ActionScript/JavaScript) to generate a series of "generic" V2D op-codes, which are converted into AVM bytecode (SWF) or ECMAScript (SVG/HTML) when the project is exported in a specific format.
* Export to SWF or SVG format.
* Import from SVG files.

...plus a few odds and ends, such as RDF metadata, etc.

What it doesn't
---------------

Both SWF and SVG files support streaming video data. However, the codecs used differ, and video data is better suited to dedicated container formats such as [MKV](http://matroska.org/). As such, V2D *probably* won't ever support embedded video data, as it is aimed at animating/scripting vector shapes more than anything else.

Compatibility
-------------

SWF files are tested using the official [Adobe Flash player](http://www.adobe.com/products/flashplayer.html), [GNU Gnash](http://gnashdev.org/), [SWFDec](http://swfdec.freedesktop.org/), [Lightspark](http://lightspark.github.io/), and [Mozilla Shumway](http://mozilla.github.io/shumway/examples/inspector/inspector.html). Compatibility with the official player is the highest priority, even for cases where behaviour differs from [the official SWF specification](https://www.adobe.com/devnet/swf.html).

SVG files are tested using [Mozilla Firefox](https://mozilla.org/firefox), [Google Chromium](http://www.chromium.org/Home), and [Inkscape](http://www.inkscape.org/). They are also verified using the [W3 validator](http://validator.w3.org/).

Licensing
---------

V2D and V2DX are licensed under the [GNU LGPL v3.0](https://www.gnu.org/copyleft/lesser.html), with the V2DUI using the "full fat" [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.html) instead. This is to ensure that any derivative projects remain Free and Open Source.

However, some files providing "auxillary" support functions (such as linked lists and base64 encoding) are licensed under the more liberal [MIT/X11 license](http://opensource.org/licenses/MIT), on the basis that they aren't specific to vector image manipulation, and could be useful to other open source projects with BSD-style licenses.

